import { Component, Injector, OnInit } from '@angular/core';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { HttpErrorResponse } from '@angular/common/http';
import { UserServiceProxy, PasswordResetModel } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';
import { finalize } from 'rxjs/operators';
import { Console } from 'console';

@Component({
  selector: 'app-confirm-account',
  templateUrl: './confirm-account.component.html',
  styleUrls: ['./confirm-account.component.css'],
  animations: [accountModuleAnimation()]
})
export class ConfirmAccountComponent extends AppComponentBase implements OnInit {
  token: string;
  loading: false;
  showPassword = false;
  submitting = false;
  resetPasswordDto = new PasswordResetModel();
  newPasswordValidationErrors: Partial<AbpValidationError>[] = [
    {
      name: 'pattern',
      localizationKey:
        'PasswordsMustBeAtLeast8CharactersContainLowercaseUppercaseNumber',
    },
  ];
  confirmNewPasswordValidationErrors: Partial<AbpValidationError>[] = [
    {
      name: 'validateEqual',
      localizationKey: 'PasswordsDoNotMatch',
    },
  ];
  /**
   *
   */
  constructor(
    injector: Injector,
    private route: ActivatedRoute,
    private router: Router,
    private ngxService: NgxUiLoaderService,
    public _userService: UserServiceProxy,
  ) {
    super(injector);
  }
  ngOnInit(): void {

    console.log('this.route.snapshot.queryParams', this.route.snapshot.queryParams);
    let obj = this.route.snapshot.queryParams;
    this.token = obj.token;

  }

  createPassword() {
    this.submitting = true;
    this.resetPasswordDto.token = this.token;
    this.ngxService.start();
    this._userService
      .resetPasswordNew(this.resetPasswordDto)
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.ngxService.stop();
        })
      )
      .subscribe((success) => {
        if (!success.hasError) {
          abp.message.success('Password created successfully', 'Success');
          // .done(function () {
          this.router.navigate(['/account/login']);
          // });
        } else {
          abp.message.error(success.message, 'Error');
        }
      });
  }

  togglePassword(): void {
    this.showPassword = !this.showPassword;
  }
}
