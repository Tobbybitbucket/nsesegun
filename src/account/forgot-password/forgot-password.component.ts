import { Component, Injector, OnInit } from '@angular/core';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
  animations: [accountModuleAnimation()]
})
export class ForgotPasswordComponent implements OnInit {
  submitting = false;
  userNameOrEmailAddress = '';
  /**
   *
   */
  constructor(
    private ngxService: NgxUiLoaderService,
    public _userService: UserServiceProxy,
    private router: Router) {

  }
  ngOnInit(): void {
  }
  sendEmail() {
    this.submitting = true;
    this.ngxService.start();
    this._userService
      .forgotPassword(this.userNameOrEmailAddress)
      .pipe(
        finalize(() => {
          this.submitting = false;
          this.ngxService.stop();
        })
      )
      .subscribe((success) => {
        if (!success.hasError) {
          abp.message.success(success.message, 'Success');
          // .done(function () {
          this.router.navigate(['/account/login']);
          // });
        } else {
          abp.message.error(success.message, 'Error');
        }
      });
  }
}
