import { Component, Injector, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  PagedListingComponentBase,
  PagedRequestDto
} from 'shared/paged-listing-component-base';
import {
  LgaServiceProxy,
  LgaDto,
  LgaDtoPagedResultDto
} from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { BehaviorSubject } from 'rxjs';
import { ViewLgaDialogComponent } from './view-lga/view-lga-dialog.component';
import { CreateLgaDialogComponent } from './create-lga/create-lga-dialog.component';
import { EditLgaDialogComponent } from './edit-lga/edit-lga-dialog.component';


class PagedLgasRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  templateUrl: './lgas.component.html',
  animations: [appModuleAnimation()]
})
export class LgasComponent extends PagedListingComponentBase<LgaDto> {
  @ViewChild('dt') table: Table;
  lgas: LgaDto[] = [];
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;
  selectedLga$: BehaviorSubject<LgaDto>;

  cols = [
    { field: 'sn', header: 'S/N' },
    { field: 'country', header: 'Country' },
    { field: 'state', header: 'State' },
    { field: 'name', header: 'Lga name' },
    { field: 'isActive', header: 'Is active' }
  ];

  buttonItems = [
    {
      label: 'Edit', visible: this.isGranted('EditLga'), command: () => {
        this.editLga(this.selectedLga$.value);
      }
    },
    {
      label: 'Delete', visible: this.isGranted('DeleteLga'), command: () => {
        this.delete(this.selectedLga$.value);
      }
    },
    {
      label: 'View', visible: this.isGranted('ViewLga'), command: () => {
        this.viewLga(this.selectedLga$.value);
      }
    }
  ];

  constructor(
    injector: Injector,
    private _lgaService: LgaServiceProxy,
    private _modalService: BsModalService
  ) {
    super(injector);

    this.selectedLga$ = new BehaviorSubject(new LgaDto());
  }

  setSelectedLga(lga: LgaDto): void {
    this.selectedLga$.next(lga);
  }

  createLga(): void {
    this.showCreateOrEditLgaDialog();
  }

  editLga(lga: LgaDto): void {
    this.showCreateOrEditLgaDialog(lga.id);
  }

  viewLga(lga: LgaDto): void {
    this.showViewLgaDialog(lga.id);
  }

  clearFilters(): void {
    this.keyword = '';
    this.isActive = undefined;
    this.getDataPage(1);
  }

  protected list(
    request: PagedLgasRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._lgaService
      .getAllLgas(
        request.keyword,
        request.isActive,
        request.skipCount,
        request.maxResultCount
      )
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: LgaDtoPagedResultDto) => {
        this.lgas = result.items;
        this.showPaging(result, pageNumber);
      });
  }

  protected delete(lga: LgaDto): void {
    abp.message.confirm(
      `${lga.name} will be deleted`,
      undefined,
      (result: boolean) => {
        if (result) {
          this._lgaService.deleteLga(lga.id).subscribe(() => {
            abp.notify.success(this.l('SuccessfullyDeleted'));
            this.refresh();
          });
        }
      }
    );
  }

  private showViewLgaDialog(id?: number): void {
    const viewLgaDialog = this._modalService.show(
      ViewLgaDialogComponent,
      {
        backdrop: 'static',
        keyboard: false,
        animated: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
        initialState: {
          id: id,
        },
      }
    );
  }

  private showCreateOrEditLgaDialog(id?: number): void {
    let createOrEditLgaDialog: BsModalRef;
    if (!id) {
      createOrEditLgaDialog = this._modalService.show(
        CreateLgaDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
        }
      );
    } else {
      createOrEditLgaDialog = this._modalService.show(
        EditLgaDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
          initialState: {
            id: id,
          },
        }
      );
    }

    createOrEditLgaDialog.content.onSave.subscribe(() => {
      this.refresh();
    });
  }
}
