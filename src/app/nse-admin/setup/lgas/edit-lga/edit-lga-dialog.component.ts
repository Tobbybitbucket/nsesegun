import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  LgaServiceProxy,
  LgaDto,
  StateDto,
  CountryDto,
  CountryServiceProxy,
  StateServiceProxy,
  State
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './edit-lga-dialog.component.html'
})
export class EditLgaDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  lga = new LgaDto();
  id: number;
  states: StateDto[] | undefined;
  selectedState: State;
  loadedState: State;
  countries: CountryDto[];
  selectedCountry: CountryDto;
  countryId: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    private _countryService: CountryServiceProxy,
    private ngxService: NgxUiLoaderService,
    private _stateService: StateServiceProxy,
    private _lgaService: LgaServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.ngxService.start();
    this.getCountries();
    this._lgaService.getLga(this.id).subscribe((result) => {
      this.lga = result.result;
      this.selectedCountry = result.result.state?.country;
      this.countryId = Number(result.result.state?.countryId);
      this.loadedState = result.result.state;
      this._stateService.getAllCountryStates(this.countryId)
        .pipe(
          finalize(() => {
            this.ngxService.stop();
          })
        )
        .subscribe(res => {
          this.states = res.result;
          this.selectedState = this.loadedState;
        });
    });
  }
  onChange(event): void {
    this.countryId = Number(event.value.id);
    this.getCountryStates();
  }
  onChangeState(event): void {
    this.selectedState = event.value;
    this.getCountryStates();
  }
  getCountries(): void {
    this._countryService.getAllCountriesList()
      .subscribe(result => {
        this.countries = result;
      });
  }
  getCountryStates(): void {
    this.ngxService.start();
    this._stateService.getAllCountryStates(this.countryId)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(res => {
        this.states = res.result;
      });
  }
  save(): void {
    this.saving = true;
    this.lga.stateId = this.selectedState.id;
    this.lga.state = this.selectedState;
    this._lgaService
      .editLga(this.lga)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
