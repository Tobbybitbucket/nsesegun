import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  LgaServiceProxy,
  LgaDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './view-lga-dialog.component.html'
})
export class ViewLgaDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  lga = new LgaDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _lgaService: LgaServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._lgaService.getLga(this.id).subscribe((result) => {
      this.lga = result.result;
    });
  }
}
