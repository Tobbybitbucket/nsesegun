import { CountryServiceProxy, StateServiceProxy, StateDto, CountryDto } from './../../../../../shared/service-proxies/service-proxies';
import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { AppComponentBase } from '@shared/app-component-base';
import {
  LgaServiceProxy,
  CreateLgaDto
} from '@shared/service-proxies/service-proxies';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';

@Component({
  templateUrl: './create-lga-dialog.component.html'
})
export class CreateLgaDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  lga = new CreateLgaDto();
  states: StateDto[] | undefined;
  selectedState: StateDto;
  countries: CountryDto[];
  selectedCountry: CountryDto;
  countryId: number;
  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    private _countryService: CountryServiceProxy,
    private ngxService: NgxUiLoaderService,
    private _stateService: StateServiceProxy,
    private _lgaService: LgaServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.getCountries();
    this.lga.isActive = true;
  }
  onChange(event): void {
    this.countryId = Number(event.value.id);
    this.getCountryStates();
  }

  getCountries(): void {
    this.ngxService.start();
    this._countryService.getAllCountriesList()
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(result => {
        this.countries = result;
      });
  }
  getCountryStates(): void {
    this.ngxService.start();
    this._stateService.getAllCountryStates(this.countryId)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(res => {
        this.states = res.result;
      });
  }
  save(): void {
    this.saving = true;
    this.lga.stateId = this.selectedState.id;
    this._lgaService
      .createLga(this.lga)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
