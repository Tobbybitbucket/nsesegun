import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  IssuingCompanyServiceProxy,
  IssuingCompanyDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './edit-issuing-company-dialog.component.html'
})
export class EditIssuingCompanyDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  issuingCompany = new IssuingCompanyDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _issuingCompanyService: IssuingCompanyServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._issuingCompanyService.getIssuingCompany(this.id).subscribe((result) => {
      this.issuingCompany = result.result;
    });
  }

  save(): void {
    this.saving = true;

    this._issuingCompanyService
      .editIssuingCompany(this.issuingCompany)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
