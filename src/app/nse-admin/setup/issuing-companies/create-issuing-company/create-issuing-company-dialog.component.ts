import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  IssuingCompanyServiceProxy,
  CreateIssuingCompanyDto
} from '@shared/service-proxies/service-proxies';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';

@Component({
  templateUrl: './create-issuing-company-dialog.component.html'
})
export class CreateIssuingCompanyDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  issuingCompany = new CreateIssuingCompanyDto();

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _issuingCompanyService: IssuingCompanyServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.issuingCompany.isActive = true;
  }

  save(): void {
    this.saving = true;
    this._issuingCompanyService
      .createIssuingCompany(this.issuingCompany)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
