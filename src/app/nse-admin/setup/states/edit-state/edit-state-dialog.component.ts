import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  StateServiceProxy,
  StateDto,
  CountryDto,
  CountryServiceProxy,
  Country
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './edit-state-dialog.component.html'
})
export class EditStateDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  state = new StateDto();
  countries: CountryDto[];
  selectedCountry: Country;
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _stateService: StateServiceProxy,
    public _countryService: CountryServiceProxy,
    private ngxService: NgxUiLoaderService,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.getCountries();
    this._stateService.getState(this.id).subscribe((result) => {
      this.state = result.result;
      this.selectedCountry = this.state.country;
    });
  }
  getCountries(): void {
    this.ngxService.start();
    this._countryService.getAllCountriesList()
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(result => {
        this.countries = result;
      });
  }
  onChange(event): void {
    console.log(this.selectedCountry);
  }
  save(): void {
    this.saving = true;
    this.state.countryId = this.selectedCountry.id;
    this.state.country = this.selectedCountry;
    this._stateService
      .editState(this.state)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
