import { CountryServiceProxy, CountryDto } from './../../../../../shared/service-proxies/service-proxies';
import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  StateServiceProxy,
  CreateStateDto
} from '@shared/service-proxies/service-proxies';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';

@Component({
  templateUrl: './create-state-dialog.component.html'
})
export class CreateStateDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  state = new CreateStateDto();
  countries: CountryDto[];
  selectedCountry: CountryDto;
  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _stateService: StateServiceProxy,
    public _countryService: CountryServiceProxy,
    private ngxService: NgxUiLoaderService,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.getCountries();
    this.state.isActive = true;
  }
  getCountries(): void {
    this.ngxService.start();
    this._countryService.getAllCountriesList()
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(result => {
        this.countries = result;
      });
  }
  onChange(event): void {
    console.log(this.selectedCountry);
  }
  save(): void {
    this.saving = true;
    this.state.countryId = this.selectedCountry.id;
    this._stateService
      .createState(this.state)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
