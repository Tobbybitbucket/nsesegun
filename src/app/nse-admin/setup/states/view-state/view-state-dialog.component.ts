import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  StateServiceProxy,
  StateDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './view-state-dialog.component.html'
})
export class ViewStateDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  state = new StateDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _stateService: StateServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._stateService.getState(this.id).subscribe((result) => {
      this.state = result.result;
    });
  }
}
