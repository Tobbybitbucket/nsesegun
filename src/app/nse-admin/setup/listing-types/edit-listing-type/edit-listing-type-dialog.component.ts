import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  ListingTypeServiceProxy,
  ListingTypeDto,
  ApplicationTypeDto,
  ApplicationType,
  ApplicationTypeServiceProxy
} from '@shared/service-proxies/service-proxies';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  templateUrl: './edit-listing-type-dialog.component.html'
})
export class EditListingTypeDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  listingType = new ListingTypeDto();
  id: number;
  applicationTypes: ApplicationTypeDto[];
  selectedApplicationType: ApplicationType;
  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _listingTypeService: ListingTypeServiceProxy,
    public _applicationTypeService: ApplicationTypeServiceProxy,
    private ngxService: NgxUiLoaderService,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.getApplicationTypes();
    this._listingTypeService.getListingType(this.id).subscribe((result) => {
      this.listingType = result.result;
      this.selectedApplicationType = this.listingType.applicationType;
    });
  }
  getApplicationTypes(): void {
    this.ngxService.start();
    this._applicationTypeService.getAllApplicationTypesList(0)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(result => {
        this.applicationTypes = result;
      });
  }
  onChange(event): void {
    console.log(this.selectedApplicationType);
  }
  save(): void {
    this.saving = true;
    this.listingType.applicationTypeId = this.selectedApplicationType.id;
    this.listingType.applicationType = this.selectedApplicationType;
    this._listingTypeService
      .editListingType(this.listingType)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
