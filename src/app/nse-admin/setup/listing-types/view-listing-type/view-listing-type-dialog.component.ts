import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  ListingTypeServiceProxy,
  ListingTypeDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './view-listing-type-dialog.component.html'
})
export class ViewListingTypeDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  listingType = new ListingTypeDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _listingTypeService: ListingTypeServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._listingTypeService.getListingType(this.id).subscribe((result) => {
      this.listingType = result.result;
    });
  }
}
