import { Component, Injector, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  PagedListingComponentBase,
  PagedRequestDto
} from 'shared/paged-listing-component-base';
import {
  ListingTypeServiceProxy,
  ListingTypeDto,
  ListingTypeDtoPagedResultDto
} from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { BehaviorSubject } from 'rxjs';
import { ViewListingTypeDialogComponent } from './view-listing-type/view-listing-type-dialog.component';
import { CreateListingTypeDialogComponent } from './create-listing-type/create-listing-type-dialog.component';
import { EditListingTypeDialogComponent } from './edit-listing-type/edit-listing-type-dialog.component';


class PagedListingTypesRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  templateUrl: './listing-types.component.html',
  animations: [appModuleAnimation()]
})
export class ListingTypesComponent extends PagedListingComponentBase<ListingTypeDto> {
  @ViewChild('dt') table: Table;
  listingTypes: ListingTypeDto[] = [];
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;
  selectedListingType$: BehaviorSubject<ListingTypeDto>;

  cols = [
    { field: 'sn', header: 'S/N' },
    { field: 'name', header: 'Listing Type name' },
    { field: 'applicationType', header: 'Application Type' },
    { field: 'isActive', header: 'Is active' }
  ];

  buttonItems = [
    {
      label: 'Edit', visible: this.isGranted('EditListingType'), command: () => {
        this.editListingType(this.selectedListingType$.value);
      }
    },
    {
      label: 'Delete', visible: this.isGranted('DeleteListingType'), command: () => {
        this.delete(this.selectedListingType$.value);
      }
    },
    {
      label: 'View', visible: this.isGranted('ViewListingType'), command: () => {
        this.viewListingType(this.selectedListingType$.value);
      }
    }
  ];

  constructor(
    injector: Injector,
    private _listingTypeService: ListingTypeServiceProxy,
    private _modalService: BsModalService
  ) {
    super(injector);

    this.selectedListingType$ = new BehaviorSubject(new ListingTypeDto());
  }

  setSelectedListingType(listingType: ListingTypeDto): void {
    this.selectedListingType$.next(listingType);
  }

  createListingType(): void {
    this.showCreateOrEditListingTypeDialog();
  }

  editListingType(listingType: ListingTypeDto): void {
    this.showCreateOrEditListingTypeDialog(listingType.id);
  }

  viewListingType(listingType: ListingTypeDto): void {
    this.showViewListingTypeDialog(listingType.id);
  }

  clearFilters(): void {
    this.keyword = '';
    this.isActive = undefined;
    this.getDataPage(1);
  }

  protected list(
    request: PagedListingTypesRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._listingTypeService
      .getAllListingTypes(
        request.keyword,
        request.isActive,
        request.skipCount,
        request.maxResultCount
      )
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: ListingTypeDtoPagedResultDto) => {
        this.listingTypes = result.items;
        this.showPaging(result, pageNumber);
      });
  }

  protected delete(listingType: ListingTypeDto): void {
    abp.message.confirm(
      `${listingType.name} will be deleted`,
      undefined,
      (result: boolean) => {
        if (result) {
          this._listingTypeService.deleteListingType(listingType.id).subscribe(() => {
            abp.notify.success(this.l('SuccessfullyDeleted'));
            this.refresh();
          });
        }
      }
    );
  }

  private showViewListingTypeDialog(id?: number): void {
    const viewListingTypeDialog = this._modalService.show(
      ViewListingTypeDialogComponent,
      {
        backdrop: 'static',
        keyboard: false,
        animated: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
        initialState: {
          id: id,
        },
      }
    );
  }

  private showCreateOrEditListingTypeDialog(id?: number): void {
    let createOrEditListingTypeDialog: BsModalRef;
    if (!id) {
      createOrEditListingTypeDialog = this._modalService.show(
        CreateListingTypeDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
        }
      );
    } else {
      createOrEditListingTypeDialog = this._modalService.show(
        EditListingTypeDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
          initialState: {
            id: id,
          },
        }
      );
    }

    createOrEditListingTypeDialog.content.onSave.subscribe(() => {
      this.refresh();
    });
  }
}
