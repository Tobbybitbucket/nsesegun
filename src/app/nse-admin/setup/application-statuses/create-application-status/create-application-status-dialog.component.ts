import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  ApplicationStatusServiceProxy,
  CreateApplicationStatusDto
} from '@shared/service-proxies/service-proxies';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';

@Component({
  templateUrl: './create-application-status-dialog.component.html'
})
export class CreateApplicationStatusDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  applicationStatus = new CreateApplicationStatusDto();

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _applicationStatusService: ApplicationStatusServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.applicationStatus.isActive = true;
  }

  save(): void {
    this.saving = true;
    this._applicationStatusService
      .createApplicationStatus(this.applicationStatus)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
