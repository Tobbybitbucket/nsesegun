import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  ApplicationStatusServiceProxy,
  ApplicationStatusDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './view-application-status-dialog.component.html'
})
export class ViewApplicationStatusDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  applicationStatus = new ApplicationStatusDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _applicationStatusService: ApplicationStatusServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._applicationStatusService.getApplicationStatus(this.id).subscribe((result) => {
      this.applicationStatus = result.result;
    });
  }
}
