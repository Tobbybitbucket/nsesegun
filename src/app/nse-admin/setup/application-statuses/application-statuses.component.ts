import { Component, Injector, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  PagedListingComponentBase,
  PagedRequestDto
} from 'shared/paged-listing-component-base';
import {
  ApplicationStatusServiceProxy,
  ApplicationStatusDto,
  ApplicationStatusDtoPagedResultDto
} from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { BehaviorSubject } from 'rxjs';
import { ViewApplicationStatusDialogComponent } from './view-application-status/view-application-status-dialog.component';
import { CreateApplicationStatusDialogComponent } from './create-application-status/create-application-status-dialog.component';
import { EditApplicationStatusDialogComponent } from './edit-application-status/edit-application-status-dialog.component';


class PagedApplicationStatusesRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  templateUrl: './application-statuses.component.html',
  animations: [appModuleAnimation()]
})
export class ApplicationStatusesComponent extends PagedListingComponentBase<ApplicationStatusDto> {
  @ViewChild('dt') table: Table;
  applicationStatuses: ApplicationStatusDto[] = [];
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;
  selectedApplicationStatus$: BehaviorSubject<ApplicationStatusDto>;

  cols = [
    { field: 'sn', header: 'S/N' },
    { field: 'name', header: 'Application Status name' },
    { field: 'description', header: 'Description' },
    { field: 'isActive', header: 'Is active' }
  ];

  buttonItems = [
    // {
    //   label: 'Edit', visible: this.isGranted('EditApplicationStatus'), command: () => {
    //     this.editApplicationStatus(this.selectedApplicationStatus$.value);
    //   }
    // },
    // {
    //   label: 'Delete', visible: this.isGranted('DeleteApplicationStatus'), command: () => {
    //     this.delete(this.selectedApplicationStatus$.value);
    //   }
    // },
    {
      label: 'View', visible: this.isGranted('ViewApplicationStatus'), command: () => {
        this.viewApplicationStatus(this.selectedApplicationStatus$.value);
      }
    }
  ];

  constructor(
    injector: Injector,
    private _applicationStatusService: ApplicationStatusServiceProxy,
    private _modalService: BsModalService
  ) {
    super(injector);

    this.selectedApplicationStatus$ = new BehaviorSubject(new ApplicationStatusDto());
  }

  setSelectedApplicationStatus(applicationStatus: ApplicationStatusDto): void {
    this.selectedApplicationStatus$.next(applicationStatus);
  }

  createApplicationStatus(): void {
    this.showCreateOrEditApplicationStatusDialog();
  }

  editApplicationStatus(applicationStatus: ApplicationStatusDto): void {
    this.showCreateOrEditApplicationStatusDialog(applicationStatus.id);
  }

  viewApplicationStatus(applicationStatus: ApplicationStatusDto): void {
    this.showViewApplicationStatusDialog(applicationStatus.id);
  }

  clearFilters(): void {
    this.keyword = '';
    this.isActive = undefined;
    this.getDataPage(1);
  }

  protected list(
    request: PagedApplicationStatusesRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._applicationStatusService
      .getAllApplicationStatuses(
        request.keyword,
        request.isActive,
        request.skipCount,
        request.maxResultCount
      )
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: ApplicationStatusDtoPagedResultDto) => {
        this.applicationStatuses = result.items;
        this.showPaging(result, pageNumber);
      });
  }

  protected delete(applicationStatus: ApplicationStatusDto): void {
    abp.message.confirm(
      `${applicationStatus.name} will be deleted`,
      undefined,
      (result: boolean) => {
        if (result) {
          this._applicationStatusService.deleteApplicationStatus(applicationStatus.id).subscribe(() => {
            abp.notify.success(this.l('SuccessfullyDeleted'));
            this.refresh();
          });
        }
      }
    );
  }

  private showViewApplicationStatusDialog(id?: number): void {
    const viewApplicationStatusDialog = this._modalService.show(
      ViewApplicationStatusDialogComponent,
      {
        backdrop: 'static',
        keyboard: false,
        animated: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
        initialState: {
          id: id,
        },
      }
    );
  }

  private showCreateOrEditApplicationStatusDialog(id?: number): void {
    let createOrEditApplicationStatusDialog: BsModalRef;
    if (!id) {
      createOrEditApplicationStatusDialog = this._modalService.show(
        CreateApplicationStatusDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
        }
      );
    } else {
      createOrEditApplicationStatusDialog = this._modalService.show(
        EditApplicationStatusDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
          initialState: {
            id: id,
          },
        }
      );
    }

    createOrEditApplicationStatusDialog.content.onSave.subscribe(() => {
      this.refresh();
    });
  }
}
