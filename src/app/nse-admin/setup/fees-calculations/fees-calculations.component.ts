import { Component, Injector, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  PagedListingComponentBase,
  PagedRequestDto
} from 'shared/paged-listing-component-base';
import {
  FeesCalculationServiceProxy,
  FeesCalculationDto,
  FeesCalculationDtoPagedResultDto
} from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { BehaviorSubject } from 'rxjs';
import { ViewFeesCalculationDialogComponent } from './view-fees-calculation/view-fees-calculation-dialog.component';
import { CreateFeesCalculationDialogComponent } from './create-fees-calculation/create-fees-calculation-dialog.component';
import { EditFeesCalculationDialogComponent } from './edit-fees-calculation/edit-fees-calculation-dialog.component';


class PagedFeesCalculationsRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  templateUrl: './fees-calculations.component.html',
  animations: [appModuleAnimation()]
})
export class FeesCalculationsComponent extends PagedListingComponentBase<FeesCalculationDto> {
  @ViewChild('dt') table: Table;
  feesCalculations: FeesCalculationDto[] = [];
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;
  selectedFeesCalculation$: BehaviorSubject<FeesCalculationDto>;

  cols = [
    { field: 'sn', header: 'S/N' },
    { field: 'start', header: 'Start' },
    { field: 'end', header: 'End' },
    { field: 'fee', header: 'Fee' },
    { field: 'isActive', header: 'Is active' }
  ];

  buttonItems = [
    {
      label: 'Edit', visible: this.isGranted('EditFeesCalculation'), command: () => {
        this.editFeesCalculation(this.selectedFeesCalculation$.value);
      }
    },
    {
      label: 'Delete', visible: this.isGranted('DeleteFeesCalculation'), command: () => {
        this.delete(this.selectedFeesCalculation$.value);
      }
    },
    {
      label: 'View', visible: this.isGranted('ViewFeesCalculation'), command: () => {
        this.viewFeesCalculation(this.selectedFeesCalculation$.value);
      }
    }
  ];

  constructor(
    injector: Injector,
    private _feesCalculationService: FeesCalculationServiceProxy,
    private _modalService: BsModalService
  ) {
    super(injector);

    this.selectedFeesCalculation$ = new BehaviorSubject(new FeesCalculationDto());
  }

  setSelectedFeesCalculation(feesCalculation: FeesCalculationDto): void {
    this.selectedFeesCalculation$.next(feesCalculation);
  }

  createFeesCalculation(): void {
    this.showCreateOrEditFeesCalculationDialog();
  }

  editFeesCalculation(feesCalculation: FeesCalculationDto): void {
    this.showCreateOrEditFeesCalculationDialog(feesCalculation.id);
  }

  viewFeesCalculation(feesCalculation: FeesCalculationDto): void {
    this.showViewFeesCalculationDialog(feesCalculation.id);
  }

  clearFilters(): void {
    this.keyword = '';
    this.isActive = undefined;
    this.getDataPage(1);
  }

  protected list(
    request: PagedFeesCalculationsRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._feesCalculationService
      .getAllFeesCalculations(
        request.keyword,
        request.isActive,
        request.skipCount,
        request.maxResultCount
      )
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: FeesCalculationDtoPagedResultDto) => {
        this.feesCalculations = result.items;
        this.showPaging(result, pageNumber);
      });
  }

  protected delete(feesCalculation: FeesCalculationDto): void {
    abp.message.confirm(
      `${feesCalculation.fee} will be deleted`,
      undefined,
      (result: boolean) => {
        if (result) {
          this._feesCalculationService.deleteFeesCalculation(feesCalculation.id).subscribe(() => {
            abp.notify.success(this.l('SuccessfullyDeleted'));
            this.refresh();
          });
        }
      }
    );
  }

  private showViewFeesCalculationDialog(id?: number): void {
    const viewFeesCalculationDialog = this._modalService.show(
      ViewFeesCalculationDialogComponent,
      {
        backdrop: 'static',
        keyboard: false,
        animated: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
        initialState: {
          id: id,
        },
      }
    );
  }

  private showCreateOrEditFeesCalculationDialog(id?: number): void {
    let createOrEditFeesCalculationDialog: BsModalRef;
    if (!id) {
      createOrEditFeesCalculationDialog = this._modalService.show(
        CreateFeesCalculationDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
        }
      );
    } else {
      createOrEditFeesCalculationDialog = this._modalService.show(
        EditFeesCalculationDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
          initialState: {
            id: id,
          },
        }
      );
    }

    createOrEditFeesCalculationDialog.content.onSave.subscribe(() => {
      this.refresh();
    });
  }
}
