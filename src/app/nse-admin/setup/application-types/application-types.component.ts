import { Component, Injector, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  PagedListingComponentBase,
  PagedRequestDto
} from 'shared/paged-listing-component-base';
import {
  ApplicationTypeServiceProxy,
  ApplicationTypeDto,
  ApplicationTypeDtoPagedResultDto
} from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { BehaviorSubject } from 'rxjs';
import { ViewApplicationTypeDialogComponent } from './view-application-type/view-application-type-dialog.component';
import { CreateApplicationTypeDialogComponent } from './create-application-type/create-application-type-dialog.component';
import { EditApplicationTypeDialogComponent } from './edit-application-type/edit-application-type-dialog.component';


class PagedApplicationTypesRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  templateUrl: './application-types.component.html',
  animations: [appModuleAnimation()]
})
export class ApplicationTypesComponent extends PagedListingComponentBase<ApplicationTypeDto> {
  @ViewChild('dt') table: Table;
  applicationTypes: ApplicationTypeDto[] = [];
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;
  selectedApplicationType$: BehaviorSubject<ApplicationTypeDto>;

  cols = [
    { field: 'sn', header: 'S/N' },
    { field: 'name', header: 'Application Type name' },
    { field: 'isActive', header: 'Is active' }
  ];

  buttonItems = [
    // {
    //   label: 'Edit', visible: this.isGranted('EditApplicationType'), command: () => {
    //     this.editApplicationType(this.selectedApplicationType$.value);
    //   }
    // },
    // {
    //   label: 'Delete', visible: this.isGranted('DeleteApplicationType'), command: () => {
    //     this.delete(this.selectedApplicationType$.value);
    //   }
    // },
    {
      label: 'View', visible: this.isGranted('ViewApplicationType'), command: () => {
        this.viewApplicationType(this.selectedApplicationType$.value);
      }
    }
  ];

  constructor(
    injector: Injector,
    private _applicationTypeService: ApplicationTypeServiceProxy,
    private _modalService: BsModalService
  ) {
    super(injector);

    this.selectedApplicationType$ = new BehaviorSubject(new ApplicationTypeDto());
  }

  setSelectedApplicationType(applicationType: ApplicationTypeDto): void {
    this.selectedApplicationType$.next(applicationType);
  }

  createApplicationType(): void {
    this.showCreateOrEditApplicationTypeDialog();
  }

  editApplicationType(applicationType: ApplicationTypeDto): void {
    this.showCreateOrEditApplicationTypeDialog(applicationType.id);
  }

  viewApplicationType(applicationType: ApplicationTypeDto): void {
    this.showViewApplicationTypeDialog(applicationType.id);
  }

  clearFilters(): void {
    this.keyword = '';
    this.isActive = undefined;
    this.getDataPage(1);
  }

  protected list(
    request: PagedApplicationTypesRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._applicationTypeService
      .getAllApplicationTypes(
        request.keyword,
        request.isActive,
        request.skipCount,
        request.maxResultCount
      )
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: ApplicationTypeDtoPagedResultDto) => {
        this.applicationTypes = result.items;
        this.showPaging(result, pageNumber);
      });
  }

  protected delete(applicationType: ApplicationTypeDto): void {
    abp.message.confirm(
      `${applicationType.name} will be deleted`,
      undefined,
      (result: boolean) => {
        if (result) {
          this._applicationTypeService.deleteApplicationType(applicationType.id).subscribe(() => {
            abp.notify.success(this.l('SuccessfullyDeleted'));
            this.refresh();
          });
        }
      }
    );
  }

  private showViewApplicationTypeDialog(id?: number): void {
    const viewApplicationTypeDialog = this._modalService.show(
      ViewApplicationTypeDialogComponent,
      {
        backdrop: 'static',
        keyboard: false,
        animated: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
        initialState: {
          id: id,
        },
      }
    );
  }

  private showCreateOrEditApplicationTypeDialog(id?: number): void {
    let createOrEditApplicationTypeDialog: BsModalRef;
    if (!id) {
      createOrEditApplicationTypeDialog = this._modalService.show(
        CreateApplicationTypeDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
        }
      );
    } else {
      createOrEditApplicationTypeDialog = this._modalService.show(
        EditApplicationTypeDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
          initialState: {
            id: id,
          },
        }
      );
    }

    createOrEditApplicationTypeDialog.content.onSave.subscribe(() => {
      this.refresh();
    });
  }
}
