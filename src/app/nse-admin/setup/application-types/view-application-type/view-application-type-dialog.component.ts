import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  ApplicationTypeServiceProxy,
  ApplicationTypeDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './view-application-type-dialog.component.html'
})
export class ViewApplicationTypeDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  applicationType = new ApplicationTypeDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _applicationTypeService: ApplicationTypeServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._applicationTypeService.getApplicationType(this.id).subscribe((result) => {
      this.applicationType = result.result;
    });
  }
}
