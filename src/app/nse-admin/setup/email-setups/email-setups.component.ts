import { Component, Injector, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  PagedListingComponentBase,
  PagedRequestDto
} from 'shared/paged-listing-component-base';
import {
  EmailSetupServiceProxy,
  EmailSetupDto,
  EmailSetupDtoPagedResultDto
} from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { BehaviorSubject } from 'rxjs';
import { ViewEmailSetupDialogComponent } from './view-email-setup/view-email-setup-dialog.component';
import { CreateEmailSetupDialogComponent } from './create-email-setup/create-email-setup-dialog.component';
import { EditEmailSetupDialogComponent } from './edit-email-setup/edit-email-setup-dialog.component';


class PagedEmailSetupsRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  templateUrl: './email-setups.component.html',
  animations: [appModuleAnimation()]
})
export class EmailSetupsComponent extends PagedListingComponentBase<EmailSetupDto> {
  @ViewChild('dt') table: Table;
  emailSetups: EmailSetupDto[] = [];
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;
  selectedEmailSetup$: BehaviorSubject<EmailSetupDto>;

  cols = [
    { field: 'sn', header: 'S/N' },
    { field: 'host', header: 'Host' },
    { field: 'port', header: 'Port' },
    { field: 'sender', header: 'Sender Name' },
    { field: 'password', header: 'Password' }
  ];

  buttonItems = [
    {
      label: 'Edit', visible: this.isGranted('EditEmailSetup'), command: () => {
        this.editEmailSetup(this.selectedEmailSetup$.value);
      }
    },
    {
      label: 'Delete', visible: this.isGranted('DeleteEmailSetup'), command: () => {
        this.delete(this.selectedEmailSetup$.value);
      }
    },
    {
      label: 'View', visible: this.isGranted('ViewEmailSetup'), command: () => {
        this.viewEmailSetup(this.selectedEmailSetup$.value);
      }
    }
  ];

  constructor(
    injector: Injector,
    private _emailSetupService: EmailSetupServiceProxy,
    private _modalService: BsModalService
  ) {
    super(injector);

    this.selectedEmailSetup$ = new BehaviorSubject(new EmailSetupDto());
  }

  setSelectedEmailSetup(emailSetup: EmailSetupDto): void {
    this.selectedEmailSetup$.next(emailSetup);
  }

  createEmailSetup(): void {
    this.showCreateOrEditEmailSetupDialog();
  }

  editEmailSetup(emailSetup: EmailSetupDto): void {
    this.showCreateOrEditEmailSetupDialog(emailSetup.id);
  }

  viewEmailSetup(emailSetup: EmailSetupDto): void {
    this.showViewEmailSetupDialog(emailSetup.id);
  }

  clearFilters(): void {
    this.keyword = '';
    this.isActive = undefined;
    this.getDataPage(1);
  }

  protected list(
    request: PagedEmailSetupsRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._emailSetupService
      .getAllEmailSetups(
        request.keyword,
        request.isActive,
        request.skipCount,
        request.maxResultCount
      )
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: EmailSetupDtoPagedResultDto) => {
        this.emailSetups = result.items;
        this.showPaging(result, pageNumber);
      });
  }

  protected delete(emailSetup: EmailSetupDto): void {
    abp.message.confirm(
      `${emailSetup.host} will be deleted`,
      undefined,
      (result: boolean) => {
        if (result) {
          this._emailSetupService.deleteEmailSetup(emailSetup.id).subscribe(() => {
            abp.notify.success(this.l('SuccessfullyDeleted'));
            this.refresh();
          });
        }
      }
    );
  }

  private showViewEmailSetupDialog(id?: number): void {
    const viewEmailSetupDialog = this._modalService.show(
      ViewEmailSetupDialogComponent,
      {
        backdrop: 'static',
        keyboard: false,
        animated: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
        initialState: {
          id: id,
        },
      }
    );
  }

  private showCreateOrEditEmailSetupDialog(id?: number): void {
    let createOrEditEmailSetupDialog: BsModalRef;
    if (!id) {
      createOrEditEmailSetupDialog = this._modalService.show(
        CreateEmailSetupDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
        }
      );
    } else {
      createOrEditEmailSetupDialog = this._modalService.show(
        EditEmailSetupDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
          initialState: {
            id: id,
          },
        }
      );
    }

    createOrEditEmailSetupDialog.content.onSave.subscribe(() => {
      this.refresh();
    });
  }
}
