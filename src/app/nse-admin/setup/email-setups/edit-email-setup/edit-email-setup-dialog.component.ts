import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  EmailSetupServiceProxy,
  EmailSetupDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './edit-email-setup-dialog.component.html'
})
export class EditEmailSetupDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  emailSetup = new EmailSetupDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _emailSetupService: EmailSetupServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._emailSetupService.getEmailSetup(this.id).subscribe((result) => {
      this.emailSetup = result.result;
    });
  }

  save(): void {
    this.saving = true;

    this._emailSetupService
      .editEmailSetup(this.emailSetup)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
