import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  RequiredApplicationDocumentServiceProxy,
  RequiredApplicationDocumentDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './view-required-application-document-dialog.component.html'
})
export class ViewRequiredApplicationDocumentDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  requiredApplicationDocument = new RequiredApplicationDocumentDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _requiredApplicationDocumentService: RequiredApplicationDocumentServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._requiredApplicationDocumentService.getRequiredApplicationDocument(this.id).subscribe((result) => {
      this.requiredApplicationDocument = result.result;
    });
  }
}
