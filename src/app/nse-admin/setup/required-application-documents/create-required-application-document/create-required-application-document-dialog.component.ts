import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  RequiredApplicationDocumentServiceProxy,
  CreateRequiredApplicationDocumentDto,
  ApplicationTypeDto,
  ApplicationTypeServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  templateUrl: './create-required-application-document-dialog.component.html'
})
export class CreateRequiredApplicationDocumentDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  requiredApplicationDocument = new CreateRequiredApplicationDocumentDto();
  applicationTypes: ApplicationTypeDto[];
  selectedApplicationType: ApplicationTypeDto;
  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _requiredApplicationDocumentService: RequiredApplicationDocumentServiceProxy,
    public _applicationTypeService: ApplicationTypeServiceProxy,
    private ngxService: NgxUiLoaderService,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.getApplicationTypes();
    this.requiredApplicationDocument.isActive = true;
    this.requiredApplicationDocument.isMandatory = true;
  }
  getApplicationTypes(): void {
    this.ngxService.start();
    this._applicationTypeService.getAllApplicationTypesList(0)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(result => {
        this.applicationTypes = result;
      });
  }
  onChange(event): void {
    console.log(this.selectedApplicationType);
  }
  save(): void {
    this.saving = true;
    this.requiredApplicationDocument.applicationTypeId = this.selectedApplicationType.id;
    this._requiredApplicationDocumentService
      .createRequiredApplicationDocument(this.requiredApplicationDocument)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
