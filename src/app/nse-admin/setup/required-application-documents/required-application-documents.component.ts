import { Component, Injector, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  PagedListingComponentBase,
  PagedRequestDto
} from 'shared/paged-listing-component-base';
import {
  RequiredApplicationDocumentServiceProxy,
  RequiredApplicationDocumentDto,
  RequiredApplicationDocumentDtoPagedResultDto
} from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { BehaviorSubject } from 'rxjs';
import { ViewRequiredApplicationDocumentDialogComponent } from './view-required-application-document/view-required-application-document-dialog.component';
import { CreateRequiredApplicationDocumentDialogComponent } from './create-required-application-document/create-required-application-document-dialog.component';
import { EditRequiredApplicationDocumentDialogComponent } from './edit-required-application-document/edit-required-application-document-dialog.component';


class PagedRequiredApplicationDocumentsRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  templateUrl: './required-application-documents.component.html',
  animations: [appModuleAnimation()]
})
export class RequiredApplicationDocumentsComponent extends PagedListingComponentBase<RequiredApplicationDocumentDto> {
  @ViewChild('dt') table: Table;
  requiredApplicationDocuments: RequiredApplicationDocumentDto[] = [];
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;
  selectedRequiredApplicationDocument$: BehaviorSubject<RequiredApplicationDocumentDto>;

  cols = [
    { field: 'sn', header: 'S/N' },
    { field: 'name', header: 'Document name' },
    { field: 'applicationType', header: 'Application Type' },
    { field: 'isMandatory', header: 'Is Mandatory' },
    { field: 'isActive', header: 'Is active' }
  ];

  buttonItems = [
    {
      label: 'Edit', visible: this.isGranted('EditRequiredApplicationDocument'), command: () => {
        this.editRequiredApplicationDocument(this.selectedRequiredApplicationDocument$.value);
      }
    },
    {
      label: 'Delete', visible: this.isGranted('DeleteRequiredApplicationDocument'), command: () => {
        this.delete(this.selectedRequiredApplicationDocument$.value);
      }
    },
    {
      label: 'View', visible: this.isGranted('ViewRequiredApplicationDocument'), command: () => {
        this.viewRequiredApplicationDocument(this.selectedRequiredApplicationDocument$.value);
      }
    }
  ];

  constructor(
    injector: Injector,
    private _requiredApplicationDocumentService: RequiredApplicationDocumentServiceProxy,
    private _modalService: BsModalService
  ) {
    super(injector);

    this.selectedRequiredApplicationDocument$ = new BehaviorSubject(new RequiredApplicationDocumentDto());
  }

  setSelectedRequiredApplicationDocument(requiredApplicationDocument: RequiredApplicationDocumentDto): void {
    this.selectedRequiredApplicationDocument$.next(requiredApplicationDocument);
  }

  createRequiredApplicationDocument(): void {
    this.showCreateOrEditRequiredApplicationDocumentDialog();
  }

  editRequiredApplicationDocument(requiredApplicationDocument: RequiredApplicationDocumentDto): void {
    this.showCreateOrEditRequiredApplicationDocumentDialog(requiredApplicationDocument.id);
  }

  viewRequiredApplicationDocument(requiredApplicationDocument: RequiredApplicationDocumentDto): void {
    this.showViewRequiredApplicationDocumentDialog(requiredApplicationDocument.id);
  }

  clearFilters(): void {
    this.keyword = '';
    this.isActive = undefined;
    this.getDataPage(1);
  }

  protected list(
    request: PagedRequiredApplicationDocumentsRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._requiredApplicationDocumentService
      .getAllRequiredApplicationDocuments(
        request.keyword,
        request.isActive,
        request.skipCount,
        request.maxResultCount
      )
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: RequiredApplicationDocumentDtoPagedResultDto) => {
        this.requiredApplicationDocuments = result.items;
        this.showPaging(result, pageNumber);
      });
  }

  protected delete(requiredApplicationDocument: RequiredApplicationDocumentDto): void {
    abp.message.confirm(
      `${requiredApplicationDocument.name} will be deleted`,
      undefined,
      (result: boolean) => {
        if (result) {
          this._requiredApplicationDocumentService.deleteRequiredApplicationDocument(requiredApplicationDocument.id).subscribe(() => {
            abp.notify.success(this.l('SuccessfullyDeleted'));
            this.refresh();
          });
        }
      }
    );
  }

  private showViewRequiredApplicationDocumentDialog(id?: number): void {
    const viewRequiredApplicationDocumentDialog = this._modalService.show(
      ViewRequiredApplicationDocumentDialogComponent,
      {
        backdrop: 'static',
        keyboard: false,
        animated: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
        initialState: {
          id: id,
        },
      }
    );
  }

  private showCreateOrEditRequiredApplicationDocumentDialog(id?: number): void {
    let createOrEditRequiredApplicationDocumentDialog: BsModalRef;
    if (!id) {
      createOrEditRequiredApplicationDocumentDialog = this._modalService.show(
        CreateRequiredApplicationDocumentDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
        }
      );
    } else {
      createOrEditRequiredApplicationDocumentDialog = this._modalService.show(
        EditRequiredApplicationDocumentDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
          initialState: {
            id: id,
          },
        }
      );
    }

    createOrEditRequiredApplicationDocumentDialog.content.onSave.subscribe(() => {
      this.refresh();
    });
  }
}
