import { CreateOrganizationDto, EnumModel, OrganizationType } from './../../../../../shared/service-proxies/service-proxies';
import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  OrganizationServiceProxy,
  OrganizationDto,
  Organization
} from '@shared/service-proxies/service-proxies';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import * as moment from 'moment';

@Component({
  templateUrl: './edit-organization-dialog.component.html'
})
export class EditOrganizationDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  organization = new CreateOrganizationDto();
  id: number;
  organizationTypes: EnumModel[];
  selectedOrganizationType: EnumModel;
  //retrievedOrganizationType: OrganizationType;
  d = new Date();
  year = this.d.getFullYear();
  month = this.d.getMonth();
  day = this.d.getDate();
  maxDate = new Date(this.year - 1, this.month, this.day);
  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _organizationService: OrganizationServiceProxy,
    private ngxService: NgxUiLoaderService,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.getOrganizationTypes();
    this._organizationService.getOrganization(this.id).subscribe((result) => {
      this.organization = result.result;
      this.selectedOrganizationType = new EnumModel();
      this.selectedOrganizationType.id = result.result.organizationType;
      console.log(this.organization.dateOfIncorporation);
      this.organization.dateOfIncorporation =
        moment(`${this.year}/${this.month}/${this.day}`, 'mm/dd/yy');//moment(this.organization.dateOfIncorporation.toString(), 'mm/dd/yy');
    });
  }
  getOrganizationTypes(): void {
    this.ngxService.start();
    this._organizationService.getOrganizationTypes()
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(result => {
        this.organizationTypes = result.result;
      });
  }
  onChange(event): void {
    console.log(this.selectedOrganizationType);
  }
  save(): void {
    this.saving = true;

    this._organizationService
      .editOrganization(this.organization)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
