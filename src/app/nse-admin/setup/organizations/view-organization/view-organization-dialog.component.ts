import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  OrganizationServiceProxy,
  OrganizationDto,
  CreateOrganizationDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './view-organization-dialog.component.html'
})
export class ViewOrganizationDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  organization = new CreateOrganizationDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _organizationService: OrganizationServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._organizationService.getOrganization(this.id).subscribe((result) => {
      this.organization = result.result;
    });
  }
}
