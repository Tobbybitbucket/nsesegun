import { Component, Injector, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  PagedListingComponentBase,
  PagedRequestDto
} from 'shared/paged-listing-component-base';
import {
  OrganizationServiceProxy,
  OrganizationDto,
  OrganizationDtoPagedResultDto
} from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { BehaviorSubject } from 'rxjs';
import { ViewOrganizationDialogComponent } from './view-organization/view-organization-dialog.component';
import { CreateOrganizationDialogComponent } from './create-organization/create-organization-dialog.component';
import { EditOrganizationDialogComponent } from './edit-organization/edit-organization-dialog.component';


class PagedOrganizationsRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  templateUrl: './organizations.component.html',
  animations: [appModuleAnimation()]
})
export class OrganizationsComponent extends PagedListingComponentBase<OrganizationDto> {
  @ViewChild('dt') table: Table;
  organizations: OrganizationDto[] = [];
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;
  selectedOrganization$: BehaviorSubject<OrganizationDto>;

  cols = [
    { field: 'sn', header: 'S/N' },
    { field: 'name', header: 'Name' },
    { field: 'type', header: 'Organization Type' },
    { field: 'email', header: 'Email' },
    // { field: 'rcNumber', header: 'Registration Number' },
    // { field: 'address', header: 'Address' },
    { field: 'dateOfIncorporation', header: 'Date Of Incorporation' },
    { field: 'isActive', header: 'Is active' }
  ];

  buttonItems = [
    {
      label: 'Edit', visible: this.isGranted('EditOrganization'), command: () => {
        this.editOrganization(this.selectedOrganization$.value);
      }
    },
    {
      label: 'Delete', visible: this.isGranted('DeleteOrganization'), command: () => {
        this.delete(this.selectedOrganization$.value);
      }
    },
    {
      label: 'View', visible: this.isGranted('ViewOrganization'), command: () => {
        this.viewOrganization(this.selectedOrganization$.value);
      }
    }
  ];

  constructor(
    injector: Injector,
    private _organizationService: OrganizationServiceProxy,
    private _modalService: BsModalService
  ) {
    super(injector);

    this.selectedOrganization$ = new BehaviorSubject(new OrganizationDto());
  }

  setSelectedOrganization(organization: OrganizationDto): void {
    this.selectedOrganization$.next(organization);
  }

  createOrganization(): void {
    this.showCreateOrEditOrganizationDialog();
  }

  editOrganization(organization: OrganizationDto): void {
    this.showCreateOrEditOrganizationDialog(organization.id);
  }

  viewOrganization(organization: OrganizationDto): void {
    this.showViewOrganizationDialog(organization.id);
  }

  clearFilters(): void {
    this.keyword = '';
    this.isActive = undefined;
    this.getDataPage(1);
  }

  protected list(
    request: PagedOrganizationsRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._organizationService
      .getAllOrganizations(
        request.keyword,
        request.isActive,
        request.skipCount,
        request.maxResultCount
      )
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: OrganizationDtoPagedResultDto) => {
        this.organizations = result.items;
        this.showPaging(result, pageNumber);
      });
  }

  protected delete(organization: OrganizationDto): void {
    abp.message.confirm(
      `${organization.name} will be deleted`,
      undefined,
      (result: boolean) => {
        if (result) {
          this._organizationService.deleteOrganization(organization.id).subscribe(() => {
            abp.notify.success(this.l('SuccessfullyDeleted'));
            this.refresh();
          });
        }
      }
    );
  }

  private showViewOrganizationDialog(id?: number): void {
    const viewOrganizationDialog = this._modalService.show(
      ViewOrganizationDialogComponent,
      {
        backdrop: 'static',
        keyboard: false,
        animated: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
        initialState: {
          id: id,
        },
      }
    );
  }

  private showCreateOrEditOrganizationDialog(id?: number): void {
    let createOrEditOrganizationDialog: BsModalRef;
    if (!id) {
      createOrEditOrganizationDialog = this._modalService.show(
        CreateOrganizationDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
        }
      );
    } else {
      createOrEditOrganizationDialog = this._modalService.show(
        EditOrganizationDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
          initialState: {
            id: id,
          },
        }
      );
    }

    createOrEditOrganizationDialog.content.onSave.subscribe(() => {
      this.refresh();
    });
  }
}
