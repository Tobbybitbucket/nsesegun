import { OrganizationType, EnumModel } from './../../../../../shared/service-proxies/service-proxies';
import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { AppComponentBase } from '@shared/app-component-base';
import {
  OrganizationServiceProxy,
  CreateOrganizationDto
} from '@shared/service-proxies/service-proxies';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';

@Component({
  templateUrl: './create-organization-dialog.component.html'
})
export class CreateOrganizationDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  organization = new CreateOrganizationDto();
  organizationTypes: EnumModel[];
  selectedOrganizationType: EnumModel;
  d = new Date();
  year = this.d.getFullYear();
  month = this.d.getMonth();
  day = this.d.getDate();
  maxDate = new Date(this.year - 1, this.month, this.day);
  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _organizationService: OrganizationServiceProxy,
    private ngxService: NgxUiLoaderService,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.getOrganizationTypes();
    this.organization.isActive = true;
  }
  getOrganizationTypes(): void {
    this.ngxService.start();
    this._organizationService.getOrganizationTypes()
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(result => {
        this.organizationTypes = result.result;
      });
  }
  onChange(event): void {
    console.log(this.selectedOrganizationType);
  }
  save(): void {
    this.saving = true;
    this.organization.organizationType = this.selectedOrganizationType.id;
    this._organizationService
      .createOrganization(this.organization)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
