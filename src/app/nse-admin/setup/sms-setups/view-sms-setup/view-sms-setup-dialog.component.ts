import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  SmsSetupServiceProxy,
  SmsSetupDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './view-sms-setup-dialog.component.html'
})
export class ViewSmsSetupDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  smsSetup = new SmsSetupDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _smsSetupService: SmsSetupServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._smsSetupService.getSmsSetup(this.id).subscribe((result) => {
      this.smsSetup = result.result;
    });
  }
}
