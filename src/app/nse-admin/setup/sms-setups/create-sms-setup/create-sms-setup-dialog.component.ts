import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  SmsSetupServiceProxy,
  CreateSmsSetupDto
} from '@shared/service-proxies/service-proxies';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';

@Component({
  templateUrl: './create-sms-setup-dialog.component.html'
})
export class CreateSmsSetupDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  smsSetup = new CreateSmsSetupDto();

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _smsSetupService: SmsSetupServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    
  }

  save(): void {
    this.saving = true;
    this._smsSetupService
      .createSmsSetup(this.smsSetup)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
