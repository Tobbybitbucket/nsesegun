import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  CountryServiceProxy,
  CountryDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './edit-country-dialog.component.html'
})
export class EditCountryDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  country = new CountryDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _countryService: CountryServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._countryService.getCountry(this.id).subscribe((result) => {
      this.country = result.result;
    });
  }

  save(): void {
    this.saving = true;

    this._countryService
      .editCountry(this.country)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
