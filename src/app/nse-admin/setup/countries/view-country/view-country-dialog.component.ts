import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  CountryServiceProxy,
  CountryDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './view-country-dialog.component.html'
})
export class ViewCountryDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  country = new CountryDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _countryService: CountryServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._countryService.getCountry(this.id).subscribe((result) => {
      this.country = result.result;
    });
  }
}
