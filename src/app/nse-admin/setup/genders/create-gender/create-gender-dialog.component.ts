import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  GenderServiceProxy,
  CreateGenderDto
} from '@shared/service-proxies/service-proxies';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';

@Component({
  templateUrl: './create-gender-dialog.component.html'
})
export class CreateGenderDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  gender = new CreateGenderDto();

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _genderService: GenderServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.gender.isActive = true;
  }

  save(): void {
    this.saving = true;
    this._genderService
      .createGender(this.gender)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
