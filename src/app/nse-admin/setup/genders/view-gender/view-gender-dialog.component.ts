import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  GenderServiceProxy,
  GenderDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './view-gender-dialog.component.html'
})
export class ViewGenderDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  gender = new GenderDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _genderService: GenderServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._genderService.getGender(this.id).subscribe((result) => {
      this.gender = result.result;
    });
  }
}
