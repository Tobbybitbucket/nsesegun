import { Component, Injector, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  PagedListingComponentBase,
  PagedRequestDto
} from 'shared/paged-listing-component-base';
import {
  GenderServiceProxy,
  GenderDto,
  GenderDtoPagedResultDto
} from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { BehaviorSubject } from 'rxjs';
import { ViewGenderDialogComponent } from './view-gender/view-gender-dialog.component';
import { CreateGenderDialogComponent } from './create-gender/create-gender-dialog.component';
import { EditGenderDialogComponent } from './edit-gender/edit-gender-dialog.component';


class PagedGendersRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  templateUrl: './genders.component.html',
  animations: [appModuleAnimation()]
})
export class GendersComponent extends PagedListingComponentBase<GenderDto> {
  @ViewChild('dt') table: Table;
  genders: GenderDto[] = [];
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;
  selectedGender$: BehaviorSubject<GenderDto>;

  cols = [
    { field: 'sn', header: 'S/N' },
    { field: 'name', header: 'Gender name' },
    { field: 'isActive', header: 'Is active' }
  ];

  buttonItems = [
    {
      label: 'Edit', visible: this.isGranted('EditGender'), command: () => {
        this.editGender(this.selectedGender$.value);
      }
    },
    {
      label: 'Delete', visible: this.isGranted('DeleteGender'), command: () => {
        this.delete(this.selectedGender$.value);
      }
    },
    {
      label: 'View', visible: this.isGranted('ViewGender'), command: () => {
        this.viewGender(this.selectedGender$.value);
      }
    }
  ];

  constructor(
    injector: Injector,
    private _genderService: GenderServiceProxy,
    private _modalService: BsModalService
  ) {
    super(injector);

    this.selectedGender$ = new BehaviorSubject(new GenderDto());
  }

  setSelectedGender(gender: GenderDto): void {
    this.selectedGender$.next(gender);
  }

  createGender(): void {
    this.showCreateOrEditGenderDialog();
  }

  editGender(gender: GenderDto): void {
    this.showCreateOrEditGenderDialog(gender.id);
  }

  viewGender(gender: GenderDto): void {
    this.showViewGenderDialog(gender.id);
  }

  clearFilters(): void {
    this.keyword = '';
    this.isActive = undefined;
    this.getDataPage(1);
  }

  protected list(
    request: PagedGendersRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._genderService
      .getAllGenders(
        request.keyword,
        request.isActive,
        request.skipCount,
        request.maxResultCount
      )
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: GenderDtoPagedResultDto) => {
        this.genders = result.items;
        this.showPaging(result, pageNumber);
      });
  }

  protected delete(gender: GenderDto): void {
    abp.message.confirm(
      `${gender.name} will be deleted`,
      undefined,
      (result: boolean) => {
        if (result) {
          this._genderService.deleteGender(gender.id).subscribe(() => {
            abp.notify.success(this.l('SuccessfullyDeleted'));
            this.refresh();
          });
        }
      }
    );
  }

  private showViewGenderDialog(id?: number): void {
    const viewGenderDialog = this._modalService.show(
      ViewGenderDialogComponent,
      {
        backdrop: 'static',
        keyboard: false,
        animated: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
        initialState: {
          id: id,
        },
      }
    );
  }

  private showCreateOrEditGenderDialog(id?: number): void {
    let createOrEditGenderDialog: BsModalRef;
    if (!id) {
      createOrEditGenderDialog = this._modalService.show(
        CreateGenderDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
        }
      );
    } else {
      createOrEditGenderDialog = this._modalService.show(
        EditGenderDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
          initialState: {
            id: id,
          },
        }
      );
    }

    createOrEditGenderDialog.content.onSave.subscribe(() => {
      this.refresh();
    });
  }
}
