import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  TitleServiceProxy,
  TitleDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './view-title-dialog.component.html'
})
export class ViewTitleDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  title = new TitleDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _titleService: TitleServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._titleService.getTitle(this.id).subscribe((result) => {
      this.title = result.result;
    });
  }
}
