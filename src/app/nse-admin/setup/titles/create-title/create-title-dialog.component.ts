import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  TitleServiceProxy,
  CreateTitleDto
} from '@shared/service-proxies/service-proxies';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';

@Component({
  templateUrl: './create-title-dialog.component.html'
})
export class CreateTitleDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  title = new CreateTitleDto();

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _titleService: TitleServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.title.isActive = true;
  }

  save(): void {
    this.saving = true;
    this._titleService
      .createTitle(this.title)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
