import { Component, Injector, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  PagedListingComponentBase,
  PagedRequestDto
} from 'shared/paged-listing-component-base';
import {
  TitleServiceProxy,
  TitleDto,
  TitleDtoPagedResultDto
} from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { BehaviorSubject } from 'rxjs';
import { ViewTitleDialogComponent } from './view-title/view-title-dialog.component';
import { CreateTitleDialogComponent } from './create-title/create-title-dialog.component';
import { EditTitleDialogComponent } from './edit-title/edit-title-dialog.component';


class PagedTitlesRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  templateUrl: './titles.component.html',
  animations: [appModuleAnimation()]
})
export class TitlesComponent extends PagedListingComponentBase<TitleDto> {
  @ViewChild('dt') table: Table;
  titles: TitleDto[] = [];
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;
  selectedTitle$: BehaviorSubject<TitleDto>;

  cols = [
    { field: 'sn', header: 'S/N' },
    { field: 'name', header: 'Title name' },
    { field: 'isActive', header: 'Is active' }
  ];

  buttonItems = [
    {
      label: 'Edit', visible: this.isGranted('EditTitle'), command: () => {
        this.editTitle(this.selectedTitle$.value);
      }
    },
    {
      label: 'Delete', visible: this.isGranted('DeleteTitle'), command: () => {
        this.delete(this.selectedTitle$.value);
      }
    },
    {
      label: 'View', visible: this.isGranted('ViewTitle'), command: () => {
        this.viewTitle(this.selectedTitle$.value);
      }
    }
  ];

  constructor(
    injector: Injector,
    private _titleService: TitleServiceProxy,
    private _modalService: BsModalService
  ) {
    super(injector);

    this.selectedTitle$ = new BehaviorSubject(new TitleDto());
  }

  setSelectedTitle(title: TitleDto): void {
    this.selectedTitle$.next(title);
  }

  createTitle(): void {
    this.showCreateOrEditTitleDialog();
  }

  editTitle(title: TitleDto): void {
    this.showCreateOrEditTitleDialog(title.id);
  }

  viewTitle(title: TitleDto): void {
    this.showViewTitleDialog(title.id);
  }

  clearFilters(): void {
    this.keyword = '';
    this.isActive = undefined;
    this.getDataPage(1);
  }

  protected list(
    request: PagedTitlesRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._titleService
      .getAllTitles(
        request.keyword,
        request.isActive,
        request.skipCount,
        request.maxResultCount
      )
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: TitleDtoPagedResultDto) => {
        this.titles = result.items;
        this.showPaging(result, pageNumber);
      });
  }

  protected delete(title: TitleDto): void {
    abp.message.confirm(
      `${title.name} will be deleted`,
      undefined,
      (result: boolean) => {
        if (result) {
          this._titleService.deleteTitle(title.id).subscribe(() => {
            abp.notify.success(this.l('SuccessfullyDeleted'));
            this.refresh();
          });
        }
      }
    );
  }

  private showViewTitleDialog(id?: number): void {
    const viewTitleDialog = this._modalService.show(
      ViewTitleDialogComponent,
      {
        backdrop: 'static',
        keyboard: false,
        animated: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
        initialState: {
          id: id,
        },
      }
    );
  }

  private showCreateOrEditTitleDialog(id?: number): void {
    let createOrEditTitleDialog: BsModalRef;
    if (!id) {
      createOrEditTitleDialog = this._modalService.show(
        CreateTitleDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
        }
      );
    } else {
      createOrEditTitleDialog = this._modalService.show(
        EditTitleDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
          initialState: {
            id: id,
          },
        }
      );
    }

    createOrEditTitleDialog.content.onSave.subscribe(() => {
      this.refresh();
    });
  }
}
