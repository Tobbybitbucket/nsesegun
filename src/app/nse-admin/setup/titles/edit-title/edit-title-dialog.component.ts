import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  TitleServiceProxy,
  TitleDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './edit-title-dialog.component.html'
})
export class EditTitleDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  title = new TitleDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _titleService: TitleServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._titleService.getTitle(this.id).subscribe((result) => {
      this.title = result.result;
    });
  }

  save(): void {
    this.saving = true;

    this._titleService
      .editTitle(this.title)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
