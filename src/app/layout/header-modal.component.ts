import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
    selector: 'app-header-modal',
    templateUrl: './header-modal.component.html',
    animations: [
        trigger('simpleFadeAnimation', [
            state('in', style({ opacity: 1 })),
            transition(':enter', [
                style({ opacity: 0 }),
                animate(300)
            ]),
            transition(':leave',
                animate(300, style({ opacity: 0 })))
        ])
    ]
})


export class HeaderModalComponent implements OnInit {

    @Input() showModal: boolean;
    @Input() hasViewAll: boolean;
    @Input() viewAllUrl: string;
    @Input() viewAllText: string;
    @Output() closedModal = new EventEmitter<any>();

    ngOnInit(): void {
    }

    closeDialog() {
        this.closedModal.emit();
    }

}
