import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { AppAuthService } from '@shared/auth/app-auth.service';
import { AppSessionService } from '@shared/session/app-session.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'header-user-menu',
  templateUrl: './header-user-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderUserMenuComponent implements OnInit {
  fullName: string;
  canChangePassword: boolean;
  organization: string;
  role: string;
  userImage$: BehaviorSubject<string>;
  constructor(private _authService: AppAuthService, private _sessionService: AppSessionService) { this.userImage$ = new BehaviorSubject('assets/img/user.png'); }
  ngOnInit(): void {
    this.fullName = `${this._sessionService.user.name} ${this._sessionService.user.surname}`;
    this.role = this._sessionService.user.userRole;
    this.organization = this._sessionService.user.organization?.name;
    this.canChangePassword = this._sessionService.user.canChangePassword;
    if (this._sessionService.user.imagePath != null && this._sessionService.user.imagePath !== '') {
      this.userImage$ = new BehaviorSubject(this._sessionService.user.imagePath);
    }
  }

  logout(): void {
    this._authService.logout();
  }
}
