import { EmailMessageServiceProxy } from './../../shared/service-proxies/service-proxies';
import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit {
  unreadMessages: BehaviorSubject<number>;
  showNotification = false;
  showMessages = false;

  notifications = [
    {
      title: 'Notification Title',
      description: 'This is the content of the notification. This is the content of the notification.',
      isActive: true,
    },
    {
      title: 'Notification Title',
      description: 'This is the content of the notification. This is the content of the notification.',
      isActive: false,
    },
    {
      title: 'Notification Title',
      description: 'This is the content of the notification. This is the content of the notification.',
      isActive: false,
    }
  ];

  messages = [
    {
      title: 'Message Title',
      description: 'This is the content of the message. This is the content of the message.',
      isActive: true,
    },
    {
      title: 'Message Title',
      description: 'This is the content of the message. This is the content of the message.',
      isActive: false,
    },
    {
      title: 'Message Title',
      description: 'This is the content of the message. This is the content of the message.',
      isActive: false,
    }
  ];
  constructor(private _emailMessageService: EmailMessageServiceProxy) { this.unreadMessages = new BehaviorSubject(0); }
  ngOnInit(): void {
    this._emailMessageService
      .getUnreadMessagesForUser()
      .subscribe((result: number) => {
        // this.unreadMessages.next(result);
        this.unreadMessages = new BehaviorSubject(result);
      });
  }
  toggleNotificationDialog() {
    this.showNotification = !this.showNotification;
  }

  toggleMessagesDialog() {
    this.showMessages = !this.showMessages;
  }
}
