// import { Component, Injector, OnInit } from "@angular/core";
// import { AppComponentBase } from "@shared/app-component-base";
// import {
//   Router,
//   RouterEvent,
//   NavigationEnd,
//   PRIMARY_OUTLET,
// } from "@angular/router";
// import { BehaviorSubject } from "rxjs";
// import { filter } from "rxjs/operators";
// import { MenuItem } from "@shared/layout/menu-item";

// @Component({
//   selector: "sidebar-menu",
//   templateUrl: "./sidebar-menu.component.html",
// })
// export class SidebarMenuComponent extends AppComponentBase implements OnInit {
//   menuItems: MenuItem[];
//   menuItemsMap: { [key: number]: MenuItem } = {};
//   activatedMenuItems: MenuItem[] = [];
//   routerEvents: BehaviorSubject<RouterEvent> = new BehaviorSubject(undefined);
//   homeRoute = "/app/home";

//   constructor(injector: Injector, private router: Router) {
//     super(injector);
//     this.router.events.subscribe(this.routerEvents);
//   }

//   ngOnInit(): void {
//     this.menuItems = this.getMenuItems();
//     this.patchMenuItems(this.menuItems);
//     this.routerEvents
//       .pipe(filter((event) => event instanceof NavigationEnd))
//       .subscribe((event) => {
//         const currentUrl = event.url !== "/" ? event.url : this.homeRoute;
//         const primaryUrlSegmentGroup = this.router.parseUrl(currentUrl).root
//           .children[PRIMARY_OUTLET];
//         if (primaryUrlSegmentGroup) {
//           this.activateMenuItems("/" + primaryUrlSegmentGroup.toString());
//         }
//       });
//   }

//   getMenuItems(): MenuItem[] {
//     return [
//       new MenuItem(this.l("HomePage"), "/app/home", "pi pi-home"),
//       new MenuItem(
//         this.l("Tenants"),
//         "/app/te  nants",
//         "fas fa-building",
//         "Pages.Tenants"
//       ),
//       new MenuItem(
//         this.l("Users"),
//         "/app/users",
//         "fas fa-users",
//         "ViewUser"
//       ),
//       new MenuItem(
//         this.l("Roles"),
//         "/app/roles",
//         "fas fa-theater-masks",
//         "ViewRole"
//       ),
//       new MenuItem(this.l("Pre-approval"), "", "far fa-file-alt", "", [
//         new MenuItem("New Application", "/app/new-application", ""),
//         new MenuItem("Resume Application", "/app/resume-application", ""),
//       ]),
//       // new MenuItem(this.l('About'), '/app/about', 'fas fa-info-circle'),
//       new MenuItem(this.l("My Workspace"), "", "fas fa-th-large", "", [
//         new MenuItem(
//           "Equity Application",
//           "/app/equity-application",
//           "",
//           null,
//           null,
//           4
//         ),
//         new MenuItem("State Bonds", "/app/state-bonds", ""),
//         new MenuItem("Corporate Bonds", "/app/corporate-bonds", ""),
//       ]),
//       new MenuItem(this.l("Messages"), "/app/messages", "far fa-envelope"),
//       new MenuItem(this.l("Profile"), "/app/profile", "far fa-user-circle"),
//       new MenuItem(this.l("Contact Us"), "/app/contact", "fas fa-phone-alt"),
//       new MenuItem(this.l("Configuration"), "/app/settings", "fas fa-cog"),
//       new MenuItem("Help", "", "far fa-file-alt", "", [
//         new MenuItem("FAQ", "/app/faq", ""),
//         new MenuItem("User Guides", "/app/userguides", ""),
//       ]),
//     ];
//   }

//   patchMenuItems(items: MenuItem[], parentId?: number): void {
//     items.forEach((item: MenuItem, index: number) => {
//       item.id = parentId ? Number(parentId + "" + (index + 1)) : index + 1;
//       if (parentId) {
//         item.parentId = parentId;
//       }
//       if (parentId || item.children) {
//         this.menuItemsMap[item.id] = item;
//       }
//       if (item.children) {
//         this.patchMenuItems(item.children, item.id);
//       }
//     });
//   }

//   activateMenuItems(url: string): void {
//     this.deactivateMenuItems(this.menuItems);
//     this.activatedMenuItems = [];
//     const foundedItems = this.findMenuItemsByUrl(url, this.menuItems);
//     foundedItems.forEach((item) => {
//       this.activateMenuItem(item);
//     });
//   }

//   deactivateMenuItems(items: MenuItem[]): void {
//     items.forEach((item: MenuItem) => {
//       item.isActive = false;
//       item.isCollapsed = true;
//       if (item.children) {
//         this.deactivateMenuItems(item.children);
//       }
//     });
//   }

//   findMenuItemsByUrl(
//     url: string,
//     items: MenuItem[],
//     foundedItems: MenuItem[] = []
//   ): MenuItem[] {
//     items.forEach((item: MenuItem) => {
//       if (item.route === url) {
//         foundedItems.push(item);
//       } else if (item.children) {
//         this.findMenuItemsByUrl(url, item.children, foundedItems);
//       }
//     });
//     return foundedItems;
//   }

//   activateMenuItem(item: MenuItem): void {
//     item.isActive = true;
//     if (item.children) {
//       item.isCollapsed = false;
//     }
//     this.activatedMenuItems.push(item);
//     if (item.parentId) {
//       this.activateMenuItem(this.menuItemsMap[item.parentId]);
//     }
//   }

//   isMenuItemVisible(item: MenuItem): boolean {
//     if (!item.permissionName) {
//       return true;
//     }
//     return this.permission.isGranted(item.permissionName);
//   }
// }
import { Component, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import {
  Router,
  RouterEvent,
  NavigationEnd,
  PRIMARY_OUTLET,
} from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { MenuItem } from '@shared/layout/menu-item';

@Component({
  selector: 'sidebar-menu',
  templateUrl: './sidebar-menu.component.html',
})
export class SidebarMenuComponent extends AppComponentBase implements OnInit {
  menuItems: MenuItem[];
  menuItemsMap: { [key: number]: MenuItem } = {};
  activatedMenuItems: MenuItem[] = [];
  routerEvents: BehaviorSubject<RouterEvent> = new BehaviorSubject(undefined);
  homeRoute = '/app/home';

  constructor(injector: Injector, private router: Router) {
    super(injector);
    this.router.events.subscribe(this.routerEvents);
  }

  ngOnInit(): void {
    this.menuItems = this.getMenuItems();
    this.patchMenuItems(this.menuItems);
    this.routerEvents
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event) => {
        const currentUrl = event.url !== '/' ? event.url : this.homeRoute;
        const primaryUrlSegmentGroup = this.router.parseUrl(currentUrl).root
          .children[PRIMARY_OUTLET];
        if (primaryUrlSegmentGroup) {
          this.activateMenuItems('/' + primaryUrlSegmentGroup.toString());
        }
      });
  }

  getMenuItems(): MenuItem[] {
    return [
      new MenuItem(this.l('Dashboard'), '/app/home', 'pi pi-home'),
      new MenuItem(
        this.l('Tenants'),
        '/app/te  nants',
        'fas fa-building',
        'Pages.Tenants'
      ),
      new MenuItem(
        this.l('Participants'),
        '/app/users',
        'fas fa-users',
        'AdminModule'
      ),
      new MenuItem(
        this.l('Roles'),
        '/app/roles',
        'fas fa-theater-masks',
        'AdminModule'
      ),
      new MenuItem(this.l('Pre-approval'), '', 'far fa-file-alt', 'AdminModule', [
        new MenuItem('New Application', '/app/new-application', ''),
        new MenuItem('Resume Application', '/app/resume-application', ''),
      ]),
      new MenuItem(this.l('System setup'), '', 'fas fa-info-circle', 'AdminModule', [
        new MenuItem(this.l('Business setup'), '', '', '', [
          new MenuItem('Organizations', '/app/organizations', 'Vieworganization'),
          new MenuItem('Application Status', '/app/application-statuses', 'ViewApplicationStatus'),
          new MenuItem('Application Type', '/app/application-types', 'ViewApplicationType'),
          new MenuItem('Listing Type', '/app/listing-types', 'ViewListingType'),
          new MenuItem('Application Documents', '/app/required-application-documents', 'ViewRequiredApplicationDocument'),
          new MenuItem('Issuing Companies', '/app/issuing-companies', 'ViewIssuingCompany'),
          new MenuItem('Market Capitalization Metrics', '/app/fees-calculation', 'ViewViewFeesCalculation'),
          new MenuItem('Email setup', '/app/email-setups', 'ViewEmailSetup'),
          new MenuItem('Sms setup', '/app/sms-setups', 'ViewSmsSetup'),
          new MenuItem('Country', '/app/countries', 'ViewCountry'),
          new MenuItem('State', '/app/states', 'ViewState'),
          new MenuItem('Lga', '/app/lgas', 'ViewLga'),
          new MenuItem('Gender', '/app/genders', 'ViewGender'),
          new MenuItem('Title', '/app/titles', 'ViewTitle')
        ])
      ]),
      new MenuItem('My Workspace', '', 'fas fa-th-large', 'OtherRolesModule', [
        new MenuItem('Users', '/app/organization-users', 'ViewUser'),
        new MenuItem('Application', '/app/new-application', ''),
      ]),
      new MenuItem(this.l('My Workspace'), '', 'fas fa-th-large', 'AdminModule', [
        new MenuItem(
          'Equity Application',
          '/app/equity-application',
          '',
          null,
          null,
          4
        ),
        new MenuItem('State Bonds', '/app/state-bonds', ''),
        new MenuItem('Corporate Bonds', '/app/corporate-bonds', ''),
      ]),
      new MenuItem('Workflow Setup', '', 'far fa-file-alt', '', [
        new MenuItem('Form Sections', '/app/workflow/form-section', ''),
        new MenuItem('Document Template', '/app/workflow/doc-template', ''),
        new MenuItem('Process Config', '/app/workflow/process-list', ''),
        new MenuItem('Pending Items', '/app/workflow/pending-list', ''),
      ]),
      new MenuItem('Messages', '', 'far fa-envelope', '', [
        new MenuItem(this.l('Messages'), '/app/email-messages', ''),
        new MenuItem('Sent Messages', '/app/sent-email-messages', ''),
      ]),
      new MenuItem('Payment Calculator', '/app/payment-calculator', 'fas fa-calculator', ''),
      new MenuItem(this.l('Profile'), '/app/profile', 'far fa-user-circle'),
      new MenuItem(this.l('Contact Us'), '/app/contact', 'fas fa-phone-alt', 'AdminModule'),
      new MenuItem(this.l('Configuration'), '/app/settings', 'fas fa-cog', 'AdminModule'),
      new MenuItem('Help', '', 'far fa-file-alt', '', [
        new MenuItem('FAQ', '/app/faq', ''),
        new MenuItem('User Guides', '/app/userguides', ''),
      ]),
    ];
  }

  patchMenuItems(items: MenuItem[], parentId?: number): void {
    items.forEach((item: MenuItem, index: number) => {
      item.id = parentId ? Number(parentId + '' + (index + 1)) : index + 1;
      if (parentId) {
        item.parentId = parentId;
      }
      if (parentId || item.children) {
        this.menuItemsMap[item.id] = item;
      }
      if (item.children) {
        this.patchMenuItems(item.children, item.id);
      }
    });
  }

  activateMenuItems(url: string): void {
    this.deactivateMenuItems(this.menuItems);
    this.activatedMenuItems = [];
    const foundedItems = this.findMenuItemsByUrl(url, this.menuItems);
    foundedItems.forEach((item) => {
      this.activateMenuItem(item);
    });
  }

  deactivateMenuItems(items: MenuItem[]): void {
    items.forEach((item: MenuItem) => {
      item.isActive = false;
      item.isCollapsed = true;
      if (item.children) {
        this.deactivateMenuItems(item.children);
      }
    });
  }

  findMenuItemsByUrl(
    url: string,
    items: MenuItem[],
    foundedItems: MenuItem[] = []
  ): MenuItem[] {
    items.forEach((item: MenuItem) => {
      if (item.route === url) {
        foundedItems.push(item);
      } else if (item.children) {
        this.findMenuItemsByUrl(url, item.children, foundedItems);
      }
    });
    return foundedItems;
  }

  activateMenuItem(item: MenuItem): void {
    item.isActive = true;
    if (item.children) {
      item.isCollapsed = false;
    }
    this.activatedMenuItems.push(item);
    if (item.parentId) {
      this.activateMenuItem(this.menuItemsMap[item.parentId]);
    }
  }

  isMenuItemVisible(item: MenuItem): boolean {
    if (!item.permissionName) {
      return true;
    }
    return this.permission.isGranted(item.permissionName);
  }
}
