import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { LayoutStoreService } from '@shared/layout/layout-store.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'sidebar-logo',
  templateUrl: './sidebar-logo.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidebarLogoComponent implements OnInit {
  sidebarExpanded$: BehaviorSubject<boolean>;

  constructor(private _layoutStore: LayoutStoreService) {
    this.sidebarExpanded$ = new BehaviorSubject(false);
  }

  ngOnInit(): void {
    this._layoutStore.sidebarExpanded.subscribe((value) => {
      this.sidebarExpanded$.next(value);
    });
  }
}
