import {
  Component,
  OnInit,
  Injector,
  ChangeDetectionStrategy,
  ViewChild,
  ChangeDetectorRef,
} from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Table } from "primeng/table";
import { Utils } from "../../shared/helpers/Utils";
import {
  ApprovalReviewServiceProxy,
  ApplicationServiceProxy,
  ApplicationDtoPagedResultDto,
  Int32ApiResult,
} from "@shared/service-proxies/service-proxies";
import { AppSessionService } from "@shared/session/app-session.service";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { finalize } from "rxjs/operators";
import { ApplicationDataService } from "@app/application/services/application-data.service";

@Component({
  templateUrl: "./home.component.html",
  animations: [appModuleAnimation()],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent extends AppComponentBase implements OnInit {
  @ViewChild("dt") table: Table;
  loading: Boolean = false;
  display: Boolean = false;
  displayAssign: Boolean = false;
  displayVersion: Boolean = false;
  organization: string;
  approvedCount: number = 0;
  declinedCount: number = 0;
  pendingCount: number = 0;
  escalationCount: number = 0;
  status = Utils.getStatus();

  applicationId: number;
  applicationTypeId: number;
  listingTypeId: number;
  applicationFields: any;
  applicationData: any;
  applicationVersion: any;
  displayApplicationStatusDialog: boolean = false;
  selectedAppStatus: any;
  updatetatusOptions;
  statusOptions = Object.keys(this.status).map((item) => {
    return { label: item, value: item.toLowerCase() };
  });

  summary: Array<any>;
  _summary: Array<any>;
  selectedAppId: number;
  selectedAppTypeId: number;
  selectedListingTypeId: number;
  printAppDialog: boolean = false;

  cols = [
    { field: "sn", header: "S/N" },
    { field: "appVersionRef", header: "Ref No." },
    { field: "issuer", header: "Issuer" },
    { field: "name", header: "Application Name" },
    { field: "date", header: "Date" },
    { field: "status", header: "Status" },
    { field: "action", header: "Action" },
  ];

  months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  constructor(
    private _approvalReviewServiceProxy: ApprovalReviewServiceProxy,
    private _sessionService: AppSessionService,
    private _applicationService: ApplicationServiceProxy,
    private _applicationDataService: ApplicationDataService,
    private router: Router,
    private route: ActivatedRoute,
    injector: Injector,
    private ngxService: NgxUiLoaderService,
    private ref: ChangeDetectorRef
  ) {
    super(injector);
  }

  applicationStatus: string = "";

  filterApplications(appStatus) {
    // this.summary = this._summary.filter(
    //   (p) => p.applicationStatusText == appStatus
    // );
    if (appStatus == "Pending") {
      this.router.navigate(["/app/workflow/pending-list"], {
        queryParams: {
          appStatus: appStatus,
        },
      });
    } else {
      this.table.filter(
        appStatus.toLowerCase(),
        "applicationStatusText",
        "equals"
      );
    }
  }
  getMenuItemsForApplications(app: any) {
    const context = app;
    var menu = [
      {
        label: context.isSubmit ? "View Application" : "Resume Application",
        command: (e) => {
          // console.log(context);
          if (context.isSubmit) {
            if (
              this._sessionService.user.organization.id > 1 ||
              context.reviewInfo.logId == null
            ) {
              this.router.navigate([`/app/view-application`], {
                queryParams: {
                  appId: context.id,
                  appTypeId: context.applicationTypeId,
                },
              });
            } else {
              this.router.navigate([`/app/workflow/pending-item`], {
                queryParams: {
                  logid: context.reviewInfo.logId,
                  itemid: context.reviewInfo.appReviewId,
                },
              });
            }
          } else {
            // console.log(context);
            // this._applicationDataService.setAppTypeId(
            //   context.applicationTypeId
            // );
            // this.router.navigate([`/app/new-application/provide-information`]);
            this.router.navigate([`/app/new-application/provide-information`], {
              queryParams: {
                appId: context.id,
                appTypeId: context.applicationTypeId,
                listingTypeId: context.listingTypeId,
              },
            });
          }
        },
      },
      {
        label: "Application Version",
        command: () => {
          this.showVersionDialog(context);
          // console.log("assign clicked");
        },
      },
      {
        label: "Download",
        command: () => {
          // console.log("Download clicked", context);
          this.selectedAppTypeId = context.applicationTypeId;
          this.selectedAppId = context.id;
          this.selectedListingTypeId = context.listingTypeId;
          this.printAppDialog = true;
          // setTimeout(() => {
          //   this.printAppDialog = false;
          // }, 3000);
        },
      },
      {
        label: 'Transaction details', visible: this.isGranted('CreateApplication'), command: () => {
          console.log(context);
          this.router.navigate(['/app/invoice'], {
            queryParams: {
              ref: context.transactionRef
            },
          });
        }
      },
    ];

    if (this._sessionService.user.userRole === "Listings Analysis Team Lead") {
      menu.push(
        {
          label: "Update Status",
          command: () => {
            this.updatetatusOptions = [
              { label: "suspended", value: "suspended" },
              { label: "withdrawn", value: "withdrawn" },
              { label: "delisted", value: "delisted" },
              { label: "listed", value: "listed" },
            ];
            // console.log(
            //   this._sessionService.user.userRole === "Listings Analysis Team Lead"
            // );
            if (
              this._sessionService.user.userRole ===
              "Listings Analysis Team Lead"
            ) {
              this.selectedAppStatus = context.applicationStatusText;
              this.selectedAppId = context.id;
              this.displayApplicationStatusDialog = !this
                .displayApplicationStatusDialog;
              // console.log("Status clicked", context, this.selectedAppStatus);
              this.ref.detectChanges();
            } else {
              abp.notify.error(
                "You are not authorized to Update An Application Status"
              );
            }
          },
        },
        {
          label: context.applicationStatus == 4 ? "Assign" : "Check Status",
          command: (e) => {
            console.log(context);
            if (context.applicationStatus == 4) {
              if (
                this._sessionService.user.userRole ===
                "Listings Analysis Team Lead"
              ) {
                this.toggleAssignDialog(context);
              } else {
                abp.notify.error(
                  "You are not authorized to Assign An Application"
                );
              }
            } else {
              let pendingrole = "";
              if (context.reviewInfo != undefined) {
                pendingrole = context.reviewInfo.pendingRole;
              }
              abp.notify.info(
                "This Application Status Is " +
                context.applicationStatusText.toUpperCase() +
                " " +
                pendingrole
              );
            }
          },
        }
      );
    }
    if (this._sessionService.user.userRole === "Dealing Member Firm Operator" ||
        this._sessionService.user.userRole === "Dealing Member Firm Admin") {
      if(context.applicationStatusText== "denied"){
        menu.push({
          label: "Resume Application",
          command: () => {
            this.router.navigate([`/app/new-application/provide-information`], {
              queryParams: {
                appId: context.id,
                appTypeId: context.applicationTypeId,
                listingTypeId: context.listingTypeId,
              },
            });
          },
        });
      } else if(context.applicationStatusText== "approved" || context.applicationStatusText== "unassigned"|| context.applicationStatusText== "pending"){
        menu.push({
          label: "Upload Documents",
          command: () => {
            //console.log(context);
            this.router.navigate([`/app/documents`], {
              queryParams: {
                appId: context.id,
              },
            });
          },
        });
      }
    }
 
    return menu;
  }

  loadDashboardCount() {
    // console.log(this._sessionService.user);
    this._approvalReviewServiceProxy
      .countApprovalLog(0, this._sessionService.user.id, 2, null, null, null)
      .subscribe((retval: Int32ApiResult) => {
        this.approvedCount = retval.result;
      });

    this._approvalReviewServiceProxy
      .countApprovalLog(0, this._sessionService.user.id, 1, null, null, null)
      .subscribe((retval: Int32ApiResult) => {
        this.pendingCount = retval.result;
      });

    this._approvalReviewServiceProxy
      .countApprovalLog(0, this._sessionService.user.id, 3, null, null, null)
      .subscribe((retval: Int32ApiResult) => {
        this.declinedCount = retval.result;
      });

    this._approvalReviewServiceProxy
      .countApprovalLog(
        0,
        this._sessionService.user.id,
        1,
        null,
        null,
        "Escalated"
      )
      .subscribe((retval: Int32ApiResult) => {
        this.escalationCount = retval.result;
      });
  }
  onDateSelect(value) {
    // this.table.filter(this.formatDate(value), "date", "equals");
    this.table.filter(this.formatDate(value), "dateCreated", "equals");
  }

  formatDate(date) {
    const month = date.getMonth();
    const day = date.getDate();

    return this.months[month] + " " + day + " " + date.getFullYear();
  }

  toggleDialog() {
    this.display = !this.display;
  }

  toggleAssignDialog(data?) {
    // console.log(data);
    this.selectedAppId = data.id;
    this.selectedAppTypeId = data.applicationTypeId;
    this.displayAssign = !this.displayAssign;
  }


  showVersionDialog(data) {
    // console.log(data);
    // this.applicationData = data;
    this.getApplicationVersions(data.appVersionRef);
  }


  getApplicationVersions(ref): void {
    this.ngxService.start();
    this._applicationService
      .getApplicationVersions(ref)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
          this.displayVersion = !this.displayVersion;
        })
      )
      .subscribe(
        (data) => {
          // console.log(data[0].versionNo);
          this.applicationVersion = data[0];
          // // this.payload.id = id;
          // this._applicationDataService.setCreateAppData(this.payload);
        },
        (error) => {
          console.log(error);
          this.ngxService.stop();
        }
      );
  }

  getApplications(
    pageNo?: number | null | undefined,
    pageSize?: number | null | undefined,
    searchText?: string | ""
  ) {
    pageNo = pageNo ?? 1;
    pageSize = pageSize ?? 20;
    this.ngxService.start();

    this._applicationService
      .getAllSummary(pageNo, pageSize, searchText)
      .pipe(
        finalize(() => {
          this.ref.detectChanges();
          this.ngxService.stop();
        })
      )
      .subscribe(
        (res: any) => {
          this.summary = res;
          this._summary = res;
          // console.log(res);
        },
        (error) => {
          console.log(error);
          this.ngxService.stop();
        }
      );
  }
  updateApplicationStatus() {
    this.ngxService.start();
    // console.log(this.selectedAppStatus);
    if (typeof this.selectedAppStatus == "string") {
      this.selectedAppStatus = this.selectedAppStatus;
    } else if (typeof this.selectedAppStatus === "object") {
      this.selectedAppStatus = this.selectedAppStatus.label;
    }
    // console.log(this.selectedAppStatus);

    this._approvalReviewServiceProxy
      .updateApplicationStatus(this.selectedAppId, this.selectedAppStatus)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(
        (res: any) => {
          // console.log(res);
          this.getApplications();
          this.displayApplicationStatusDialog = false;
        },
        (error) => {
          console.log(error);
          this.ngxService.stop();
        }
      );
  }

  ngOnInit(): void {
    this.organization = this._sessionService.user.organization?.name;

    //this.applicationStatus = this.route.snapshot.queryParamMap.get("applicationStatus");

    this.getApplications();

    this.loadDashboardCount();
  }
}
