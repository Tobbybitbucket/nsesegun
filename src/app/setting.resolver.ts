import { EmailMessageServiceProxy } from './../shared/service-proxies/service-proxies';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
@Injectable({
    providedIn: 'root'
})

export class EmailMessageViewResolver implements Resolve<any> {
    constructor(private _emailMessageService: EmailMessageServiceProxy) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this._emailMessageService.getAllEmailMessages();
    }
}
@Injectable({
    providedIn: 'root'
})
export class SentEmailMessageViewResolver implements Resolve<any> {
    constructor(private _emailMessageService: EmailMessageServiceProxy) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this._emailMessageService.getAllSentEmailMessages();
    }
}
