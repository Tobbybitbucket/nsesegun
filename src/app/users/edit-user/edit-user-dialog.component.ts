import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  UserServiceProxy,
  UserDto,
  RoleDto
} from '@shared/service-proxies/service-proxies';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  templateUrl: './edit-user-dialog.component.html'
})
export class EditUserDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  user = new UserDto();
  roles: RoleDto[] = [];
  checkedRolesMap: { [key: string]: boolean } = {};
  id: number;
  organizationId: number;
  selectedRole: RoleDto;
  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _userService: UserServiceProxy,
    private ngxService: NgxUiLoaderService,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    console.log(this.organizationId);
    this._userService.get(this.id).subscribe((result) => {
      this.user = result;
    });
    this.GetRolesForOrganization();
  }
  GetRolesForOrganization(): void {
    this.ngxService.start();
    this._userService.getRolesForOrganization(this.organizationId).subscribe((result) => {
      this.roles = result;
      this.selectedRole = new RoleDto();
      this.selectedRole.id = this.user.roleId;
      this.ngxService.stop();
    });
  }

  onChange(event): void {
    console.log(this.selectedRole);
  }

  save(): void {
    this.saving = true;

    this.user.roleId = this.selectedRole.id;

    this._userService
      .update(this.user)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
