import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  UserDto,
  UserServiceProxy
} from '@shared/service-proxies/service-proxies';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Table } from 'primeng/table';
import { BehaviorSubject } from 'rxjs';
import { request } from 'http';
import { AppComponentBase } from '@shared/app-component-base';
import { ActivatedRoute } from '@angular/router';
import { CreateUserDialogComponent } from '../create-user/create-user-dialog.component';
import { EditUserDialogComponent } from '../edit-user/edit-user-dialog.component';

@Component({
  templateUrl: './participant-users.component.html',
  animations: [appModuleAnimation()]
})

export class ParticipantUsersComponent extends AppComponentBase implements OnInit {
  @ViewChild('dt') table: Table;
  organizationId: number;
  organizationName: string;
  organizationTypeName: string;
  users: UserDto[] = [];
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;
  isTableLoading = false;
  selectedUser$: BehaviorSubject<UserDto>;

  cols = [
    { field: 'sn', header: 'S/N' },
    { field: 'organization', header: 'Organization' },
    { field: 'fullName', header: 'Full Name' },
    { field: 'email', header: 'Email' },
    { field: 'phone', header: 'Phone' },
    { field: 'role', header: 'Role' },
    { field: 'createdOn', header: 'Created On' },
    { field: 'isActive', header: 'Is active' },
    { field: 'action', header: 'Action' }
  ];

  buttonItems = [
    {
      label: 'Edit', visible: this.isGranted('EditUser'), command: () => {
        this.editUser(this.selectedUser$.value);
      }
    },
    {
      label: 'Delete', visible: this.isGranted('DeleteUser'), command: () => {
        this.delete(this.selectedUser$.value);
      }
    }
  ];

  constructor(
    injector: Injector,
    private _userService: UserServiceProxy,
    private ngxService: NgxUiLoaderService,
    private route: ActivatedRoute,
    private _modalService: BsModalService
  ) {
    super(injector);
    this.selectedUser$ = new BehaviorSubject(new UserDto());
  }

  setSelectedUser(user: UserDto): void {
    this.selectedUser$.next(user);
  }

  createUser(): void {
    this.showCreateOrEditUserDialog();
  }

  editUser(user: UserDto): void {
    this.showCreateOrEditUserDialog(user.id);
  }

  ngOnInit() {
    this.ngxService.start();
    this.route.paramMap.subscribe(x => {
      this.organizationId = +x.get('organizationId');
    });

    this.organizationName = this.route.snapshot.queryParamMap.get('organizationName');
    this.organizationTypeName = this.route.snapshot.queryParamMap.get('organizationTypeName');
    this.GetAllUsersForOrganization();
  }
  GetAllUsersForOrganization(): void {
    this.isTableLoading = true;
    this._userService.getAllUsersForOrganization(this.organizationId).subscribe((result: UserDto[]) => {
      this.users = result;
      this.isTableLoading = false;
    });
  }
  protected delete(user: UserDto): void {
    abp.message.confirm(
      `${user.name} will be deleted`,
      undefined,
      (result: boolean) => {
        if (result) {
          this._userService.delete(user.id).subscribe(() => {
            abp.notify.success(this.l('SuccessfullyDeleted'));
            this.GetAllUsersForOrganization();
          });
        }
      }
    );
  }


  private showCreateOrEditUserDialog(id?: number): void {
    let createOrEditUserDialog: BsModalRef;
    if (!id) {
      createOrEditUserDialog = this._modalService.show(
        CreateUserDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
          initialState: {
            organizationId: this.organizationId,
            organizationName: this.organizationName,
            organizationTypeName: this.organizationTypeName,
          },
        }
      );
    } else {
      createOrEditUserDialog = this._modalService.show(
        EditUserDialogComponent,
        {
          backdrop: 'static',
          keyboard: false,
          animated: true,
          ignoreBackdropClick: true,
          class: 'modal-lg',
          initialState: {
            id: id,
            organizationId: this.organizationId,
            organizationName: this.organizationName,
            organizationTypeName: this.organizationTypeName,
          },
        }
      );
    }
    createOrEditUserDialog.content.onSave.subscribe(() => {
      this.GetAllUsersForOrganization();
    });
  }
}
