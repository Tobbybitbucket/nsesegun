import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  UserServiceProxy,
  OrganizationDto,
  OrganizationServiceProxy,
  OrganizationDtoListApiResult
} from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { BehaviorSubject } from 'rxjs';
import { request } from 'http';
import { AppComponentBase } from '@shared/app-component-base';
import { ViewOrganizationDialogComponent } from '@app/nse-admin/setup/organizations/view-organization/view-organization-dialog.component';
import { EditOrganizationDialogComponent } from '@app/nse-admin/setup/organizations/edit-organization/edit-organization-dialog.component';
import { CreateParticipantUserDialogComponent } from './create-participant-user/create-participant-user-dialog.component';

@Component({
  templateUrl: './users.component.html',
  animations: [appModuleAnimation()]
})

export class UsersComponent extends AppComponentBase implements OnInit {
  @ViewChild('dt') table: Table;
  organizations: OrganizationDto[] = [];
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;
  isTableLoading = false;
  selectedOrganization$: BehaviorSubject<OrganizationDto>;

  cols = [
    { field: 'sn', header: 'S/N' },
    { field: 'organization', header: 'Organization' },
    { field: 'type', header: 'Type' },
    { field: 'email', header: 'Email' },
    { field: 'users', header: 'User count' },
    { field: 'isActive', header: 'Is active' },
    { field: 'action', header: 'Action' }
  ];

  buttonItems = [
    {
      label: 'Edit', visible: this.isGranted('EditOrganization'), command: () => {
        this.editOrganization(this.selectedOrganization$.value);
      }
    },
    {
      label: 'Delete', visible: this.isGranted('DeleteOrganization'), command: () => {
        this.delete(this.selectedOrganization$.value);
      }
    },
    {
      label: 'View', visible: this.isGranted('ViewOrganization'), command: () => {
        this.viewOrganization(this.selectedOrganization$.value);
      }
    }
  ];

  constructor(
    injector: Injector,
    private _userService: UserServiceProxy,
    private _organizationService: OrganizationServiceProxy,
    public _modalService: BsModalService
  ) {
    super(injector);
    this.selectedOrganization$ = new BehaviorSubject(new OrganizationDto());
  }

  setSelectedOrganization(organization: OrganizationDto): void {
    this.selectedOrganization$.next(organization);
  }

  createUser(): void {
    this.showCreateParticipantUserDialog();
  }

  editOrganization(organization: OrganizationDto): void {
    this.showEditOrganizationDialog(organization.id);
  }

  viewOrganization(organization: OrganizationDto): void {
    this.showViewOrganizationDialog(organization.id);
  }

  ngOnInit() {
    this.getAllOrganizationUsers();
  }
  getAllOrganizationUsers(): void {
    this.isTableLoading = true;
    this._organizationService.getAllOrganizationUsers().subscribe((result: OrganizationDtoListApiResult) => {
      this.organizations = result.result;
      this.isTableLoading = false;
    });
  }
  protected delete(organization: OrganizationDto): void {
    abp.message.confirm(
      `${organization.name} will be deleted`,
      undefined,
      (result: boolean) => {
        if (result) {
          this._organizationService.deleteOrganization(organization.id).subscribe(() => {
            abp.notify.success(this.l('SuccessfullyDeleted'));
            this.getAllOrganizationUsers();
          });
        }
      }
    );
  }

  private showViewOrganizationDialog(id?: number): void {
    const viewOrganizationDialog = this._modalService.show(
      ViewOrganizationDialogComponent,
      {
        backdrop: 'static',
        keyboard: false,
        animated: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
        initialState: {
          id: id,
        },
      }
    );
  }

  private showCreateParticipantUserDialog(): void {
    let createParticipantUser: BsModalRef;
    createParticipantUser = this._modalService.show(
      CreateParticipantUserDialogComponent,
      {
        backdrop: 'static',
        keyboard: false,
        animated: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
      }
    );
    console.log(createParticipantUser);
    createParticipantUser.content.onSave.subscribe(() => {
      this.getAllOrganizationUsers();
    });
  }


  private showEditOrganizationDialog(id?: number): void {
    let editOrganizationDialog: BsModalRef;
    editOrganizationDialog = this._modalService.show(
      EditOrganizationDialogComponent,
      {
        backdrop: 'static',
        keyboard: false,
        animated: true,
        ignoreBackdropClick: true,
        class: 'modal-lg',
        initialState: {
          id: id,
        },
      }
    );
    editOrganizationDialog.content.onSave.subscribe(() => {
      this.getAllOrganizationUsers();
    });
  }
}
