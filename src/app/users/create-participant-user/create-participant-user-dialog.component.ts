import { Organization } from './../../../shared/service-proxies/service-proxies';
import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  UserServiceProxy,
  CreateUserDto,
  RoleDto,
  OrganizationServiceProxy,
  EnumModel,
  OrganizationDto
} from '@shared/service-proxies/service-proxies';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  templateUrl: './create-participant-user-dialog.component.html'
})
export class CreateParticipantUserDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  createAsDomainUserChecked = true;
  organizationId: number;
  userCreationlabel: string;
  selectedRole: RoleDto;
  user = new CreateUserDto();
  roles: RoleDto[] = [];
  organizationTypes: EnumModel[];
  selectedOrganizationType: EnumModel;
  selectedOrganization: OrganizationDto;
  organizations: OrganizationDto[] = [];
  @Output() onSave = new EventEmitter<any>();
  userTypePlaceholder: string;

  constructor(
    injector: Injector,
    public _userService: UserServiceProxy,
    public _organizationService: OrganizationServiceProxy,
    private ngxService: NgxUiLoaderService,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.user.isActive = true;
    this.user.createAsDomainUser = true;
    this.userCreationlabel = 'Validate user';
    this.userTypePlaceholder = 'Supply a valid email';
    this.getOrganizationTypes();
  }
  getOrganizationTypes(): void {
    this.ngxService.start();
    this._organizationService.getOrganizationTypes()
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(result => {
        this.organizationTypes = result.result;
      });
  }
  getOrganizationByType(): void {
    this.ngxService.start();
    this._organizationService.getOrganizationByType(this.selectedOrganizationType)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(result => {
        this.organizations = result.result;
      });
  }
  onOrganizationTypeChange(event): void {
    this.selectedOrganizationType = event.value;
    // if (this.selectedOrganizationType.name === 'Nse') {
    //   this.userCreationlabel = 'Validate against AD';
    //   this.userTypePlaceholder = 'AD login Email';
    // } else {
    //   this.userCreationlabel = 'Validate against X-Boss';
    //   this.userTypePlaceholder = 'X-Boss login Email';
    // }
    if (this.createAsDomainUserChecked) {
      if (this.selectedOrganizationType.name === 'Nse') {
        this.userCreationlabel = 'Validate against AD';
        this.userTypePlaceholder = 'AD login email';
      } else {
        this.userCreationlabel = 'Validate against X-Boss';
        this.userTypePlaceholder = 'X-Boss login email';
      }
    } else {
      this.userTypePlaceholder = 'Supply a valid email';
    }
    this.getOrganizationByType();
    console.log(this.selectedOrganizationType);
  }

  getRolesForOrganization(): void {
    this.ngxService.start();
    this._userService.getRolesForOrganization(this.organizationId).subscribe((result) => {
      this.roles = result;
      this.ngxService.stop();
    });
  }

  onOrganizationChange(event): void {
    this.organizationId = event.value.id;
    this.getRolesForOrganization();
    console.log(this.selectedOrganizationType);
  }

  onRoleChange(event): void {
    console.log(this.selectedRole);
  }
  createAsDomainUserChange(event) {
    console.log(event.checked);
    this.createAsDomainUserChecked = event.checked;
    if (this.createAsDomainUserChecked && this.selectedOrganizationType) {
      if (this.selectedOrganizationType.name === 'Nse') {
        this.userCreationlabel = 'Validate against AD';
        this.userTypePlaceholder = 'AD login email';
      } else {
        this.userCreationlabel = 'Validate against X-Boss';
        this.userTypePlaceholder = 'X-Boss login email';
      }
    } else {
      this.userTypePlaceholder = 'Supply a valid email';
    }
  }
  save(): void {
    this.saving = true;
    this.user.roleId = this.selectedRole.id;
    this.user.organizationId = this.selectedOrganization.id;

    this._userService
      .create(this.user)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
