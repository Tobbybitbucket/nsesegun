import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  UserServiceProxy,
  CreateUserDto,
  RoleDto
} from '@shared/service-proxies/service-proxies';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  templateUrl: './create-user-dialog.component.html'
})
export class CreateUserDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  organizationId: number;
  organizationName: string;
  organizationTypeName: string;
  selectedRole: RoleDto;
  user = new CreateUserDto();
  roles: RoleDto[] = [];
  userCreationlabel: string;
  userTypePlaceholder: string;
  @Output() onSave = new EventEmitter<any>();


  constructor(
    injector: Injector,
    public _userService: UserServiceProxy,
    private ngxService: NgxUiLoaderService,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.user.isActive = true;
    this.user.createAsDomainUser = true;
    if (this.organizationTypeName === 'Nse') {
      this.userCreationlabel = 'Validate against AD';
      this.userTypePlaceholder = 'AD login email';
    } else {
      this.userCreationlabel = 'Validate against X-Boss';
      this.userTypePlaceholder = 'X-Boss login email';
    }
    this.GetRolesForOrganization();
  }
  createAsDomainUserChange(event) {
    console.log(event.checked);
    if (event.checked) {
      this.userTypePlaceholder = 'X-Boss login email';
    } else {
      this.userTypePlaceholder = 'Supply a valid email';
    }
  }
  GetRolesForOrganization(): void {
    this.ngxService.start();
    this._userService.getRolesForOrganization(this.organizationId).subscribe((result) => {
      this.roles = result;
      this.ngxService.stop();
    });
  }

  onChange(event): void {
    console.log(this.selectedRole);
  }

  save(): void {
    this.saving = true;

    this.user.roleId = this.selectedRole.id;
    this.user.organizationId = this.organizationId;

    this._userService
      .create(this.user)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
