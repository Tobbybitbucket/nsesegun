import { Component, OnInit, Injector } from '@angular/core';
import { FeesCalculationServiceProxy, ApplicationServiceProxy, FeeCalculatorRequestModel, NameIdModel } from '@shared/service-proxies/service-proxies';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-payment-calculator',
  templateUrl: './payment-calculator.component.html',
  styleUrls: ['./payment-calculator.component.css']
})
export class PaymentCalculatorComponent implements OnInit {
  amount: number;
  payNow = false;
  enablePay = false;
  paymentModel = new FeeCalculatorRequestModel();
  selectedBoardType: NameIdModel;
  boardTypes: NameIdModel[];
  constructor(
    private _feesCalculationService: FeesCalculationServiceProxy,
    private _applicationService: ApplicationServiceProxy,
    private ngxService: NgxUiLoaderService,
    private router: Router) { }

  ngOnInit(): void {
    this.getBoardTypes();
  }
  getBoardTypes(): void {
    this.ngxService.start();
    this._applicationService.getBoardTypes()
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      ).subscribe((result) => {
        this.boardTypes = result;
      });
  }
  onChange(event): void {
    console.log(this.selectedBoardType);
    this.viewChange();
  }
  numberOfSharesChange(event): void {
    this.paymentModel.numberOfShares = event;
    this.viewChange();
  }
  priceChange(event): void {
    this.paymentModel.price = event;
    this.viewChange();
  }
  viewChange() {
    console.log(this.paymentModel);
    if (
      this.selectedBoardType === undefined ||
      this.paymentModel.numberOfShares === undefined ||
      this.paymentModel.price === undefined ||
      this.selectedBoardType === null ||
      this.paymentModel.numberOfShares === null ||
      this.paymentModel.price === null
    ) {
      this.enablePay = false;
    } else {
      this.enablePay = true;
    }
  }
  computeApplicationFee() {
    this.ngxService.start();
    this.paymentModel.boardType = this.selectedBoardType.name;
    console.log(this.paymentModel);
    this._feesCalculationService
      .computeApplicationFee(this.paymentModel)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe((result) => {
        this.amount = result.amount;
      });
  }
}
