import { UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { UtilityServiceProxyService } from './../../shared/service-proxies/utility-service-proxies';
import { HttpErrorResponse } from '@angular/common/http';
import { TitleServiceProxy, TitleDto, UserDto, OrganizationDto, TokenAuthServiceProxy } from './../../shared/service-proxies/service-proxies';
import { Component, OnInit, Injector, ChangeDetectionStrategy } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppSessionService } from '@shared/session/app-session.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { finalize } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Component({
    templateUrl: './profile.component.html',
    animations: [appModuleAnimation()],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileComponent extends AppComponentBase implements OnInit {
    user = new UserDto();
    titles: TitleDto[];
    selectedTitle: TitleDto;
    submitting = false;
    picForm = false;
    fileExt = 'JPG, GIF, PNG';
    maxSize = 10;
    errors: Array<string> = [];
    formData: FormData = new FormData();
    userImage$: BehaviorSubject<string>;
    constructor(injector: Injector, private _sessionService: AppSessionService,
        private _titleService: TitleServiceProxy, private _userService: UserServiceProxy,
        private _utilityService: UtilityServiceProxyService,
        private ngxService: NgxUiLoaderService) {
        super(injector);
        this.userImage$ = new BehaviorSubject('assets/img/user.png');
    }

    ngOnInit(): void {
        this.getTitles();
        console.log(this._sessionService);
    }

    getTitles(): void {
        this.ngxService.start();
        this.user.organization = this._sessionService.user.organization;
        this._titleService.getAllTitlesList()
            .pipe(
                finalize(() => {
                    this.ngxService.stop();
                })
            )
            .subscribe(result => {
                this.titles = result;
                this.user.name = this._sessionService.user.name;
                this.user.emailAddress = this._sessionService.user.emailAddress;
                this.user.userName = this._sessionService.user.userName;
                this.user.surname = this._sessionService.user.surname;
                this.user.roleName = this._sessionService.user.userRole;
                this.user.organizationName = this._sessionService.user.organization?.name;
                if (this._sessionService.user.title != null) {
                    this.selectedTitle = this._sessionService.user.title;
                }
                if (this._sessionService.user.imagePath != null && this._sessionService.user.imagePath !== '') {
                    this.userImage$ = new BehaviorSubject(this._sessionService.user.imagePath);
                } else {

                }
            });
    }
    save(): void {
        this.submitting = true;
        this.user.titleId = this.selectedTitle.id;
        this.user.title = this.selectedTitle;
        this._userService.updateProfile(this.user)
            .pipe(
                finalize(() => {
                    this.submitting = false;
                    this.ngxService.stop();
                })
            )
            .subscribe(x => {
                if (!x.hasError) {
                    abp.message.success(x.message, 'profile successfully updated');
                    this.notify.info(x.message);
                }
            });
    }
    changeProfilePicture() {
        this.ngxService.start();
        this._utilityService.updateProfilePicture(this.formData).pipe(
            finalize(() => {
                this.ngxService.stop();
            })
        ).subscribe(x => {
            if (!x.hasError) {
                abp.message.success(x.message, 'profile picture successfully updated');
            }
        },
            (err: HttpErrorResponse) => {
                if (err.status === 401 || err.status === 403 || err.status === 500 || err.status === 400) {
                    abp.message.error(err.error || err.statusText, 'Status');
                }
            }
        );
    }
    onChange(event): void {
        console.log(this.selectedTitle);
    }
    onFileChange(event) {
        this.errors = [];
        this.formData = new FormData();
        console.log(event);
        const files = event.target.files;
        if (files.length > 0 && (!this.isValidFiles(files))) {
            return;
        }

        if (files.length > 0) {
            this.formData.append('ProfilePicture', files[0], files[0].name);
            this.picForm = true;
            const file: File = files[0];
            const reader = new FileReader();
            reader.addEventListener('load', (e: any) => {
                this.userImage$.next(e.target.result);
            });
            reader.readAsDataURL(file);
        }
    }

    private isValidFiles(files) {
        if (files.length > 1) {
            this.errors.push('Error: At a time you can upload only one file');
            return;
        }
        this.isValidFileExtension(files);
        return this.errors.length === 0;
    }
    private isValidFileExtension(files) {
        const extensions = (this.fileExt.split(',')).map(
            function (x) {
                return x.toLocaleUpperCase().trim();
            });

        const ext = files[0].name.toUpperCase().split('.').pop() || files[0].name;
        const exists = extensions.includes(ext);
        if (!exists) {
            this.errors.push('Error (Extension): ' + files[0].name);
        }
        this.isValidFileSize(files[0]);

    }

    private isValidFileSize(file) {
        const fileSizeinMB = file.size / (1024 * 1000);
        const size = Math.round(fileSizeinMB * 100) / 100;
        if (size > this.maxSize) {
            this.errors.push('Error (File Size): ' + file.name + ': exceed file size limit of ' + this.maxSize + 'MB ( ' + size + 'MB )');
        }
    }
}
