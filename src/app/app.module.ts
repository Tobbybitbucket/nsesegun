import { ConfirmPaymentComponent } from './payments/confirm-payment/confirm-payment.component';
import { UtilityServiceProxyService } from './../shared/service-proxies/utility-service-proxies';
import { CreateOrganizationUserDialogComponent } from './users/create-organization-user/create-organization-user-dialog.component';
import { CreateParticipantUserDialogComponent } from './users/create-participant-user/create-participant-user-dialog.component';
import { FormSectionDialogComponent } from './workflow/form-section-dialog/form-section-dialog.component';
import { DocTemplateItemDialogComponent } from './workflow/doc-template-item-dialog/doc-template-item-dialog.component';
import { ProcessItemDialogComponent } from './workflow/process-item-dialog/process-item-dialog.component';
import { StepItemDialogComponent } from './workflow/step-item-dialog/step-item-dialog.component';
import { CreateEmailMessageDialogComponent } from './email-messages/create-email-message-dialog/create-email-message-dialog.component';
import { ForwardEmailMessageDialogComponent } from './email-messages/forward-email-message-dialog/forward-email-message-dialog.component';
import { ReplyEmailMessageDialogComponent } from './email-messages/reply-email-message-dialog/reply-email-message-dialog.component';
import { EmptyStateComponent } from './../shared/components/empty-state/empty-state.component';
import { CommentsComponent } from './workspace/comments/comments.component';
import { UtilityService } from '@shared/helpers/Utils';
// Application Status
import { ApplicationStatusesComponent } from './nse-admin/setup/application-statuses/application-statuses.component';
import { ViewApplicationStatusDialogComponent } from './nse-admin/setup/application-statuses/view-application-status/view-application-status-dialog.component';
import { CreateApplicationStatusDialogComponent } from './nse-admin/setup/application-statuses/create-application-status/create-application-status-dialog.component';
import { EditApplicationStatusDialogComponent } from './nse-admin/setup/application-statuses/edit-application-status/edit-application-status-dialog.component';
// Application Type
import { ApplicationTypesComponent } from './nse-admin/setup/application-types/application-types.component';
import { ViewApplicationTypeDialogComponent } from './nse-admin/setup/application-types/view-application-type/view-application-type-dialog.component';
import { CreateApplicationTypeDialogComponent } from './nse-admin/setup/application-types/create-application-type/create-application-type-dialog.component';
import { EditApplicationTypeDialogComponent } from './nse-admin/setup/application-types/edit-application-type/edit-application-type-dialog.component';
// Country
import { CountriesComponent } from './nse-admin/setup/countries/countries.component';
import { CreateCountryDialogComponent } from './nse-admin/setup/countries/create-country/create-country-dialog.component';
import { EditCountryDialogComponent } from './nse-admin/setup/countries/edit-country/edit-country-dialog.component';
import { ViewCountryDialogComponent } from './nse-admin/setup/countries/view-country/view-country-dialog.component';
// Email Setup
import { EmailSetupsComponent } from './nse-admin/setup/email-setups/email-setups.component';
import { CreateEmailSetupDialogComponent } from './nse-admin/setup/email-setups/create-email-setup/create-email-setup-dialog.component';
import { EditEmailSetupDialogComponent } from './nse-admin/setup/email-setups/edit-email-setup/edit-email-setup-dialog.component';
import { ViewEmailSetupDialogComponent } from './nse-admin/setup/email-setups/view-email-setup/view-email-setup-dialog.component';
// Gender
import { GendersComponent } from './nse-admin/setup/genders/genders.component';
import { CreateGenderDialogComponent } from './nse-admin/setup/genders/create-gender/create-gender-dialog.component';
import { EditGenderDialogComponent } from './nse-admin/setup/genders/edit-gender/edit-gender-dialog.component';
import { ViewGenderDialogComponent } from './nse-admin/setup/genders/view-gender/view-gender-dialog.component';
// Organization
import { OrganizationsComponent } from './nse-admin/setup/organizations/organizations.component';
import {
  CreateOrganizationDialogComponent
} from './nse-admin/setup/organizations/create-organization/create-organization-dialog.component';
import { EditOrganizationDialogComponent } from './nse-admin/setup/organizations/edit-organization/edit-organization-dialog.component';
import { ViewOrganizationDialogComponent } from './nse-admin/setup/organizations/view-organization/view-organization-dialog.component';
// Issuing Company
import { IssuingCompaniesComponent } from './nse-admin/setup/issuing-companies/issuing-companies.component';
import { CreateIssuingCompanyDialogComponent } from './nse-admin/setup/issuing-companies/create-issuing-company/create-issuing-company-dialog.component';
import { EditIssuingCompanyDialogComponent } from './nse-admin/setup/issuing-companies/edit-issuing-company/edit-issuing-company-dialog.component';
import { ViewIssuingCompanyDialogComponent } from './nse-admin/setup/issuing-companies/view-issuing-company/view-issuing-company-dialog.component';
// Lga
import { LgasComponent } from './nse-admin/setup/lgas/lgas.component';
import { CreateLgaDialogComponent } from './nse-admin/setup/lgas/create-lga/create-lga-dialog.component';
import { EditLgaDialogComponent } from './nse-admin/setup/lgas/edit-lga/edit-lga-dialog.component';
import { ViewLgaDialogComponent } from './nse-admin/setup/lgas/view-lga/view-lga-dialog.component';
// Listing Type
import { ListingTypesComponent } from './nse-admin/setup/listing-types/listing-types.component';
import { CreateListingTypeDialogComponent } from './nse-admin/setup/listing-types/create-listing-type/create-listing-type-dialog.component';
import { EditListingTypeDialogComponent } from './nse-admin/setup/listing-types/edit-listing-type/edit-listing-type-dialog.component';
import { ViewListingTypeDialogComponent } from './nse-admin/setup/listing-types/view-listing-type/view-listing-type-dialog.component';

// Fees Calculation
import { FeesCalculationsComponent } from './nse-admin/setup/fees-calculations/fees-calculations.component';
import { CreateFeesCalculationDialogComponent } from './nse-admin/setup/fees-calculations/create-fees-calculation/create-fees-calculation-dialog.component';
import { EditFeesCalculationDialogComponent } from './nse-admin/setup/fees-calculations/edit-fees-calculation/edit-fees-calculation-dialog.component';
import { ViewFeesCalculationDialogComponent } from './nse-admin/setup/fees-calculations/view-fees-calculation/view-fees-calculation-dialog.component';
// Sms Setup
import { SmsSetupsComponent } from './nse-admin/setup/sms-setups/sms-setups.component';
import { CreateSmsSetupDialogComponent } from './nse-admin/setup/sms-setups/create-sms-setup/create-sms-setup-dialog.component';
import { EditSmsSetupDialogComponent } from './nse-admin/setup/sms-setups/edit-sms-setup/edit-sms-setup-dialog.component';
import { ViewSmsSetupDialogComponent } from './nse-admin/setup/sms-setups/view-sms-setup/view-sms-setup-dialog.component';
// State
import { StatesComponent } from './nse-admin/setup/states/states.component';
import { CreateStateDialogComponent } from './nse-admin/setup/states/create-state/create-state-dialog.component';
import { EditStateDialogComponent } from './nse-admin/setup/states/edit-state/edit-state-dialog.component';
import { ViewStateDialogComponent } from './nse-admin/setup/states/view-state/view-state-dialog.component';
// Title
import { TitlesComponent } from './nse-admin/setup/titles/titles.component';
import { CreateTitleDialogComponent } from './nse-admin/setup/titles/create-title/create-title-dialog.component';
import { EditTitleDialogComponent } from './nse-admin/setup/titles/edit-title/edit-title-dialog.component';
import { ViewTitleDialogComponent } from './nse-admin/setup/titles/view-title/view-title-dialog.component';
// import { BusyIfDirective } from './../shared/utils/busy-if.directive';
// import { AppBsModalModule } from '@shared/common/appBsModal/app-bs-modal.module';
// import { NgModule } from '@angular/core';
// import { CommonModule, CurrencyPipe, DecimalPipe } from '@angular/common';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { HttpClientJsonpModule } from '@angular/common/http';
// import { AngularEditorModule } from '@kolkov/angular-editor';
// import { HttpClientModule } from '@angular/common/http';
// import { ModalModule } from 'ngx-bootstrap/modal';
// import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
// import { CollapseModule } from 'ngx-bootstrap/collapse';
// import { TabsModule } from 'ngx-bootstrap/tabs';
// import { NgxPaginationModule } from 'ngx-pagination';
// import { AppRoutingModule } from './app-routing.module';
// import { PaginatorModule } from 'primeng/paginator';
// import { TableModule } from 'primeng/table';
// import { ToggleButtonModule } from 'primeng/togglebutton';
// import { CardModule } from 'primeng/card';
// import { SplitButtonModule } from 'primeng/splitbutton';
// import { ButtonModule } from 'primeng/button';
// import { AccordionModule } from 'primeng/accordion';
// import { DialogModule } from 'primeng/dialog';
// import { ProgressSpinnerModule } from 'primeng/progressspinner';
// import { EditorModule } from 'primeng/editor';
// import { DropdownModule } from 'primeng/dropdown';
// import { TabViewModule } from 'primeng/tabview';
// import { InputTextModule } from 'primeng/inputtext';
// import { FileUploadModule } from 'primeng/fileupload';
// import { CalendarModule } from 'primeng/calendar';
// import { SidebarModule } from 'primeng/sidebar';
// import { MultiSelectModule } from 'primeng/multiselect';
// import { ScrollSpyDirective } from '../shared/directives/scrollspy.directive';
// import { SelectButtonModule } from 'primeng/selectbutton';
// import { InputSwitchModule } from 'primeng/inputswitch';
// import { RadioButtonModule } from 'primeng/radiobutton';
// import { InputTextareaModule } from 'primeng/inputtextarea';
// import { InputNumberModule } from 'primeng/inputnumber';
// import { InputMaskModule } from 'primeng/inputmask';
// import { ChartModule } from 'primeng/chart';
// import { ChipsModule } from 'primeng/chips';
// import { SliderModule } from 'primeng/slider';
// import { CheckboxModule } from 'primeng/checkbox';
// import { StepsModule } from 'primeng/steps';
// import { InplaceModule } from 'primeng/inplace';
// import { AppComponent } from './app.component';
// import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
// import { SharedModule } from '@shared/shared.module';
// import { HomeComponent } from '@app/home/home.component';
// import { AboutComponent } from '@app/about/about.component';
// import { DocumentChecklistComponent } from './application/document-checklist/document-checklist.component';
import { AppBsModalModule } from '@shared/common/appBsModal/app-bs-modal.module';
import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule } from '@angular/common/http';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgxPaginationModule } from 'ngx-pagination';
import { AppRoutingModule } from './app-routing.module';
import { PaginatorModule } from 'primeng/paginator';
import { TableModule } from 'primeng/table';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { CardModule } from 'primeng/card';
import { SplitButtonModule } from 'primeng/splitbutton';
import { ButtonModule } from 'primeng/button';
import { AccordionModule } from 'primeng/accordion';
import { DialogModule } from 'primeng/dialog';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { EditorModule } from 'primeng/editor';
import { DropdownModule } from 'primeng/dropdown';
import { TabViewModule } from 'primeng/tabview';
import { InputTextModule } from 'primeng/inputtext';
import { FileUploadModule } from 'primeng/fileupload';
import { CalendarModule } from 'primeng/calendar';
import { SidebarModule } from 'primeng/sidebar';
import { MultiSelectModule } from 'primeng/multiselect';
import { ScrollSpyDirective } from '../shared/directives/scrollspy.directive';
import { SelectButtonModule } from 'primeng/selectbutton';
import { InputSwitchModule } from 'primeng/inputswitch';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputMaskModule } from 'primeng/inputmask';
import { ChartModule } from 'primeng/chart';
import { ChipsModule } from 'primeng/chips';
import { SliderModule } from 'primeng/slider';
import { CheckboxModule } from 'primeng/checkbox';
import { StepsModule } from 'primeng/steps';
import { InplaceModule } from 'primeng/inplace';
import { TooltipModule } from 'primeng/tooltip';
import { AppComponent } from './app.component';
import { ServiceProxyModule } from '@shared/service-proxies/service-proxy.module';
import { SharedModule } from '@shared/shared.module';
import { HomeComponent } from '@app/home/home.component';
import { AboutComponent } from '@app/about/about.component';
import { DocumentChecklistComponent } from './application/document-checklist/document-checklist.component';

// tenants
import { TenantsComponent } from '@app/tenants/tenants.component';
import { CreateTenantDialogComponent } from './tenants/create-tenant/create-tenant-dialog.component';
import { EditTenantDialogComponent } from './tenants/edit-tenant/edit-tenant-dialog.component';
// roles
import { RolesComponent } from '@app/roles/roles.component';
import { CreateRoleDialogComponent } from './roles/create-role/create-role-dialog.component';
import { EditRoleDialogComponent } from './roles/edit-role/edit-role-dialog.component';
// users
import { UsersComponent } from '@app/users/users.component';
import { CreateUserDialogComponent } from '@app/users/create-user/create-user-dialog.component';
import { EditUserDialogComponent } from '@app/users/edit-user/edit-user-dialog.component';
import { ChangePasswordComponent } from './users/change-password/change-password.component';
import { ResetPasswordDialogComponent } from './users/reset-password/reset-password.component';
// layout
import { HeaderComponent } from './layout/header.component';
import { HeaderModalComponent } from './layout/header-modal.component';
import { HeaderLeftNavbarComponent } from './layout/header-left-navbar.component';
import { HeaderLanguageMenuComponent } from './layout/header-language-menu.component';
import { HeaderUserMenuComponent } from './layout/header-user-menu.component';
import { FooterComponent } from './layout/footer.component';
import { SidebarComponent } from './layout/sidebar.component';
import { SidebarLogoComponent } from './layout/sidebar-logo.component';
import { SidebarUserPanelComponent } from './layout/sidebar-user-panel.component';
import { SidebarMenuComponent } from './layout/sidebar-menu.component';
import { FaqComponent } from './faq/faq.component';
import { EmailMessagesComponent } from './email-messages/email-messages.component';
import { UserGuidesComponent } from './user-guides/user-guides.component';
import { ApplicationComponent } from './application/application.component';
import { ApplicationDialogComponent } from './application/application-dialog/application-dialog.component';
import { AssignApplicationDialogComponent } from './application/assign-application-dialog/assign-application-dialog.component';
import { ProfileComponent } from './profile/profile.component';
import { SettingsComponent } from './settings/settings.component';
import { EquityApplicationComponent } from './workspace/equity-application/equity-application.component';
import { CorporateBondsComponent } from './workspace/corporate-bonds/corporate-bonds.component';
import { StateBondsComponent } from './workspace/state-bonds/state-bonds.component';
import { ProvideInformationComponent } from './application/provide-information/provide-information.component';
import { UploadDocumentationComponent } from './application/upload-documentation/upload-documentation.component';
import { ReviewComponent } from './application/review/review.component';
import { ProcessListComponent } from './workflow/process-list/process-list.component';
import { DocTemplateListComponent } from './workflow/doc-template-list/doc-template-list.component';
import { FormSectionComponent } from './workflow/form-section/form-section.component';
import { NgInitDirective } from './application/provide-information/ng-init.directive';

// services
import {
  RoleServiceProxy,
  ApprovalSetupServiceProxy,
  DocTemplateServiceProxy,
  ApprovalReviewServiceProxy,
  ApplicationLetterServiceProxy,
  ApprovalLogDocServiceProxy,
  ApprovalLogMessageServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { StaticDataService } from '../shared/service-proxies/static-data.service';
import { ParticipantUsersComponent } from './users/participant-users/participant-users.component';
import { OrganizationUsersComponent } from './users/organization-users/organization-users.component';
import { RequiredApplicationDocumentsComponent } from './nse-admin/setup/required-application-documents/required-application-documents.component';
import { CreateRequiredApplicationDocumentDialogComponent } from './nse-admin/setup/required-application-documents/create-required-application-document/create-required-application-document-dialog.component';
import { EditRequiredApplicationDocumentDialogComponent } from './nse-admin/setup/required-application-documents/edit-required-application-document/edit-required-application-document-dialog.component';
import { ViewRequiredApplicationDocumentDialogComponent } from './nse-admin/setup/required-application-documents/view-required-application-document/view-required-application-document-dialog.component';
import { PendingListComponent } from './workflow/pending-list/pending-list.component';
import { PendingItemComponent } from './workflow/pending-item/pending-item.component';
import { ApplicationOverviewComponent } from './application/application-overview/application-overview.component';
import { ViewApplicationComponent } from './view-application/view-application.component';
import { ApplicationLetterComponent } from './workflow/application-letter/application-letter.component';
import { AppVersionDialogComponent } from './application/app-version-dialog/app-version-dialog.component';
import { PendingItemReviewsComponent } from './workflow/pending-item-reviews/pending-item-reviews.component';
import { WorkflowServiceProxyService } from '@shared/service-proxies/workflow-service-proxy.service';
import { InvoiceComponent } from './application-invoice/invoice.component';
import { SentEmailMessagesComponent } from './email-messages/sent-email-messages/sent-email-messages';
import { InvoiceDetailsComponent } from './application-invoice/invoice-details.component';
import { DocumentsComponent } from './documents/documents.component';
import { PaymentCalculatorComponent } from './payment-calculator/payment-calculator/payment-calculator.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    FaqComponent,
    ProfileComponent,
    SettingsComponent,
    UserGuidesComponent,
    // tenants
    TenantsComponent,
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    // roles
    RolesComponent,
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    // users
    UsersComponent,
    ParticipantUsersComponent,
    OrganizationUsersComponent,
    CreateUserDialogComponent,
    CreateOrganizationUserDialogComponent,
    CreateParticipantUserDialogComponent,
    EditUserDialogComponent,
    ChangePasswordComponent,
    ResetPasswordDialogComponent,
    // application
    ApplicationComponent,
    ProvideInformationComponent,
    ApplicationDialogComponent,
    AssignApplicationDialogComponent,
    EquityApplicationComponent,
    CorporateBondsComponent,
    StateBondsComponent,
    UploadDocumentationComponent,
    DocumentChecklistComponent,
    ReviewComponent,
    FormSectionComponent,
    FormSectionDialogComponent,
    StepItemDialogComponent,
    ProcessItemDialogComponent,
    DocTemplateItemDialogComponent,
    EmailMessagesComponent,
    SentEmailMessagesComponent,
    CommentsComponent,
    DocTemplateListComponent,
    EmptyStateComponent,
    // layout
    HeaderComponent,
    HeaderModalComponent,
    HeaderLeftNavbarComponent,
    HeaderLanguageMenuComponent,
    HeaderUserMenuComponent,
    FooterComponent,
    SidebarComponent,
    SidebarLogoComponent,
    SidebarUserPanelComponent,
    SidebarMenuComponent,
    ScrollSpyDirective,
    CreateEmailMessageDialogComponent,
    ForwardEmailMessageDialogComponent,
    ReplyEmailMessageDialogComponent,
    // Application Status
    ApplicationStatusesComponent,
    CreateApplicationStatusDialogComponent,
    EditApplicationStatusDialogComponent,
    ViewApplicationStatusDialogComponent,
    // Application Type
    ApplicationTypesComponent,
    CreateApplicationTypeDialogComponent,
    EditApplicationTypeDialogComponent,
    ViewApplicationTypeDialogComponent,
    // Country
    CountriesComponent,
    CreateCountryDialogComponent,
    EditCountryDialogComponent,
    ViewCountryDialogComponent,
    // Email Setup
    EmailSetupsComponent,
    CreateEmailSetupDialogComponent,
    EditEmailSetupDialogComponent,
    ViewEmailSetupDialogComponent,
    // Gender
    GendersComponent,
    CreateGenderDialogComponent,
    EditGenderDialogComponent,
    ViewGenderDialogComponent,
    // Organization
    OrganizationsComponent,
    CreateOrganizationDialogComponent,
    EditOrganizationDialogComponent,
    ViewOrganizationDialogComponent,
    // Issuing Company
    IssuingCompaniesComponent,
    CreateIssuingCompanyDialogComponent,
    EditIssuingCompanyDialogComponent,
    ViewIssuingCompanyDialogComponent,
    // Lga
    LgasComponent,
    CreateLgaDialogComponent,
    EditLgaDialogComponent,
    ViewLgaDialogComponent,
    // Listing Type
    ListingTypesComponent,
    CreateListingTypeDialogComponent,
    EditListingTypeDialogComponent,
    ViewListingTypeDialogComponent,

    // Fees Calculation
    FeesCalculationsComponent,
    CreateFeesCalculationDialogComponent,
    EditFeesCalculationDialogComponent,
    ViewFeesCalculationDialogComponent,
    // Required Application Document
    RequiredApplicationDocumentsComponent,
    CreateRequiredApplicationDocumentDialogComponent,
    EditRequiredApplicationDocumentDialogComponent,
    ViewRequiredApplicationDocumentDialogComponent,
    // Sms Setup
    SmsSetupsComponent,
    CreateSmsSetupDialogComponent,
    EditSmsSetupDialogComponent,
    ViewSmsSetupDialogComponent,
    // State
    StatesComponent,
    CreateStateDialogComponent,
    EditStateDialogComponent,
    ViewStateDialogComponent,
    // Title
    TitlesComponent,
    CreateTitleDialogComponent,
    EditTitleDialogComponent,
    ViewTitleDialogComponent,
    // BusyIfDirective,
    FormSectionComponent,
    PendingListComponent,
    ProcessListComponent,
    PendingItemComponent,
    FormSectionDialogComponent,
    DocTemplateListComponent,
    DocTemplateItemDialogComponent,
    ApplicationOverviewComponent,
    ViewApplicationComponent,
    NgInitDirective,
    ApplicationLetterComponent,
    AppVersionDialogComponent,
    PendingItemReviewsComponent,
    InvoiceComponent,
    InvoiceDetailsComponent,
    ConfirmPaymentComponent,
    DocumentsComponent,
    PaymentCalculatorComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    ModalModule.forChild(),
    BsDropdownModule,
    CollapseModule,
    TabsModule,
    ToggleButtonModule,
    AppRoutingModule,
    ServiceProxyModule,
    SharedModule,
    NgxPaginationModule,
    CardModule,
    TableModule,
    SplitButtonModule,
    ButtonModule,
    AccordionModule,
    DialogModule,
    ProgressSpinnerModule,
    AngularEditorModule,
    MultiSelectModule,
    DropdownModule,
    TabViewModule,
    InputTextModule,
    FileUploadModule,
    EditorModule,
    CalendarModule,
    SidebarModule,
    SelectButtonModule,
    InputSwitchModule,
    RadioButtonModule,
    SliderModule,
    InputTextareaModule,
    InputNumberModule,
    InputMaskModule,
    CheckboxModule,
    StepsModule,
    ChipsModule,
    ChartModule,
    AppBsModalModule,
    InplaceModule,
    TooltipModule,
  ],
  providers: [
    ApprovalSetupServiceProxy,
    DocTemplateServiceProxy,
    ApprovalReviewServiceProxy,
    RoleServiceProxy,
    ApplicationLetterServiceProxy,
    ApprovalLogDocServiceProxy,
    ApprovalLogMessageServiceProxy,
    StaticDataService,
    CurrencyPipe,
    DecimalPipe,
    UtilityService, ApprovalSetupServiceProxy, DocTemplateServiceProxy,
    UtilityServiceProxyService, ApprovalReviewServiceProxy, RoleServiceProxy,
    ApplicationLetterServiceProxy, ApprovalLogDocServiceProxy, ApprovalLogMessageServiceProxy,
    WorkflowServiceProxyService
  ],
  entryComponents: [
    // tenants
    CreateTenantDialogComponent,
    EditTenantDialogComponent,
    // roles
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    // users
    CreateUserDialogComponent,
    CreateOrganizationUserDialogComponent,
    CreateParticipantUserDialogComponent,
    EditUserDialogComponent,
    ResetPasswordDialogComponent,
    // application
    ApplicationDialogComponent,
    // Application Status
    CreateApplicationStatusDialogComponent,
    EditApplicationStatusDialogComponent,
    ViewApplicationStatusDialogComponent,
    // Application Type
    CreateApplicationTypeDialogComponent,
    EditApplicationTypeDialogComponent,
    ViewApplicationTypeDialogComponent,
    // Country
    CreateCountryDialogComponent,
    EditCountryDialogComponent,
    ViewCountryDialogComponent,
    // Email Setup
    CreateEmailSetupDialogComponent,
    EditEmailSetupDialogComponent,
    ViewEmailSetupDialogComponent,
    // Gender
    CreateGenderDialogComponent,
    EditGenderDialogComponent,
    ViewGenderDialogComponent,
    // Organization
    CreateOrganizationDialogComponent,
    EditOrganizationDialogComponent,
    ViewOrganizationDialogComponent,
    // Issuing Company
    CreateIssuingCompanyDialogComponent,
    EditIssuingCompanyDialogComponent,
    ViewIssuingCompanyDialogComponent,
    // Lga
    CreateLgaDialogComponent,
    EditLgaDialogComponent,
    ViewLgaDialogComponent,
    // Listing Type
    CreateListingTypeDialogComponent,
    EditListingTypeDialogComponent,
    ViewListingTypeDialogComponent,

    // Listing Type
    CreateFeesCalculationDialogComponent,
    EditFeesCalculationDialogComponent,
    ViewFeesCalculationDialogComponent,
    // Required Application Document
    CreateRequiredApplicationDocumentDialogComponent,
    EditRequiredApplicationDocumentDialogComponent,
    ViewRequiredApplicationDocumentDialogComponent,
    // Sms Setup
    CreateSmsSetupDialogComponent,
    EditSmsSetupDialogComponent,
    ViewSmsSetupDialogComponent,
    // State
    CreateStateDialogComponent,
    EditStateDialogComponent,
    ViewStateDialogComponent,
    // Title
    CreateTitleDialogComponent,
    EditTitleDialogComponent,
    ViewTitleDialogComponent,
    // application
    ApplicationDialogComponent,
    CommentsComponent,
    FormSectionComponent,
    ProcessListComponent,
    EmptyStateComponent,
    CreateEmailMessageDialogComponent,
    ForwardEmailMessageDialogComponent,
    ReplyEmailMessageDialogComponent
  ],
})
export class AppModule { }
