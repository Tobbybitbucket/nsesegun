import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-upload-documentation',
    templateUrl: './upload-documentation.component.html',

})


export class UploadDocumentationComponent implements OnInit {
    submitting = false;


    @Output() openNext = new EventEmitter<any>();
    @Output() openPrev = new EventEmitter<any>();


    handleOpenNext() {
        this.openNext.emit();
    }

    handleOpenPrev() {
        this.openPrev.emit();
    }

    ngOnInit(): void {
    }

}
