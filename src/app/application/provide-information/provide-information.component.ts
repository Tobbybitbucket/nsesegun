import {
  ApplicationServiceProxy,
  CreateApplicationDto,
  UtilizationOfProceed,
  ShareholdingStructure,
  DirectorBeneficialInterest,
  SubsidiaryOfIssuer,
  BuyerDirectorBeneficialInterest,
  IssuerDirectorBeneficialInterest,
  SponsorsForListingFGNReopening,
  AlreadyListedBondsForListingFGNReopening,
  PromoterProfile,
  ProposedUtilizationOfFund,
  ShareholdingStructureOfBuyer,
  IssuingCompanyServiceProxy,
  IssuingCompanyDto,
  CreateIssuingCompanyDto,
  CreateIssuingCompanyDtoApiResult,
  GetIssuerBondDirectoryResponseModel,
  GetIssuerCompanyDirectoryResponseModel,
  XissuerServiceProxy,
  GetIssuerCompanyDirectoryResponseModelApiResult,
  GetIssuerBondDirectoryResponseModelApiResult,
  ApplicationType,
} from "@shared/service-proxies/service-proxies";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { ApplicationDataService } from "../services/application-data.service";
import { AppComponentBase } from "@shared/app-component-base";
import {
  Component,
  Input,
  Output,
  EventEmitter,
  AfterViewInit,
  HostListener,
  OnInit,
  Injector,
  ChangeDetectorRef,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MenuItem } from "primeng/api";
import { finalize } from "rxjs/operators";
import { Observable } from "rxjs";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { AppConsts } from "@shared/AppConsts";
import { CurrencyPipe, DecimalPipe } from "@angular/common";
import * as moment from "moment";
import { indexOf } from "lodash";

@Component({
  selector: "app-provide-information",
  templateUrl: "./provide-information.component.html",
  styleUrls: ["./provide-information.component.css"],
})
export class ProvideInformationComponent
  extends AppComponentBase
  implements OnInit, AfterViewInit {
  localizationSourceName = AppConsts.localization.defaultLocalizationSourceName;
  submitting = false;
  currentSection = "section1";
  screenHeight: any;
  screenWidth: any;
  applicationData: any;
  applicationTypeId: number;
  applicationType: any;
  listingTypeId: number;
  issuerCompanyId: number;
  activeIndex: number = 1;
  payload: CreateApplicationDto | any = {};
  @Output() openNext = new EventEmitter<any>();
  application: CreateApplicationDto;
  payloadArrays: Array<any> = [];

  constructor(
    private route: ActivatedRoute,
    private _applicationDataService: ApplicationDataService,
    private _applicationService: ApplicationServiceProxy,
    private ngxService: NgxUiLoaderService,
    private _issuingCompanyService: XissuerServiceProxy,
    private router: Router,
    injector: Injector,
    private currencyPipe: CurrencyPipe,
    private decimalPipe: DecimalPipe,
    private ref: ChangeDetectorRef
  ) {
    super(injector);
    this.onResize();
  }

  l(key: string, ...args: any[]): string {
    let localizedText = this.localization.localize(
      key,
      this.localizationSourceName
    );

    if (!localizedText) {
      localizedText = key;
    }

    if (!args || !args.length) {
      return localizedText;
    }

    args.unshift(localizedText);
    return abp.utils.formatString.apply(this, args);
  }

  value: Date;

  // arr = [
  //   { label: "Select City", value: null },
  //   { label: "New York", value: { id: 1, name: "New York", code: "NY" } },
  //   { label: "Rome", value: { id: 2, name: "Rome", code: "RM" } },
  //   { label: "London", value: { id: 3, name: "London", code: "LDN" } },
  //   { label: "Istanbul", value: { id: 4, name: "Istanbul", code: "IST" } },
  //   { label: "Paris", value: { id: 5, name: "Paris", code: "PRS" } },
  // ];

  @HostListener("window:resize", ["$event"])
  onResize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
  }

  ngAfterViewInit() {
    // this.getApplicationForm(this.applicationTypeId);
  }

  handleOpenNext() {
    this.application = this.payload;
    this._applicationDataService.setCreateAppData(this.application);
    if (!this.route.snapshot.queryParamMap.get("appId")) {
      this.router.navigate(["/app/new-application/document-checklist"], {
        queryParams: {
          appTypeId: this.applicationTypeId,
          listingTypeId: this.listingTypeId,
          issuerCompanyId: this.issuerCompanyId,
        },
      });
    } else {
      this.router.navigate(["/app/new-application/document-checklist"], {
        queryParams: {
          appId: this.route.snapshot.queryParamMap.get("appId"),
          appTypeId: this.applicationTypeId,
          listingTypeId: this.listingTypeId,
        },
      });
    }
    // this.openNext.emit();
  }
  saveApplication(): void {
    this.application = this.payload;
    this.getApplicationProps();
    this.application.organizationId = 1;
    this.application.isSubmit = false;
    this.application.transactionDate = this.application.transactionDate
      ? moment(this.application.transactionDate, "YYYY-MM-DD")
      : moment();
    if (this.application.requiredDocuments) {
      this.application.requiredDocuments = this.application.requiredDocuments.filter(
        (doc) => {
          doc.id !== 0;
        }
      );
    }
    if (this.application.applicationType) {
      this.application.applicationType = ApplicationType.fromJS(
        this.application.applicationType
      );
    }
    // console.log(this.application);
    console.log(this.application, JSON.stringify(this.application));

    this._applicationDataService.setCreateAppData(this.application);
    this.ngxService.start();
    this._applicationService
      .createApplication(this.application)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe((res) => {
        console.log(res);
        this.notify.info(this.l("SavedSuccessfully"));
        this.router.navigate(["/app/home"]);
        //this.handleOpenNext();
      });
  }
  onSectionChange(sectionId: string) {
    this.currentSection = sectionId;
  }

  scrollTo(section) {
    document.querySelector("#" + section).scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest",
    });
  }
  getApplicationForm(appId) {
    if (appId == 0) {
      this.router.navigate(["/app/home"]);
    } else {
      this._applicationDataService.appDataState.subscribe((data) => {
        // console.log(data, appId);
        if (data)
          this.applicationData = data.result.filter(
            (type) => type.id === appId
          )[0].sections;
        this.applicationType = data.result.filter(
          (type) => type.id === appId
        )[0].type;
        // console.log(this.applicationData);

        this._applicationDataService.setapplicationType(this.applicationType);
      });
    }
  }
  getApplicationData(id: number) {
    this.ngxService.start();
    if (id) {
      this._applicationService
        .getApplication(id)
        .pipe(
          finalize(() => {
            // this.getApplicationFields(this.applicationTypeId);
            this.ngxService.stop();
          })
        )
        .subscribe((data) => {
          // console.log(data.result);
          this.payload = data.result;
          this.payload.id = id;
          this._applicationDataService.setCreateAppData(this.payload);
          // console.log(this.payloadArrays);
          var applicationData = this.applicationData;
          let difference = this.payloadArrays.filter(
            (x) => !Object.keys(this.payload).includes(x)
          );
          difference.forEach((val) => {
            // console.log(val);
            // this.fieldReset = true;
            this.payload[val] = [];
            var reset = {
              id: 1,
              sn: 1,
              applicationId: data.result.id,
            };
            switch (val) {
              case "lstUtilizationOfProceed":
                this.payload[val].push(UtilizationOfProceed.fromJS(reset));
                // console.log(this.payload[fieldName]);
                break;
              case "lstShareholdingStructure":
                this.payload[val].push(ShareholdingStructure.fromJS(reset));
                break;
              case "lstDirectorBeneficialInterest":
                this.payload[val].push(
                  DirectorBeneficialInterest.fromJS(reset)
                );
                break;
              case "lstSubsidiaryOfIssuer":
                this.payload[val].push(SubsidiaryOfIssuer.fromJS(reset));
                break;
              case "lstBuyerDirectorBeneficialInterest":
                this.payload[val].push(
                  BuyerDirectorBeneficialInterest.fromJS(reset)
                );
                break;
              case "lstIssuerDirectorBeneficialInterest":
                this.payload[val].push(
                  IssuerDirectorBeneficialInterest.fromJS(reset)
                );
                break;
              case "lstSponsorsForListingFGNReopening":
                this.payload[val].push(
                  SponsorsForListingFGNReopening.fromJS(reset)
                );
                break;
              case "lstAlreadyListedBondsForListingFGNReopening":
                this.payload[val].push(
                  AlreadyListedBondsForListingFGNReopening.fromJS(reset)
                );
                break;
              case "lstPromoterProfiles":
                this.payload[val].push(PromoterProfile.fromJS(reset));
                break;
              case "lstProposedUtilizationOfFunds":
                this.payload[val].push(ProposedUtilizationOfFund.fromJS(reset));
                break;
              case "lstShareholdingStructureOfBuyer":
                this.payload[val].push(
                  ShareholdingStructureOfBuyer.fromJS(reset)
                );
                break;

              default:
                break;
            }
            // this.payload[val].push(PromoterProfile.fromJS(reset));
            // console.log(this.payload[val], this.payload);
          });
          this.payloadArrays.forEach(
            (val) => {
              if (this.payload[val][0].sn !== 1) {
                this.payload[val][0].sn = 1;
                this.payload[val][0].id = 1;
                this.payload[val][0].applicationId = this.payload.id;
              } else {
                applicationData.filter((appData) => {
                  if (appData.fields[0].name === val) {
                    var newSec = {
                      id: appData.fields[0].data.length + 1,
                      sn: appData.fields[0].data.length + 1,
                      applicationId: appData.fields[0].data[0].applicationId,
                      row: appData.fields[0].data[0].row,
                    };
                    appData.fields[0].data.push(newSec);
                    applicationData.filter((appData) => {
                      if (appData.name === appData.name) {
                        appData = appData;
                        // data[appData.fields[0].name];
                      }
                    });
                  }
                });
              }
              // this.payload.filter(arr => {
              //   return
              // })
            },
            (error) => {
              console.log(error);
              this.ngxService.stop();
            }
          );
        });
    } else {
      this._applicationDataService.createAppDataState.subscribe((data) => {
        this.payload = data;
        setTimeout(() => {
          var applicationData = this.applicationData;
          this.payloadArrays.forEach((val) => {
            Object.keys(data).forEach(function (item) {
              // console.log(item);
              if (item == val) {
                if (data[item].length === 0) {
                  applicationData.filter((appData) => {
                    if (appData.fields[0].name === item) {
                      data[item][0] = {
                        id: 1,
                        sn: 1,
                        applicationId: null,
                      };
                      var models = appData.fields[0].data[0].row.map(
                        (row) => row.name
                      );
                      models.forEach((element) => {
                        data[item][0][element] = "";
                      });
                    }
                  });
                } else if (data[item].length > 0) {
                  console.log(item, data[item]);
                  applicationData.filter((appData) => {
                    if (appData.fields[0].name === item) {
                      for (let index = 1; index < data[item].length; index++) {
                        var newSec = {
                          id: data[item][index].id,
                          sn: data[item][index].sn,
                          applicationId: data[item][index].applicationId,
                          row: appData.fields[0].data[0].row,
                        };
                        appData.fields[0].data.push(newSec);
                        applicationData.filter((appData) => {
                          if (appData.name === appData.name) {
                            appData = appData;
                            data[appData.fields[0].name];
                          }
                        });
                      }
                      console.log(appData);
                    }
                  });
                }
              }
            });
          });
        }, 4000);

        this.ngxService.stop();
      });
    }
  }
  getIssuerCompanyInfo(issuerCompanyId) {
    if (this.applicationTypeId == 7 || this.applicationTypeId == 9) {
      //SUPPLEMENTARY LISTING OF MUTUAL FUNDS/ EXCHANGE TRADED FUNDS (ETFS)/REAL ESTATE INVESTMENT TRUST (REITS)
      //SUPPLEMENTARY LISTING OF EQUITIES
      //let issuerCompanies: GetIssuerCompanyDirectoryResponseModel[] = JSON.parse(localStorage.getItem('issuerCompanies'));
      this._issuingCompanyService
        .getIssuerCompanyDirectoryById(issuerCompanyId)
        .pipe(finalize(() => { }))
        .subscribe(
          (result: GetIssuerCompanyDirectoryResponseModelApiResult) => {
            this.payload.nameOfIssuer = result.result.companyName;
            this.payload.isinOfSecurity = result.result.internationSecIn;
            this.payload.symbolCodeOfSecurity = result.result.symbol;
          }
        );
    } else if (this.applicationTypeId == 10) {
      // LISTING OF FGN REOPENING
      this._issuingCompanyService
        .getIssuerBondDirectoryById(issuerCompanyId)
        .pipe(finalize(() => { }))
        .subscribe((result: GetIssuerBondDirectoryResponseModelApiResult) => {
          this.payload.nameOfIssuer = result.result.name;
          this.payload.isinOfSecurity = result.result.isin;
          this.payload.quantityVolume = ""; //companyData.volume;
        });
    }
    // this.ngxService.start();
    // this._issuingCompanyService
    //   .getIssuingCompany(issuerCompanyId)
    //   .pipe(
    //     finalize(() => {
    //       this.ngxService.stop();
    //     })
    //   )
    //   .subscribe((result: CreateIssuingCompanyDtoApiResult) => {
    //     console.log(result.result.name, "HERE");
    //     this.payload.nameOfIssuer = result.result.name;
    //   });
  }

  getApplicationProps() {
    if (!this.route.snapshot.queryParamMap.get("listingTypeId")) {
      this._applicationDataService.listingTypetate.subscribe((d) => {
        this.payload.listingTypeId = +d;
      });
    }
    if (!this.route.snapshot.queryParamMap.get("appTypeId")) {
      this._applicationDataService.appTypeIdState.subscribe((data) => {
        this.applicationTypeId = +data;
        this.payload.applicationTypeId = this.applicationTypeId;
      });
    }
    if (!this.route.snapshot.queryParamMap.get("issuerCompanyId")) {
      console.log("ran");
      this._applicationDataService.issuerCompanyIdState.subscribe((data) => {
        this.issuerCompanyId = +data;
        this.payload.issuerCompanyId = this.issuerCompanyId;
        this.getIssuerCompanyInfo(this.issuerCompanyId);
      });
    }
  }
  fieldReset: boolean = false;
  setPayloadArrayData(fieldName, fieldData, rows) {
    if (typeof this.payload[fieldName] !== "object") {
      this.payload[fieldName] = [];
    } else {
      this.payload[fieldName] = this.payload[fieldName];
    }
    var models = rows.map((row) => row.name);
    fieldData.forEach((element) => {
      var object = {
        sn: element.sn,
        id: element.id,
        applicationId: element.applicationId,
      };
      models.forEach((element) => {
        object[element] = null;
      });

      const found = this.payload[fieldName].find(
        (element) => element.id == object.id
      );
      if (found && typeof this.payload[fieldName] == "object") {
        console.log("run");
      } else if (!this.fieldReset) {
        // this.payload[fieldName].push(object);
        switch (fieldName) {
          case "lstUtilizationOfProceed":
            this.payload[fieldName].push(UtilizationOfProceed.fromJS(object));
            // console.log(this.payload[fieldName]);
            break;
          case "lstShareholdingStructure":
            this.payload[fieldName].push(ShareholdingStructure.fromJS(object));
            break;
          case "lstDirectorBeneficialInterest":
            this.payload[fieldName].push(
              DirectorBeneficialInterest.fromJS(object)
            );
            break;
          case "lstSubsidiaryOfIssuer":
            this.payload[fieldName].push(SubsidiaryOfIssuer.fromJS(object));
            break;
          case "lstBuyerDirectorBeneficialInterest":
            this.payload[fieldName].push(
              BuyerDirectorBeneficialInterest.fromJS(object)
            );
            break;
          case "lstIssuerDirectorBeneficialInterest":
            this.payload[fieldName].push(
              IssuerDirectorBeneficialInterest.fromJS(object)
            );
            break;
          case "lstSponsorsForListingFGNReopening":
            this.payload[fieldName].push(
              SponsorsForListingFGNReopening.fromJS(object)
            );
            break;
          case "lstAlreadyListedBondsForListingFGNReopening":
            this.payload[fieldName].push(
              AlreadyListedBondsForListingFGNReopening.fromJS(object)
            );
            break;
          case "lstPromoterProfiles":
            this.payload[fieldName].push(PromoterProfile.fromJS(object));
            break;
          case "lstProposedUtilizationOfFunds":
            this.payload[fieldName].push(
              ProposedUtilizationOfFund.fromJS(object)
            );
            break;
          case "lstShareholdingStructureOfBuyer":
            this.payload[fieldName].push(
              ShareholdingStructureOfBuyer.fromJS(object)
            );
            break;

          default:
            break;
        }
      }
    });
    this.payloadArrays.push(fieldName);
    // console.log(this.payload[fieldName]);
  }
  addTableField(sec) {
    var newSec = {
      id: sec.fields[0].data.length + 1,
      sn: sec.fields[0].data.length + 1,
      applicationId: sec.fields[0].data[0].applicationId,
      row: sec.fields[0].data[0].row,
    };
    sec.fields[0].data.push(newSec);
    this.applicationData.filter((appData) => {
      if (appData.name === sec.name) {
        this.fieldReset = true;
        // console.log(
        //   this.payload[appData.fields[0].name][0].id - 1,
        //   sec.fields[0].data.length - 1,
        //   this.payload[appData.fields[0].name],
        //   sec.fields[0].data[0].row
        // );
        const defaultObject = this.payload[appData.fields[0].name][0];
        const deepClone = JSON.parse(JSON.stringify(defaultObject));
        deepClone.sn = sec.fields[0].data.length;
        deepClone.id = sec.fields[0].data.length;
        deepClone.applicationId = this.payload.id;
        var rows = sec.fields[0].data[0].row.map((row) => row.name);
        rows.forEach((element) => {
          deepClone[element] = null;
        });
        // console.log(deepClone);
        switch (appData.fields[0].name) {
          case "lstUtilizationOfProceed":
            this.payload[appData.fields[0].name].push(
              UtilizationOfProceed.fromJS(deepClone)
            );
            break;
          case "lstShareholdingStructure":
            this.payload[appData.fields[0].name].push(
              ShareholdingStructure.fromJS(deepClone)
            );
            break;
          case "lstDirectorBeneficialInterest":
            this.payload[appData.fields[0].name].push(
              DirectorBeneficialInterest.fromJS(deepClone)
            );
            break;
          case "lstSubsidiaryOfIssuer":
            this.payload[appData.fields[0].name].push(
              SubsidiaryOfIssuer.fromJS(deepClone)
            );
            break;
          case "lstBuyerDirectorBeneficialInterest":
            this.payload[appData.fields[0].name].push(
              BuyerDirectorBeneficialInterest.fromJS(deepClone)
            );
            break;
          case "lstIssuerDirectorBeneficialInterest":
            this.payload[appData.fields[0].name].push(
              IssuerDirectorBeneficialInterest.fromJS(deepClone)
            );
            break;
          case "lstSponsorsForListingFGNReopening":
            this.payload[appData.fields[0].name].push(
              SponsorsForListingFGNReopening.fromJS(deepClone)
            );
            break;
          case "lstAlreadyListedBondsForListingFGNReopening":
            this.payload[appData.fields[0].name].push(
              AlreadyListedBondsForListingFGNReopening.fromJS(deepClone)
            );
            break;
          case "lstPromoterProfiles":
            this.payload[appData.fields[0].name].push(
              PromoterProfile.fromJS(deepClone)
            );
            break;
          case "lstProposedUtilizationOfFunds":
            this.payload[appData.fields[0].name].push(
              ProposedUtilizationOfFund.fromJS(deepClone)
            );
            break;
          case "lstShareholdingStructureOfBuyer":
            this.payload[appData.fields[0].name].push(
              ShareholdingStructureOfBuyer.fromJS(deepClone)
            );
            break;

          default:
            break;
        }
        appData = sec;
        this.payload[appData.fields[0].name];
      }
    });
    // console.log(this.applicationData);
  }
  removeTableField(sec, data) {
    this.applicationData.filter((appData) => {
      if (appData.name === sec.name) {
        appData.fields[0].data.forEach((element) => {
          if (element.id == data.id && element.sn == data.sn) {
            if (appData.fields[0].data.length >= 2) {
              this.payload[appData.fields[0].name].splice(
                appData.fields[0].data.indexOf(element),
                1
              );
              appData.fields[0].data.splice(
                appData.fields[0].data.indexOf(element),
                1
              );
              // console.log(
              //   this.payload[appData.fields[0].name],
              //   appData.fields[0].data.indexOf(element)
              // );
            }
          }
        });
      }
    });
  }
  // doFormLogic(operation, total, value: any) {
  doFormLogic(logics?, array?, operation?: string) {
    // console.log(logics);
    if (logics) {
      logics.forEach((logic) => {
        switch (logic.operation) {
          case "Addition":
            var a: number = +this.payload[logic.value.a];
            var b: number = +this.payload[logic.value.b];
            this.payload[logic.total] = a + b;
            break;
          case "Multiplication":
            var a: number = +this.payload[logic.value.a];
            var b: number = +this.payload[logic.value.b];
            if (
              this.payload[logic.value.a] !== undefined &&
              this.payload[logic.value.b] !== undefined
            ) {
              this.payload[logic.total] = a * b;
            }
            break;
          case "Summation":
            this.payload[logic.total] = this.payload[array].reduce(function (
              accumulator,
              object
            ) {
              return accumulator + object[logic.value.a];
            },
              0);

            break;
          case "DivisionThenMultiplyByHundred":
            // this.payload[logic.total] = this.payload[array].reduce(function (
            //   accumulator,
            //   object
            // ) {
            //   return (object[logic.value.a] / this.payload[logic.value.b]) * 100;
            // },
            //   0);

            // break;
            var a: number = +this.payload[array]?.[0].amount;
            var b: number = +this.payload[logic.value.b];
            if (
              a !== undefined &&
              b !== undefined
            ) {
              this.payload[logic.total] = (a / b) * 100;
            }
            break;
          default:
            break;
        }
        // console.log(this.payload[logic.total]);
      });
      // // this.ref.detectChanges();
    }
  }
  ngOnInit() {
    this.applicationTypeId = +this.route.snapshot.queryParamMap.get(
      "appTypeId"
    );
    this.issuerCompanyId = +this.route.snapshot.queryParamMap.get(
      "issuerCompanyId"
    );
    this.getApplicationData(+this.route.snapshot.queryParamMap.get("appId"));
    this.getApplicationProps();
    // if (typeof this.applicationTypeId === "number") {
    //   this._applicationDataService.setAppTypeId(this.applicationTypeId);
    //   console.log("SET");
    // }
    this._applicationDataService.setAppView("information");
    this._applicationDataService.setListingTypeId(
      +this.route.snapshot.queryParamMap.get("listingTypeId")
    );
    this.payload.listingTypeId = Number(
      this.route.snapshot.queryParamMap.get("listingTypeId")
    );
    this.getApplicationForm(this.applicationTypeId);
    this.getIssuerCompanyInfo(this.issuerCompanyId);
    this.listingTypeId = this.payload.listingTypeId;

    if (this.route.snapshot.queryParamMap.get("listingTypeId")) {
      this.route.snapshot.queryParamMap.get("listingTypeId");
    }
  }
}
