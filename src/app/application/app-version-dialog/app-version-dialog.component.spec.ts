import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppVersionDialogComponent } from './app-version-dialog.component';

describe('AppVersionDialogComponent', () => {
  let component: AppVersionDialogComponent;
  let fixture: ComponentFixture<AppVersionDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppVersionDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppVersionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
