import { Component, OnInit } from "@angular/core";
import { ApplicationDataService } from "../services/application-data.service";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-application-overview",
  templateUrl: "./application-overview.component.html",
  styleUrls: ["./application-overview.component.css"],
})
export class ApplicationOverviewComponent implements OnInit {
  applicationTypeId;
  listingTypeId;
  issuerCompanyId;
  constructor(
    private _applicationDataService: ApplicationDataService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this._applicationDataService.setAppView("instruction");
    this.applicationTypeId = Number(
      this.route.snapshot.queryParamMap.get("appTypeId")
    );
    this.listingTypeId = Number(
      this.route.snapshot.queryParamMap.get("listingTypeId")
    );
    this.issuerCompanyId = Number(
      this.route.snapshot.queryParamMap.get("issuerCompanyId")
    );
    // set application id state if entry point is review
    this._applicationDataService.setAppTypeId = this.applicationTypeId;
  }
  startForm() {
    this.router.navigate(["/app/new-application/provide-information"], {
      queryParams: {
        appTypeId: this.applicationTypeId,
        listingTypeId: this.listingTypeId,
        issuerCompanyId: this.issuerCompanyId,
      },
    });
  }
}
