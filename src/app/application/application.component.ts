import {
  RequiredApplicationDocumentServiceProxy,
  ApplicationServiceProxy,
  CreateApplicationDto,
  DocumentUploadModel,
} from "./../../shared/service-proxies/service-proxies";
import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  HostListener,
} from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { finalize } from "rxjs/operators";
import { ApplicationDataService } from "./services/application-data.service";

@Component({
  selector: "app-application",
  templateUrl: "./application.component.html",
  styleUrls: ["./application.component.css"],
  animations: [appModuleAnimation()],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationComponent implements OnInit, OnDestroy {
  @Output() openNext = new EventEmitter<any>();
  @Output() openPrev = new EventEmitter<any>();
  @Output() gotoFirstPage = new EventEmitter<any>();
  currentSection = "section1";
  selectedDocument: any;
  application: CreateApplicationDto;
  documents: DocumentUploadModel[];
  submitting = false;
  index = 0;
  loading = false;
  applicationType: any;
  applicationTypeId: number;
  applicationId: number;
  listingTypeId: number;
  issuerCompanyId: number;
  applicationView: string = "";
  value: Date;
  screenHeight: any;
  screenWidth: any;
  buttonItems = [
    {
      label: "Delete",
      command: () => {
        this.delete(this.selectedDocument);
      },
    },
  ];

  cols = [
    { field: "sn", header: "S/N" },
    { field: "name", header: "Expected Document Name" },
    { field: "document", header: "Document Uploaded" },
    { field: "action", header: "" },
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _applicationService: ApplicationServiceProxy,
    private ngxService: NgxUiLoaderService,
    private _applicationDataService: ApplicationDataService
  ) {}
  @HostListener("window:resize", ["$event"])
  onResize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
  }
  ngOnInit(): void {
    this.applicationTypeId = +this.route.snapshot.queryParamMap.get(
      "appTypeId"
    );
    this.applicationId = +this.route.snapshot.queryParamMap.get("appId");
    this.issuerCompanyId = +this.route.snapshot.queryParamMap.get(
      "issuerCompanyId"
    );
    // console.log(
    //   this.route.snapshot.queryParamMap.get("appTypeId"),
    //   this.applicationTypeId
    // );
    if (this.route.snapshot.queryParamMap.get("appTypeId")) {
      this._applicationDataService.setAppTypeId(this.applicationTypeId);
    }

    if (this.route.snapshot.queryParamMap.get("issuerCompanyId")) {
      this._applicationDataService.setissuerCompanyId(this.issuerCompanyId);
    }

    // console.log(this.route.snapshot.queryParamMap.get("appId"), this.applicationId);
    if (this.route.snapshot.queryParamMap.get("appId")) {
      // console.log(this.route.snapshot.queryParamMap.get("appId"), this.applicationId);
      this._applicationDataService.setAppId(this.applicationId);
    }
    // this.getApplicationTypeDocs();

    this.getApplicationState();
    this.getApplications();
    this._applicationDataService.applicationTypeState.subscribe((data) => {
      this.applicationType = data;
      // console.log(data);
    });
  }

  getApplicationState() {
    let app_id = +this.route.snapshot.queryParamMap.get("appTypeId");
    // console.log(app_id);

    // if (typeof app_id === "number") {
    //   this._applicationDataService.setAppTypeId(app_id);
    // }
    this._applicationDataService.appTypeIdState.subscribe((data) => {
      this.applicationTypeId = data;
    });

    this._applicationDataService.appViewState.subscribe((data) => {
      this.applicationView = data;
      console.log(this.applicationView);
    });

    this.listingTypeId = +this.route.snapshot.queryParamMap.get(
      "listingTypeId"
    );
    // this.applicationId,
    this._applicationDataService.appIdState.subscribe((data) => {
      this.applicationId = data;
      // console.log(this.applicationId);
    });
  }

  goToSection(section: string) {
    // console.log(section);
    this.router.navigate([`/app/new-application/${section}`], {
      queryParams: {
        appTypeId: this.applicationTypeId,
      },
    });

    // NB>>   appNameIs not needed in the params
    // appTypeName: this.selectedApplicationType.name,
  }
  delete(document: any): void {
    document.document = "";
  }
  getApplicationTypeDocs(): void {
    this.ngxService.start();
    this._applicationService
      .getApplicationRequiredDocuments(this.applicationTypeId)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(
        (res) => {
          console.log(res.result);
          this.application = res.result;
          this.documents = res.result.requiredDocuments;
        },
        (error) => {
          console.log(error);
          this.ngxService.stop();
        }
      );
  }

  // openNext() {
  //   this.index = (this.index === 2) ? 0 : this.index + 1;
  // }

  // openPrev() {
  //   this.index = (this.index === 0) ? 2 : this.index - 1;
  // }

  openFirstPage() {
    console.log(this.index);
    this.index = 0;
  }
  handleOpenNext() {
    this.openNext.emit();
  }

  handlePrev() {
    this.openPrev.emit();
  }
  handleGotoFirstPage() {
    this.gotoFirstPage.emit();
  }
  setSelectedDocument(document: any): void {
    console.log("set Selected Document ", event);
    this.selectedDocument = document;
  }

  onSectionChange(sectionId: string) {
    this.currentSection = sectionId;
  }

  fileChangeEvent(event: Event, document: any) {
    // console.log('FileUpload event ', event);
    const element = event.currentTarget as HTMLInputElement;
    const fileList: FileList | null = element.files;
    if (fileList) {
      // console.log('FileUpload -> files', fileList);
      if (this.validateFile(fileList[0].name)) {
        if (this.validateFileSize(fileList[0].size)) {
          document.document = fileList[0].name;
          document.file = fileList[0];
          console.log("document -> files", document);
        } else {
          abp.notify.error("File size must not be more than 5MB");
        }
      } else {
        abp.notify.error("please select a supported file");
      }
    } else {
      abp.notify.error("File size must not be more than 5MB");
    }
  }

  validateFile(name: String) {
    const ext = name.substring(name.lastIndexOf(".") + 1);
    if (
      ext.toLowerCase() === "doc" ||
      ext.toLowerCase() === "docx" ||
      ext.toLowerCase() === "xls" ||
      ext.toLowerCase() === "jpg" ||
      ext.toLowerCase() === "png" ||
      ext.toLowerCase() === "jpeg" ||
      ext.toLowerCase() === "pdf" ||
      ext.toLowerCase() === "xlsx"
    ) {
      return true;
    } else {
      return false;
    }
  }

  validateFileSize(size) {
    if (size > 5000000) {
      return false;
    } else {
      return true;
    }
  }

  scrollTo(section) {
    document.querySelector("#" + section).scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest",
    });
  }

  getApplications(
    keyword?: string | null | undefined,
    isActive?: boolean | null | undefined,
    skipCount?: number | undefined,
    maxResultCount?: number | undefined
  ) {
    this._applicationService
      .getAllApplications(keyword, isActive, skipCount, maxResultCount)
      .subscribe((res) => {
        console.log(res);
      });
  }
  saveApplication(): void {
    // this._applicationService
    //   .createApplication(this.application)
    //   .pipe(
    //     finalize(() => {
    //       // this.saving = false;
    //     })
    //   )
    //   .subscribe(() => {
    //     // this.notify.info(this.l('SavedSuccessfully'));
    //   });
  }
  ngOnDestroy() {
    this._applicationDataService.setCreateAppData({});
    this._applicationDataService.setDocumentChecklist([]);
    this._applicationDataService.setAppId(0);
  }
}
