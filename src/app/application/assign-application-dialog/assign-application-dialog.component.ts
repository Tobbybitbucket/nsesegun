import { Component, OnInit, Input, Injector, Output, EventEmitter } from "@angular/core";
import { ApprovalReviewServiceProxy } from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { AppComponentBase } from "@shared/app-component-base";

// interface City {
//   name: string;
//   code: string
// }

@Component({
  selector: "app-assign-application-dialog",
  templateUrl: "./assign-application-dialog.component.html",
  styleUrls: ["./assign-application-dialog.component.css"],
})
export class AssignApplicationDialogComponent
  extends AppComponentBase
  implements OnInit {
  @Input() visible: boolean;
  @Input() applicationTypeId: any;
  @Input() applicationId: any;
  @Output() onClose: EventEmitter<any> = new EventEmitter();
  @Output("getApplications") getApplications: EventEmitter<any> = new EventEmitter();

  selectedValue: any = {};
  listingAnalysts: Array<any>;
  // arr = [
  //   { label: "Tobi", value: { id: 1, name: "Tobi", code: "TB" } },
  //   { label: "Miracle", value: { id: 2, name: "Miracle", code: "MR" } },
  //   { label: "Francis", value: { id: 3, name: "Francis", code: "FR" } },
  //   { label: "William", value: { id: 4, name: "William", code: "WM" } },
  //   { label: "Austin", value: { id: 5, name: "Austin", code: "AU" } },
  // ];

  constructor(
    private _approvalReviewServiceProxy: ApprovalReviewServiceProxy,
    private ngxService: NgxUiLoaderService,
    injector: Injector
  ) {
    super(injector);
  }

  getListingAnalysts() {
    // this.ngxService.start();
    this._approvalReviewServiceProxy
      .fetchListingAnalysts()
      .pipe(
        finalize(() => {
          // this.ngxService.stop();
        })
      )
      .subscribe((data) => {
        this.listingAnalysts = data.map((data) => ({
          label: data.name,
          value: data.id,
        }));
        // console.log(data, this.listingAnalysts);
        // this.notify.info(this.l("SavedSuccessfully"));
        // this.router.navigate(["/app/home"]);
      });
  }
  assignApplication() {
    console.log(this.applicationTypeId, this.applicationId, this.selectedValue);
    this.ngxService.start();
    this._approvalReviewServiceProxy
      .assignListingAnalyst(
        this.applicationId,
        this.selectedValue.value,
        this.applicationTypeId
      )
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe((res) => {
        console.log(res);
        this.notify.info(this.l("Assigned Successfully"));
        this.getApplications.emit();
        this.visible = false;
      });
  }

  closeModal(): void {
    this.onClose.emit();
  }

  ngOnInit(): void {
    this.getListingAnalysts();
  }
}
