import {
  Component,
  Output,
  EventEmitter,
  AfterViewInit,
  HostListener,
  ViewChild,
  OnInit,
  ChangeDetectorRef,
} from "@angular/core";
import { Table } from "primeng/table";
import { ApplicationDataService } from "../services/application-data.service";
import {
  ApprovalLogDocServiceProxy,
  DocLogServiceProxy,
  UploadDocModel,
  IUploadDocModel,
  DocumentUploadModel,
  ApplicationServiceProxy,
  IDocumentUploadModel,
} from "@shared/service-proxies/service-proxies";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { finalize } from "rxjs/operators";

@Component({
  selector: "document-checklist",
  templateUrl: "./document-checklist.component.html",
})
export class DocumentChecklistComponent implements AfterViewInit, OnInit {
  submitting = false;
  currentSection = "section2";
  screenHeight: any;
  screenWidth: any;
  @Output() openNext = new EventEmitter<any>();
  @Output() openPrev = new EventEmitter<any>();
  @ViewChild("dt") table: Table;
  applicationTypeId: number;
  applicationId: number;
  listingTypeId: number;
  issuerCompanyId: number;
  requiredDocuments: DocumentUploadModel | any;
  fileObject: IUploadDocModel;
  fileObjectList: Array<IUploadDocModel> = [];
  documents: IDocumentUploadModel;
  documentChecklist: Array<IDocumentUploadModel> = [];
  constructor(
    private _applicationDataService: ApplicationDataService,
    private route: ActivatedRoute,
    private ngxService: NgxUiLoaderService,
    private _applicationService: ApplicationServiceProxy,
    private _approvalLogDocService: ApprovalLogDocServiceProxy,
    private _docLogService: DocLogServiceProxy,
    private router: Router,
    private ref: ChangeDetectorRef
  ) {
    this.onResize();
  }
  getRequiredDocuments(appId) {
    this._applicationDataService.appDataState.subscribe((data) => {
      // console.log(data);
      this.requiredDocuments = data.result.filter(
        (type) => type.id === appId
      )[0].requiredDocuments;
      // console.log(this.requiredDocuments);
    });
  }
  getApplicationTypeDocs(): void {
    this.ngxService.start();
    this._approvalLogDocService
      .getDocs(this.applicationTypeId)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(
        (res) => {
          // console.log(res.result, "getApplicationTypeDocs");
          //   this.application = res.result;
          //   this.documents = res.result.requiredDocuments;
        },
        (error) => {
          console.log(error);
          this.ngxService.stop();
        }
      );
  }
  getApplicationData(id?: number) {
    if (id) {
      this.ngxService.start();
      this._applicationService
        .getApplication(id)
        .pipe(
          finalize(() => {
            this.ngxService.stop();
          })
        )
        .subscribe((data) => {
          // console.log(
          //   data.result.requiredDocuments,
          //   "HERE",
          //   this.requiredDocuments
          // );
          var docs = data.result.requiredDocuments;
          this.requiredDocuments.forEach((data) => {
            docs.filter((dt) => {
              if (data.name === dt.name) {
                // console.log(dt);
                return (data.document = dt.document);
              }
            });
            this.ref.detectChanges();
          });
          // console.log(this.requiredDocuments);
        });
    }
  }
  ngOnInit(): void {
    this._applicationDataService.setAppView("document");
    this.applicationTypeId = +this.route.snapshot.queryParamMap.get(
      "appTypeId"
    );
    this.applicationId = +this.route.snapshot.queryParamMap.get("appId");

    this.listingTypeId = +this.route.snapshot.queryParamMap.get(
      "listingTypeId"
    );
    this.issuerCompanyId = +this.route.snapshot.queryParamMap.get(
      "issuerCompanyId"
    );
    if (typeof this.applicationTypeId === "number") {
      this.getRequiredDocuments(this.applicationTypeId);
    }
    this.getApplicationTypeDocs();

    if (this.route.snapshot.queryParamMap.get("appId")) {
      this.getApplicationData(+this.route.snapshot.queryParamMap.get("appId"));
    }
  }

  selectedDocument: any;
  loading = false;

  buttonItems = [
    {
      label: "Delete",
      command: () => {
        this.delete(this.selectedDocument);
      },
    },
  ];

  cols = [
    { field: "id", header: "S/N" },
    { field: "name", header: "Expected Document Name" },
    { field: "document", header: "Document Uploaded" },
    { field: "action", header: "" },
  ];

  @HostListener("window:resize", ["$event"])
  onResize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
  }

  delete(document: any): void {
    document.document = "";
  }

  ngAfterViewInit() {}

  handleOpenNext() {
    this.openNext.emit();
    if (this.route.snapshot.queryParamMap.get("appId")) {
      this.router.navigate(["/app/new-application/review"], {
        queryParams: {
          appId: this.applicationId,
          appTypeId: this.applicationTypeId,
          listingTypeId: this.listingTypeId,
        },
      });
    } else {
      this.router.navigate(["/app/new-application/review"], {
        queryParams: {
          appTypeId: this.applicationTypeId,
          listingTypeId: this.listingTypeId,
          issuerCompanyId: this.issuerCompanyId,
        },
      });
    }
  }

  handlePrev() {
    if (this.route.snapshot.queryParamMap.get("appId")) {
      this.router.navigate(["/app/new-application/provide-information"], {
        queryParams: {
          appId: this.applicationId,
          appTypeId: this.applicationTypeId,
          listingTypeId: this.listingTypeId,
        },
      });
    } else {
      this.router.navigate(["/app/new-application/provide-information"], {
        queryParams: {
          appTypeId: this.applicationTypeId,
          listingTypeId: this.listingTypeId,
          issuerCompanyId: this.issuerCompanyId,
        },
      });
    }
    this.openPrev.emit();
  }

  setSelectedDocument(document: any): void {
    console.log("set Selected Document ", event);
    this.selectedDocument = document;
  }

  onSectionChange(sectionId: string) {
    this.currentSection = sectionId;
  }

  validateFile(name: String) {
    const ext = name.substring(name.lastIndexOf(".") + 1);
    if (
      ext.toLowerCase() === "doc" ||
      ext.toLowerCase() === "docx" ||
      ext.toLowerCase() === "xls" ||
      ext.toLowerCase() === "jpg" ||
      ext.toLowerCase() === "png" ||
      ext.toLowerCase() === "jpeg" ||
      ext.toLowerCase() === "pdf" ||
      ext.toLowerCase() === "xlsx"
    ) {
      return true;
    } else {
      return false;
    }
  }

  validateFileSize(size) {
    if (size > 5000000) {
      return false;
    } else {
      return true;
    }
  }

  scrollTo(section) {
    document.querySelector("#" + section).scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest",
    });
  }

  fileChangeEvent(files: FileList, doc) {
    let me = this;
    let file = files[0];
    let ext = file.name.split(".").pop();
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      me.ngxService.start();
      me.fileObject = {
        fileBase64String: reader.result
          .toString()
          .substring(reader.result.toString().lastIndexOf(",") + 1),
        fileName: doc.name,
        fileExt: ext,
        fileType: "APPLICATION_FORM",
        applicationNo: "",
        is_post_approval: false,
      };
      me.documents = {
        id: doc.id,
        name: doc.name,
        document: "",
        file: "",
      };
    };
    setTimeout(() => {
      var obj = this.fileObjectList.find(
        (element) => element.fileName === this.fileObject.fileName
      );

      if (typeof obj == "object") {
        this.fileObjectList.forEach((element, index) => {
          if (element.fileName === this.fileObject.fileName) {
            this.fileObjectList[index] = this.fileObject;
            this.documentChecklist[index] = this.documents;
          }
        });
      } else if (typeof obj == "undefined") {
        this.fileObjectList.push(me.fileObject);
        this.documentChecklist.push(me.documents);
      }
      me.ngxService.stop();
    }, 3000);
    reader.onerror = function (error) {
      console.log("Error: ", error);
    };
  }

  processDocument() {
    // console.log(this.fileObjectList);
    if (this.fileObjectList.length !== 0) {
      this.fileObjectList.forEach((element, index) => {
        this.ngxService.start();
        this.uploadDocument(element, index);
      });
    } else {
      this.handleOpenNext();
    }
  }

  public uploadDocument(data, index): void {
    this.ngxService.start();
    this._docLogService
      .uploadDoc(data)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(
        (res) => {
          // console.log(res.result.message);
          this.documentChecklist.filter((element) => {
            if (element.name == data.fileName)
              element.document = res.result.message;
          });
          // console.log(this.documentChecklist,data);
          // this.documentChecklist[index].document = data.result.message;
          this._applicationDataService.setDocumentChecklist(
            this.documentChecklist
          );
          this.handleOpenNext();
        },
        (error) => {
          console.log(error);
          this.ngxService.stop();
        }
      );
  }
}
