import {
  ListingTypeServiceProxy,
  ApplicationTypeCategory,
  XissuerServiceProxy,
  GetIssuerCompanyDirectoryResponseModel,
  GetIssuerBondDirectoryResponseModel,
  NameIdModelIListApiResult,
  NameIdModel,
} from "./../../../shared/service-proxies/service-proxies";
import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import {
  ListingTypeDto,
  ApplicationTypeDto,
  ApplicationTypeServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { ApplicationDataService } from "../services/application-data.service";


@Component({
  selector: "app-application-dialog",
  templateUrl: "./application-dialog.component.html",
  styleUrls: ["./application-dialog.component.css"],
})
export class ApplicationDialogComponent implements OnInit {
  @Input() visible: boolean;
  @Output() onClose: EventEmitter<any> = new EventEmitter();
  listingTypes: ListingTypeDto[] | undefined;
  selectedListingType: ListingTypeDto;
  applicationTypes: ApplicationTypeDto[];
  issuerCompanies: NameIdModel[];
  _issuerBonds: NameIdModel[];
  issuerBonds: NameIdModel[];
  applicationTypeCategories: ApplicationTypeCategory[];
  selectedApplicationType: ApplicationTypeDto;
  applicationTypeId: number;
  applicationTypeCategoryId: number;
  issuerBondOrCompanyId: number;
  selectedValue = {};
  selectedValue2 = {};
  saving = false;
  showIssuerCompanyDiv = false;
  showIssuerBondDiv = false;
  constructor(
    private router: Router,
    private ngxService: NgxUiLoaderService,
    private _applicationTypeService: ApplicationTypeServiceProxy,
    private _issuingCompanyService: XissuerServiceProxy,
    private _listingTypeService: ListingTypeServiceProxy,
    private _applicationDataService: ApplicationDataService
  ) { }

  ngOnInit(): void {
    this.getApplicationTypeCategories();
    this.getIssuerCompanies();
    this.getBondCompanies();
    this.issuerBondOrCompanyId = 0;
    this.applicationTypeCategoryId = 0;
    this.applicationTypeId = 0;
  }

  onChangeAppCat(event): void {
    this.applicationTypeCategoryId = Number(event.value.id);
    this.getApplicationTypes();
  }
  onChangeAppType(event): void {
    this.applicationTypeId = Number(event.value.id);
    this.selectedApplicationType = event.value;
    this.getApplicationTypeListingTypes();

    this.showIssuerBondDiv = false;
    this.showIssuerCompanyDiv = false;
    if (this.applicationTypeId == 7 || this.applicationTypeId == 9) {
      //SUPPLEMENTARY LISTING OF MUTUAL FUNDS/ EXCHANGE TRADED FUNDS (ETFS)/REAL ESTATE INVESTMENT TRUST (REITS)
      //SUPPLEMENTARY LISTING OF EQUITIES
      this.showIssuerCompanyDiv = true;
    } else if (this.applicationTypeId == 32) {
      // LISTING OF FGN REOPENING
      this.showIssuerBondDiv = true;
      this.issuerBonds = this._issuerBonds; //.filter(p=>p.bondType=='Federal DEBT');
    }
  }
  onChangeIssuer(event): void {
    this.issuerBondOrCompanyId = Number(event.value.id);
  }
  getApplicationTypeCategories(): void {
    // this.ngxService.start();
    this._applicationTypeService
      .getAllApplicationTypeCategoryList()
      .pipe(
        finalize(() => {
          // this.ngxService.stop();
        })
      )
      .subscribe((result) => {
        this.applicationTypeCategories = result;
      });
  }
  getApplicationTypes(): void {
    this.ngxService.start();
    this._applicationTypeService
      .getAllApplicationTypesList(this.applicationTypeCategoryId)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe((result) => {
        this.applicationTypes = result;
      });
  }
  getApplicationTypeListingTypes(): void {
    this.ngxService.start();
    this._listingTypeService
      .getAllApplicationTypeListingTypes(this.applicationTypeId)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe((res) => {
        this.listingTypes = res.result;
      });
  }

  getIssuerCompanies(): void {
    this.issuerCompanies = JSON.parse(localStorage.getItem("issuerCompanies"));

    if (this.issuerCompanies == undefined || this.issuerCompanies == null) {
      this._issuingCompanyService
        .getIssuerCompanyList()
        .pipe(
          finalize(() => {
            // this.ngxService.stop();
          })
        )
        .subscribe((result: NameIdModelIListApiResult) => {
          localStorage.setItem(
            "issuerCompanies",
            JSON.stringify(result.result)
          );
          this.issuerCompanies = result.result;
          this.issuerBondOrCompanyId = 0;
        });
    }
  }
  getBondCompanies(): void {
    this._issuerBonds = JSON.parse(localStorage.getItem("_issuerBonds"));

    if (this._issuerBonds == undefined || this._issuerBonds == null) {
      this._issuingCompanyService
        .getIssuerBondList()
        .pipe(
          finalize(() => {
            // this.ngxService.stop();
          })
        )
        .subscribe((result: NameIdModelIListApiResult) => {
          localStorage.setItem("_issuerBonds", JSON.stringify(result.result));
          this._issuerBonds = result.result;
          this.issuerBondOrCompanyId = 0;
        });
    }
  }

  showDialog() {
    this.saving = true;
    let listTypeId;
    if (this.issuerBondOrCompanyId == 0 && this.showIssuerCompanyDiv) {
      abp.notify.error("Please Select issuer Company");
      return;
    } else if (this.issuerBondOrCompanyId == 0 && this.showIssuerBondDiv) {
      abp.notify.error("Please Select Bond");
      return;
    } else if (!this.selectedListingType) {
      //listTypeId = 2;
      abp.notify.error("Please Select Listing Type");
      return;
    } else {
      listTypeId = this.selectedListingType.id;
    }
    this._applicationDataService.setAppView("instruction");
    // console.log(this.applicationTypeId);

    // this._applicationDataService.setAppTypeId(+this.applicationTypeId);

    this.router.navigate(["/app/new-application/instruction"], {
      queryParams: {
        appTypeId: this.applicationTypeId,
        listingTypeId: listTypeId,
        issuerCompanyId: this.issuerBondOrCompanyId,
      },
    });



    // NB>>   appNameIs not needed in the params
    // appTypeName: this.selectedApplicationType.name,
  }

  closeModal(): void {
    this.onClose.emit();
  }
  
}
