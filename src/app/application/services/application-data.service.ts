import { CreateApplicationDto } from "@shared/service-proxies/service-proxies";
import {
  mergeMap as _observableMergeMap,
  catchError as _observableCatch,
} from "rxjs/operators";
import {
  Observable,
  throwError as _observableThrow,
  of as _observableOf,
  of,
  BehaviorSubject,
} from "rxjs";
// import { Rx } from 'rxjs/Rx';
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class ApplicationDataService {
  applicationData: any = {
    result: [
      {
        id: 5,
        type: "INITIAL LISTING OF EQUITIES",
        requiredDocuments: [
          {
            id: 1,
            name: "Prospectus",
            document: "",
            file: null,
            required: true,
          },
          {
            id: 2,
            name: "Memorandum and Articles of Association",
            document: "",
            file: null,
          },
          {
            id: 3,
            name: "Certificate of Incorporation",
            document: "",
            file: null,
          },
          { id: 4, name: "Board Resolution", document: "", file: null },
          { id: 5, name: "Other Documents ", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfIssuer",
                label: "Name of Issuer",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "applicationType",
                label: "Type of Application",
                fieldType: "dropdown",
                dropdownOutput: "object",
                required: true,
                options: [
                  {
                    label: "Initial Public Offering",
                    value: "Initial Public Offering",
                  },
                  {
                    label: "Listing by Introduction",
                    value: "Listing by Introduction",
                  },
                  {
                    label: "Merger and Acquisition",
                    value: "Merger and Acquisition",
                  },
                  { label: "Others" },
                ],
              },
              {
                id: 3,
                name: "description",
                label: "Description of Application",
                fieldType: "textarea",
                required: true,
              },
            ],
          },
          {
            name: "Parties to the Listing",
            fields: [
              {
                id: 1,
                name: "sponsoringTradingLicenseHolder",
                label: "Sponsoring Trading License Holder",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "jointSponsoringTradingLicenseHolders",
                label: "Joint Sponsoring Trading License Holder(s)",
                fieldType: "input",
                required: false,
              },
              {
                id: 3,
                name: "issuingHouse",
                label: "Issuing House",
                fieldType: "input",
                required: false,
              },
              {
                id: 4,
                name: "jointIssuingHouses",
                label: "Joint Issuing House(s)",
                fieldType: "input",
                required: false,
              },
              {
                id: 5,
                name: "auditor",
                label: "Auditor",
                fieldType: "input",
                required: false,
              },
              {
                id: 6,
                name: "reportingAccountant",
                label: "Reporting Accountant",
                fieldType: "input",
                required: false,
              },
              {
                id: 7,
                name: "solicitorsToTheIssue",
                label: "Solicitor(s) to the Issue",
                fieldType: "input",
                required: false,
              },
              {
                id: 8,
                name: "registrar",
                label: "Registrar",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Share Capital Base & Listing Data",
            fields: [
              {
                id: 1,
                name: "authorizedShareCapitalVolume",
                label: "Authorized Share Capital (Volume)",
                required: false,
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                ],
              },
              {
                id: 2,
                name: "authorizedShareCapitalValue",
                label: "Authorized Share Capital (Value)",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 3,
                name: "issuedAndFullyPaidVolume",
                label: "Issued and Fully Paid (Volume)",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                  {
                    operation: "Addition",
                    total: "unitsToBeListed",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "unitsToBeOffered",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 4,
                name: "issuedAndFullyPaidValue",
                label: "Issued and Fully Paid (Value)",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 5,
                name: "nominalValue",
                label: "Nominal Value",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 6,
                name: "unitsToBeOffered",
                label: "Units to be Offered",
                format: "decimal",
                fieldType: "number",
                required: true,
                logics: [
                  {
                    operation: "Addition",
                    total: "unitsToBeListed",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "unitsToBeOffered",
                    },
                  },
                  {
                    operation: "Multiplication",
                    total: "transactionValue",
                    value: {
                      a: "unitsToBeOffered",
                      b: "transactionValue",
                    },
                  },
                ],
              },
              {
                id: 7,
                name: "unitsToBeListed",
                label: "Units to be Listed",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Addition",
                    total: "unitsToBeListed",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "unitsToBeOffered",
                    },
                  },
                  {
                    operation: "Multiplication",
                    total: "marketCapitalisation",
                    value: {
                      a: "unitsToBeListed",
                      b: "offerOrListingPrice",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 8,
                name: "offerOrListingPrice",
                label: "Offer/Listing Price ",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "marketCapitalisation",
                    value: {
                      a: "unitsToBeListed",
                      b: "offerOrListingPrice",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 9,
                name: "marketCapitalisation",
                label: "Market Capitalisation",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "marketCapitalisation",
                    value: {
                      a: "unitsToBeListed",
                      b: "offerOrListingPrice",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 12,
                name: "offerValue",
                label: "Offer value",
                format: "decimal",
                fieldType: "number",
                required: true,
              },
              {
                id: 13,
                name: "otherCurrency",
                label: "Other Currency",
                fieldType: "input",
                required: false,
              },
              {
                id: 10,
                name: "board",
                label: "Board",
                fieldType: "dropdown",
                dropdownOutput: "string",
                required: true,
                options: ["Premium", "Main", "Growth – Entry Segment", "Growth – Standard Segment", "ASeM"],
              },
              {
                id: 11,
                label: "For Book Building",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "priceRange",
                    label: "Price Range",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name: "transactionValue",
                    label: "Transaction Value",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                    logics: [
                      {
                        operation: "Multiplication",
                        total: "transactionValue",
                        value: {
                          a: "unitsToBeOffered",
                          b: "transactionValue",
                        },
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Utilization of the Proceeds (Public Offer)",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstUtilizationOfProceed",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "utilization",
                        label: "Utilization",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "amount",
                        label: "Amount (N)",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountInNairaSubtotal",
                            value: { a: "amount" },
                          },
                          {
                            operation: "DivisionThenMultiplyByHundred",
                            total: "percentOfGross",
                            value: {
                              a: "amount",
                              b: "offerValue",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "percentOfGross",
                        label: "% of Gross proceed ",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfGrossProceedSubtotal",
                            value: { a: "percentOfGross" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "estimatedCompletionPeriod",
                        label: "Estimated Completion Period",
                        fieldType: "input",
                        logics: [
                          {
                            operation: "Summation",
                            total: "estimatedCompletionPeriodSubtotal",
                            value: { a: "estimatedCompletionPeriod" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Subtotal",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaSubtotal",
                        label: "Amount (N)",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountInNairaSubtotal",
                            value: { a: "amount" },
                          },
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedSubtotal",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfGrossProceedSubtotal",
                            value: { a: "percentOfGross" },
                          },
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodSubtotal",
                        label: "Estimated Completion Period",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "estimatedCompletionPeriodSubtotal",
                            value: { a: "estimatedCompletionPeriod" },
                          },
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "estimatedCompletionPeriodCostOfIssue",
                              b: "estimatedCompletionPeriodSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                  {
                    name: "Cost of Issue",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaCostOfIssue",
                        label: "Amount (N)",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedCostOfIssue",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodCostOfIssue",
                        label: "Estimated Completion Period",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "estimatedCompletionPeriodCostOfIssue",
                              b: "estimatedCompletionPeriodSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                  {
                    name: "Grand Total",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaGrandTotal",
                        label: "Subtotal",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedGrandTotal",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodGrandTotal",
                        label: "Estimated Completion Period",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Outstanding Debt Obligation",
            fields: [
              {
                id: 1,
                name: "outstandingDebtObligation",
                label: "",
                description:
                  "Provide brief details of the issuer’s debt obligations, other than in the ordinary course of business",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Claims and Litigation",
            fields: [
              {
                id: 1,
                name: "totalNumberOfCasesInstitutedAgainstTheIssuer",
                label: "Total number of cases instituted against the Issuer",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Addition",
                    total: "totalNumberOfCasesInvolvedByTheIssuer",
                    value: {
                      a: "totalNumberOfCasesInstitutedAgainstTheIssuer",
                      b: "totalNumberOfCasesInstitutedByTheIssuer",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 2,
                name: "totalNumberOfCasesInstitutedByTheIssuer",
                label: "Total number of cases instituted by the Issuer",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Addition",
                    total: "totalNumberOfCasesInvolvedByTheIssuer",
                    value: {
                      a: "totalNumberOfCasesInstitutedAgainstTheIssuer",
                      b: "totalNumberOfCasesInstitutedByTheIssuer",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 3,
                name: "totalNumberOfCasesInvolvedByTheIssuer",
                label: "Total number of cases involved by the Issuer",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Addition",
                    total: "totalNumberOfCasesInvolvedByTheIssuer",
                    value: {
                      a: "totalNumberOfCasesInstitutedAgainstTheIssuer",
                      b: "totalNumberOfCasesInstitutedByTheIssuer",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 4,
                label: "Indebtedness",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "indebtednessValueInNaira",
                    format: "currency",
                    label: "Value (N)",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name: "indebtednessValueInOthercurrency",
                    format: "currency",
                    label: "Value (Other currency)",
                    currency: "non",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 5,
                label:
                  "Total amount claimed from the number of cases instituted against the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedAgainstTheIssuerInNaira",
                    label: "Value (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedAgainstTheIssuerInOthercurrency",
                    label: "Value (Other currency)",
                    currency: "non",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 6,
                label:
                  "Total amount claimed from the number of cases instituted by the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedByTheIssuerInNaira",
                    label: "Value (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedByTheIssuerInOthercurrency",
                    label: "Value (Other currency)",
                    currency: "non",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
            ],
          },
          {
            name: "Issuer’s Profile",
            fields: [
              {
                id: 1,
                name: "briefDescriptionOfIssuer",
                label: "Brief Description of Issuer",
                tooltip:
                  "The Issuer’s profile should include the date of incorporation, nature of business, industry.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Reason for Listing",
            fields: [
              {
                id: 1,
                name: "briefDescriptionOfReasonForListing",
                label: "Brief Description of reason for listing",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Promoter(s) Profile",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstPromoterProfiles",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "fullName",
                        label: "Full Name",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "briefProfile",
                        label: "Brief Profile ",
                        fieldType: "textarea",
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Shareholding Structure of the Issuer",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstShareholdingStructure",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "nameOfShareholder",
                        label: "Name of Shareholder",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "noOfSharesHeldPreOfferOrMerger",
                        label: "Pre-Offer/Merger No of Shares Held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "noOfSharesHeldPreOfferOrMergerTotal",
                            value: { a: "noOfSharesHeldPreOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "percentageOfHoldingPreOfferOrMerger",
                        label: "Pre-Offer/Merger % of Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfHoldingPreOfferOrMergerTotal",
                            value: { a: "percentageOfHoldingPreOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "noOfSharesHeldPostOfferOrMerger",
                        label: "Post-Offer/Merger No of Shares Held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "noOfSharesHeldPostOfferOrMergerTotal",
                            value: { a: "noOfSharesHeldPostOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "percentageOfHoldingPostOfferOrMerger",
                        label: "Post-Offer/Mergerr % of Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfHoldingPostOfferOrMergerTotal",
                            value: {
                              a: "percentageOfHoldingPostOfferOrMerger",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Totals",
                    fields: [
                      {
                        id: 1,
                        name: "noOfSharesHeldPreOfferOrMergerTotal",
                        label: "Pre-Offer/Merger No of Shares Held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "noOfSharesHeldPreOfferOrMergerTotal",
                            value: { a: "noOfSharesHeldPreOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfHoldingPreOfferOrMergerTotal",
                        label: "Pre-Offer/Merger % of Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfHoldingPreOfferOrMergerTotal",
                            value: { a: "percentageOfHoldingPreOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "noOfSharesHeldPostOfferOrMergerTotal",
                        label: "Post-Offer/Merger No of Shares Held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "noOfSharesHeldPostOfferOrMergerTotal",
                            value: { a: "noOfSharesHeldPostOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "percentageOfHoldingPostOfferOrMergerTotal",
                        label: "Post-Offer/Merger % of Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfHoldingPostOfferOrMergerTotal",
                            value: {
                              a: "percentageOfHoldingPostOfferOrMerger",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Directors and Their Beneficial Interests",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstDirectorBeneficialInterest",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "nameOfDirector",
                        label: "Name of Directors",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "designation",
                        label: "Designation",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 3,
                        name: "directShareholding",
                        label: "Direct Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "directShareholdingTotal",
                            value: { a: "directShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "indirectHolding",
                        label: "Indirect Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "indirectHoldingTotal",
                            value: { a: "indirectHolding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 5,
                        name: "totalShareholding",
                        label: "Total Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "totalShareholdingTotal",
                            value: { a: "totalShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 6,
                        name: "percentageHolding",
                        label: "%tage held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageHeldTotal",
                            value: { a: "percentageHolding" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Totals",
                    fields: [
                      {
                        id: 1,
                        name: "directShareholdingTotal",
                        label: "Direct Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "directShareholdingTotal",
                            value: { a: "directShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "indirectHoldingTotal",
                        label: "Indirect Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "indirectHoldingTotal",
                            value: { a: "indirectHolding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "totalShareholding",
                        label: "Total Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "totalShareholdingTotal",
                            value: { a: "totalShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "percentageHeldTotal",
                        label: "%tage held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageHeldTotal",
                            value: { a: "percentageHolding" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Subsidiaries of the Issuer",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstSubsidiaryOfIssuer",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "nameOfSubsidiary",
                        label: "Name of subsidiary ",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "dateOfAcquisition",
                        label: "Date of Acquisition",
                        fieldType: "date",
                        required: false,
                      },
                      {
                        id: 3,
                        name: "percentageHeldByTheIssuerInTheSubsidiary",
                        label:
                          "Percentage Held by the Issuer in the Subsidiary ",
                        fieldType: "input",
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Profile of the Other Companies (For Merger only)",
            fields: [
              {
                id: 1,
                name:
                  "provideABriefProfileOfTheOtherCompaniesInvolvedInTheMerger",
                label: "",
                description:
                  "Provide a brief profile of the other companies involved in the merger and names of their directors.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Material Contracts",
            fields: [
              {
                id: 1,
                name:
                  "provideAListOfTheAgreementsAndContractsWhichTheIssuerHasEnteredIntoMaterialContracts",
                label:
                  "Provide a list of the agreements and contracts which the Issuer has entered into that are material to offer and/or  listing or to its business, other than in the ordinary course of business.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          // {
          //   name: "Upload Supporting Documents",
          //   fields: [
          //     {
          //       id: 1,
          //       label: "Prospectus",
          //       fieldType: "file",
          //       required: true,
          //     },
          //     {
          //       id: 2,
          //       label: "Memorandum and Articles of Association ",
          //       fieldType: "file",
          //       required: false,
          //     },
          //     {
          //       id: 3,
          //       label: "Certificate of Incorporation",
          //       fieldType: "file",
          //       required: false,
          //     },
          //     {
          //       id: 4,
          //       label: "Board Resolution",
          //       fieldType: "file",
          //       required: false,
          //     },
          //     {
          //       id: 4,
          //       label: "Other Documents",
          //       tooltip: "link for checklist",
          //       fieldType: "file",
          //       required: false,
          //     },
          //   ],
          // },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTradingLicenseHolderDeclaration",
                label:
                  "Managing Director of the Sponsoring Trading License Holder",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to the best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
      {
        id: 6,
        type:
          "INITIAL LISTING OF MUTUAL FUNDS/ EXCHANGE TRADED FUNDS (ETFS)/REAL ESTATE INVESTMENT TRUST (REITS)",
        requiredDocuments: [
          {
            id: 1,
            name: "Prospectus",
            document: "",
            file: null,
            required: true,
          },
          {
            id: 2,
            name: "Listing Memorandum",
            document: "",
            file: null,
          },
          { id: 3, name: "Other Documents", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfFundManagerOrIssuer",
                label: "Name of Fund Manager/Issuer",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "listingMethodsByIntroductionOrPublicOffer",
                label: "Listing Methods (by introduction or Public Offer)",
                fieldType: "input",
                required: true,
              },
              {
                id: 3,
                name: "description",
                label: "Description of Application",
                fieldType: "textarea",
                required: true,
              },
            ],
          },
          {
            name: "Professional parties to the Listing",
            fields: [
              {
                id: 1,
                label: "Name",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "sponsoringTradingLicenseHolder",
                    label: "Sponsoring Trading License Holder ",
                    fieldType: "input",
                    required: false,
                  },
                  {
                    id: 2,
                    name: "nameOfFundManagerOrIssuer",
                    label: "Fund Manager/Issuer",
                    fieldType: "input",
                    required: false,
                  },
                  {
                    id: 3,
                    name: "issuingHouse",
                    label: "Issuing House",
                    fieldType: "input",
                    required: false,
                  },
                  {
                    id: 4,
                    name: "assetsManager",
                    label: "Assets Manager",
                    fieldType: "input",
                    required: false,
                  },
                ],
              },
            ],
          },
          {
            name: "Listing Data",
            fields: [
              {
                id: 1,
                name: "methodOfOffer",
                label: "Method of Offer",
                fieldType: "input",
                required: false,
              },
              {
                id: 2,
                name: "volumeBeingOffered",
                label: "Volume being offered",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "valueOfTheOffer",
                    value: { a: "volumeBeingOffered", b: "priceValue" },
                  },
                ],
                required: false,
              },
              {
                id: 3,
                name: "priceValue",
                label: "Price (Value)",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "valueOfTheOffer",
                    value: { a: "volumeBeingOffered", b: "priceValue" },
                  },
                ],
                required: false,
              },
              {
                id: 4,
                name: "valueOfTheOffer",
                label: "Value of the offer",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "valueOfTheOffer",
                    value: { a: "volumeBeingOffered", b: "priceValue" },
                  },
                ],
                required: false,
              },
              {
                id: 5,
                name: "minimumSubscription",
                label: "Minimum subscription",
                fieldType: "input",
                required: false,
              },
              {
                id: 6,
                name: "rating",
                label: "Rating",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Background information about the Fund",
            fields: [
              {
                id: 1,
                name: "provideBriefDetailsOfTheFund",
                label: "",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Proposed Utilisation of Funds",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstProposedUtilizationOfFunds",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "descriptionofProposedAsset",
                        label: "Description of Proposed Asset",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "amountAllocated",
                        label: "Amount Allocated (N)",
                        fieldType: "number",
                        format: "currency",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountAllocatedInNairaTotal",
                            value: { a: "amountAllocated" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "percentageOfTargetWeighting",
                        label: "Target Weighting (%)",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "targetWeightingPercentageTotal",
                            value: { a: "percentageOfTargetWeighting" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Totals",
                    fields: [
                      {
                        id: 1,
                        name: "amountAllocatedInNairaTotal",
                        label: "Amount Allocated (N)",
                        fieldType: "number",
                        format: "currency",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountAllocatedInNairaTotal",
                            value: { a: "amountAllocated" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "targetWeightingPercentageTotal",
                        label: "Target Weighting (%)",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "targetWeightingPercentageTotal",
                            value: { a: "percentageOfTargetWeighting" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Investment Objectives/Strategy and Policies of the Fund",
            fields: [
              {
                id: 1,
                name: "investmentObjectiveStrategyPolicyOfFund",
                label: "",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "About the Fund Manager",
            fields: [
              {
                id: 1,
                name: "briefDescriptionOfFundManager",
                label: "Brief description of Fund Manager",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Outstanding Debt Obligation",
            fields: [
              {
                id: 1,
                name:
                  "provideBriefDetailsOfTheFundManagersCurrentDebtObligationsOtherThanInTheOrdinaryCourseOfBusiness",
                description:
                  "(Provide brief details of the Fund Manager’s current debt obligations, other than in the ordinary course of business.)",
                label: "",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Claims, Litigation and Indebtedness",
            fields: [
              {
                id: 1,
                label: "Cases instituted against the Fund Manager",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "numberOfCasesInstitutedAgainstTheFundManager",
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "numberOfCasesInstitutedAgainstTheFundManagerValueClaimedInNaira",
                    format: "currency",
                    label: "Value Claimed (N)",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 2,
                label: "Cases instituted by the Fund Manager",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "numberOfCasesInstitutedByTheFundManager",
                    label: "Number",
                    fieldType: "input",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "numberOfCasesInstitutedByTheFundManagerValueClaimedInNaira",
                    format: "currency",
                    label: "Value Claimed (N)",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 3,
                label: "Cases involved by the Fund Manager",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "numberOfCasesInvolvedByTheFundManager",
                    label: "Number",
                    fieldType: "input",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "numberOfCasesInvolvedByTheFundManagerValueClaimedInNaira",
                    format: "currency",
                    label: "Value Claimed (N)",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 4,
                label: "Indebtedness",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "indebtednessValueInNaira",
                    format: "currency",
                    label: "Value (N)",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name: "indebtednessValueInOthercurrency",
                    label: "Value (Other currency)",
                    format: "currency",
                    currency: "non",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
            ],
          },
          {
            name: "Material Contracts",
            fields: [
              {
                id: 1,
                name:
                  "provideAListOfTheAgreementsAndContractsWhichTheIssuerHasEnteredIntoMaterialContracts",
                label:
                  "Provide a list of the agreements and contracts which the Fund Manager has entered into that are material to offer and/or  listing or to its business, other than in the ordinary course of business.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          // {
          //   name: "Upload Supporting Documents",
          //   fields: [
          //     {
          //       id: 1,
          //       label: "Prospectus",
          //       fieldType: "file",
          //       required: true,
          //     },
          //     {
          //       id: 2,
          //       label: "Listing Memorandum",
          //       fieldType: "file",
          //       required: true,
          //     },
          //     {
          //       id: 3,
          //       label: "Other Documents (as listed out in the checklists)",
          //       fieldType: "file",
          //       required: false,
          //     },
          //   ],
          // },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTheSponsoringTradingLicenseHolder",
                label:
                  "Managing Director of the Sponsoring Trading License Holder",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to be best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
      {
        id: 7,
        type:
          "SUPPLEMENTARY LISTING OF MUTUAL FUNDS/ EXCHANGE TRADED FUNDS (ETFS)/REAL ESTATE INVESTMENT TRUST (REITS)",
        requiredDocuments: [
          {
            id: 1,
            name:
              "Evidence of SEC registration of the additional units to be listed",
            document: "",
            file: null,
            required: true,
          },
          { id: 2, name: "Other Documents", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfFundManagerOrIssuer",
                label: "Name of Fund Manager/Issuer",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "nameOfFund",
                label: "Name of Fund",
                fieldType: "dropdown",
                dropdownOutput: "string",
                options: ["mutual fund", "ETFs", "REITs currently listed"],
                required: true,
              },
            ],
          },
          {
            name: "Professional parties to the Listing",
            fields: [
              {
                id: 1,
                label: "Name",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "sponsoringTradingLicenseHolder",
                    label: "Sponsoring Trading License Holder",
                    fieldType: "input",
                    required: false,
                  },
                ],
              },
            ],
          },
          {
            name: "Listing Data",
            fields: [
              {
                id: 1,
                name: "currentUnitsOutstanding",
                label: "Current units outstanding",
                fieldType: "input",
                required: false,
              },
              {
                id: 2,
                name: "additionalUnitsToBeListed",
                label: "Additional units to be listed",
                fieldType: "input",
                required: false,
              },
              {
                id: 3,
                name: "postListingUnitsOutstanding",
                label: "Post listing units outstanding",
                fieldType: "input",
                required: false,
              },
            ],
          },
          // {
          //   name: "Upload Supporting Documents",
          //   fields: [
          //     {
          //       id: 1,
          //       label:
          //         "Evidence of SEC registration of the additional units to be listed",
          //       fieldType: "file",
          //       required: true,
          //     },
          //     {
          //       id: 3,
          //       label: "Other Documents",
          //       fieldType: "file",
          //       required: false,
          //     },
          //   ],
          // },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTheSponsoringTradingLicenseHolder",
                label:
                  "Managing Director of the Sponsoring Trading License Holder",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to be best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
      {
        id: 8,
        type: "VOLUNTARY DELISTING",
        requiredDocuments: [
          {
            id: 1,
            name: "Information Memorandum",
            document: "",
            file: null,
            required: true,
          },
          { id: 2, name: "Board Resolution", document: "", file: null },
          {
            id: 3,
            name: "Shareholders’ Resolution",
            document: "",
            file: null,
          },
          { id: 4, name: "Other Documents", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfIssuer",
                label: "Name of Issuer",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "applicationType",
                label: "Type of Application",
                fieldType: "dropdown",
                dropdownOutput: "object",
                options: [
                  {
                    label: "Voluntary Delisting ",
                    value: "Voluntary Delisting ",
                  },
                ],
                required: false,
              },
              {
                id: 3,
                name: "description",
                label: "Description of Application",
                fieldType: "textarea",
                required: true,
              },
            ],
          },
          {
            name: "Reasons for Delisting",
            fields: [
              {
                id: 1,
                name: "reasonsForDelisting",
                label: "Name",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Exit Consideration",
            fields: [
              {
                id: 1,
                name: "exitConsideration",
                label: "",
                fieldType: "textarea",
                required: false,
                description:
                  "The Issuer has provided the following exit opportunities to its minority shareholders: ",
              },
            ],
          },
          {
            name: "History and Business of the Issuer’s Profile",
            fields: [
              {
                id: 1,
                name: "historyAndBusinesOfTheIssuerProfile",
                label:
                  "This information should also contain the date of incorporation and history of significant restructuring, date of listing on The Nigerian Stock Exchange and on any other exchange, the business of the Issuer and its place in the industry.",
                fieldType: "textarea",
                required: false,
                description: "",
              },
            ],
          },
          {
            name: "Share Capital Base & Delisting Data ",
            fields: [
              {
                id: 1,
                name: "authorizedShareCapitalVolume",
                label: "Authorized Share Capital (Volume)",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 2,
                name: "authorizedShareCapitalValue",
                label: "Authorized Share Capital (Value)",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 3,
                name: "nominalValue",
                label: "Nominal Value",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                  {
                    operation: "Multiplication",
                    total: "marketPriceAsAtTheDateOfApplication",
                    value: {
                      a: "issuedAndFullyPaidValue",
                      b: "nominalValue",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 4,
                name: "issuedAndFullyPaidValue",
                label: "Issued and Fully Paid",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "marketPriceAsAtTheDateOfApplication",
                    value: {
                      a: "issuedAndFullyPaidValue",
                      b: "nominalValue",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 5,
                name: "marketPriceAsAtTheDateOfApplication",
                label: "Market Price (as at the date of application)",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "marketPriceAsAtTheDateOfApplication",
                    value: {
                      a: "issuedAndFullyPaidValue",
                      b: "nominalValue",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 6,
                name: "marketCapitalisationAsAtTheDateOfApplication",
                label: "Market Capitalisation (as at the date of application)",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 7,
                name: "highestMarketPriceInLast6Months",
                label:
                  "The highest market price in the last six months ending on the date of application",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 8,
                name: "board",
                label: "Board",
                fieldType: "dropdown",
                dropdownOutput: "string",
                required: true,
                options: ["Premium", "Main", "Growth – Entry Segment", "Growth – Standard Segment", "ASeM"],
              },
            ],
          },
          {
            name: "Shareholding Structure of the Issuer",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstShareholdingStructure",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "nameOfShareholder",
                        label: "Name of Shareholder",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "noOfSharesHeldPreOfferOrMerger",
                        label: "Pre-Offer/Merger No of Shares Held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "noOfSharesHeldPreOfferOrMergerTotal",
                            value: { a: "noOfSharesHeldPreOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "percentageOfHoldingPreOfferOrMerger",
                        label: "Pre-Offer/Merger % of Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfHoldingPreOfferOrMergerTotal",
                            value: { a: "percentageOfHoldingPreOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "noOfSharesHeldPostOfferOrMerger",
                        label: "Post-Offer/Merger No of Shares Held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "noOfSharesHeldPostOfferOrMergerTotal",
                            value: { a: "noOfSharesHeldPostOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "percentageOfHoldingPostOfferOrMerger",
                        label: "Post-Offer/Mergerr % of Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfHoldingPostOfferOrMergerTotal",
                            value: {
                              a: "percentageOfHoldingPostOfferOrMerger",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Totals",
                    fields: [
                      {
                        id: 1,
                        name: "noOfSharesHeldPreOfferOrMergerTotal",
                        label: "Pre-Offer/Merger No of Shares Held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "noOfSharesHeldPreOfferOrMergerTotal",
                            value: { a: "noOfSharesHeldPreOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfHoldingPreOfferOrMergerTotal",
                        label: "Pre-Offer/Merger % of Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfHoldingPreOfferOrMergerTotal",
                            value: { a: "percentageOfHoldingPreOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "noOfSharesHeldPostOfferOrMergerTotal",
                        label: "Post-Offer/Merger No of Shares Held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "noOfSharesHeldPostOfferOrMergerTotal",
                            value: { a: "noOfSharesHeldPostOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "percentageOfHoldingPostOfferOrMergerTotal",
                        label: "Post-Offer/Merger % of Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfHoldingPostOfferOrMergerTotal",
                            value: {
                              a: "percentageOfHoldingPostOfferOrMerger",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Directors and Their Beneficial Interests",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstDirectorBeneficialInterest",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "nameOfDirector",
                        label: "Name of Directors",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "designation",
                        label: "Designation",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 3,
                        name: "directShareholding",
                        label: "Direct Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "directShareholdingTotal",
                            value: { a: "directShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "indirectHolding",
                        label: "Indirect Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "indirectHoldingTotal",
                            value: { a: "indirectHolding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 5,
                        name: "totalShareholding",
                        label: "Total Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "totalShareholdingTotal",
                            value: { a: "totalShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 6,
                        name: "percentageHolding",
                        label: "%tage held",
                        fieldType: "input",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageHeldTotal",
                            value: { a: "percentageHolding" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Totals",
                    fields: [
                      {
                        id: 1,
                        name: "directShareholdingTotal",
                        label: "Direct Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "directShareholdingTotal",
                            value: { a: "directShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "indirectHoldingTotal",
                        label: "Indirect Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "indirectHoldingTotal",
                            value: { a: "indirectHolding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "totalShareholdingTotal",
                        label: "Total Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "totalShareholdingTotal",
                            value: { a: "totalShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "percentageHeldTotal",
                        label: "%tage held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageHeldTotal",
                            value: { a: "percentageHolding" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          // {
          //   name: "Upload Supporting Documents",
          //   fields: [
          //     {
          //       id: 1,
          //       label: "Information Memorandum",
          //       fieldType: "file",
          //       required: true,
          //     },
          //     {
          //       id: 2,
          //       label: "Board Resolution",
          //       fieldType: "file",
          //       required: false,
          //     },
          //     {
          //       id: 3,
          //       label: "Shareholders’ Resolution",
          //       fieldType: "file",
          //       required: false,
          //     },
          //     {
          //       id: 4,
          //       label: "Other Documents",
          //       fieldType: "file",
          //       required: false,
          //     },
          //   ],
          // },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTheSponsoringTradingLicenseHolder",
                label:
                  "Managing Director of Sponsoring Trading License Holder ",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to be best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
      {
        id: 9,
        type: "SUPPLEMENTARY LISTING OF EQUITIES",
        requiredDocuments: [
          {
            id: 1,
            name:
              "Rights Circular/Placement Memorandum/Information Memorandum ",
            document: "",
            file: null,
            required: true,
          },
          { id: 2, name: "Board Resolution", document: "", file: null },
          {
            id: 3,
            name: "Shareholders’ Resolution",
            document: "",
            file: null,
          },
          { id: 4, name: "Other Documents", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfIssuer",
                label: "Name of Issuer",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "applicationType",
                label: "Type of Application",
                fieldType: "dropdown",
                dropdownOutput: "object",
                required: true,
                options: [
                  { label: "Rights Issue", value: "Rights Issue" },
                  { label: "Placing", value: "Placing" },
                  {
                    label: "Debt Conversion to Equity",
                    value: "Debt Conversion to Equity",
                  },
                  {
                    label: "Staff share Based Payment",
                    value: "Staff share Based Payment",
                  },
                  {
                    label: "Mergers & Acquisition",
                    value: "Mergers & Acquisition",
                  },
                  { label: "Others" },
                ],
              },
              {
                id: 3,
                name: "description",
                label: "Description of Application",
                fieldType: "textarea",
                required: true,
              },
            ],
          },
          {
            name: "Parties to the Listing",
            fields: [
              {
                id: 1,
                name: "tradingLicenseHolder",
                label: "Trading License Holder",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "jointTradingLicenseHolder",
                label: "Joint Trading License Holder(s)",
                fieldType: "input",
                required: false,
              },
              {
                id: 3,
                name: "issuingHouse",
                label: "Issuing House",
                fieldType: "input",
                required: false,
              },
              {
                id: 4,
                name: "jointIssuingHouses",
                label: "Joint Issuing House(s)",
                fieldType: "input",
                required: false,
              },
              {
                id: 5,
                name: "auditor",
                label: "Auditor",
                fieldType: "input",
                required: false,
              },
              {
                id: 6,
                name: "reportingAccountant",
                label: "Reporting Accountant",
                fieldType: "input",
                required: false,
              },
              {
                id: 7,
                name: "solicitorsToTheIssue",
                label: "Solicitor(s) to the Issue",
                fieldType: "input",
                required: false,
              },
              {
                id: 8,
                name: "registrar",
                label: "Registrar",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name:
              "About the Scheme (For Staff Share Based Payment Scheme Transaction Type)",
            fields: [
              {
                id: 1,
                name: "descriptionOfTheScheme",
                label: "Description of the Scheme",
                fieldType: "textarea",
                required: true,
              },
              {
                id: 2,
                name: "objectiveOfTheScheme",
                label: "Objective of the Scheme",
                fieldType: "textarea",
                required: true,
              },
              {
                id: 3,
                name: "qualificationCriteria",
                label: "Qualification Criteria",
                fieldType: "textarea",
                required: true,
              },
            ],
          },
          {
            name: "Share Capital Base & Listing Data",
            fields: [
              {
                id: 1,
                name: "authorizedShareCapitalVolume",
                label: "Authorized Share Capital (Volume)",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 2,
                name: "authorizedShareCapitalValue",
                label: "Authorized Share Capital (Value)",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 3,
                name: "issuedAndFullyPaidVolume",
                label: "Issued and Fully Paid (Volume)",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                  {
                    operation: "Multiplication",
                    total: "PreOfferMarketCapitalization",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "marketPricePerShareAsAtDateOfApplication",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 4,
                name: "issuedAndFullyPaidValue",
                label: "Issued and Fully Paid (Value)",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                  {
                    operation: "Addition",
                    total: "PostOfferMarketCapitalization",
                    value: {
                      a: "issuedAndFullyPaidValue",
                      b: "ProposedOfferVolume",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 5,
                name: "nominalValue",
                label: "Nominal Value",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 6,
                name: "ProposedOfferVolume",
                label: "Proposed Offer (Volume)",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "ProposedOfferValue",
                    value: {
                      a: "ProposedOfferVolume",
                      b: "OfferPrice",
                    },
                  },
                  {
                    operation: "Addition",
                    total: "PostOfferMarketCapitalization",
                    value: {
                      a: "issuedAndFullyPaidValue",
                      b: "ProposedOfferVolume",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 7,
                name: "OfferPrice",
                label: "Offer Price",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "ProposedOfferValue",
                    value: {
                      a: "ProposedOfferVolume",
                      b: "OfferPrice",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 8,
                name: "ProposedOfferValue",
                label: "Proposed Offer (Value)",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "ProposedOfferValue",
                    value: {
                      a: "ProposedOfferVolume",
                      b: "OfferPrice",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 9,
                name: "marketPricePerShareAsAtDateOfApplication",
                label: "Market Price Per Share as at date of Application ",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "PreOfferMarketCapitalization ",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "marketPricePerShareAsAtDateOfApplication",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 10,
                name: "PreOfferMarketCapitalization",
                label: "Pre-Offer Market Capitalization",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "PreOfferMarketCapitalization",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "marketPricePerShareAsAtDateOfApplication",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 11,
                name: "PostOfferMarketCapitalization",
                label: "Post-Offer Market Capitalization",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Addition",
                    total: "PostOfferMarketCapitalization",
                    value: {
                      a: "issuedAndFullyPaidValue",
                      b: "ProposedOfferVolume",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 12,
                name: "allotmentBasis",
                label: "Allotment Basis (For a Rights Issue)",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 13,
                name: "shareCapitalBaseAndListingDataQualificationdate",
                label: "Qualification Date",
                fieldType: "date",
                required: false,
              },
              {
                id: 14,
                name: "marketCapitalisation",
                label: "Market Capitalisation",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "marketCapitalisation",
                    value: {
                      a: "postOfferSizeVolume",
                      b: "offerOrListingPrice",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 15,
                name: "board",
                label: "Board",
                fieldType: "dropdown",
                dropdownOutput: "string",
                required: true,
                options: ["Premium", "Main", "Growth – Entry Segment", "Growth – Standard Segment", "ASeM"],
              },
              {
                id: 16,
                label: "For Debt Conversion to Equity Transaction Type",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "dateOfBorrowing",
                    label: "Date of Borrowing",
                    fieldType: "date",
                    required: false,
                  },
                  {
                    id: 2,
                    name: "interestRateOfTheBondOrNote",
                    label: "Interest rate of the bond/note ",
                    fieldType: "input",
                    required: false,
                  },
                  {
                    id: 3,
                    name: "interestPaymentDates",
                    label: "Interest payment date(s)",
                    fieldType: "date",
                    required: false,
                  },
                  {
                    id: 4,
                    name: "maturityDate",
                    label: "Maturity Date",
                    fieldType: "date",
                    required: false,
                  },
                ],
              },
              {
                id: 17,
                label: "For Staff Share Based Payment Scheme Transaction Type",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "numberOfSharesInTheSchemeThatHasBeenPaidUp",
                    label:
                      "Number of Shares in the Scheme that has been Paid Up",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name: "numberOfStaffToWhomShareMayBeIssuedUnderTheScheme",
                    label:
                      "Number of Staff to whom share may be issued under the scheme",
                    format: "decimal",
                    fieldType: "number",
                    required: true,
                  },
                  {
                    id: 3,
                    name:
                      "numberOfStaffToWhomSharesAreAllottedWhereSharesHasAlreadyByAllotted",
                    label:
                      "Number of staff to whom shares are allotted (Where shares has already by allotted)",
                    format: "decimal",
                    fieldType: "number",
                    required: true,
                  },
                ],
              },
            ],
          },
          {
            name: "Utilization of the Proceeds (Public Offer)",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstUtilizationOfProceed",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "utilization",
                        label: "Utilization",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "amount",
                        label: "Amount (N)",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountInNairaSubtotal",
                            value: { a: "amount" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "percentOfGross",
                        label: "% of Gross proceed ",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfGrossProceedSubtotal",
                            value: { a: "percentOfGross" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "estimatedCompletionPeriod",
                        label: "Estimated Completion Period",
                        fieldType: "input",
                        logics: [
                          {
                            operation: "Summation",
                            total: "estimatedCompletionPeriodSubtotal",
                            value: { a: "estimatedCompletionPeriod" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Subtotal",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaSubtotal",
                        label: "Amount (N)",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountInNairaSubtotal",
                            value: { a: "amount" },
                          },
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedSubtotal",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfGrossProceedSubtotal",
                            value: { a: "percentOfGross" },
                          },
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodSubtotal",
                        label: "Estimated Completion Period",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "estimatedCompletionPeriodSubtotal",
                            value: { a: "estimatedCompletionPeriod" },
                          },
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "estimatedCompletionPeriodCostOfIssue",
                              b: "estimatedCompletionPeriodSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                  {
                    name: "Cost of Issue",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaCostOfIssue",
                        label: "Subtotal",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedCostOfIssue",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodCostOfIssue",
                        label: "Estimated Completion Period",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "estimatedCompletionPeriodCostOfIssue",
                              b: "estimatedCompletionPeriodSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                  {
                    name: "Grand Total",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaGrandTotal",
                        label: "Subtotal",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedGrandTotal",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodGrandTotal",
                        label: "Estimated Completion Period",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Outstanding Debt Obligation",
            fields: [
              {
                id: 1,
                name: "outstandingDebtObligation",
                label: "",
                description:
                  "Provide brief details of the issuer’s debt obligations, other than in the ordinary course of business",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Claims and Litigation",
            fields: [
              {
                id: 1,
                name: "totalNumberOfCasesInstitutedAgainstTheIssuer",
                label: "Total number of cases instituted against the Issuer",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Addition",
                    total: "totalNumberOfCasesInvolvedByTheIssuer",
                    value: {
                      a: "totalNumberOfCasesInstitutedAgainstTheIssuer",
                      b: "totalNumberOfCasesInstitutedByTheIssuer",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 2,
                name: "totalNumberOfCasesInstitutedByTheIssuer",
                label: "Total number of cases instituted by the Issuer",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Addition",
                    total: "totalNumberOfCasesInvolvedByTheIssuer",
                    value: {
                      a: "totalNumberOfCasesInstitutedAgainstTheIssuer",
                      b: "totalNumberOfCasesInstitutedByTheIssuer",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 3,
                name: "totalNumberOfCasesInvolvedByTheIssuer",
                label: "Total number of cases involved by the Issuer",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Addition",
                    total: "totalNumberOfCasesInvolvedByTheIssuer",
                    value: {
                      a: "totalNumberOfCasesInstitutedAgainstTheIssuer",
                      b: "totalNumberOfCasesInstitutedByTheIssuer",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 4,
                label: "Indebtedness",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "indebtednessValueInNaira",
                    format: "currency",
                    label: "Value (N)",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name: "indebtednessValueInOthercurrency",
                    format: "currency",
                    label: "Value (Other currency)",
                    currency: "non",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 5,
                label:
                  "Total amount claimed from the number of cases instituted against the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedAgainstTheIssuerInNaira",
                    label: "Value (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedAgainstTheIssuerInOthercurrency",
                    label: "Value (Other currency)",
                    currency: "non",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 6,
                label:
                  "Total amount claimed from the number of cases instituted by the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedByTheIssuerInNaira",
                    label: "Value (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedByTheIssuerInOthercurrency",
                    label: "Value (Other currency)",
                    currency: "non",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
            ],
          },
          {
            name: "Issuer’s Profile",
            fields: [
              {
                id: 1,
                name: "briefDescriptionOfIssuer",
                label: "Brief Description of Issuer",
                tooltip:
                  "The Issuer’s profile should include the date of incorporation, nature of business, industry.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Shareholding Structure of the Issuer",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstShareholdingStructure",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "nameOfShareholder",
                        label: "Name of Shareholder",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "noOfSharesHeldPreOfferOrMerger",
                        label: "Pre-Offer/Merger No of Shares Held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "noOfSharesHeldPreOfferOrMergerTotal",
                            value: { a: "noOfSharesHeldPreOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "percentageOfHoldingPreOfferOrMerger",
                        label: "Pre-Offer/Merger % of Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfHoldingPreOfferOrMergerTotal",
                            value: { a: "percentageOfHoldingPreOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "noOfSharesHeldPostOfferOrMerger",
                        label: "Post-Offer/Merger No of Shares Held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "noOfSharesHeldPostOfferOrMergerTotal",
                            value: { a: "noOfSharesHeldPostOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "percentageOfHoldingPostOfferOrMerger",
                        label: "Post-Offer/Mergerr % of Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfHoldingPostOfferOrMergerTotal",
                            value: {
                              a: "percentageOfHoldingPostOfferOrMerger",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Totals",
                    fields: [
                      {
                        id: 1,
                        name: "noOfSharesHeldPreOfferOrMergerTotal",
                        label: "Pre-Offer/Merger No of Shares Held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "noOfSharesHeldPreOfferOrMergerTotal",
                            value: { a: "noOfSharesHeldPreOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfHoldingPreOfferOrMergerTotal",
                        label: "Pre-Offer/Merger % of Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfHoldingPreOfferOrMergerTotal",
                            value: { a: "percentageOfHoldingPreOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "noOfSharesHeldPostOfferOrMergerTotal",
                        label: "Post-Offer/Merger No of Shares Held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "noOfSharesHeldPostOfferOrMergerTotal",
                            value: { a: "noOfSharesHeldPostOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "percentageOfHoldingPostOfferOrMergerTotal",
                        label: "Post-Offer/Merger % of Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfHoldingPostOfferOrMergerTotal",
                            value: {
                              a: "percentageOfHoldingPostOfferOrMerger",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Directors and Their Beneficial Interests",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstDirectorBeneficialInterest",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "nameOfDirector",
                        label: "Name of Directors",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "designation",
                        label: "Designation",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 3,
                        name: "directShareholding",
                        label: "Direct Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "directShareholdingTotal",
                            value: { a: "directShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "indirectHolding",
                        label: "Indirect Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "indirectHoldingTotal",
                            value: { a: "indirectHolding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 5,
                        name: "totalShareholding",
                        label: "Total Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "totalShareholdingTotal",
                            value: { a: "totalShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 6,
                        name: "percentageHolding",
                        label: "%tage held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageHeldTotal",
                            value: { a: "percentageHolding" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Totals",
                    fields: [
                      {
                        id: 1,
                        name: "directShareholdingTotal",
                        label: "Direct Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "directShareholdingTotal",
                            value: { a: "directShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "indirectHoldingTotal",
                        label: "Indirect Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "indirectHoldingTotal",
                            value: { a: "indirectHolding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "totalShareholdingTotal",
                        label: "Total Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "totalShareholdingTotal",
                            value: { a: "totalShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "percentageHeldTotal",
                        label: "%tage held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageHeldTotal",
                            value: { a: "percentageHolding" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Profile of the Other Companies (For Merger only)",
            fields: [
              {
                id: 1,
                name:
                  "provideABriefProfileOfTheOtherCompaniesInvolvedInTheMerger",
                label: "",
                description:
                  "Provide a brief profile of the other companies involved in the merger and names of their directors.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Material Contracts",
            fields: [
              {
                id: 1,
                name:
                  "provideAListOfTheAgreementsAndContractsWhichTheIssuerHasEnteredIntoMaterialContracts",
                label:
                  "Provide a list of the agreements and contracts which the Issuer has entered into that are material to offer and/or  listing or to its business, other than in the ordinary course of business.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          // {
          //   name: "Upload Supporting Documents",
          //   fields: [
          //     {
          //       id: 1,
          //       label:
          //         "Rights Circular/Placement Memorandum/Information Memorandum",
          //       fieldType: "file",
          //       required: true,
          //     },
          //     {
          //       id: 2,
          //       label: "Board Resolution",
          //       fieldType: "file",
          //       required: false,
          //     },
          //     {
          //       id: 3,
          //       label: "Shareholders Resolution",
          //       fieldType: "file",
          //       required: false,
          //     },
          //     {
          //       id: 4,
          //       label: "Other Documents",
          //       tooltip: "link for checklist",
          //       fieldType: "file",
          //       required: false,
          //     },
          //   ],
          // },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTradingLicenseHolderDeclaration",
                label:
                  "Managing Director of the Sponsoring Trading License Holder",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to the best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
      {
        id: 10,
        type: "LISTING OF FGN REOPENING",
        requiredDocuments: [
          {
            id: 1,
            name: "Allotment Result from DMO",
            document: "",
            file: null,
            required: true,
          },
          { id: 2, name: "Other Documents", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfIssuer",
                label: "Name of Issuer",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "symbolCodeOfSecurity",
                label: "Symbol Code of Security",
                fieldType: "input",
                required: false,
              },
              {
                id: 3,
                name: "isinOfSecurity",
                label: "ISIN of Security",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Parties to the Listing",
            fields: [
              {
                id: 1,
                name: "sponsoringTradingLicenseHolder",
                label: "Sponsoring Trading License Holder",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "jointSponsoringTradingLicenseHolders",
                label:
                  "Joint Sponsoring Trading License Holder (s) (should accommodate more than one firm)",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Listing Data",
            fields: [
              {
                id: 1,
                name: "currentUnitsOutstanding",
                label: "Current units outstanding",
                fieldType: "input",
                required: false,
              },
              {
                id: 2,
                name: "additionalUnitsToBeListed",
                label: "Additional units to be listed",
                fieldType: "input",
                required: false,
              },
              {
                id: 3,
                name: "postListingUnitsOutstanding",
                label: "Post listing units outstanding",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name:
              "Upload Supporting Document (Reference to be made to checklists)",
            fields: [
              {
                id: 1,
                label: "Allotment Result from DMO",
                fieldType: "file",
                required: true,
              },
              {
                id: 4,
                label: "Other Documents",
                tooltip: "link for checklist",
                fieldType: "file",
                required: false,
              },
            ],
          },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTradingLicenseHolderDeclaration",
                label:
                  "Managing Director of the Sponsoring Trading License Holder",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to the best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
      {
        id: 11,
        type: "LISTING OF FGN AND STATE BONDS",
        requiredDocuments: [
          {
            id: 1,
            name: "Prospectus/Pricing Supplement",
            document: "",
            file: null,
            required: true,
          },
          { id: 2, name: "Other Documents", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfIssuer",
                label: "Name of Issuer",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "applicationType",
                label: "Type of Application",
                fieldType: "dropdown",
                dropdownOutput: "object",
                required: true,
                options: [
                  {
                    label: "Initial Public Offering",
                    value: "Initial Public Offering",
                  },
                  {
                    label: "Listing by Introduction",
                    value: "Listing by Introduction",
                  },
                  {
                    label: "Merger and Acquisition",
                    value: "Merger and Acquisition",
                  },
                  { label: "Others" },
                ],
              },
              {
                id: 3,
                name: "description",
                label: "Description of Application",
                fieldType: "textarea",
                required: true,
              },
            ],
          },
          {
            name: "Parties to the Listing",
            fields: [
              {
                id: 1,
                name: "sponsoringTradingLicenseHolder",
                label: "Sponsoring Trading License Holder",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "jointSponsoringTradingLicenseHolders",
                label:
                  "Joint Sponsoring Trading License Holder (s) (should accommodate more than one firm)",
                fieldType: "textarea",
                required: false,
              },
              {
                id: 3,
                name: "issuingHouse",
                label: "Issuing House",
                fieldType: "input",
                required: false,
              },
              {
                id: 4,
                name: "jointIssuingHouses",
                label: "Joint Issuing House(s)",
                fieldType: "input",
                required: false,
              },
              {
                id: 5,
                name: "auditor",
                label: "Auditor",
                fieldType: "input",
                required: false,
              },
              {
                id: 6,
                name: "reportingAccountant",
                label: "Reporting Accountant",
                fieldType: "input",
                required: false,
              },
              {
                id: 7,
                name: "solicitorsToTheIssue",
                label: "Solicitor(s) to the Issue",
                fieldType: "input",
                required: false,
              },
              {
                id: 8,
                name: "registrar",
                label: "Registrars",
                fieldType: "input",
                required: false,
              },
              {
                id: 9,
                name: "trustees",
                label: "Trustees",
                fieldType: "input",
                required: false,
              },
              {
                id: 10,
                name: "receivingBanks",
                label: "Receiving Bank",
                fieldType: "input",
                required: false,
              },
              {
                id: 11,
                name: "ratingAgencies",
                label: "Rating Agencies",
                fieldType: "input",
                required: false,
              },
              {
                id: 12,
                name: "etc",
                label: "e.t.c",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Listing Data",
            fields: [
              {
                id: 1,
                name: "descriptionOfTheBond",
                label: "Description of the Bond",
                fieldType: "input",
                required: false,
              },
              {
                id: 2,
                name: "quantityVolume",
                label: "Quantity (Volume)",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 3,
                name: "parValueValue",
                label: "Par Value (Value) ",
                format: "currency",
                fieldType: "number",
                required: false,
              },
              {
                id: 4,
                name: "issuePriceValue",
                label: "Issue Price (Value)",
                format: "currency",
                fieldType: "number",
                required: false,
              },
              {
                id: 5,
                name: "unitsOfSaleVolume",
                label: "Units of Sale (Volume)",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 6,
                name: "methodOfOffer",
                label: "Method of Offer",
                fieldType: "input",
                required: false,
              },
              {
                id: 7,
                name: "tenorYears",
                label: "Tenor (Years)",
                fieldType: "input",
                required: false,
              },
              {
                id: 8,
                name: "seriesNumber",
                label: "Series number (Number) ",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 9,
                name: "allotmentOrIssueDate",
                label: "Allotment/Issue Date",
                fieldType: "date",
                required: false,
              },
              {
                id: 10,
                name: "offerOpens",
                label: "Offer Opens",
                fieldType: "input",
                required: false,
              },
              {
                id: 11,
                name: "offerCloses",
                label: "Offer Closes ",
                fieldType: "input",
                required: false,
              },
              {
                id: 12,
                name: "maturityDate",
                label: "Maturity Date ",
                fieldType: "date",
                required: false,
              },
              {
                id: 13,
                name: "couponRate",
                label: "Coupon Rate",
                fieldType: "input",
                required: false,
              },
              {
                id: 14,
                name: "couponPaymentDate",
                label: "Coupon payment date ",
                fieldType: "date",
                required: false,
              },
              {
                id: 15,
                name: "repaymentStructure",
                label: "Repayment Structure ",
                fieldType: "input",
                required: false,
              },
              {
                id: 16,
                name: "amortizingYesOrNo",
                label: "Amortizing (Yes or No)",
                fieldType: "input",
                required: false,
              },
              {
                id: 17,
                name: "theStockExchangesWhereTheIssuerIsListedIfAny",
                label:
                  "The stock exchange(s) where the Issuer is listed (if any)",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Purpose and Utilization of the Proceeds",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstUtilizationOfProceed",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "utilization",
                        label: "Utilization",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "amount",
                        label: "Amount (N)",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountInNairaSubtotal",
                            value: { a: "amount" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "percentOfGross",
                        label: "% of Gross proceed ",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfGrossProceedSubtotal",
                            value: { a: "percentOfGross" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "estimatedCompletionPeriod",
                        label: "Estimated Completion Period",
                        fieldType: "input",
                        logics: [
                          {
                            operation: "Summation",
                            total: "estimatedCompletionPeriodSubtotal",
                            value: { a: "estimatedCompletionPeriod" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Subtotal",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaSubtotal",
                        label: "Amount (N)",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountInNairaSubtotal",
                            value: { a: "amount" },
                          },
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedSubtotal",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfGrossProceedSubtotal",
                            value: { a: "percentOfGross" },
                          },
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodSubtotal",
                        label: "Estimated Completion Period",
                        fieldType: "input",
                        logics: [
                          {
                            operation: "Summation",
                            total: "estimatedCompletionPeriodSubtotal",
                            value: { a: "estimatedCompletionPeriod" },
                          },
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "estimatedCompletionPeriodCostOfIssue",
                              b: "estimatedCompletionPeriodSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                  {
                    name: "Cost of Issue",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaCostOfIssue",
                        label: "Amount (N)",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedCostOfIssue",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodCostOfIssue",
                        label: "Estimated Completion Period",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "estimatedCompletionPeriodCostOfIssue",
                              b: "estimatedCompletionPeriodSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                  {
                    name: "Grand Total",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaGrandTotal",
                        label: "Subtotal",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedGrandTotal",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodGrandTotal",
                        label: "Estimated Completion Period",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Outstanding Debt Obligation",
            fields: [
              {
                id: 1,
                name: "outstandingDebtObligation",
                label: "",
                description:
                  "(Provide brief details of the issuer’s current debt obligations such as other debt instruments issued in the past) ",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Claims and Litigation",
            fields: [
              {
                id: 1,
                label: "Cases instituted against the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    label: "Number",
                    fieldType: "number",
                    format: "decimal",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedAgainstTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 2,
                name: "totalNumberOfCasesInstitutedByTheIssuer",
                label: "Cases instituted by the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    label: "Number",
                    fieldType: "number",
                    format: "decimal",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedByTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 3,
                label: "Total number of cases involved by the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "totalNumberOfCasesInvolvedByTheIssuer",
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name: "totalNumberOfCasesInvolvedByTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 4,
                label: "Indebtedness",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "indebtednessValueInNaira",
                    label: "Value (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
            ],
          },
          {
            name: "Issuer’s Profile",
            fields: [
              {
                id: 1,
                name: "briefDescriptionOfIssuer",
                label: "Brief Description of Issuer",
                tooltip:
                  "The Issuer’s profile should include the date of incorporation, nature of business, industry.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Material Contracts",
            fields: [
              {
                id: 1,
                name:
                  "provideAListOfTheAgreementsAndContractsWhichTheIssuerHasEnteredIntoMaterialContracts",
                label:
                  "Provide a list of the agreements and contracts which the Issuer has entered into that are material to offer and/or  listing or to its business, other than in the ordinary course of business.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          // {
          //   name: "Upload Supporting Documents",
          //   fields: [
          //     {
          //       id: 1,
          //       label: "Prospectus/Pricing Supplement",
          //       fieldType: "file",
          //       required: true,
          //     },
          //     {
          //       id: 2,
          //       label: "Other Documents",
          //       tooltip: "link for checklist",
          //       fieldType: "file",
          //       required: false,
          //     },
          //   ],
          // },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTradingLicenseHolderDeclaration",
                label:
                  "Managing Director of the Sponsoring Trading License Holder",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to the best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
      {
        id: 12,
        type: "LISTING OF CORPORATE BOND",
        requiredDocuments: [
          {
            id: 1,
            name: "Prospectus/Pricing Supplement",
            document: "",
            file: null,
            required: true,
          },
          { id: 2, name: "Other Documents", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfIssuer",
                label: "Name of Issuer",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "applicationType",
                label: "Type of Application",
                fieldType: "dropdown",
                dropdownOutput: "object",
                required: true,
                options: [
                  {
                    label: "Initial Public Offering",
                    value: "Initial Public Offering",
                  },
                  {
                    label: "Listing by Introduction",
                    value: "Listing by Introduction",
                  },
                  {
                    label: "Merger and Acquisition",
                    value: "Merger and Acquisition",
                  },
                  { label: "Others" },
                ],
              },
              {
                id: 3,
                name: "description",
                label: "Description of Application",
                fieldType: "textarea",
                required: true,
              },
            ],
          },
          {
            name: "Parties to the Listing",
            fields: [
              {
                id: 1,
                name: "sponsoringTradingLicenseHolder",
                label: "Sponsoring Trading License Holder",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "jointSponsoringTradingLicenseHolders",
                label:
                  "Joint Sponsoring Trading License Holder (s) (should accommodate more than one firm)",
                fieldType: "textarea",
                required: false,
              },
              {
                id: 3,
                name: "issuingHouse",
                label: "Issuing House",
                fieldType: "input",
                required: false,
              },
              {
                id: 4,
                name: "jointIssuingHouses",
                label: "Joint Issuing House(s)",
                fieldType: "input",
                required: false,
              },
              {
                id: 5,
                name: "auditor",
                label: "Auditor",
                fieldType: "input",
                required: false,
              },
              {
                id: 6,
                name: "reportingAccountant",
                label: "Reporting Accountant",
                fieldType: "input",
                required: false,
              },
              {
                id: 7,
                name: "solicitorsToTheIssue",
                label: "Solicitor(s) to the Issue",
                fieldType: "input",
                required: false,
              },
              {
                id: 8,
                name: "registrar",
                label: "Registrars",
                fieldType: "input",
                required: false,
              },
              {
                id: 9,
                name: "trustees",
                label: "Trustees",
                fieldType: "input",
                required: false,
              },
              {
                id: 10,
                name: "receivingBanks",
                label: "Receiving Bank",
                fieldType: "input",
                required: false,
              },
              {
                id: 11,
                name: "ratingAgencies",
                label: "Rating Agencies",
                fieldType: "input",
                required: false,
              },
              {
                id: 12,
                name: "trustees",
                label: "e.t.c",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Share Capital Base of Issuer and Listing Data ",
            fields: [
              {
                id: 1,
                name: "authorizedShareCapitalVolume",
                label: "Authorized Share Capital (Volume)",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 2,
                name: "authorizedShareCapitalValue",
                label: "Authorized Share Capital (Value)",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 3,
                name: "issuedAndFullyPaidVolume",
                label: "Issued and Fully Paid (Volume)",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 4,
                name: "issuedAndFullyPaidValue",
                label: "Issued and Fully Paid (Value)",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 5,
                name: "nominalValue",
                label: "Nominal Value",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 6,
                name: "descriptionOfTheBond",
                label: "Description of the Bond",
                fieldType: "input",
                required: false,
              },
              {
                id: 7,
                name: "quantityVolume",
                label: "Quantity (Volume)",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 8,
                name: "parValueValue",
                label: "Par Value (Value)",
                format: "currency",
                fieldType: "number",
                required: false,
              },
              {
                id: 9,
                name: "issuePriceValue",
                label: "Issue Price (Value)",
                format: "currency",
                fieldType: "number",
                required: false,
              },
              {
                id: 10,
                name: "unitsOfSaleVolume",
                label: "Units of Sale (Volume)",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 11,
                name: "methodOfOffer",
                label: "Method of Offer",
                fieldType: "input",
                required: false,
              },
              {
                id: 12,
                name: "tenorYears",
                label: "Tenor (Years)",
                fieldType: "input",
                required: false,
              },
              {
                id: 13,
                name: "seriesNumber",
                label: "Series number (Number) ",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 14,
                name: "allotmentOrIssueDate",
                label: "Issue Date",
                fieldType: "date",
                required: false,
              },
              {
                id: 15,
                name: "offerOpens",
                label: "Offer Opens",
                fieldType: "input",
                required: false,
              },
              {
                id: 16,
                name: "offerCloses",
                label: "Offer Closes ",
                fieldType: "input",
                required: false,
              },
              {
                id: 17,
                name: "maturityDate",
                label: "Maturity Date ",
                fieldType: "date",
                required: false,
              },
              {
                id: 18,
                name: "couponRate",
                label: "Coupon Rate",
                fieldType: "input",
                required: false,
              },
              {
                id: 19,
                name: "couponPaymentDate",
                label: "Coupon payment date ",
                fieldType: "date",
                required: false,
              },
              {
                id: 20,
                name: "repaymentStructure",
                label: "Repayment Structure ",
                fieldType: "input",
                required: false,
              },
              {
                id: 21,
                name: "amortizingYesOrNo",
                label: "Amortizing (Yes or No)",
                fieldType: "input",
                required: false,
              },
              {
                id: 22,
                name: "theStockExchangesWhereTheIssuerIsListedIfAny",
                label:
                  "The stock exchange(s) where the Issuer is listed (if any)",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Purpose and Utilization of the Proceeds",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstUtilizationOfProceed",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "utilization",
                        label: "Utilization",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "amount",
                        label: "Amount (N)",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountInNairaSubtotal",
                            value: { a: "amount" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "percentOfGross",
                        label: "% of Gross proceed ",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfGrossProceedSubtotal",
                            value: { a: "percentOfGross" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "estimatedCompletionPeriod",
                        label: "Estimated Completion Period",
                        fieldType: "input",
                        logics: [
                          {
                            operation: "Summation",
                            total: "estimatedCompletionPeriodSubtotal",
                            value: { a: "estimatedCompletionPeriod" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Subtotal",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaSubtotal",
                        label: "Amount (N)",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountInNairaSubtotal",
                            value: { a: "amount" },
                          },
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedSubtotal",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfGrossProceedSubtotal",
                            value: { a: "percentOfGross" },
                          },
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodSubtotal",
                        label: "Estimated Completion Period",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "estimatedCompletionPeriodSubtotal",
                            value: { a: "estimatedCompletionPeriod" },
                          },
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "estimatedCompletionPeriodCostOfIssue",
                              b: "estimatedCompletionPeriodSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                  {
                    name: "Cost of Issue",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaCostOfIssue",
                        label: "Subtotal",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedCostOfIssue",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodCostOfIssue",
                        label: "Estimated Completion Period",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "estimatedCompletionPeriodCostOfIssue",
                              b: "estimatedCompletionPeriodSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                  {
                    name: "Grand Total",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaGrandTotal",
                        label: "Subtotal",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedGrandTotal",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodGrandTotal",
                        label: "Estimated Completion Period",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Outstanding Debt Obligation",
            fields: [
              {
                id: 1,
                name: "outstandingDebtObligation",
                label: "",
                description:
                  "(Provide brief details of the issuer’s current debt obligations such as other debt instruments issued in the past) ",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Claims and Litigation",
            fields: [
              {
                id: 1,
                label: "Cases instituted against the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedAgainstTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 2,
                name: "totalNumberOfCasesInstitutedByTheIssuer",
                label: "Cases instituted by the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedByTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 3,
                label: "Total number of cases involved by the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "totalNumberOfCasesInvolvedByTheIssuer",
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name: "totalNumberOfCasesInvolvedByTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 4,
                label: "Indebtedness",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "indebtednessValueInNaira",
                    label: "Value (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
            ],
          },
          {
            name: "Issuer’s Profile",
            fields: [
              {
                id: 1,
                name: "briefDescriptionOfIssuer",
                label: "Brief Description of Issuer",
                tooltip:
                  "The Issuer’s profile should include the date of incorporation, nature of business, industry.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Material Contracts",
            fields: [
              {
                id: 1,
                name:
                  "provideAListOfTheAgreementsAndContractsWhichTheIssuerHasEnteredIntoMaterialContracts",
                label:
                  "Provide a list of the agreements and contracts which the Issuer has entered into that are material to offer and/or  listing or to its business, other than in the ordinary course of business.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          // {
          //   name: "Upload Supporting Documents",
          //   fields: [
          //     {
          //       id: 1,
          //       label: "Prospectus/Pricing Supplement",
          //       fieldType: "file",
          //       required: true,
          //     },
          //     {
          //       id: 2,
          //       label: "Other Documents",
          //       tooltip: "link for checklist",
          //       fieldType: "file",
          //       required: false,
          //     },
          //   ],
          // },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTradingLicenseHolderDeclaration",
                label:
                  "Managing Director of the Sponsoring Trading License Holder",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to the best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
      {
        id: 13,
        type: "BLOCK DIVESTMENT",
        requiredDocuments: [
          {
            id: 1,
            name: "Information Memorandum",
            document: "",
            file: null,
            required: true,
          },
          { id: 2, name: "Other Documents", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfIssuer",
                label:
                  "Name of Issuer (whose shares are the subject of the transaction)",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "applicationType",
                label: "Type of Application",
                fieldType: "dropdown",
                dropdownOutput: "object",
                required: true,
                options: [
                  {
                    label: "Initial Public Offering",
                    value: "Initial Public Offering",
                  },
                  {
                    label: "Listing by Introduction",
                    value: "Listing by Introduction",
                  },
                  {
                    label: "Merger and Acquisition",
                    value: "Merger and Acquisition",
                  },
                  { label: "Others" },
                ],
              },
              {
                id: 3,
                name: "description",
                label: "Description of Application",
                fieldType: "textarea",
                required: true,
              },
            ],
          },
          {
            name: "Parties to the Listing",
            fields: [
              {
                id: 1,
                name: "tradingLicenseHolderToTheSeller",
                label: "Trading License Holder to the seller",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "tradingLicenseHolderToTheBuyers",
                label: "Trading License Holder to the buyers",
                fieldType: "textarea",
                required: false,
              },
              {
                id: 3,
                name: "nameOfSeller",
                label: "Name of seller",
                fieldType: "input",
                required: false,
              },
              {
                id: 4,
                name: "nameOfBuyer",
                label: "Name of buyer",
                fieldType: "input",
                required: false,
              },
              {
                id: 5,
                name: "blockDivestmentPrice",
                label: "Block divestment price",
                fieldType: "input",
                required: false,
              },
              {
                id: 6,
                name: "currentMarketPriceOfTheStock",
                label: "Current market price of the stock",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Issuer’s Profile",
            fields: [
              {
                id: 1,
                name: "briefDescriptionOfIssuer",
                label: "Brief Description of Issuer",
                tooltip:
                  "The Issuer’s profile should include the date of incorporation, nature of business, industry.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Share Capital of Issuer",
            fields: [
              {
                id: 1,
                name: "authorizedShareCapitalVolume",
                label: "Authorized Share Capital (Volume)",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 2,
                name: "authorizedShareCapitalValue",
                label: "Authorized Share Capital (Value)",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 3,
                name: "issuedAndFullyPaidVolume",
                label: "Issued and Fully Paid (Volume)",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 4,
                name: "issuedAndFullyPaidValue",
                label: "Issued and Fully Paid (Value)",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 5,
                name: "nominalValue",
                label: "Nominal Value",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: true,
              },
            ],
          },
          {
            name: "Shareholding Structure of the Issuer",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstShareholdingStructure",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "nameOfShareholder",
                        label: "Name of Shareholder",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "noOfSharesHeldPreOfferOrMerger",
                        label: "Pre-Offer/Merger No of Shares Held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "noOfSharesHeldPreOfferOrMergerTotal",
                            value: { a: "noOfSharesHeldPreOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "percentageOfHoldingPreOfferOrMerger",
                        label: "Pre-Offer/Merger % of Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfHoldingPreOfferOrMergerTotal",
                            value: { a: "percentageOfHoldingPreOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "noOfSharesHeldPostOfferOrMerger",
                        label: "Post-Offer/Merger No of Shares Held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "noOfSharesHeldPostOfferOrMergerTotal",
                            value: { a: "noOfSharesHeldPostOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "percentageOfHoldingPostOfferOrMerger",
                        label: "Post-Offer/Mergerr % of Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfHoldingPostOfferOrMergerTotal",
                            value: {
                              a: "percentageOfHoldingPostOfferOrMerger",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Totals",
                    fields: [
                      {
                        id: 1,
                        name: "noOfSharesHeldPreOfferOrMergerTotal",
                        label: "Pre-Offer/Merger No of Shares Held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "noOfSharesHeldPreOfferOrMergerTotal",
                            value: { a: "noOfSharesHeldPreOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfHoldingPreOfferOrMergerTotal",
                        label: "Pre-Offer/Merger % of Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfHoldingPreOfferOrMergerTotal",
                            value: { a: "percentageOfHoldingPreOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "noOfSharesHeldPostOfferOrMergerTotal",
                        label: "Post-Offer/Merger No of Shares Held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "noOfSharesHeldPostOfferOrMergerTotal",
                            value: { a: "noOfSharesHeldPostOfferOrMerger" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "percentageOfHoldingPostOfferOrMergerTotal",
                        label: "Post-Offer/Merger % of Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfHoldingPostOfferOrMergerTotal",
                            value: {
                              a: "percentageOfHoldingPostOfferOrMerger",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name:
              "Board of Directors of the Issuers and their Beneficial Interests",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstIssuerDirectorBeneficialInterest",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "nameOfDirector",
                        label: "Name of Directors",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "designation",
                        label: "Designation",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 3,
                        name: "directShareholding",
                        label: "Direct Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "directShareholdingTotal",
                            value: { a: "directShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "indirectHolding",
                        label: "Indirect Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "indirectHoldingTotal",
                            value: { a: "indirectHolding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 5,
                        name: "totalShareholding",
                        label: "Total Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "totalShareholdingTotal",
                            value: { a: "totalShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 6,
                        name: "percentageHolding",
                        label: "%tage held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageHeldTotal",
                            value: { a: "percentageHolding" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Totals",
                    fields: [
                      {
                        id: 1,
                        name: "directShareholdingTotal",
                        label: "Direct Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "directShareholdingTotal",
                            value: { a: "directShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "indirectHoldingTotal",
                        label: "Indirect Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "indirectHoldingTotal",
                            value: { a: "indirectHolding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "totalShareholdingTotal",
                        label: "Total Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "totalShareholdingTotal",
                            value: { a: "totalShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "percentageHeldTotal",
                        label: "%tage held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageHeldTotal",
                            value: { a: "percentageHolding" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Information on the Buyers",
            fields: [
              {
                id: 1,
                name: "briefDescriptionOfBuyer",
                label: "Brief Description of Buyer",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name:
              "Shareholding Structure of the Buyer (If an institutional body)",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstShareholdingStructureOfBuyer",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "nameOfShareHolder",
                        label: "Shareholders",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "noOfSharesHeld",
                        label: "Shares Held",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 3,
                        name: "percentageOfHolding",
                        label: "% Age Held",
                        fieldType: "input",
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Total",
                    fields: [
                      {
                        id: 1,
                        name: "noOfSharesHeldTotal",
                        label: "Shares Held",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfHoldingTotal",
                        label: "% Age Held",
                        fieldType: "input",
                        required: true,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name:
              "Board of Directors of the Buyer(s) and their Beneficial Interests",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstBuyerDirectorBeneficialInterest",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "nameOfDirector",
                        label: "Name of Directors",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "designation",
                        label: "Designation",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 3,
                        name: "directShareholding",
                        label: "Direct Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "directShareholdingTotal",
                            value: { a: "directShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "indirectHolding",
                        label: "Indirect Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "indirectHoldingTotal",
                            value: { a: "indirectHolding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 5,
                        name: "totalShareholding",
                        label: "Total Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "totalShareholdingTotal",
                            value: { a: "totalShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 6,
                        name: "percentageHolding",
                        label: "%tage held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageHeldTotal",
                            value: { a: "percentageHolding" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Totals",
                    fields: [
                      {
                        id: 1,
                        name: "directShareholdingTotal",
                        label: "Direct Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "directShareholdingTotal",
                            value: { a: "directShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "indirectHoldingTotal",
                        label: "Indirect Holding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "indirectHoldingTotal",
                            value: { a: "indirectHolding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "totalShareholdingTotal",
                        label: "Total Shareholding",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "totalShareholdingTotal",
                            value: { a: "totalShareholding" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "percentageHeldTotal",
                        label: "%tage held",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageHeldTotal",
                            value: { a: "percentageHolding" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          // {
          //   name: "Upload Supporting Documents",
          //   fields: [
          //     {
          //       id: 1,
          //       label: "Information Memorandum",
          //       fieldType: "file",
          //       required: true,
          //     },
          //     {
          //       id: 2,
          //       label: "Other Documents",
          //       tooltip: "link for checklist",
          //       fieldType: "file",
          //       required: false,
          //     },
          //   ],
          // },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTradingLicenseHolder",
                label: "Managing Director of Trading License Holder",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to the best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
      {
        id: 24,
        type:
          "INITIAL LISTING (OPEN-END FUND)",
        requiredDocuments: [
          {
            id: 1,
            name: "Prospectus",
            document: "",
            file: null,
            required: true,
          },
          {
            id: 2,
            name: "Listing Memorandum",
            document: "",
            file: null,
          },
          { id: 3, name: "Other Documents", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfFundManagerOrIssuer",
                label: "Name of Fund Manager/Issuer",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "listingMethodsByIntroductionOrPublicOffer",
                label: "Listing Methods (by introduction or Public Offer)",
                fieldType: "input",
                required: true,
              },
              {
                id: 3,
                name: "description",
                label: "Description of Application",
                fieldType: "textarea",
                required: true,
              },
            ],
          },
          {
            name: "Professional parties to the Listing",
            fields: [
              {
                id: 1,
                label: "Name",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "sponsoringTradingLicenseHolder",
                    label: "Sponsoring Trading License Holder ",
                    fieldType: "input",
                    required: false,
                  },
                  {
                    id: 2,
                    name: "nameOfFundManagerOrIssuer",
                    label: "Fund Manager/Issuer",
                    fieldType: "input",
                    required: false,
                  },
                  {
                    id: 3,
                    name: "issuingHouse",
                    label: "Issuing House",
                    fieldType: "input",
                    required: false,
                  },
                  {
                    id: 4,
                    name: "assetsManager",
                    label: "Assets Manager",
                    fieldType: "input",
                    required: false,
                  },
                ],
              },
            ],
          },
          {
            name: "Listing Data",
            fields: [
              {
                id: 1,
                name: "methodOfOffer",
                label: "Method of Offer",
                fieldType: "input",
                required: false,
              },
              {
                id: 2,
                name: "volumeBeingOffered",
                label: "Volume being offered",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "valueOfTheOffer",
                    value: { a: "volumeBeingOffered", b: "priceValue" },
                  },
                ],
                required: false,
              },
              {
                id: 3,
                name: "priceValue",
                label: "Price (Value)",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "valueOfTheOffer",
                    value: { a: "volumeBeingOffered", b: "priceValue" },
                  },
                ],
                required: false,
              },
              {
                id: 4,
                name: "valueOfTheOffer",
                label: "Value of the offer",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "valueOfTheOffer",
                    value: { a: "volumeBeingOffered", b: "priceValue" },
                  },
                ],
                required: false,
              },
              {
                id: 5,
                name: "minimumSubscription",
                label: "Minimum subscription",
                fieldType: "input",
                required: false,
              },
              {
                id: 6,
                name: "rating",
                label: "Rating",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Background information about the Fund",
            fields: [
              {
                id: 1,
                name: "provideBriefDetailsOfTheFund",
                label: "",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Proposed Utilisation of Funds",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstProposedUtilizationOfFunds",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "descriptionofProposedAsset",
                        label: "Description of Proposed Asset",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "amountAllocated",
                        label: "Amount Allocated (N)",
                        fieldType: "number",
                        format: "currency",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountAllocatedInNairaTotal",
                            value: { a: "amountAllocated" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "percentageOfTargetWeighting",
                        label: "Target Weighting (%)",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "targetWeightingPercentageTotal",
                            value: { a: "percentageOfTargetWeighting" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Totals",
                    fields: [
                      {
                        id: 1,
                        name: "amountAllocatedInNairaTotal",
                        label: "Amount Allocated (N)",
                        fieldType: "number",
                        format: "currency",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountAllocatedInNairaTotal",
                            value: { a: "amountAllocated" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "targetWeightingPercentageTotal",
                        label: "Target Weighting (%)",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "targetWeightingPercentageTotal",
                            value: { a: "percentageOfTargetWeighting" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Investment Objectives/Strategy and Policies of the Fund",
            fields: [
              {
                id: 1,
                name: "investmentObjectiveStrategyPolicyOfFund",
                label: "",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "About the Fund Manager",
            fields: [
              {
                id: 1,
                name: "briefDescriptionOfFundManager",
                label: "Brief description of Fund Manager",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Outstanding Debt Obligation",
            fields: [
              {
                id: 1,
                name:
                  "provideBriefDetailsOfTheFundManagersCurrentDebtObligationsOtherThanInTheOrdinaryCourseOfBusiness",
                description:
                  "(Provide brief details of the Fund Manager’s current debt obligations, other than in the ordinary course of business.)",
                label: "",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Claims, Litigation and Indebtedness",
            fields: [
              {
                id: 1,
                label: "Cases instituted against the Fund Manager",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "numberOfCasesInstitutedAgainstTheFundManager",
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "numberOfCasesInstitutedAgainstTheFundManagerValueClaimedInNaira",
                    format: "currency",
                    label: "Value Claimed (N)",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 2,
                label: "Cases instituted by the Fund Manager",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "numberOfCasesInstitutedByTheFundManager",
                    label: "Number",
                    fieldType: "input",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "numberOfCasesInstitutedByTheFundManagerValueClaimedInNaira",
                    format: "currency",
                    label: "Value Claimed (N)",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 3,
                label: "Cases involved by the Fund Manager",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "numberOfCasesInvolvedByTheFundManager",
                    label: "Number",
                    fieldType: "input",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "numberOfCasesInvolvedByTheFundManagerValueClaimedInNaira",
                    format: "currency",
                    label: "Value Claimed (N)",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 4,
                label: "Indebtedness",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "indebtednessValueInNaira",
                    format: "currency",
                    label: "Value (N)",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name: "indebtednessValueInOthercurrency",
                    label: "Value (Other currency)",
                    format: "currency",
                    currency: "non",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
            ],
          },
          {
            name: "Material Contracts",
            fields: [
              {
                id: 1,
                name:
                  "provideAListOfTheAgreementsAndContractsWhichTheIssuerHasEnteredIntoMaterialContracts",
                label:
                  "Provide a list of the agreements and contracts which the Fund Manager has entered into that are material to offer and/or  listing or to its business, other than in the ordinary course of business.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTheSponsoringTradingLicenseHolder",
                label:
                  "Managing Director of the Sponsoring Trading License Holder",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to be best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
      {
        id: 25,
        type:
          "INITIAL LISTING (CLOSED-END FUND)",
        requiredDocuments: [
          {
            id: 1,
            name: "Prospectus",
            document: "",
            file: null,
            required: true,
          },
          {
            id: 2,
            name: "Listing Memorandum",
            document: "",
            file: null,
          },
          { id: 3, name: "Other Documents", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfFundManagerOrIssuer",
                label: "Name of Fund Manager/Issuer",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "listingMethodsByIntroductionOrPublicOffer",
                label: "Listing Methods (by introduction or Public Offer)",
                fieldType: "input",
                required: true,
              },
              {
                id: 3,
                name: "description",
                label: "Description of Application",
                fieldType: "textarea",
                required: true,
              },
            ],
          },
          {
            name: "Professional parties to the Listing",
            fields: [
              {
                id: 1,
                label: "Name",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "sponsoringTradingLicenseHolder",
                    label: "Sponsoring Trading License Holder ",
                    fieldType: "input",
                    required: false,
                  },
                  {
                    id: 2,
                    name: "nameOfFundManagerOrIssuer",
                    label: "Fund Manager/Issuer",
                    fieldType: "input",
                    required: false,
                  },
                  {
                    id: 3,
                    name: "issuingHouse",
                    label: "Issuing House",
                    fieldType: "input",
                    required: false,
                  },
                  {
                    id: 4,
                    name: "assetsManager",
                    label: "Assets Manager",
                    fieldType: "input",
                    required: false,
                  },
                ],
              },
            ],
          },
          {
            name: "Listing Data",
            fields: [
              {
                id: 1,
                name: "methodOfOffer",
                label: "Method of Offer",
                fieldType: "input",
                required: false,
              },
              {
                id: 2,
                name: "volumeBeingOffered",
                label: "Volume being offered",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "valueOfTheOffer",
                    value: { a: "volumeBeingOffered", b: "priceValue" },
                  },
                ],
                required: false,
              },
              {
                id: 3,
                name: "priceValue",
                label: "Price (Value)",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "valueOfTheOffer",
                    value: { a: "volumeBeingOffered", b: "priceValue" },
                  },
                ],
                required: false,
              },
              {
                id: 4,
                name: "valueOfTheOffer",
                label: "Value of the offer",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "valueOfTheOffer",
                    value: { a: "volumeBeingOffered", b: "priceValue" },
                  },
                ],
                required: false,
              },
              {
                id: 5,
                name: "minimumSubscription",
                label: "Minimum subscription",
                fieldType: "input",
                required: false,
              },
              {
                id: 6,
                name: "rating",
                label: "Rating",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Background information about the Fund",
            fields: [
              {
                id: 1,
                name: "provideBriefDetailsOfTheFund",
                label: "",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Proposed Utilisation of Funds",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstProposedUtilizationOfFunds",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "descriptionofProposedAsset",
                        label: "Description of Proposed Asset",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "amountAllocated",
                        label: "Amount Allocated (N)",
                        fieldType: "number",
                        format: "currency",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountAllocatedInNairaTotal",
                            value: { a: "amountAllocated" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "percentageOfTargetWeighting",
                        label: "Target Weighting (%)",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "targetWeightingPercentageTotal",
                            value: { a: "percentageOfTargetWeighting" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Totals",
                    fields: [
                      {
                        id: 1,
                        name: "amountAllocatedInNairaTotal",
                        label: "Amount Allocated (N)",
                        fieldType: "number",
                        format: "currency",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountAllocatedInNairaTotal",
                            value: { a: "amountAllocated" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "targetWeightingPercentageTotal",
                        label: "Target Weighting (%)",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "targetWeightingPercentageTotal",
                            value: { a: "percentageOfTargetWeighting" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Investment Objectives/Strategy and Policies of the Fund",
            fields: [
              {
                id: 1,
                name: "investmentObjectiveStrategyPolicyOfFund",
                label: "",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "About the Fund Manager",
            fields: [
              {
                id: 1,
                name: "briefDescriptionOfFundManager",
                label: "Brief description of Fund Manager",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Outstanding Debt Obligation",
            fields: [
              {
                id: 1,
                name:
                  "provideBriefDetailsOfTheFundManagersCurrentDebtObligationsOtherThanInTheOrdinaryCourseOfBusiness",
                description:
                  "(Provide brief details of the Fund Manager’s current debt obligations, other than in the ordinary course of business.)",
                label: "",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Claims, Litigation and Indebtedness",
            fields: [
              {
                id: 1,
                label: "Cases instituted against the Fund Manager",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "numberOfCasesInstitutedAgainstTheFundManager",
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "numberOfCasesInstitutedAgainstTheFundManagerValueClaimedInNaira",
                    format: "currency",
                    label: "Value Claimed (N)",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 2,
                label: "Cases instituted by the Fund Manager",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "numberOfCasesInstitutedByTheFundManager",
                    label: "Number",
                    fieldType: "input",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "numberOfCasesInstitutedByTheFundManagerValueClaimedInNaira",
                    format: "currency",
                    label: "Value Claimed (N)",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 3,
                label: "Cases involved by the Fund Manager",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "numberOfCasesInvolvedByTheFundManager",
                    label: "Number",
                    fieldType: "input",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "numberOfCasesInvolvedByTheFundManagerValueClaimedInNaira",
                    format: "currency",
                    label: "Value Claimed (N)",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 4,
                label: "Indebtedness",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "indebtednessValueInNaira",
                    format: "currency",
                    label: "Value (N)",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name: "indebtednessValueInOthercurrency",
                    label: "Value (Other currency)",
                    format: "currency",
                    currency: "non",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
            ],
          },
          {
            name: "Material Contracts",
            fields: [
              {
                id: 1,
                name:
                  "provideAListOfTheAgreementsAndContractsWhichTheIssuerHasEnteredIntoMaterialContracts",
                label:
                  "Provide a list of the agreements and contracts which the Fund Manager has entered into that are material to offer and/or  listing or to its business, other than in the ordinary course of business.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTheSponsoringTradingLicenseHolder",
                label:
                  "Managing Director of the Sponsoring Trading License Holder",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to be best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
      {
        id: 26,
        type:
          "SUPPLEMENTARY LISTING (OPEN-END FUND)",
        requiredDocuments: [
          {
            id: 1,
            name:
              "Evidence of SEC registration of the additional units to be listed",
            document: "",
            file: null,
            required: true,
          },
          { id: 2, name: "Other Documents", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfFundManagerOrIssuer",
                label: "Name of Fund Manager/Issuer",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "nameOfFund",
                label: "Name of Fund",
                fieldType: "dropdown",
                dropdownOutput: "string",
                options: ["mutual fund", "ETFs", "REITs currently listed"],
                required: true,
              },
            ],
          },
          {
            name: "Professional parties to the Listing",
            fields: [
              {
                id: 1,
                label: "Name",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "sponsoringTradingLicenseHolder",
                    label: "Sponsoring Trading License Holder",
                    fieldType: "input",
                    required: false,
                  },
                ],
              },
            ],
          },
          {
            name: "Listing Data",
            fields: [
              {
                id: 1,
                name: "currentUnitsOutstanding",
                label: "Current units outstanding",
                fieldType: "input",
                required: false,
              },
              {
                id: 2,
                name: "additionalUnitsToBeListed",
                label: "Additional units to be listed",
                fieldType: "input",
                required: false,
              },
              {
                id: 3,
                name: "postListingUnitsOutstanding",
                label: "Post listing units outstanding",
                fieldType: "input",
                required: false,
              },
            ],
          },
          // {
          //   name: "Upload Supporting Documents",
          //   fields: [
          //     {
          //       id: 1,
          //       label:
          //         "Evidence of SEC registration of the additional units to be listed",
          //       fieldType: "file",
          //       required: true,
          //     },
          //     {
          //       id: 3,
          //       label: "Other Documents",
          //       fieldType: "file",
          //       required: false,
          //     },
          //   ],
          // },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTheSponsoringTradingLicenseHolder",
                label:
                  "Managing Director of the Sponsoring Trading License Holder",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to be best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
      {
        id: 27,
        type:
          "SUPPLEMENTARY LISTING (CLOSED-END FUND)",
        requiredDocuments: [
          {
            id: 1,
            name:
              "Evidence of SEC registration of the additional units to be listed",
            document: "",
            file: null,
            required: true,
          },
          { id: 2, name: "Other Documents", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfFundManagerOrIssuer",
                label: "Name of Fund Manager/Issuer",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "nameOfFund",
                label: "Name of Fund",
                fieldType: "dropdown",
                dropdownOutput: "string",
                options: ["mutual fund", "ETFs", "REITs currently listed"],
                required: true,
              },
            ],
          },
          {
            name: "Professional parties to the Listing",
            fields: [
              {
                id: 1,
                label: "Name",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "sponsoringTradingLicenseHolder",
                    label: "Sponsoring Trading License Holder",
                    fieldType: "input",
                    required: false,
                  },
                ],
              },
            ],
          },
          {
            name: "Listing Data",
            fields: [
              {
                id: 1,
                name: "currentUnitsOutstanding",
                label: "Current units outstanding",
                fieldType: "input",
                required: false,
              },
              {
                id: 2,
                name: "additionalUnitsToBeListed",
                label: "Additional units to be listed",
                fieldType: "input",
                required: false,
              },
              {
                id: 3,
                name: "postListingUnitsOutstanding",
                label: "Post listing units outstanding",
                fieldType: "input",
                required: false,
              },
            ],
          },
          // {
          //   name: "Upload Supporting Documents",
          //   fields: [
          //     {
          //       id: 1,
          //       label:
          //         "Evidence of SEC registration of the additional units to be listed",
          //       fieldType: "file",
          //       required: true,
          //     },
          //     {
          //       id: 3,
          //       label: "Other Documents",
          //       fieldType: "file",
          //       required: false,
          //     },
          //   ],
          // },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTheSponsoringTradingLicenseHolder",
                label:
                  "Managing Director of the Sponsoring Trading License Holder",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to be best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
      {
        id: 28,
        type: "INITIAL LISTING",
        requiredDocuments: [
          {
            id: 1,
            name: "Prospectus/Pricing Supplement",
            document: "",
            file: null,
            required: true,
          },
          { id: 2, name: "Other Documents", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfIssuer",
                label: "Name of Issuer",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "applicationType",
                label: "Type of Application",
                fieldType: "dropdown",
                dropdownOutput: "object",
                required: true,
                options: [
                  {
                    label: "Initial Public Offering",
                    value: "Initial Public Offering",
                  },
                  {
                    label: "Listing by Introduction",
                    value: "Listing by Introduction",
                  },
                  {
                    label: "Merger and Acquisition",
                    value: "Merger and Acquisition",
                  },
                  { label: "Others" },
                ],
              },
              {
                id: 3,
                name: "description",
                label: "Description of Application",
                fieldType: "textarea",
                required: true,
              },
            ],
          },
          {
            name: "Parties to the Listing",
            fields: [
              {
                id: 1,
                name: "sponsoringTradingLicenseHolder",
                label: "Sponsoring Trading License Holder",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "jointSponsoringTradingLicenseHolders",
                label:
                  "Joint Sponsoring Trading License Holder (s) (should accommodate more than one firm)",
                fieldType: "textarea",
                required: false,
              },
              {
                id: 3,
                name: "issuingHouse",
                label: "Issuing House",
                fieldType: "input",
                required: false,
              },
              {
                id: 4,
                name: "jointIssuingHouses",
                label: "Joint Issuing House(s)",
                fieldType: "input",
                required: false,
              },
              {
                id: 5,
                name: "auditor",
                label: "Auditor",
                fieldType: "input",
                required: false,
              },
              {
                id: 6,
                name: "reportingAccountant",
                label: "Reporting Accountant",
                fieldType: "input",
                required: false,
              },
              {
                id: 7,
                name: "solicitorsToTheIssue",
                label: "Solicitor(s) to the Issue",
                fieldType: "input",
                required: false,
              },
              {
                id: 8,
                name: "registrar",
                label: "Registrars",
                fieldType: "input",
                required: false,
              },
              {
                id: 9,
                name: "trustees",
                label: "Trustees",
                fieldType: "input",
                required: false,
              },
              {
                id: 10,
                name: "receivingBanks",
                label: "Receiving Bank",
                fieldType: "input",
                required: false,
              },
              {
                id: 11,
                name: "ratingAgencies",
                label: "Rating Agencies",
                fieldType: "input",
                required: false,
              },
              {
                id: 12,
                name: "trustees",
                label: "e.t.c",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Share Capital Base of Issuer and Listing Data ",
            fields: [
              {
                id: 1,
                name: "authorizedShareCapitalVolume",
                label: "Authorized Share Capital (Volume)",
                fieldType: "number",
                format: "decimal",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 2,
                name: "authorizedShareCapitalValue",
                label: "Authorized Share Capital (Value)",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 3,
                name: "issuedAndFullyPaidVolume",
                label: "Issued and Fully Paid (Volume)",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 4,
                name: "issuedAndFullyPaidValue",
                label: "Issued and Fully Paid (Value)",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 5,
                name: "nominalValue",
                label: "Nominal Value",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 6,
                name: "descriptionOfTheBond",
                label: "Description of the Bond",
                fieldType: "input",
                required: false,
              },
              {
                id: 7,
                name: "quantityVolume",
                label: "Quantity (Volume)",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 8,
                name: "parValueValue",
                label: "Par Value (Value)",
                format: "currency",
                fieldType: "number",
                required: false,
              },
              {
                id: 9,
                name: "issuePriceValue",
                label: "Issue Price (Value)",
                format: "currency",
                fieldType: "number",
                required: false,
              },
              {
                id: 10,
                name: "unitsOfSaleVolume",
                label: "Units of Sale (Volume)",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 11,
                name: "methodOfOffer",
                label: "Method of Offer",
                fieldType: "input",
                required: false,
              },
              {
                id: 12,
                name: "tenorYears",
                label: "Tenor (Years)",
                fieldType: "input",
                required: false,
              },
              {
                id: 13,
                name: "seriesNumber",
                label: "Series number (Number) ",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 14,
                name: "allotmentOrIssueDate",
                label: "Issue Date",
                fieldType: "date",
                required: false,
              },
              {
                id: 15,
                name: "offerOpens",
                label: "Offer Opens",
                fieldType: "input",
                required: false,
              },
              {
                id: 16,
                name: "offerCloses",
                label: "Offer Closes ",
                fieldType: "input",
                required: false,
              },
              {
                id: 17,
                name: "maturityDate",
                label: "Maturity Date ",
                fieldType: "date",
                required: false,
              },
              {
                id: 18,
                name: "couponRate",
                label: "Coupon Rate",
                fieldType: "input",
                required: false,
              },
              {
                id: 19,
                name: "couponPaymentDate",
                label: "Coupon payment date ",
                fieldType: "date",
                required: false,
              },
              {
                id: 20,
                name: "repaymentStructure",
                label: "Repayment Structure ",
                fieldType: "input",
                required: false,
              },
              {
                id: 21,
                name: "amortizingYesOrNo",
                label: "Amortizing (Yes or No)",
                fieldType: "input",
                required: false,
              },
              {
                id: 22,
                name: "theStockExchangesWhereTheIssuerIsListedIfAny",
                label:
                  "The stock exchange(s) where the Issuer is listed (if any)",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Purpose and Utilization of the Proceeds",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstUtilizationOfProceed",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "utilization",
                        label: "Utilization",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "amount",
                        label: "Amount (N)",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountInNairaSubtotal",
                            value: { a: "amount" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "percentOfGross",
                        label: "% of Gross proceed ",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfGrossProceedSubtotal",
                            value: { a: "percentOfGross" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "estimatedCompletionPeriod",
                        label: "Estimated Completion Period",
                        fieldType: "input",
                        logics: [
                          {
                            operation: "Summation",
                            total: "estimatedCompletionPeriodSubtotal",
                            value: { a: "estimatedCompletionPeriod" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Subtotal",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaSubtotal",
                        label: "Amount (N)",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountInNairaSubtotal",
                            value: { a: "amount" },
                          },
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedSubtotal",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfGrossProceedSubtotal",
                            value: { a: "percentOfGross" },
                          },
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodSubtotal",
                        label: "Estimated Completion Period",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "estimatedCompletionPeriodSubtotal",
                            value: { a: "estimatedCompletionPeriod" },
                          },
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "estimatedCompletionPeriodCostOfIssue",
                              b: "estimatedCompletionPeriodSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                  {
                    name: "Cost of Issue",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaCostOfIssue",
                        label: "Subtotal",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedCostOfIssue",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodCostOfIssue",
                        label: "Estimated Completion Period",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "estimatedCompletionPeriodCostOfIssue",
                              b: "estimatedCompletionPeriodSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                  {
                    name: "Grand Total",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaGrandTotal",
                        label: "Subtotal",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedGrandTotal",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodGrandTotal",
                        label: "Estimated Completion Period",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Outstanding Debt Obligation",
            fields: [
              {
                id: 1,
                name: "outstandingDebtObligation",
                label: "",
                description:
                  "(Provide brief details of the issuer’s current debt obligations such as other debt instruments issued in the past) ",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Claims and Litigation",
            fields: [
              {
                id: 1,
                label: "Cases instituted against the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedAgainstTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 2,
                name: "totalNumberOfCasesInstitutedByTheIssuer",
                label: "Cases instituted by the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedByTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 3,
                label: "Total number of cases involved by the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "totalNumberOfCasesInvolvedByTheIssuer",
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name: "totalNumberOfCasesInvolvedByTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 4,
                label: "Indebtedness",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "indebtednessValueInNaira",
                    label: "Value (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
            ],
          },
          {
            name: "Issuer’s Profile",
            fields: [
              {
                id: 1,
                name: "briefDescriptionOfIssuer",
                label: "Brief Description of Issuer",
                tooltip:
                  "The Issuer’s profile should include the date of incorporation, nature of business, industry.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Material Contracts",
            fields: [
              {
                id: 1,
                name:
                  "provideAListOfTheAgreementsAndContractsWhichTheIssuerHasEnteredIntoMaterialContracts",
                label:
                  "Provide a list of the agreements and contracts which the Issuer has entered into that are material to offer and/or  listing or to its business, other than in the ordinary course of business.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          // {
          //   name: "Upload Supporting Documents",
          //   fields: [
          //     {
          //       id: 1,
          //       label: "Prospectus/Pricing Supplement",
          //       fieldType: "file",
          //       required: true,
          //     },
          //     {
          //       id: 2,
          //       label: "Other Documents",
          //       tooltip: "link for checklist",
          //       fieldType: "file",
          //       required: false,
          //     },
          //   ],
          // },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTradingLicenseHolderDeclaration",
                label:
                  "Managing Director of the Sponsoring Trading License Holder",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to the best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
      {
        id: 29,
        type: "SUPPLEMENTARY LISTING",
        requiredDocuments: [
          {
            id: 1,
            name: "Prospectus/Pricing Supplement",
            document: "",
            file: null,
            required: true,
          },
          { id: 2, name: "Other Documents", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfIssuer",
                label: "Name of Issuer",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "applicationType",
                label: "Type of Application",
                fieldType: "dropdown",
                dropdownOutput: "object",
                required: true,
                options: [
                  {
                    label: "Initial Public Offering",
                    value: "Initial Public Offering",
                  },
                  {
                    label: "Listing by Introduction",
                    value: "Listing by Introduction",
                  },
                  {
                    label: "Merger and Acquisition",
                    value: "Merger and Acquisition",
                  },
                  { label: "Others" },
                ],
              },
              {
                id: 3,
                name: "description",
                label: "Description of Application",
                fieldType: "textarea",
                required: true,
              },
            ],
          },
          {
            name: "Parties to the Listing",
            fields: [
              {
                id: 1,
                name: "sponsoringTradingLicenseHolder",
                label: "Sponsoring Trading License Holder",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "jointSponsoringTradingLicenseHolders",
                label:
                  "Joint Sponsoring Trading License Holder (s) (should accommodate more than one firm)",
                fieldType: "textarea",
                required: false,
              },
              {
                id: 3,
                name: "issuingHouse",
                label: "Issuing House",
                fieldType: "input",
                required: false,
              },
              {
                id: 4,
                name: "jointIssuingHouses",
                label: "Joint Issuing House(s)",
                fieldType: "input",
                required: false,
              },
              {
                id: 5,
                name: "auditor",
                label: "Auditor",
                fieldType: "input",
                required: false,
              },
              {
                id: 6,
                name: "reportingAccountant",
                label: "Reporting Accountant",
                fieldType: "input",
                required: false,
              },
              {
                id: 7,
                name: "solicitorsToTheIssue",
                label: "Solicitor(s) to the Issue",
                fieldType: "input",
                required: false,
              },
              {
                id: 8,
                name: "registrar",
                label: "Registrars",
                fieldType: "input",
                required: false,
              },
              {
                id: 9,
                name: "trustees",
                label: "Trustees",
                fieldType: "input",
                required: false,
              },
              {
                id: 10,
                name: "receivingBanks",
                label: "Receiving Bank",
                fieldType: "input",
                required: false,
              },
              {
                id: 11,
                name: "ratingAgencies",
                label: "Rating Agencies",
                fieldType: "input",
                required: false,
              },
              {
                id: 12,
                name: "trustees",
                label: "e.t.c",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Share Capital Base of Issuer and Listing Data ",
            fields: [
              {
                id: 1,
                name: "authorizedShareCapitalVolume",
                label: "Authorized Share Capital (Volume)",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 2,
                name: "authorizedShareCapitalValue",
                label: "Authorized Share Capital (Value)",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: false,
              },
              {
                id: 3,
                name: "issuedAndFullyPaidVolume",
                label: "Issued and Fully Paid (Volume)",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 4,
                name: "issuedAndFullyPaidValue",
                label: "Issued and Fully Paid (Value)",
                format: "currency",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 5,
                name: "nominalValue",
                label: "Nominal Value",
                format: "decimal",
                fieldType: "number",
                logics: [
                  {
                    operation: "Multiplication",
                    total: "authorizedShareCapitalValue",
                    value: {
                      a: "authorizedShareCapitalVolume",
                      b: "nominalValue",
                    },
                  },
                  {
                    operation: "Multiplication",
                    total: "issuedAndFullyPaidValue",
                    value: {
                      a: "issuedAndFullyPaidVolume",
                      b: "nominalValue",
                    },
                  },
                ],
                required: true,
              },
              {
                id: 6,
                name: "descriptionOfTheBond",
                label: "Description of the Bond",
                fieldType: "input",
                required: false,
              },
              {
                id: 7,
                name: "quantityVolume",
                label: "Quantity (Volume)",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 8,
                name: "parValueValue",
                label: "Par Value (Value)",
                format: "currency",
                fieldType: "number",
                required: false,
              },
              {
                id: 9,
                name: "issuePriceValue",
                label: "Issue Price (Value)",
                format: "currency",
                fieldType: "number",
                required: false,
              },
              {
                id: 10,
                name: "unitsOfSaleVolume",
                label: "Units of Sale (Volume)",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 11,
                name: "methodOfOffer",
                label: "Method of Offer",
                fieldType: "input",
                required: false,
              },
              {
                id: 12,
                name: "tenorYears",
                label: "Tenor (Years)",
                fieldType: "input",
                required: false,
              },
              {
                id: 13,
                name: "seriesNumber",
                label: "Series number (Number) ",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 14,
                name: "allotmentOrIssueDate",
                label: "Issue Date",
                fieldType: "date",
                required: false,
              },
              {
                id: 15,
                name: "offerOpens",
                label: "Offer Opens",
                fieldType: "input",
                required: false,
              },
              {
                id: 16,
                name: "offerCloses",
                label: "Offer Closes ",
                fieldType: "input",
                required: false,
              },
              {
                id: 17,
                name: "maturityDate",
                label: "Maturity Date ",
                fieldType: "date",
                required: false,
              },
              {
                id: 18,
                name: "couponRate",
                label: "Coupon Rate",
                fieldType: "input",
                required: false,
              },
              {
                id: 19,
                name: "couponPaymentDate",
                label: "Coupon payment date ",
                fieldType: "date",
                required: false,
              },
              {
                id: 20,
                name: "repaymentStructure",
                label: "Repayment Structure ",
                fieldType: "input",
                required: false,
              },
              {
                id: 21,
                name: "amortizingYesOrNo",
                label: "Amortizing (Yes or No)",
                fieldType: "input",
                required: false,
              },
              {
                id: 22,
                name: "theStockExchangesWhereTheIssuerIsListedIfAny",
                label:
                  "The stock exchange(s) where the Issuer is listed (if any)",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Purpose and Utilization of the Proceeds",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstUtilizationOfProceed",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "utilization",
                        label: "Utilization",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "amount",
                        label: "Amount (N)",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountInNairaSubtotal",
                            value: { a: "amount" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "percentOfGross",
                        label: "% of Gross proceed ",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfGrossProceedSubtotal",
                            value: { a: "percentOfGross" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "estimatedCompletionPeriod",
                        label: "Estimated Completion Period",
                        fieldType: "input",
                        logics: [
                          {
                            operation: "Summation",
                            total: "estimatedCompletionPeriodSubtotal",
                            value: { a: "estimatedCompletionPeriod" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Subtotal",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaSubtotal",
                        label: "Amount (N)",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountInNairaSubtotal",
                            value: { a: "amount" },
                          },
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedSubtotal",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfGrossProceedSubtotal",
                            value: { a: "percentOfGross" },
                          },
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodSubtotal",
                        label: "Estimated Completion Period",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "estimatedCompletionPeriodSubtotal",
                            value: { a: "estimatedCompletionPeriod" },
                          },
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "estimatedCompletionPeriodCostOfIssue",
                              b: "estimatedCompletionPeriodSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                  {
                    name: "Cost of Issue",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaCostOfIssue",
                        label: "Subtotal",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedCostOfIssue",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodCostOfIssue",
                        label: "Estimated Completion Period",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "estimatedCompletionPeriodCostOfIssue",
                              b: "estimatedCompletionPeriodSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                  {
                    name: "Grand Total",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaGrandTotal",
                        label: "Subtotal",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedGrandTotal",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodGrandTotal",
                        label: "Estimated Completion Period",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Outstanding Debt Obligation",
            fields: [
              {
                id: 1,
                name: "outstandingDebtObligation",
                label: "",
                description:
                  "(Provide brief details of the issuer’s current debt obligations such as other debt instruments issued in the past) ",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Claims and Litigation",
            fields: [
              {
                id: 1,
                label: "Cases instituted against the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedAgainstTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 2,
                name: "totalNumberOfCasesInstitutedByTheIssuer",
                label: "Cases instituted by the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedByTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 3,
                label: "Total number of cases involved by the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "totalNumberOfCasesInvolvedByTheIssuer",
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name: "totalNumberOfCasesInvolvedByTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 4,
                label: "Indebtedness",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "indebtednessValueInNaira",
                    label: "Value (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
            ],
          },
          {
            name: "Issuer’s Profile",
            fields: [
              {
                id: 1,
                name: "briefDescriptionOfIssuer",
                label: "Brief Description of Issuer",
                tooltip:
                  "The Issuer’s profile should include the date of incorporation, nature of business, industry.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Material Contracts",
            fields: [
              {
                id: 1,
                name:
                  "provideAListOfTheAgreementsAndContractsWhichTheIssuerHasEnteredIntoMaterialContracts",
                label:
                  "Provide a list of the agreements and contracts which the Issuer has entered into that are material to offer and/or  listing or to its business, other than in the ordinary course of business.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          // {
          //   name: "Upload Supporting Documents",
          //   fields: [
          //     {
          //       id: 1,
          //       label: "Prospectus/Pricing Supplement",
          //       fieldType: "file",
          //       required: true,
          //     },
          //     {
          //       id: 2,
          //       label: "Other Documents",
          //       tooltip: "link for checklist",
          //       fieldType: "file",
          //       required: false,
          //     },
          //   ],
          // },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTradingLicenseHolderDeclaration",
                label:
                  "Managing Director of the Sponsoring Trading License Holder",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to the best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
      {
        id: 31,
        type: "INITIAL LISTING OF FEDERAL GOVERNMENT BONDS",
        requiredDocuments: [
          {
            id: 1,
            name: "Prospectus/Pricing Supplement",
            document: "",
            file: null,
            required: true,
          },
          { id: 2, name: "Other Documents", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfIssuer",
                label: "Name of Issuer",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "applicationType",
                label: "Type of Application",
                fieldType: "dropdown",
                dropdownOutput: "object",
                required: true,
                options: [
                  {
                    label: "Initial Public Offering",
                    value: "Initial Public Offering",
                  },
                  {
                    label: "Listing by Introduction",
                    value: "Listing by Introduction",
                  },
                  {
                    label: "Merger and Acquisition",
                    value: "Merger and Acquisition",
                  },
                  { label: "Others" },
                ],
              },
              {
                id: 3,
                name: "description",
                label: "Description of Application",
                fieldType: "textarea",
                required: true,
              },
            ],
          },
          {
            name: "Parties to the Listing",
            fields: [
              {
                id: 1,
                name: "sponsoringTradingLicenseHolder",
                label: "Sponsoring Trading License Holder",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "jointSponsoringTradingLicenseHolders",
                label:
                  "Joint Sponsoring Trading License Holder (s) (should accommodate more than one firm)",
                fieldType: "textarea",
                required: false,
              },
              {
                id: 3,
                name: "issuingHouse",
                label: "Issuing House",
                fieldType: "input",
                required: false,
              },
              {
                id: 4,
                name: "jointIssuingHouses",
                label: "Joint Issuing House(s)",
                fieldType: "input",
                required: false,
              },
              {
                id: 5,
                name: "auditor",
                label: "Auditor",
                fieldType: "input",
                required: false,
              },
              {
                id: 6,
                name: "reportingAccountant",
                label: "Reporting Accountant",
                fieldType: "input",
                required: false,
              },
              {
                id: 7,
                name: "solicitorsToTheIssue",
                label: "Solicitor(s) to the Issue",
                fieldType: "input",
                required: false,
              },
              {
                id: 8,
                name: "registrar",
                label: "Registrars",
                fieldType: "input",
                required: false,
              },
              {
                id: 9,
                name: "trustees",
                label: "Trustees",
                fieldType: "input",
                required: false,
              },
              {
                id: 10,
                name: "receivingBanks",
                label: "Receiving Bank",
                fieldType: "input",
                required: false,
              },
              {
                id: 11,
                name: "ratingAgencies",
                label: "Rating Agencies",
                fieldType: "input",
                required: false,
              },
              {
                id: 12,
                name: "etc",
                label: "e.t.c",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Listing Data",
            fields: [
              {
                id: 1,
                name: "descriptionOfTheBond",
                label: "Description of the Bond",
                fieldType: "input",
                required: false,
              },
              {
                id: 2,
                name: "quantityVolume",
                label: "Quantity (Volume)",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 3,
                name: "parValueValue",
                label: "Par Value (Value) ",
                format: "currency",
                fieldType: "number",
                required: false,
              },
              {
                id: 4,
                name: "issuePriceValue",
                label: "Issue Price (Value)",
                format: "currency",
                fieldType: "number",
                required: false,
              },
              {
                id: 5,
                name: "unitsOfSaleVolume",
                label: "Units of Sale (Volume)",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 6,
                name: "methodOfOffer",
                label: "Method of Offer",
                fieldType: "input",
                required: false,
              },
              {
                id: 7,
                name: "tenorYears",
                label: "Tenor (Years)",
                fieldType: "input",
                required: false,
              },
              {
                id: 8,
                name: "seriesNumber",
                label: "Series number (Number) ",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 9,
                name: "allotmentOrIssueDate",
                label: "Allotment/Issue Date",
                fieldType: "date",
                required: false,
              },
              {
                id: 10,
                name: "offerOpens",
                label: "Offer Opens",
                fieldType: "input",
                required: false,
              },
              {
                id: 11,
                name: "offerCloses",
                label: "Offer Closes ",
                fieldType: "input",
                required: false,
              },
              {
                id: 12,
                name: "maturityDate",
                label: "Maturity Date ",
                fieldType: "date",
                required: false,
              },
              {
                id: 13,
                name: "couponRate",
                label: "Coupon Rate",
                fieldType: "input",
                required: false,
              },
              {
                id: 14,
                name: "couponPaymentDate",
                label: "Coupon payment date ",
                fieldType: "date",
                required: false,
              },
              {
                id: 15,
                name: "repaymentStructure",
                label: "Repayment Structure ",
                fieldType: "input",
                required: false,
              },
              {
                id: 16,
                name: "amortizingYesOrNo",
                label: "Amortizing (Yes or No)",
                fieldType: "input",
                required: false,
              },
              {
                id: 17,
                name: "theStockExchangesWhereTheIssuerIsListedIfAny",
                label:
                  "The stock exchange(s) where the Issuer is listed (if any)",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Purpose and Utilization of the Proceeds",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstUtilizationOfProceed",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "utilization",
                        label: "Utilization",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "amount",
                        label: "Amount (N)",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountInNairaSubtotal",
                            value: { a: "amount" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "percentOfGross",
                        label: "% of Gross proceed ",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfGrossProceedSubtotal",
                            value: { a: "percentOfGross" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "estimatedCompletionPeriod",
                        label: "Estimated Completion Period",
                        fieldType: "input",
                        logics: [
                          {
                            operation: "Summation",
                            total: "estimatedCompletionPeriodSubtotal",
                            value: { a: "estimatedCompletionPeriod" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Subtotal",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaSubtotal",
                        label: "Amount (N)",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountInNairaSubtotal",
                            value: { a: "amount" },
                          },
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedSubtotal",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfGrossProceedSubtotal",
                            value: { a: "percentOfGross" },
                          },
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodSubtotal",
                        label: "Estimated Completion Period",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "estimatedCompletionPeriodSubtotal",
                            value: { a: "estimatedCompletionPeriod" },
                          },
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "estimatedCompletionPeriodCostOfIssue",
                              b: "estimatedCompletionPeriodSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                  {
                    name: "Cost of Issue",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaCostOfIssue",
                        label: "Amount (N)",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedCostOfIssue",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodCostOfIssue",
                        label: "Estimated Completion Period",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "estimatedCompletionPeriodCostOfIssue",
                              b: "estimatedCompletionPeriodSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                  {
                    name: "Grand Total",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaGrandTotal",
                        label: "Subtotal",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedGrandTotal",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodGrandTotal",
                        label: "Estimated Completion Period",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Outstanding Debt Obligation",
            fields: [
              {
                id: 1,
                name: "outstandingDebtObligation",
                label: "",
                description:
                  "(Provide brief details of the issuer’s current debt obligations such as other debt instruments issued in the past) ",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Claims and Litigation",
            fields: [
              {
                id: 1,
                label: "Cases instituted against the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedAgainstTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 2,
                name: "totalNumberOfCasesInstitutedByTheIssuer",
                label: "Cases instituted by the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedByTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 3,
                label: "Total number of cases involved by the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "totalNumberOfCasesInvolvedByTheIssuer",
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name: "totalNumberOfCasesInvolvedByTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 4,
                label: "Indebtedness",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "indebtednessValueInNaira",
                    label: "Value (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
            ],
          },
          {
            name: "Issuer’s Profile",
            fields: [
              {
                id: 1,
                name: "briefDescriptionOfIssuer",
                label: "Brief Description of Issuer",
                tooltip:
                  "The Issuer’s profile should include the date of incorporation, nature of business, industry.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Material Contracts",
            fields: [
              {
                id: 1,
                name:
                  "provideAListOfTheAgreementsAndContractsWhichTheIssuerHasEnteredIntoMaterialContracts",
                label:
                  "Provide a list of the agreements and contracts which the Issuer has entered into that are material to offer and/or  listing or to its business, other than in the ordinary course of business.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTradingLicenseHolderDeclaration",
                label:
                  "Managing Director of the Sponsoring Trading License Holder",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to the best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
      {
        id: 34,
        type: "INITIAL LISTING OF STATE GOVERNMENT BONDS",
        requiredDocuments: [
          {
            id: 1,
            name: "Prospectus/Pricing Supplement",
            document: "",
            file: null,
            required: true,
          },
          { id: 2, name: "Other Documents", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfIssuer",
                label: "Name of Issuer",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "applicationType",
                label: "Type of Application",
                fieldType: "dropdown",
                dropdownOutput: "object",
                required: true,
                options: [
                  {
                    label: "Initial Public Offering",
                    value: "Initial Public Offering",
                  },
                  {
                    label: "Listing by Introduction",
                    value: "Listing by Introduction",
                  },
                  {
                    label: "Merger and Acquisition",
                    value: "Merger and Acquisition",
                  },
                  { label: "Others" },
                ],
              },
              {
                id: 3,
                name: "description",
                label: "Description of Application",
                fieldType: "textarea",
                required: true,
              },
            ],
          },
          {
            name: "Parties to the Listing",
            fields: [
              {
                id: 1,
                name: "sponsoringTradingLicenseHolder",
                label: "Sponsoring Trading License Holder",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "jointSponsoringTradingLicenseHolders",
                label:
                  "Joint Sponsoring Trading License Holder (s) (should accommodate more than one firm)",
                fieldType: "textarea",
                required: false,
              },
              {
                id: 3,
                name: "issuingHouse",
                label: "Issuing House",
                fieldType: "input",
                required: false,
              },
              {
                id: 4,
                name: "jointIssuingHouses",
                label: "Joint Issuing House(s)",
                fieldType: "input",
                required: false,
              },
              {
                id: 5,
                name: "auditor",
                label: "Auditor",
                fieldType: "input",
                required: false,
              },
              {
                id: 6,
                name: "reportingAccountant",
                label: "Reporting Accountant",
                fieldType: "input",
                required: false,
              },
              {
                id: 7,
                name: "solicitorsToTheIssue",
                label: "Solicitor(s) to the Issue",
                fieldType: "input",
                required: false,
              },
              {
                id: 8,
                name: "registrar",
                label: "Registrars",
                fieldType: "input",
                required: false,
              },
              {
                id: 9,
                name: "trustees",
                label: "Trustees",
                fieldType: "input",
                required: false,
              },
              {
                id: 10,
                name: "receivingBanks",
                label: "Receiving Bank",
                fieldType: "input",
                required: false,
              },
              {
                id: 11,
                name: "ratingAgencies",
                label: "Rating Agencies",
                fieldType: "input",
                required: false,
              },
              {
                id: 12,
                name: "etc",
                label: "e.t.c",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Listing Data",
            fields: [
              {
                id: 1,
                name: "descriptionOfTheBond",
                label: "Description of the Bond",
                fieldType: "input",
                required: false,
              },
              {
                id: 2,
                name: "quantityVolume",
                label: "Quantity (Volume)",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 3,
                name: "parValueValue",
                label: "Par Value (Value) ",
                format: "currency",
                fieldType: "number",
                required: false,
              },
              {
                id: 4,
                name: "issuePriceValue",
                label: "Issue Price (Value)",
                format: "currency",
                fieldType: "number",
                required: false,
              },
              {
                id: 5,
                name: "unitsOfSaleVolume",
                label: "Units of Sale (Volume)",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 6,
                name: "methodOfOffer",
                label: "Method of Offer",
                fieldType: "input",
                required: false,
              },
              {
                id: 7,
                name: "tenorYears",
                label: "Tenor (Years)",
                fieldType: "input",
                required: false,
              },
              {
                id: 8,
                name: "seriesNumber",
                label: "Series number (Number) ",
                format: "decimal",
                fieldType: "number",
                required: false,
              },
              {
                id: 9,
                name: "allotmentOrIssueDate",
                label: "Allotment/Issue Date",
                fieldType: "date",
                required: false,
              },
              {
                id: 10,
                name: "offerOpens",
                label: "Offer Opens",
                fieldType: "input",
                required: false,
              },
              {
                id: 11,
                name: "offerCloses",
                label: "Offer Closes ",
                fieldType: "input",
                required: false,
              },
              {
                id: 12,
                name: "maturityDate",
                label: "Maturity Date ",
                fieldType: "date",
                required: false,
              },
              {
                id: 13,
                name: "couponRate",
                label: "Coupon Rate",
                fieldType: "input",
                required: false,
              },
              {
                id: 14,
                name: "couponPaymentDate",
                label: "Coupon payment date ",
                fieldType: "date",
                required: false,
              },
              {
                id: 15,
                name: "repaymentStructure",
                label: "Repayment Structure ",
                fieldType: "input",
                required: false,
              },
              {
                id: 16,
                name: "amortizingYesOrNo",
                label: "Amortizing (Yes or No)",
                fieldType: "input",
                required: false,
              },
              {
                id: 17,
                name: "theStockExchangesWhereTheIssuerIsListedIfAny",
                label:
                  "The stock exchange(s) where the Issuer is listed (if any)",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Purpose and Utilization of the Proceeds",
            fields: [
              {
                id: 1,
                label: "",
                name: "lstUtilizationOfProceed",
                fieldType: "table",
                data: [
                  {
                    id: 1,
                    sn: 1,
                    applicationId: 5,
                    row: [
                      {
                        id: 1,
                        name: "utilization",
                        label: "Utilization",
                        fieldType: "input",
                        required: false,
                      },
                      {
                        id: 2,
                        name: "amount",
                        label: "Amount (N)",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountInNairaSubtotal",
                            value: { a: "amount" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "percentOfGross",
                        label: "% of Gross proceed ",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfGrossProceedSubtotal",
                            value: { a: "percentOfGross" },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 4,
                        name: "estimatedCompletionPeriod",
                        label: "Estimated Completion Period",
                        fieldType: "input",
                        logics: [
                          {
                            operation: "Summation",
                            total: "estimatedCompletionPeriodSubtotal",
                            value: { a: "estimatedCompletionPeriod" },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
                summary: [
                  {
                    name: "Subtotal",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaSubtotal",
                        label: "Amount (N)",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "amountInNairaSubtotal",
                            value: { a: "amount" },
                          },
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedSubtotal",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "percentageOfGrossProceedSubtotal",
                            value: { a: "percentOfGross" },
                          },
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodSubtotal",
                        label: "Estimated Completion Period",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Summation",
                            total: "estimatedCompletionPeriodSubtotal",
                            value: { a: "estimatedCompletionPeriod" },
                          },
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "estimatedCompletionPeriodCostOfIssue",
                              b: "estimatedCompletionPeriodSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                  {
                    name: "Cost of Issue",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaCostOfIssue",
                        label: "Amount (N)",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedCostOfIssue",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodCostOfIssue",
                        label: "Estimated Completion Period",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "estimatedCompletionPeriodCostOfIssue",
                              b: "estimatedCompletionPeriodSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                  {
                    name: "Grand Total",
                    fields: [
                      {
                        id: 1,
                        name: "amountInNairaGrandTotal",
                        label: "Subtotal",
                        format: "currency",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "amountInNairaGrandTotal",
                            value: {
                              a: "amountInNairaCostOfIssue",
                              b: "amountInNairaSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 2,
                        name: "percentageOfGrossProceedGrandTotal",
                        label: "% Of Gross Proceed",
                        format: "decimal",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "percentageOfGrossProceedGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                      {
                        id: 3,
                        name: "estimatedCompletionPeriodGrandTotal",
                        label: "Estimated Completion Period",
                        fieldType: "number",
                        logics: [
                          {
                            operation: "Addition",
                            total: "estimatedCompletionPeriodGrandTotal",
                            value: {
                              a: "percentageOfGrossProceedCostOfIssue",
                              b: "percentageOfGrossProceedSubtotal",
                            },
                          },
                        ],
                        required: false,
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            name: "Outstanding Debt Obligation",
            fields: [
              {
                id: 1,
                name: "outstandingDebtObligation",
                label: "",
                description:
                  "(Provide brief details of the issuer’s current debt obligations such as other debt instruments issued in the past) ",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Claims and Litigation",
            fields: [
              {
                id: 1,
                label: "Cases instituted against the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedAgainstTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 2,
                name: "totalNumberOfCasesInstitutedByTheIssuer",
                label: "Cases instituted by the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name:
                      "totalAmountClaimedFromTheNumberOfCasesInstitutedByTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 3,
                label: "Total number of cases involved by the Issuer",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "totalNumberOfCasesInvolvedByTheIssuer",
                    label: "Number",
                    format: "decimal",
                    fieldType: "number",
                    required: false,
                  },
                  {
                    id: 2,
                    name: "totalNumberOfCasesInvolvedByTheIssuerInNaira",
                    label: "Value Claimed (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
              {
                id: 4,
                label: "Indebtedness",
                fieldType: "sub-section",
                subSection: [
                  {
                    id: 1,
                    name: "indebtednessValueInNaira",
                    label: "Value (N)",
                    format: "currency",
                    fieldType: "number",
                    required: false,
                  },
                ],
              },
            ],
          },
          {
            name: "Issuer’s Profile",
            fields: [
              {
                id: 1,
                name: "briefDescriptionOfIssuer",
                label: "Brief Description of Issuer",
                tooltip:
                  "The Issuer’s profile should include the date of incorporation, nature of business, industry.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Material Contracts",
            fields: [
              {
                id: 1,
                name:
                  "provideAListOfTheAgreementsAndContractsWhichTheIssuerHasEnteredIntoMaterialContracts",
                label:
                  "Provide a list of the agreements and contracts which the Issuer has entered into that are material to offer and/or  listing or to its business, other than in the ordinary course of business.",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTradingLicenseHolderDeclaration",
                label:
                  "Managing Director of the Sponsoring Trading License Holder",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to the best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
      {
        id: 32,
        type: "REOPENING OF FEDERAL GOVERNMENT BONDS",
        requiredDocuments: [
          {
            id: 1,
            name: "Allotment Result from DMO",
            document: "",
            file: null,
            required: true,
          },
          { id: 2, name: "Other Documents", document: "", file: null },
        ],
        sections: [
          {
            name: "Introduction",
            fields: [
              {
                id: 1,
                name: "nameOfIssuer",
                label: "Name of Issuer",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "symbolCodeOfSecurity",
                label: "Symbol Code of Security",
                fieldType: "input",
                required: false,
              },
              {
                id: 3,
                name: "isinOfSecurity",
                label: "ISIN of Security",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name: "Parties to the Listing",
            fields: [
              {
                id: 1,
                name: "sponsoringTradingLicenseHolder",
                label: "Sponsoring Trading License Holder",
                fieldType: "input",
                required: true,
              },
              {
                id: 2,
                name: "jointSponsoringTradingLicenseHolders",
                label:
                  "Joint Sponsoring Trading License Holder (s) (should accommodate more than one firm)",
                fieldType: "textarea",
                required: false,
              },
            ],
          },
          {
            name: "Listing Data",
            fields: [
              {
                id: 1,
                name: "currentUnitsOutstanding",
                label: "Current units outstanding",
                fieldType: "input",
                required: false,
              },
              {
                id: 2,
                name: "additionalUnitsToBeListed",
                label: "Additional units to be listed",
                fieldType: "input",
                required: false,
              },
              {
                id: 3,
                name: "postListingUnitsOutstanding",
                label: "Post listing units outstanding",
                fieldType: "input",
                required: false,
              },
            ],
          },
          {
            name:
              "Upload Supporting Document (Reference to be made to checklists)",
            fields: [
              {
                id: 1,
                label: "Allotment Result from DMO",
                fieldType: "file",
                required: true,
              },
              {
                id: 4,
                label: "Other Documents",
                tooltip: "link for checklist",
                fieldType: "file",
                required: false,
              },
            ],
          },
          {
            name: "Declaration",
            fields: [
              {
                id: 1,
                name: "managingDirectorOfTradingLicenseHolderDeclaration",
                label:
                  "Managing Director of the Sponsoring Trading License Holder",
                fieldType: "textarea",
                description:
                  "I confirm that the above information is accurate to the best of my knowledge.",
                required: false,
              },
            ],
          },
        ],
      },
    ],
  };

  constructor() { }
  defaultPayload: any = {
    applicationTypeId: 5,
    listingTypeId: 2,
    lstUtilizationOfProceed: [
      {
        sn: 1,
        id: 1,
        applicationId: 5,
        utilization: "23",
        amountInNaira: "60000",
        percentageOfGrossProceed: "65003",
        estimatedCompletionPeriod: "80000",
      },
    ],
    lstShareholdingStructure: [
      {
        sn: 1,
        id: 1,
        applicationId: 5,
        nameOfShareholder: "ADE DANIEL",
        noOfSharesHeldPreOfferOrMerger: 44,
        percentageOfHoldingPreOfferOrMerger: 52,
        noOfSharesHeldPostOfferOrMerger: 45,
        percentageOfHoldingPostOfferOrMerger: 68,
      },
    ],
    lstDirectorBeneficialInterest: [
      {
        sn: 1,
        id: 1,
        applicationId: 5,
        nameOfDirector: "ADE DANIEL",
        designation: "Account",
        directShareholding: "750",
        indirectHolding: "3000",
        totalShareholding: "750",
        percentageHolding: "30",
      },
    ],
    lstSubsidiaryOfIssuer: [
      {
        sn: 1,
        id: 1,
        applicationId: 5,
        nameOfSubsidiary: "Marzouq Mohammed-Ali",
        dateOfAcquisition: "2020-10-29",
        percentageHeldByTheIssuerInTheSubsidiary: "63",
      },
    ],
    nameOfIssuer: "MIKE SSA",
    applicationType: {
      label: "Merger and Acquisition",
      value: "Merger and Acquisition",
    },
    description: "MIKE SSA IPO",
    sponsoringTradingLicenseHolder: "MIKE SSA",
    jointSponsoringTradingLicenseHolders: "MIKE SSA",
    issuingHouse: "MIKE SSA",
    jointIssuingHouses: "MIKE SSA",
    auditor: "MIKE SSA",
    reportingAccountant: "MIKE SSA",
    solicitorsToTheIssue: "MIKE SSA",
    registrar: "MIKE SSA",
    authorizedShareCapitalVolume: 45,
    authorizedShareCapitalValue: 452000,
    issuedAndFullyPaidValue: 30000000,
    issuedAndFullyPaidVolume: 23,
    nominalValue: 65,
    proposedOfferOrListingSizeVolume: 89000,
    postOfferSizeVolume: 42,
    offerOrListingPrice: "6900",
    board: "Main",
    marketCapitalisation: 14,
    priceRange: 32,
    transactionValue: 4500,
    amountInNairaSubtotal: "45000",
    percentageOfGrossProceedSubtotal: "30",
    estimatedCompletionPeriodSubtotal: "65",
    amountInNairaCostOfIssue: "56",
    percentageOfGrossProceedCostOfIssue: "89",
    estimatedCompletionPeriodCostOfIssue: "5623",
    amountInNairaGrandTotal: "7450",
    percentageOfGrossProceedGrandTotal: "23",
    estimatedCompletionPeriodGrandTotal: "1450",
    outstandingDebtObligation: "ADE DANIEL Merger and Acquisition",
    totalNumberOfCasesInstitutedAgainstTheIssuer: 63,
    totalNumberOfCasesInstitutedByTheIssuer: 6,
    totalNumberOfCasesInvolvedByTheIssuer: 72,
    indebtednessValueInNaira: "12300",
    indebtednessValueInOthercurrency: "450",
    totalAmountClaimedFromTheNumberOfCasesInstitutedAgainstTheIssuerInNaira:
      "630",
    totalAmountClaimedFromTheNumberOfCasesInstitutedAgainstTheIssuerInOthercurrency:
      "780",
    totalAmountClaimedFromTheNumberOfCasesInstitutedByTheIssuerInNaira: "12300",
    totalAmountClaimedFromTheNumberOfCasesInstitutedByTheIssuerInOthercurrency:
      "560",
    briefDescriptionOfIssuer: "ADE DANIEL Merger and Acquisition",
    briefDescriptionOfReasonForListing:
      "Merger and Acquisition Merger and Acquisition",
    fullName: "ADE DANIEL",
    briefProfile: "ADE DANIEL",
    noOfSharesHeldPreOfferOrMergerTotal: 43,
    percentageOfHoldingPreOfferOrMergerTotal: 9,
    percentageOfHoldingPostOfferOrMergerTotal: 120,
    noOfSharesHeldPostOfferOrMergerTotal: 30,
    "": 12463,
    managingDirectorOfTradingLicenseHolderDeclaration: "YES",
    organizationId: 1,
    isSubmit: true,
    provideABriefProfileOfTheOtherCompaniesInvolvedInTheMerger: "JOHN DOE",
    provideAListOfTheAgreementsAndContractsWhichTheIssuerHasEnteredIntoMaterialContracts:
      "JOHN DOE",
  };
  defaultPayload2: any = {
    listingTypeId: 4,
    lstProposedUtilizationOfFunds: [
      {
        sn: 1,
        applicationId: 5,
        descriptionofProposedAsset: "Bola Adeola",
        amountAllocated: 1900000,
        percentageOfTargetWeighting: 69,
        id: 1,
      },
    ],
    nameOfFundManagerOrIssuer: "Bola Adeola",
    listingMethodsByIntroductionOrPublicOffer: "Introduction",
    description: "Bola Adebola",
    sponsoringTradingLicenseHolder: "Bola Adebola",
    issuingHouse: "Bola Adebola",
    assetsManager: "Bola Adebola",
    methodOfOffer: "Introduction",
    volumeBeingOffered: 19000000,
    priceValue: 19000000,
    valueOfTheOffer: 361000000000000,
    minimumSubscription: "7800000",
    rating: "Perfect",
    provideBriefDetailsOfTheFund: "Bola Adeola",
    amountAllocatedInNairaTotal: 1900000,
    targetWeightingPercentageTotal: 69,
    investmentObjectiveStrategyPolicyOfFund: "Bola Adeola",
    briefDescriptionOfFundManager: "Bola Adeola",
    provideBriefDetailsOfTheFundManagersCurrentDebtObligationsOtherThanInTheOrdinaryCourseOfBusiness:
      "Bola Adeola",
    numberOfCasesInstitutedAgainstTheFundManager: "560000",
    numberOfCasesInstitutedAgainstTheFundManagerValueClaimedInNaira: 69000,
    numberOfCasesInstitutedByTheFundManager: "56",
    numberOfCasesInstitutedByTheFundManagerValueClaimedInNaira: 7800,
    numberOfCasesInvolvedByTheFundManager: "450",
    numberOfCasesInvolvedByTheFundManagerValueClaimedInNaira: 690,
    indebtednessValueInNaira: 12000,
    indebtednessValueInOthercurrency: 360000,
    provideAListOfTheAgreementsAndContractsWhichTheIssuerHasEnteredIntoMaterialContracts:
      "Bola Adeola",
    managingDirectorOfTheSponsoringTradingLicenseHolder: "Bola Adebola",
    organizationId: 1,
    isSubmit: false,
    transactionDate: null,
  };

  // manage application state

  // Application Data

  private appData = new BehaviorSubject(this.applicationData);
  appDataState = this.appData.asObservable();
  setAppData(data) {
    this.appData.next(data);
  }

  // Create Application Data
  private createAppData = new BehaviorSubject({});
  createAppDataState = this.createAppData.asObservable();
  setCreateAppData(data) {
    this.createAppData.next(data);
  }

  // Application Type ID

  private appTypeId = new BehaviorSubject(0);
  appTypeIdState = this.appTypeId.asObservable();
  setAppTypeId(data) {
    this.appTypeId.next(data);
  }

  // Application  ID

  private appId = new BehaviorSubject(0);
  appIdState = this.appId.asObservable();
  setAppId(data) {
    this.appId.next(data);
  }

  // Selected ListingType Id

  private listingTypeId = new BehaviorSubject(0);
  listingTypetate = this.listingTypeId.asObservable();
  setListingTypeId(data) {
    this.listingTypeId.next(data);
  }

  private issuerCompanyId = new BehaviorSubject(0);
  issuerCompanyIdState = this.issuerCompanyId.asObservable();
  setissuerCompanyId(data) {
    this.issuerCompanyId.next(data);
  }

  // Application View

  private appView = new BehaviorSubject(" ");
  appViewState = this.appView.asObservable();
  setAppView(data) {
    this.appView.next(data);
  }

  // Documents Checklist

  private documentChecklist = new BehaviorSubject([]);
  documentChecklistState = this.documentChecklist.asObservable();
  setDocumentChecklist(data) {
    this.documentChecklist.next(data);
  }

  // Aplication Type Name

  private applicationType = new BehaviorSubject("");
  applicationTypeState = this.applicationType.asObservable();
  setapplicationType(data) {
    this.applicationType.next(data);
  }
}
