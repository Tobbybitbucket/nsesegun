import { NameIdModel } from "./../../../shared/service-proxies/service-proxies";
import {
  Component,
  Output,
  EventEmitter,
  OnInit,
  Injector,
  ChangeDetectorRef,
  Input,
  SimpleChanges,
  OnChanges,
} from "@angular/core";
import { ApplicationDataService } from "../services/application-data.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable } from "rxjs";
import {
  CreateApplicationDto,
  ApplicationServiceProxy,
  DocumentUploadModel,
  FeeCalculatorRequestModel,
  FeesCalculationServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { finalize } from "rxjs/operators";
import { AppComponentBase } from "@shared/app-component-base";
import * as moment from "moment";
import html2canvas from "html2canvas";
import { jsPDF } from "jspdf";

@Component({
  selector: "review",
  templateUrl: "./review.component.html",
})
export class ReviewComponent
  extends AppComponentBase
  implements OnInit, OnChanges {
  submitting = false;
  disableForm = false;
  form = {
    field1: "Lorem Ipsum",
    field2: "Lorem Ipsum",
    field3: "Lorem Ipsum",
    field4: "Lorem Ipsum",
  };
  @Input() applicationTypeId: number;
  amount: number;
  payNow = false;
  enablePay = false;
  @Input() applicationId: number;
  @Input() listingTypeId: number;
  @Input() printApp: boolean;
  issuerCompanyId: number;
  createAppFields: Observable<any>;
  applicationData: any = {};
  uploadedDocuments: any = [];
  requiredDocuments: any;
  createAppPayload: CreateApplicationDto;
  paymentModel = new FeeCalculatorRequestModel();
  selectedBoardType: NameIdModel;
  boardTypes: NameIdModel[];
  constructor(
    private route: ActivatedRoute,
    private _applicationDataService: ApplicationDataService,
    private _feesCalculationService: FeesCalculationServiceProxy,
    private _applicationService: ApplicationServiceProxy,
    private ngxService: NgxUiLoaderService,
    private router: Router,
    injector: Injector,
    private ref: ChangeDetectorRef
  ) {
    super(injector);
    // this.onResize();
  }
  getBoardTypes(): void {
    this._applicationService.getBoardTypes().subscribe((result) => {
      this.boardTypes = result;
    });
  }
  onChange(event): void {
    console.log(this.selectedBoardType);
    this.viewChange();
  }
  numberOfSharesChange(event): void {
    this.paymentModel.numberOfShares = event;
    this.viewChange();
  }
  priceChange(event): void {
    this.paymentModel.price = event;
    this.viewChange();
  }
  viewChange() {
    console.log(this.paymentModel);
    if (
      this.selectedBoardType === undefined ||
      this.paymentModel.numberOfShares === undefined ||
      this.paymentModel.price === undefined ||
      this.selectedBoardType === null ||
      this.paymentModel.numberOfShares === null ||
      this.paymentModel.price === null
    ) {
      this.enablePay = false;
    } else {
      this.enablePay = true;
    }
  }
  PayCheck(event) {
    console.log("PayCheck event " + event);
    console.log("PayCheck paynow " + this.payNow);
  }
  getCreateAppFields(appId) {
    this._applicationDataService.appDataState.subscribe((data) => {
      this.createAppFields = data.result.filter(
        (type) => type.id === appId
      )[0].sections;
      this.requiredDocuments = data.result.filter(
        (type) => type.id === appId
      )[0].requiredDocuments;
      console.log(this.requiredDocuments);
    });
  }
  getApplicationData(id?) {
    if (!id) {
      this._applicationDataService.createAppDataState.subscribe((data) => {
        console.log(data, "from review", Object.keys(data).length);
        this.applicationData = data;
        if (Object.keys(data).length == 0) {
          // this.handleGotoFirstPage();
        }
      });
      console.log(this.applicationData);
    } else {
      this.ngxService.start();
      this._applicationService
        .getApplication(id)
        .pipe(
          finalize(() => {
            this.ngxService.stop();
          })
        )
        .subscribe((data) => {
          this.applicationData = data.result;
          this.ref.detectChanges();
        });
    }
  }
  getUploadedDocs() {
    this._applicationDataService.documentChecklistState.subscribe(
      (documents) => {
        console.log(documents, "docs from review");
        this.uploadedDocuments = documents;
      }
    );
    // console.log(this.applicationData);
  }
  isObject(val) {
    return typeof val === "object";
  }
  ngOnInit(): void {
    this.getApplicationData(+this.route.snapshot.queryParamMap.get("appId"));
    this._applicationDataService.setAppView("review");
    this.applicationTypeId = +this.route.snapshot.queryParamMap.get(
      "appTypeId"
    );

    this.applicationId = +this.route.snapshot.queryParamMap.get("appId");

    this.listingTypeId = +this.route.snapshot.queryParamMap.get(
      "listingTypeId"
    );
    this.issuerCompanyId = +this.route.snapshot.queryParamMap.get(
      "issuerCompanyId"
    );
    this.getCreateAppFields(this.applicationTypeId);
    this.getUploadedDocs();
    this.getBoardTypes();
    // if (!this.applicationTypeId || this.applicationTypeId == 0) {
    //   this.router.navigate(["/app/new-application/provide-information"]);
    // }
  }
  ngOnChanges(changes: SimpleChanges) {
    this.getApplicationData(this.applicationId);
    this.getCreateAppFields(this.applicationTypeId);
    // console.log(this.listingTypeId);
    // console.log(this.applicationId);
    // console.log(this.printApp);
  }

  @Output() openPrev = new EventEmitter<any>();
  @Output() gotoFirstPage = new EventEmitter<any>();

  handlePrev() {
    if (this.route.snapshot.queryParamMap.get("appId")) {
      this.router.navigate(["/app/new-application/document-checklist"], {
        queryParams: {
          appId: this.applicationId,
          appTypeId: this.applicationTypeId,
          listingTypeId: this.listingTypeId,
        },
      });
    } else {
      this.router.navigate(["/app/new-application/document-checklist"], {
        queryParams: {
          appTypeId: this.applicationTypeId,
          listingTypeId: this.listingTypeId,
          issuerCompanyId: this.issuerCompanyId,
        },
      });
    }
    // this.openPrev.emit();
  }

  handleGotoFirstPage() {
    if (this.route.snapshot.queryParamMap.get("appId")) {
      this.router.navigate(["/app/new-application/provide-information"], {
        queryParams: {
          appId: this.applicationId,
          appTypeId: this.applicationTypeId,
          listingTypeId: this.listingTypeId,
        },
      });
    } else {
      this.router.navigate(["/app/new-application/provide-information"], {
        queryParams: {
          appTypeId: this.applicationTypeId,
          listingTypeId: this.listingTypeId,
          issuerCompanyId: this.issuerCompanyId,
        },
      });
    }

    // this.gotoFirstPage.emit();
  }
  computeApplicationFee() {
    this.ngxService.start();
    this.paymentModel.boardType = this.selectedBoardType.name;
    console.log(this.paymentModel);
    this._feesCalculationService
      .computeApplicationFee(this.paymentModel)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe((result) => {
        this.amount = result.amount;
      });
  }
  toggleDisableForm() {
    this.disableForm = !this.disableForm;
  }
  // createApplication() {
  //   this.applicationData.transactionDate = moment();
  //   console.log(
  //     this.createAppPayload,
  //     this.applicationData,
  //     this.applicationData.transactionDate,
  //     JSON.stringify(this.applicationData.transactionDate)
  //   );
  // }
  createApplication() {
    this.ngxService.start();
    let formValid = true;

    this.applicationData;
    this.uploadedDocuments;

    this.createAppPayload = this.applicationData;

    if (this.createAppPayload.requiredDocuments == undefined) {
      this.createAppPayload.requiredDocuments = [];
    }

    if (this.uploadedDocuments) {
      this.uploadedDocuments.forEach((element) => {
        this.createAppPayload.requiredDocuments.push(
          DocumentUploadModel.fromJS(element)
        );
      });
      this.uploadedDocuments = [];
    }
    this.createAppPayload.transactionDate = moment();
    this.createAppPayload.isSubmit = true;

    if (this.createAppPayload.requiredDocuments.length >= 1) {
      const mandatoryDocuments = this.requiredDocuments.filter(function (doc) {
        return doc.required;
      });
      this.createAppPayload.requiredDocuments.forEach((doc, index, array) => {
        if (doc.name == null) {
          array.splice(index, 1);
        }
        mandatoryDocuments.filter(function (mand) {
          if (mand.name === doc.name && doc.document === "") {
            formValid = false;
            // this.handlePrev();
            abp.message.error("Please Upload Required Documents");
          } else if (mand.name === doc.name && doc.document !== "") {
            formValid = true;
          } else if (array.length === 0) {
            formValid = false;
            abp.message.error("Please Upload Required Documents");
          }
        });
      });
    } else if (this.createAppPayload.requiredDocuments.length == 0) {
      formValid = false;
      console.log("Please Upload Required Documents");
      abp.message.error("Please Upload Required Documents");
    }
    // console.log(
    //   this.createAppPayload.requiredDocuments,
    //   this.createAppPayload,
    //   JSON.stringify(this.createAppPayload)
    // );
    // if (formValid) {
    //   this._applicationService
    //     .createApplication(this.createAppPayload)
    //     .pipe(
    //       finalize(() => {
    //         this.ngxService.stop();
    //       })
    //     )
    //     .subscribe(
    //       (res) => {
    //         console.log(res);
    //         this.notify.info(this.l("SavedSuccessfully"));
    //         this.router.navigate(["/app/home"]);
    //       },
    //       (error) => {
    //         console.log(error);
    //         this.ngxService.stop();
    //       }
    //     );
    // } else {
    //   this.ngxService.stop();
    // }
    if (formValid) {
      if (this.payNow) {
        if (
          this.selectedBoardType === undefined ||
          this.paymentModel.numberOfShares === undefined ||
          this.paymentModel.price === undefined ||
          this.selectedBoardType === null ||
          this.paymentModel.numberOfShares === null ||
          this.paymentModel.price === null
        ) {
          this.notify.error(this.l("Please review payment calculator value"));
          this.message.error(this.l("Please review payment calculator value"));
          this.ngxService.stop();
          return;
        }
        this.createAppPayload.payNow = this.payNow;
        this.createAppPayload.boardType = this.paymentModel.boardType;
        this.createAppPayload.numberOfShares = this.paymentModel.numberOfShares;
        this.createAppPayload.price = this.paymentModel.price;
      }
      console.log(this.createAppPayload);
      this._applicationService
        .createApplication(this.createAppPayload)
        .pipe(
          finalize(() => {
            this.ngxService.stop();
          })
        )
        .subscribe(
          (res) => {
            console.log(res);
            this.notify.info(this.l("SavedSuccessfully"));
            if (!res.hasError) {
              if (this.payNow) {
                this.router.navigate(["/app/pay"], {
                  queryParams: {
                    ref: res.message,
                  },
                });
              } else {
                this.router.navigate(["/app/home"]);
              }
            } else {
              this.router.navigate(["/app/home"]);
            }
          },
          (error) => {
            console.log(error);
            this.ngxService.stop();
          }
        );
    } else {
      this.ngxService.stop();
    }
  }
  printReview() {
    html2canvas(document.getElementById("reviewPage")).then((canvas) => {
      var pdf = new jsPDF("p", "mm", "a4");
      var width = pdf.internal.pageSize.getWidth();
      var height = pdf.internal.pageSize.getHeight();
      var imgData = canvas.toDataURL("image/jpeg", 1.0);
      pdf.addImage(imgData, 0, 0, width, height);
      pdf.save("AppReview.pdf");
    });
  }
}
