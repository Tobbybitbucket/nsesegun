
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
    selector: 'app-comments',
    templateUrl: './comments.component.html'
})

export class CommentsComponent implements OnInit {
    @Input() visible: boolean;
    @Output() toggle = new EventEmitter<any>();


    constructor() { }

    ngOnInit() {
    }

    handleToggle() {
        this.toggle.emit();
    }
}
