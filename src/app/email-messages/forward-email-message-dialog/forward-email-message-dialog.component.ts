import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  EmailMessageServiceProxy,
  EmailMessageDto,
  UserDto,
  User,
  UserServiceProxy
} from '@shared/service-proxies/service-proxies';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  templateUrl: './forward-email-message-dialog.component.html'
})
export class ForwardEmailMessageDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  emailMessage = new EmailMessageDto();
  id: number;
  users: UserDto[];
  selectedUser: User;
  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _emailMessageService: EmailMessageServiceProxy,
    public _userService: UserServiceProxy,
    private ngxService: NgxUiLoaderService,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.ngxService.start();
    this.getUsers();
    this._emailMessageService.getEmailMessage(this.id).pipe(
      finalize(() => {
        this.saving = false;
        this.ngxService.stop();
      })
    ).subscribe((result) => {
      this.emailMessage = result.result;
      this.selectedUser = this.emailMessage.to;
    });
  }
  getUsers(): void {
    this.ngxService.start();
    this._userService.getAllActiveUsers()
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(result => {
        this.users = result;
      });
  }
  onChange(event): void {
    console.log(this.selectedUser);
  }
  save(): void {
    this.saving = true;
    this.ngxService.start();
    this.emailMessage.toId = this.selectedUser.id;
    // this.emailMessage.to = this.selectedUser;
    this.emailMessage.isForward = true;
    this.emailMessage.isReply = false;
    this.emailMessage.parentId = this.id;
    this._emailMessageService
      .createEmailMessage(this.emailMessage)
      .pipe(
        finalize(() => {
          this.bsModalRef.hide();
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.onSave.emit();
      });
  }
}
