import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailMessagesComponent } from './email-messages.component';

describe('MessagesComponent', () => {
  let component: EmailMessagesComponent;
  let fixture: ComponentFixture<EmailMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmailMessagesComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
