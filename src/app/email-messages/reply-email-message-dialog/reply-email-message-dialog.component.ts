import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  EmailMessageServiceProxy,
  EmailMessageDto
} from '@shared/service-proxies/service-proxies';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  templateUrl: './reply-email-message-dialog.component.html'
})
export class ReplyEmailMessageDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  emailMessage = new EmailMessageDto();
  id: number;
  initialMessage = '';
  emailSender = '';
  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _emailMessageService: EmailMessageServiceProxy,
    private ngxService: NgxUiLoaderService,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.ngxService.start();
    console.log(this.id);
    this._emailMessageService.getEmailMessage(this.id).pipe(
      finalize(() => {
        this.saving = false;
        this.ngxService.stop();
      })
    ).subscribe((result) => {
      console.log(result);
      this.emailMessage = result.result;
      this.initialMessage = this.emailMessage.message;
      this.emailMessage.message = '';
      this.emailSender = `${this.emailMessage.from.name} ${this.emailMessage.from.surname} (${this.emailMessage.from.emailAddress})`;
    });
  }

  save(): void {
    this.saving = true;
    this.ngxService.start();
    this.emailMessage.isReply = true;
    this.emailMessage.parentId = this.id;
    this._emailMessageService
      .createEmailMessage(this.emailMessage)
      .pipe(
        finalize(() => {
          this.saving = false;
          this.bsModalRef.hide();
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.onSave.emit();
      });
  }
}
