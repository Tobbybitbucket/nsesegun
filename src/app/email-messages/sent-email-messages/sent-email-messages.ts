import { AppComponentBase } from '@shared/app-component-base';
import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import {
  EmailMessageDto
} from '@shared/service-proxies/service-proxies';
import { Table } from 'primeng/table';
import { BehaviorSubject } from 'rxjs';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './sent-email-messages.html',
  animations: [appModuleAnimation()]
})
export class SentEmailMessagesComponent extends AppComponentBase implements OnInit {
  @ViewChild('dt') table: Table;
  emailMessages: EmailMessageDto[] = [];
  isTableLoading = false;
  selectedEmailMessage$: BehaviorSubject<EmailMessageDto>;

  cols = [
    { field: 'sn', header: 'S/N' },
    { field: 'to', header: 'To' },
    { field: 'subject', header: 'Subject' },
    { field: 'message', header: 'Message' },
    { field: 'createdOn', header: 'Created On' },
    { field: 'read', header: 'Read' }
  ];
  constructor(
    injector: Injector,
    private route: ActivatedRoute,
  ) {
    super(injector);
    this.selectedEmailMessage$ = new BehaviorSubject(new EmailMessageDto());
  }

  ngOnInit() {
    this.emailMessages = this.route.snapshot.data.data;
  }
}
