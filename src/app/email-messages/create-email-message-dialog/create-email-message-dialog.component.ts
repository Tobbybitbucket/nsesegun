import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  EmailMessageServiceProxy,
  CreateEmailMessageDto,
  UserDto,
  UserServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AbpValidationError } from '@shared/components/validation/abp-validation.api';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  templateUrl: './create-email-message-dialog.component.html'
})
export class CreateEmailMessageDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  emailMessage = new CreateEmailMessageDto();
  users: UserDto[];
  selectedUser: UserDto;
  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _emailMessageService: EmailMessageServiceProxy,
    public _userService: UserServiceProxy,
    private ngxService: NgxUiLoaderService,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.getUsers();
  }
  getUsers(): void {
    this.ngxService.start();
    this._userService.getAllActiveUsers()
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(result => {
        this.users = result;
      });
  }
  onChange(event): void {
    console.log(this.selectedUser);
  }
  save(): void {
    console.log(this.emailMessage);
    this.saving = true;
    this.ngxService.start();
    this.emailMessage.toId = this.selectedUser.id;
    this._emailMessageService
      .createEmailMessage(this.emailMessage)
      .pipe(
        finalize(() => {
          this.saving = false;
          this.bsModalRef.hide();
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.onSave.emit();
      });
  }
}
