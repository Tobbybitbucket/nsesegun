import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { finalize } from 'rxjs/operators';
import {EditorModule} from 'primeng/editor';
import {DocTemplateApiResult, ApprovalProcess, MessageOutApiResult, DocTemplate, ApprovalSetupServiceProxy, 
  DocTemplateServiceProxy, ApprovalProcessIListApiResult} from  "@shared/service-proxies/service-proxies";
import { template } from 'lodash';  
import { HttpClientModule} from '@angular/common/http';
import { AngularEditorModule, AngularEditorConfig } from '@kolkov/angular-editor';
import { jsPDF } from "jspdf";
//import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';


@Component({
  selector: 'app-doc-template-item-dialog',
  templateUrl: './doc-template-item-dialog.component.html',
  styleUrls: ['./doc-template-item-dialog.component.css']
})
export class DocTemplateItemDialogComponent implements OnInit {

  @Input() visible: boolean;
  @Input() id: number;
  @Input() name: string;
  @Input() docTemplate: DocTemplate;
  @Input()  approvalProcessList: ApprovalProcess[];

  @Output("loadDocTemplates") loadDocTemplates: EventEmitter<any> = new EventEmitter();

  dialogTitle:string ="MANAGE " + name + ' TEMPLATE';
  selectedValue = {};
  loading: Boolean = false;
 
  constructor( 
    private _docTemplateServiceProxy: DocTemplateServiceProxy,
    ) { }

  ngOnInit(): void { 
    this.docTemplate = new DocTemplate(); 
    this.docTemplate.id= 0 ;
    this.docTemplate.name='';
    this.docTemplate.template='';
    this.docTemplate.approvalProcessId=0;
  }
  ngOnLoad(){
   
    console.log(this.docTemplate);
  }

  getPdf(){
    // const doc = new jsPDF();
    // console.log(this.docTemplate.template);
    // doc.html(this.docTemplate.template);
    // doc.save("a4.pdf");
 
    html2canvas(document.getElementById("template_editor")).then(canvas => {

      var pdf = new jsPDF('p', 'pt', [canvas.width, canvas.height]);

      var imgData  = canvas.toDataURL("image/jpeg", 1.0);
      pdf.addImage(imgData,0,0,canvas.width, canvas.height);
      pdf.save('converteddoc.pdf');

  });

  // let data = document.getElementById('template_editor');  
  //   html2canvas(data).then(canvas => {
  //     const contentDataURL = canvas.toDataURL('image/png')  
  //     let pdf = new jspdf('l', 'cm', 'a4'); //Generates PDF in landscape mode
  //     // let pdf = new jspdf('p', 'cm', 'a4'); Generates PDF in portrait mode
  //     pdf.addImage(contentDataURL, 'PNG', 0, 0, 29.7, 21.0);  
  //     pdf.save('Filename.pdf');   
  //   }); 

  }

  saveform(){
    this.docTemplate.id = this.id;
    this.docTemplate.name = this.name; 
    this._docTemplateServiceProxy.saveDocTemplate(this.docTemplate).subscribe((retval:MessageOutApiResult)=>{
      if(retval.hasError){
        abp.notify.error(retval.message);
      } else {
        abp.notify.success(retval.message);
        this.loadDocTemplates.emit();
        this.visible=false;
      }
    }); 
  }

  
  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '300px',
      minHeight: '300px',
      maxHeight: '300px',
      width: 'auto',
      minWidth: 'auto',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      [
        'insertImage',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
      ]
    ],
};


}
 