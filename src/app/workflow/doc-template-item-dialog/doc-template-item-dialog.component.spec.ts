import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocTemplateItemDialogComponent } from './doc-template-item-dialog.component';

describe('DocTemplateItemDialogComponent', () => {
  let component: DocTemplateItemDialogComponent;
  let fixture: ComponentFixture<DocTemplateItemDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocTemplateItemDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocTemplateItemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
