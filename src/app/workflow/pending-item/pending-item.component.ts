import { Router } from "@angular/router";
import { async } from "rxjs";
import {
  Component,
  OnInit,
  Injector,
  ChangeDetectionStrategy,
  ViewChild,
} from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Table } from "primeng/table";
import { Utils } from "../../../shared/helpers/Utils";
import { AppSessionService } from "../../../shared/session/app-session.service";
import { ActivatedRoute } from "@angular/router";
import { jsPDF } from "jspdf";
//import * as jsPDF from 'jspdf';
import html2canvas from "html2canvas";
import { AngularEditorModule, AngularEditorConfig } from '@kolkov/angular-editor';
import {
  AppIds, 
  ApprovalSetupServiceProxy,
  ApprovalResponseDTOApiResult,
  DocTemplateServiceProxy,
  DocTemplateApiResult,
  ApprovalReviewServiceProxy,
  ApprovalLogDTOIListApiResult,
  ApprovalLogDocDTO,
  ApprovalLogDTO,
  ApprovalLogApiResult,
  ApprovalLog,
  BooleanApiResult,
  ApplicationLetterServiceProxy,
  ApplicationLetterIListApiResult,
  ApplicationLetter,
  ReviewButtonText,
  ApprovalLogModelApiResult,
  ApprovalLogModel,
  MessageOutApiResult,
} from "@shared/service-proxies/service-proxies";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { finalize } from "rxjs/operators";

@Component({
  selector: "app-pending-item",
  templateUrl: "./pending-item.component.html",
  styleUrls: ["./pending-item.component.css"],
})
export class PendingItemComponent extends AppComponentBase implements OnInit {
  appId: number;
  appTypeId: number = 0;
  displayItemDialog: boolean = true;
  viewApprovalLog: boolean;
  viewApplicationLetters: boolean;
  downloadAppLetterModal: boolean;
  selectedApplicationLetter: any;
  reviewButtons : any = {};

  isStepOne:boolean = false;
  selectedValue: any = {};
  listingAnalysts: Array<any>;

  constructor(
    private _docTemplateServiceProxy: DocTemplateServiceProxy,
    private _applicationLetterServiceProxy: ApplicationLetterServiceProxy,
    private _approvalReviewServiceProxy: ApprovalReviewServiceProxy,
    private _approvalSetupServiceProxy: ApprovalSetupServiceProxy,
    private _sessionService: AppSessionService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    private ngxService: NgxUiLoaderService,
    injector: Injector
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.reviewButtons.approvalText="Approve";
    this.reviewButtons.rejectionText="Decline"; 

    this.activatedRoute.queryParams //.p(params => params.order)
      .subscribe((params) => { 
        this.logid = params.logid;
        this.itemid = params.itemid;
        // console.log(this.logid);
        // console.log(this.itemid);

        this.getAppIds();

        this.loadApprovalLog();

        this.getButtonText(); 
      });
  }
  getAppIds() {
    this.ngxService.start();
    this._approvalReviewServiceProxy
      .getAppIds(this.itemid)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(
        (retval: AppIds) => {
          // console.log(retval, "LOOK NO MORe");
          this.appId = retval.appId;
          this.appTypeId = retval.appTypeId;
         // this.getApplicationLetters(retval.appId);
          this.getApprovalLog(retval.appId);
        },
        (error) => {
          console.log(error);
          this.ngxService.stop();
        }
      );
  }

  getListingAnalysts() {
    // this.ngxService.start();
    this._approvalReviewServiceProxy
      .fetchListingAnalysts()
      .pipe(
        finalize(() => {
          // this.ngxService.stop();
        })
      )
      .subscribe((data) => {
        this.listingAnalysts = data.map((data) => ({
          label: data.name,
          value: data.id,
        }));
        // console.log(data, this.listingAnalysts);
        // this.notify.info(this.l("SavedSuccessfully"));
        // this.router.navigate(["/app/home"]);
      });
  }

  gotoItem(data: any) {
    this.router.navigate(["/workflow/pending-item/"]);
  }
  showDeclineBtn: boolean = true;
  showSendBack: boolean = false;
  showDocView: boolean = false;
  showAppView: boolean = false;
  loading: boolean = false;
  lstApprovalLog: any = [];
  ApprovalLetters: any = [];
  itemid: number = 0;
  logid: number = 0;
  isAuthorized: boolean = false;
  isDocEditView: boolean = false;
  alogItem = new ApprovalLog();
  appLetter = new ApplicationLetter();
  alogModelItem = new ApprovalLogModel();

  cols = [
    { field: "id", header: "S/N" },
    { field: "name", header: "Process Name" },
    // { field: 'description', header: 'Description' },
  ];
  docTemplate: ApplicationLetter;

  editLetter(){
    this.isDocEditView=true;
  }
  saveLetter(){
    this.ngxService.start();
    this.loading=true;
    this.appLetter.id=this.alogItem.itemId;
    this.appLetter.body = this.docTemplate.body;
    this._applicationLetterServiceProxy.saveApplicationLetter(this.appLetter)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
          this.loading = false;
        })
      )
      .subscribe((retval: MessageOutApiResult) => {
        if(retval.result.isSuccessful){
          abp.notify.success(retval.result.message);
          this.isDocEditView=false;
        }
        this.ngxService.stop(); 
        this.loading = false;
      });
  }

  getAlogItem(){
    this.alogItem.approvalProcessId=  this.alogModelItem.approvalProcessId;
    this.alogItem.approvalStepId=  this.alogModelItem.approvalStepId;
    this.alogItem.flag =  this.alogModelItem.flag;
    this.alogItem.id=  this.alogModelItem.id;
    this.alogItem.itemId=  this.alogModelItem.itemId;
    this.alogItem.notifyDate=  this.alogModelItem.notifyDate;
    this.alogItem.reviewDate=  this.alogModelItem.reviewDate;
    this.alogItem.reviewStatus=  this.alogModelItem.reviewStatus;
    this.alogItem.reviewedBy=  this.alogModelItem.reviewedBy;
    this.alogItem.roleId=  this.alogModelItem.roleId;
    this.alogItem.stepSn=  this.alogModelItem.stepSn;
    this.alogItem.userId=  this.alogModelItem.userId; 
  }


  loadApprovalLog() {
    this.ngxService.start();
    this.loading = true;
    this._approvalReviewServiceProxy
      .fetchItemApprovalLog(this.itemid)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe((retval: ApprovalLogDTOIListApiResult) => {
        // console.log(retval);
        if (retval.hasError) {
          this.ngxService.stop();
          this.router.navigate(["login"]);
        } else {
          this.lstApprovalLog = retval.result; 
          this.loading = false;
        }
      });
  }

  getApprovalLog(applicationId) {
    this.ngxService.start();
    this._approvalReviewServiceProxy
      .getApprovalLogModel(this.logid)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(
        (retval: ApprovalLogModelApiResult) => {
          if (retval.hasError) {
            this.router.navigate(["login"]);
          } else {
            this.alogModelItem = retval.result;
            this.getAlogItem();
            if (this.alogModelItem.stepSn > 1) {
              this.showSendBack = true;
            } 
            if (this.alogModelItem.roleId ==21 || this.alogModelItem.stepSn==1) {
              this.showDeclineBtn = false;
            }
            if (this.alogModelItem.swimLane==2) {
              this.showSendBack = false;
            }
            if (this.alogModelItem.swimLane==1) {
              this.showDeclineBtn = false;
            }
            
            this.hasAuthorithy();

            if (this.alogItem.approvalProcessId >= 10) {
              console.log('is letter');
              this._applicationLetterServiceProxy
                .getApplicationLetter(0, this.alogModelItem.itemId)
                .subscribe((retval: ApplicationLetterIListApiResult) => {
                  console.log(retval);
                  if (retval.hasError) {
                  } else {
                    this.docTemplate = retval.result[0];
                  }
                });
              this.showDocView = true;
              this.showAppView = false;
            } else {
              this.showDocView = false;
              this.showAppView = true;
            }
          }
        },
        (error) => {
          this.ngxService.stop();
        }
      );
  }

  getApplicationLetters(applicationId: number) {
    this._applicationLetterServiceProxy
      .getApplicationLetter(applicationId, 0)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe((retval) => {
        console.log(retval, "DONEEE");
        this.ApprovalLetters = retval.result;
      });
  }

  hasAuthorithy() {
    this._approvalReviewServiceProxy
      .hasAuthorithy(this.alogItem.id, this._sessionService.userId)
      .subscribe((retval: BooleanApiResult) => {
        // console.log(retval);

        if (retval.hasError) {
          this.router.navigate(["login"]);
        } else {
          if (retval.result == false) {
            this.isAuthorized = false;
            abp.notify.error("You are not authorized to review this request");
          } else {
            this.isAuthorized = true;
            if(this.alogItem.stepSn==1 && this.alogItem.approvalProcessId<=9){
              this.getListingAnalysts();
              this.isStepOne=true;
            }
          }
        }
      });
  }


  
  getButtonText(){
    this._approvalReviewServiceProxy.getReviewButtonText(this.logid)
    .subscribe((retval: ReviewButtonText) => {
      console.log(retval);
       this.reviewButtons.approvalText = retval.approvalText;
       this.reviewButtons.rejectionText = retval.rejectionText;
    })
  }

  submitForm(reviewstatus: number) {
    this.alogItem.reviewStatus = reviewstatus;

    this.postApprovalLog();
  }

  postApprovalLog() {
    this.loading = true;
    if(this.alogItem.stepSn==1) {
     if(this.selectedValue==null || this.selectedValue==undefined){
        abp.notify.error("Please Select A Listing Analyst For Peer Review");
        return;
     } else if(this.selectedValue.value==0){
        abp.notify.error("Please Select A Listing Analyst For Peer Review");
        return;
    } else {
        this.alogItem.userId = this.selectedValue.value;
     }
    }

    this._approvalReviewServiceProxy
      .postApprovalLog(this.alogItem)
      .subscribe((retval: ApprovalResponseDTOApiResult) => {
        if (retval.hasError) {
          this.router.navigate(["login"]);
        } else {
          this.lstApprovalLog = retval.result;
          abp.notify.info(retval.result.message);
          this.loading = false;
          this.isAuthorized = false;
          setTimeout(() => {
            this.router.navigate(["app/workflow/pending-list"]);
          }, 4000);
        }
      });
  }

  getPdf(applicationLetter) {
    html2canvas(document.getElementById('template_editor'+applicationLetter.id)).then((canvas) => {
      var pdf = new jsPDF("p", "pt", [canvas.width, canvas.height]);

      var imgData = canvas.toDataURL("image/jpeg", 1.0);
      pdf.addImage(imgData,0,0,canvas.width, canvas.height);
      pdf.save(applicationLetter.title+".pdf");
    });
  }

  downloadApplicationLetter(applicationLetter) {
    this.downloadAppLetterModal = true;
    this.viewApplicationLetters = false;
    this.selectedApplicationLetter = applicationLetter;
  }

  reviewForm(): void {
    this.router.navigate([`/app/workflow/pending-item/review`], {
      queryParams: {
        appId: this.appId,
        appTypeId: this.appTypeId,
        logId: this.logid,
        itemid: this.itemid,
        lane: this.alogModelItem.swimLane,
      },
    });
  }
  loadDocTemplates() {};



  
  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '700px',
      minHeight: '300px',
      maxHeight: '700px',
      width: 'auto',
      minWidth: 'auto',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '', 
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      [
        'insertImage',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
      ]
    ],
};
}
