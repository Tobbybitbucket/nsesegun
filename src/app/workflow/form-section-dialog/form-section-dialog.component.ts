import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { finalize } from 'rxjs/operators';
import {ApprovalSetupServiceProxy, MessageOutApiResult, FormSection} from  "@shared/service-proxies/service-proxies";


@Component({
  selector: 'app-form-section-dialog',
  templateUrl: './form-section-dialog.component.html',
  styleUrls: ['./form-section-dialog.component.css']
})
export class FormSectionDialogComponent implements OnInit {

  @Input() visible: boolean;
  @Input() id: number;
  @Input() name: string;

  @Output("loadFormSections") loadFormSections: EventEmitter<any> = new EventEmitter();

  dialogTitle:string ="New Section";
  selectedValue = {};
  formsection = new FormSection();
 
  constructor(private _approvalSetupServiceProxy: ApprovalSetupServiceProxy,) { }

  ngOnInit(): void {
    if(this.id>0){
      this.dialogTitle = "Update Section";
    } else {
      this.dialogTitle = "New Section";
    }
  }

  saveform(){
    this.formsection.id = this.id;
    this.formsection.name = this.name; 
    this._approvalSetupServiceProxy.saveFormSection(this.formsection).subscribe((retval:MessageOutApiResult)=>{
      if(retval.hasError){
        abp.notify.error(retval.message);
      } else {
        abp.notify.success(retval.message);
        this.loadFormSections.emit();
        this.visible=false;
      }
    }); 
  }

}
 