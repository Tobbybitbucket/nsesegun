import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSectionDialogComponent } from './form-section-dialog.component';

describe('FormSectionDialogComponent', () => {
  let component: FormSectionDialogComponent;
  let fixture: ComponentFixture<FormSectionDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormSectionDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSectionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
