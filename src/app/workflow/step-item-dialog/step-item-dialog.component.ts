import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { finalize } from 'rxjs/operators';
import {ApprovalSetupServiceProxy, MessageOutApiResult, ApprovalProcess, ApprovalStep
   , RoleServiceProxy, RoleDto, RoleDtoPagedResultDto } from  "@shared/service-proxies/service-proxies";
import { getLocaleExtraDayPeriodRules } from '@angular/common';


@Component({
  selector: 'app-step-item-dialog',
  templateUrl: './step-item-dialog.component.html',
  styleUrls: ['./step-item-dialog.component.css']
})
export class StepItemDialogComponent implements OnInit {

  @Input() visible: boolean;
  @Input() stepItem: ApprovalStep;
  @Input() processName: string;
  @Output() onClose: EventEmitter<any> = new EventEmitter();
  @Output("reloadSteps") reloadSteps: EventEmitter<any> = new EventEmitter();

  dialogTitle:string ="New Section";
  selectedValue = {}; 
 
  constructor(private _approvalSetupServiceProxy: ApprovalSetupServiceProxy,
    private _roleServiceProxy: RoleServiceProxy,) { }

  ngOnInit(): void {
    
    this.loadRoles();
  }
  lstGroup: RoleDto[];
  loadRoles(){
    
    this._roleServiceProxy.getAll('',0,100).subscribe((retval:RoleDtoPagedResultDto)=>{
      console.log(retval);
      this.lstGroup  = retval.items;
    }); 
  }
  ngOnLoad(){
    if(this.stepItem.id>0){
      this.dialogTitle = "Update Step In " +this.processName;
    } else {
      this.dialogTitle = "Add New Step To " +this.processName;
    }
    console.log(this.dialogTitle);

  }

  arr = [ 
    { label: 'Application Type 1', value: 1 },
    { label: 'Application Type 2', value: 2 }
  ];
  lstSteptypes = [ 
    { label: 'Group Mode', value:  'Group' },
    { label: 'User Mode', value:'User' } 
  ];

  lstUsers = [ 
    { label: 'Miracle Emenyonu', value: 1 },
    { label: 'Toby Amodu', value: 2 },
    { label: 'Val Akpowevwe', value: 3 },
    { label: 'Obinna Eze', value: 4 }
  ];
  
  // lstGroup = [ 
  //   { label: 'Listing Analyst', value: 1 },
  //   { label: 'Team Lead - Listing', value: 2 },
  //   { label: 'Broker User', value: 3 }
  // ];
   
  lstSwim = [ 
    { label: 'Swim Lane 1', value: 1 },
    { label: 'Swim Lane 2', value: 2 },
    { label: 'Swim Lane 3', value: 3 }
  ];

  saveform(){ 
    // this.stepItem.roleId = this.stepItem.roleId.value;
    // this.stepItem.escalationPrivilegeId = this.stepItem.escalationPrivilegeId.value;
    // this.stepItem.escalationsUserId = this.stepItem.escalationsUserId.value;
    // this.stepItem.swimLane = this.stepItem.swimLane.value;
    console.log(this.stepItem);

    this._approvalSetupServiceProxy.saveApprovalStep(this.stepItem).subscribe((retval:MessageOutApiResult)=>{
      if(retval.hasError){
        abp.notify.error(retval.message);
      } else {
        abp.notify.success(retval.message);
        this.reloadSteps.emit();
        this.visible=false;
      }
    }); 
  }

  closeModal(): void {
    this.onClose.emit();
  }

}