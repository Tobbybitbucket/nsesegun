import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepItemDialogComponent } from './step-item-dialog.component';

describe('StepItemDialogComponent', () => {
  let component: StepItemDialogComponent;
  let fixture: ComponentFixture<StepItemDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepItemDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepItemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
