import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingItemReviewsComponent } from './pending-item-reviews.component';

describe('PendingItemReviewsComponent', () => {
  let component: PendingItemReviewsComponent;
  let fixture: ComponentFixture<PendingItemReviewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingItemReviewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingItemReviewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
