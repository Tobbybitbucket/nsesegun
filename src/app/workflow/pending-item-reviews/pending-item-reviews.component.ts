import {
  Component,
  Injector,
  OnInit,
  ViewChild,
  ElementRef,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import {
  ApprovalLogMessage,
  ApprovalLogMessageServiceProxy,
  ApprovalLogDoc,
  ApprovalLogDocServiceProxy,
  DocTemplateServiceProxy,
  DocTemplateIListApiResult,
  ApplicationLetterServiceProxy,
  DocTemplate,
  ApprovalProcess,
  MessageOutApiResult,
  DocTemplateApiResult,
  ApprovalSetupServiceProxy,
  ApprovalProcessIListApiResult,
  ApplicationLetter,
  DocLogServiceProxy,
  ApprovalLogDocDTO,
} from "@shared/service-proxies/service-proxies";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { Observable } from "rxjs";
import { finalize } from "rxjs/operators";
import { ApplicationDataService } from "../../application/services/application-data.service";
import { AppSessionService } from "../../../shared/session/app-session.service";
import * as moment from "moment";
import { AppComponentBase } from "@shared/app-component-base";
import {
  AngularEditorModule,
  AngularEditorConfig,
} from "@kolkov/angular-editor";

@Component({
  selector: "app-pending-item-reviews",
  templateUrl: "./pending-item-reviews.component.html",
  styleUrls: ["./pending-item-reviews.component.css"],
})
export class PendingItemReviewsComponent
  extends AppComponentBase
  implements OnInit {
  itemid: number;

  appId: number;
  appTypeId: number;
  approvalLogId: number;
  formSections: any;
  formSectionId: any;
  fileObject: any;
  message: any;
  reviewMessage: ApprovalLogMessage | any = {};
  reviewMessages: Array<any>;
  approvalLogDocs: ApprovalLogDoc;
  lstApprovalLogDocs: ApprovalLogDocDTO[];
  initiateLetterDialog: boolean = false;
  displayApplicationLetter: boolean = false;
  lstDocTemplates: any = [];
  selectedDocTemplate;
  docTemplate: DocTemplate;
  approvalProcessList: ApprovalProcess[];
  id: number;
  swimLane: number=1;
  name: string;
  dialogTitle: string;
  item_id: number = 0;
  item_name: string = "";
  applicationLetter: ApplicationLetter | any = {};
  @ViewChild("reviewFile", { static: false }) reviewFileInput: ElementRef;
  constructor(
    private _approvalLogMessageServiceProxy: ApprovalLogMessageServiceProxy,
    private _approvalLogDocServiceProxy: ApprovalLogDocServiceProxy,
    private _applicationDataService: ApplicationDataService,
    private _docTemplateServiceProxy: DocTemplateServiceProxy,
    private _applicationLetterServiceProxy: ApplicationLetterServiceProxy,
    private _approvalSetupServiceProxy: ApprovalSetupServiceProxy,
    private _docLogServiceProxy: DocLogServiceProxy,
    private route: ActivatedRoute,
    private ngxService: NgxUiLoaderService,
    private _sessionService: AppSessionService,
    private router: Router,
    injector: Injector
  ) {
    super(injector);
  }

  getReviewMessages(applicationId: number) {
    this.ngxService.start();
    this._approvalLogMessageServiceProxy
      .getReviewMessages(applicationId)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(
        (res) => {
          res.result.forEach((msg) => {
            this.formSections.find((sec) => {
              if (sec.value === msg.formSectionId)
                msg.formSectionName = sec.name;
            });
          });
          this.reviewMessages = res.result.filter(x=>x.swimLane == this.swimLane);
          this.getApprovalLogDocs(applicationId);
        },
        (error) => {
          console.log(error);
          this.ngxService.stop();
        }
      );
  }
  getApprovalLogDocs(applicationId: number) {
    this.ngxService.start();
    this._approvalLogDocServiceProxy
      .getDocs(applicationId)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(
        (data) => {
          console.log(data);
          this.lstApprovalLogDocs = data.result;
          this.reviewMessages.forEach((msg) => {
            data.result.find((doc) => {
              if (doc.approvalLogId === msg.approvalLogId)
                msg.filePath = doc.filePath;
            });
          });
        },
        (error) => {
          console.log(error);
          this.ngxService.stop();
        }
      );
  }
  getFormSections(appTypeId: number) {
    this._applicationDataService.appDataState.subscribe((res) => {
      // console.log(res, appTypeId);
      var data = res.result.filter((type) => {
        if (type.id == appTypeId) return type;
      });
      this.formSections = data[0]?.sections.map((sec, index) => ({
        name: sec.name,
        value: index + 1,
      }));
    });
    this.formSections.push({
      name: "Others or (Appraisal/Deficiency Letters)",
      value: 0,
    });
    // console.log(this.formSections);
  }

  onFileUpload(files: FileList) {
    // console.log(files);
    let me = this;
    let file = files[0];
    let name = file.name;
    let ext = file.name.split(".").pop();
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      //me.ngxService.start();
      me.fileObject = {
        fileBase64String: reader.result
          .toString()
          .substring(reader.result.toString().lastIndexOf(",") + 1),
        fileName: name,
        fileExt: ext,
      };
      //me.ngxService.stop();
    };
  }
  uploadReviewDoc() {
    if (this.fileObject) {
      this.fileObject = {
        fileBase64String: this.fileObject.fileBase64String,
        fileName: this.fileObject.fileName,
        fileExt: this.fileObject.fileExt,
        fileType: "WORKFLOW_REVIEW",
        applicationNo: "",
      };

      this.ngxService.start();
      this._docLogServiceProxy
        .uploadDoc(this.fileObject)
        .pipe(
          finalize(() => {
            // this.ngxService.stop();
          })
        )
        .subscribe(
          (data) => {
            // console.log(data);
            this.notify.info(this.l("SavedSuccessfully"));
            this.saveReviewDoc(data.result.message, this.fileObject);
          },
          (error) => {
            console.log(error);
            this.ngxService.stop();
          }
        );
    }
  }
  saveReviewDoc(filePath, fileObject) {
    var doc: ApprovalLogDoc | any = {
      id: 0,
      approvalLogId: this.approvalLogId,
      uploadedById: 0,
      fileName: fileObject.fileName,
      fileTitle: fileObject.fileName,
      filePath: filePath,
      dateCreated: moment(),
    };
    // console.log(filePath, doc);
    this.ngxService.start();
    this._approvalLogDocServiceProxy
      .saveDoc(doc)
      .pipe(
        finalize(() => {
          this.ngxService.stop();
        })
      )
      .subscribe(
        (data) => {
          // console.log(data);
          this.notify.info(this.l("SavedSuccessfully"));
          this.getReviewMessages(this.appId);
          //this.getApprovalLogDocs(this.appId);
          this.reviewFileInput.nativeElement.value = "";
        },
        (error) => {
          console.log(error);
          this.ngxService.stop();
        }
      );
  }
  SaveReviewMessage(reviewForm) {
    if (this.reviewMessage.message) {
      // console.log(this.reviewMessage, this.formSectionId);
      this.reviewMessage = {
        message: this.reviewMessage.message,
        formSectionId: this.formSectionId?.value ? this.formSectionId.value : 0,
        postedById: 0,
        approvalLogId: this.approvalLogId,
        id: 0,
        date: moment().toISOString(),
      };
      // console.log(this.reviewMessage);
      this.ngxService.start();
      this._approvalLogMessageServiceProxy
        .saveReviewMessages(this.reviewMessage)
        .pipe(
          finalize(() => {
            this.ngxService.stop();
          })
        )
        .subscribe(
          (data) => {
            console.log(data);
            this.notify.info(this.l("SavedSuccessfully"));
            reviewForm.form.reset();
            this.getReviewMessages(this.appId);
          },
          (error) => {
            console.log(error);
            this.ngxService.stop();
          }
        );
    }
    if (this.fileObject) {
      this.uploadReviewDoc();
    }
  }
  handleOpenPrevious() {
    this.router.navigate([`/app/workflow/pending-item`], {
      queryParams: {
        logid: this.approvalLogId,
        itemid: this.itemid,
      },
    });
  }
  loadDocTemplates() {
    // this.loading = true;
    // this.displayItemDialog = false;
    this._docTemplateServiceProxy
      .getDocTemplateList()
      .subscribe((retval: DocTemplateIListApiResult) => {
        this.lstDocTemplates = retval.result;
        // console.log(this.lstDocTemplates, "What");
      });
  }

  showInitiateLetterDialog() {
    this.initiateLetterDialog = !this.initiateLetterDialog;
  }
  // getApplicationLetter(selectedDocTemplate) {
  //   console.log(selectedDocTemplate.id);
  //   this._applicationLetterServiceProxy
  //     .getApplicationLetter(this.appId, selectedDocTemplate.id)
  //     .subscribe((template) => {
  //       console.log(template);
  //       // this.lstDocTemplates = retval.result;
  //       this.initiateLetterDialog = false;
  //       this.displayApplicationLetter = true;
  //       this.dialogTitle = "MANAGE " + this.name + " TEMPLATE";
  //     });
  // }
  editDocTemplate(data: any = null) {
    this.ngxService.start();
    if (data == null) {
      this.item_id = 0;
      this.item_name = "";
      this.displayApplicationLetter = true;
      this.initiateLetterDialog = false;
      this.ngxService.stop();
    } else {
      this.item_id = data.id;
      this.item_name = data.name;
      this._docTemplateServiceProxy
        .getDocTemplate("", this.item_id)
        .subscribe((retval: DocTemplateApiResult) => {
          // console.log(retval);
          if (retval.hasError) {
            abp.notify.error(retval.message);
          } else {
            this.docTemplate = retval.result;
            this.dialogTitle = "MANAGE " + this.item_name + " TEMPLATE";
          }
          // console.log(this.docTemplate);
          this.displayApplicationLetter = true;
          this.initiateLetterDialog = false;
          this.ngxService.stop();
        });
    }
    this.loadApprovalProcessList();
  }
  loadApprovalProcessList() {
    this._approvalSetupServiceProxy
      .getApprovalProcesses(0)
      .subscribe((retval: ApprovalProcessIListApiResult) => {
        if (retval.hasError) {
          abp.notify.error(retval.message);
        } else {
          this.approvalProcessList = retval.result;
          // console.log(this.approvalProcessList);
        }
      });
  }
  saveform() {
    this.ngxService.start();
    this.applicationLetter.applicationId = this.appId;
    this.applicationLetter.docTemplateId = this.docTemplate.id;
    this.applicationLetter.title = this.docTemplate.name;
    this.applicationLetter.body = this.docTemplate.template;
    this.applicationLetter.initiatedBy = this._sessionService.getShownLoginName();
    this.applicationLetter.reviewStatusId = 1;
    this.applicationLetter.isApproved = false;
    this.applicationLetter.sendForApproval = 1;
    this.applicationLetter.id = 0;

    // console.log(this.docTemplate, this.applicationLetter, this.appId);
    this._applicationLetterServiceProxy
      .saveApplicationLetter(this.applicationLetter)
      .subscribe((retval: MessageOutApiResult) => {
        if (retval.hasError) {
          abp.notify.error(retval.message);
          this.ngxService.stop();
        } else {
          abp.notify.success(retval.result.message);
          // this.loadDocTemplates.emit();
          this.displayApplicationLetter = false;
          this.ngxService.stop();
        }
      });
  }
  ngOnInit(): void {
    this.itemid = Number(this.route.snapshot.queryParamMap.get("itemid"));

    this.swimLane=Number(this.route.snapshot.queryParamMap.get("lane"));
    this.appId = Number(this.route.snapshot.queryParamMap.get("appId"));
    this.appTypeId = Number(this.route.snapshot.queryParamMap.get("appTypeId"));
    this.approvalLogId = Number(this.route.snapshot.queryParamMap.get("logId"));
    this.getReviewMessages(this.appId);
    this.getFormSections(this.appTypeId);
    //this.getApprovalLogDocs(this.appId);
    // console.log(this._sessionService.userId);
    this.loadDocTemplates();
    this.docTemplate = new DocTemplate();
    this.docTemplate.id = 0;
    this.docTemplate.name = "";
    this.docTemplate.template = "";
    this.docTemplate.approvalProcessId = 0;
  }

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: "300px",
    minHeight: "300px",
    maxHeight: "300px",
    width: "auto",
    minWidth: "auto",
    translate: "yes",
    enableToolbar: true,
    showToolbar: true,
    placeholder: "Enter text here...",
    defaultParagraphSeparator: "",
    defaultFontName: "",
    defaultFontSize: "",
    fonts: [
      { class: "arial", name: "Arial" },
      { class: "times-new-roman", name: "Times New Roman" },
      { class: "calibri", name: "Calibri" },
      { class: "comic-sans-ms", name: "Comic Sans MS" },
    ],
    sanitize: true,
    toolbarPosition: "top",
    toolbarHiddenButtons: [
      ["insertImage", "insertVideo", "insertHorizontalRule", "removeFormat"],
    ],
  };
}
