import {ApprovalSetupServiceProxy, ApprovalProcessIListApiResult, ApprovalReviewServiceProxy, 
  ApprovalStepIListApiResult, ApprovalStep, ApprovalProcess, MessageOutApiResult} from  "@shared/service-proxies/service-proxies";
import {Router } from '@angular/router'; 
import { async } from 'rxjs'; 
import { Component, OnInit, Injector, ChangeDetectionStrategy, ViewChild, EventEmitter } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Utils } from '../../../shared/helpers/Utils';
import { ApprovalResponseDTOApiResult } from "@shared/service-proxies/service-proxies";

@Component({
selector: 'app-process-list',
templateUrl: './process-list.component.html',
styleUrls: ['./process-list.component.css']
})
export class ProcessListComponent  extends AppComponentBase implements OnInit {


constructor(
  private _approvalSetupServiceProxy: ApprovalSetupServiceProxy,
  private _approvalReviewServiceProxy: ApprovalReviewServiceProxy,
  public router: Router,
  injector: Injector) {
  super(injector);
} 

ngOnInit(): void {
  this.loadProcesses()
}


@ViewChild('dt') table: Table;
processLoading: Boolean = false;
stepLoading: Boolean = false;
displayProcessDialog: Boolean = false; 
displayStepDialog: Boolean = false; 
lstProcesses :ApprovalProcess[] = [];
lstSteps:ApprovalStep[]=[];
item_id:number = 0;
item_name:string = '';
status = Utils.getStatus();
showSteps: Boolean=false;
processItem = new ApprovalProcess();
processName:string="";
stepItem = new ApprovalStep();
selectedProcessId:number = 0;
statusOptions = Object.keys(this.status).map(item => {
  return { label: item, value: item.toLowerCase() };
});

cols = [
  { field: 'id', header: 'S/N' },
  { field: 'name', header: 'Process Name' }, 
  // { field: 'description', header: 'Description' }, 
];
 
buttonItems = [ 
  {
    label: 'Edit', command: (e) => {
      console.log(e); 
    }
  }
]; 


showProcessDialog() {
  this.displayProcessDialog = !this.displayProcessDialog;

} 

showStepDialog() {
  this.displayStepDialog = !this.displayStepDialog;

} 

editProcess(id=0){
  console.log('edit process');
  if(id==0){
    this.processItem.id=0;
  } else { 
    this.processItem = this.lstProcesses.filter(p=>p.id==id)[0];
  }
  this.showProcessDialog();
}

loadProcesses() {
  this.processLoading=true;
  this._approvalSetupServiceProxy.getApprovalProcesses(0).subscribe((retval:ApprovalProcessIListApiResult)=>{
    if(retval.hasError){
      this.router.navigate(['login']);
    } else {
      this.lstProcesses = retval.result;
      console.log(this.lstProcesses);
      this.processLoading=false;
    }
  });  
} 


editStep(id=0){
  console.log('edit step');
  if(id==0){
    this.stepItem.id=0;
    this.stepItem.approvalProcessId = this.selectedProcessId;
  } else { 
    this.stepItem = this.lstSteps.filter(p=>p.id==id)[0];
  }
  this.showStepDialog();
  console.log(this.processName);
}

removeStep(processid,id){
  if(confirm('Are you sure you want to delete this step?')){
    this._approvalSetupServiceProxy.deleteApprovalStep(processid,id).subscribe((retval:MessageOutApiResult)=>{
      if(retval.hasError){
        abp.notify.error(retval.message);
      } else {
        abp.notify.success(retval.message);
        this.loadSteps(processid,0); 
      }
    }); 
  }
}

reloadSteps(){
  this.loadSteps(this.stepItem.approvalProcessId,0);
}

loadSteps(processid, id=0) {
  this.selectedProcessId = processid;
  this.processName = this.lstProcesses.filter(p=>p.id==processid)[0].name;
  this.showSteps=true;
  this.stepLoading=true;
  this._approvalSetupServiceProxy.getApprovalSteps(processid,id).subscribe((retval:ApprovalStepIListApiResult)=>{
    if(retval.hasError){
      abp.notify.error(retval.message);
      this.router.navigate(['login']);
    } else {
      this.lstSteps = retval.result;
      console.log(this.lstSteps);
      this.stepLoading=false;
    }
  });  
} 

simulateProcess(id){
  if(confirm('You are about to simulate this process. Continue?')) {
    this._approvalReviewServiceProxy.startApprovalWorkflow(id, 0).subscribe((retval:ApprovalResponseDTOApiResult)=>{
      console.log(retval);

      if(retval.hasError){
        abp.notify.error(retval.message);
        //this.router.navigate(['login']);
      } else {
        abp.notify.success(retval.message); 
        this.router.navigate(['/app/workflow/pending-list']);
      }
    })
  }
}


}
