import { ApprovalProcessIListApiResult, ApprovalSetupServiceProxy, ApprovalReviewServiceProxy, 
  ApprovalLogDTOIListApiResult, ApprovalLogDocDTO } from  "@shared/service-proxies/service-proxies";
import {Router } from '@angular/router'; 
import { async } from 'rxjs'; 
import { Component, OnInit, Injector, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Utils } from '../../../shared/helpers/Utils';
import { AppSessionService } from '../../../shared/session/app-session.service';

@Component({
  selector: 'app-pending-list',
  templateUrl: './pending-list.component.html',
  styleUrls: ['./pending-list.component.css']
})
export class PendingListComponent extends AppComponentBase implements OnInit {
 

  constructor(
    private _approvalReviewServiceProxy: ApprovalReviewServiceProxy,
    private _approvalSetupServiceProxy:ApprovalSetupServiceProxy,
    private _sessionService: AppSessionService,
    public router: Router,
    injector: Injector) {
    super(injector);
  }

  
  ngOnInit(): void {
    this.loadProcesses(); 
    this.loadPendingItems();
  }
 
 
  gotoItem(data:any){
    this.router.navigate(['app/workflow/pending-item'], { queryParams: { logid: data.id, itemid:data.itemId } });
  }
loading:boolean = false;
lstPendingItems :any = [];
lstProcesses :any = [];
processid : number = 0;
cols = [
  { field: 'id', header: 'S/N' },
  { field: 'name', header: 'Process Name' }, 
  // { field: 'description', header: 'Description' }, 
];
  loadPendingItems() {
    console.log(this.processid);
    this.loading=true;
    this._approvalReviewServiceProxy.fetchMyPendingApprovalLog(this.processid, this._sessionService.user.id).subscribe((retval:ApprovalLogDTOIListApiResult)=>{
      if(retval.hasError){
        this.router.navigate(['login']);
      } else {
        this.lstPendingItems = retval.result;
       
        console.log(this.lstPendingItems);
        this.loading=false;
      }
    });  
  }


  loadProcesses() { 
    this._approvalSetupServiceProxy.getApprovalProcesses(0).subscribe((retval:ApprovalProcessIListApiResult)=>{
      if(retval.hasError){
        this.router.navigate(['login']);
      } else {
        this.lstProcesses = retval.result; 
      }
    });  
  } 
 
}
