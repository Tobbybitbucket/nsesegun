import {ApprovalSetupServiceProxy, FormSectionIListApiResult, FormSection} from  "@shared/service-proxies/service-proxies";
import {Router } from '@angular/router'; 
import { async } from 'rxjs'; 
import { Component, OnInit, Injector, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/table';
import { Utils } from '../../../shared/helpers/Utils';

@Component({
  selector: 'app-form-section',
  // animations: [appModuleAnimation()],
  // changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './form-section.component.html',
  styleUrls: ['./form-section.component.css']
})
export class FormSectionComponent extends AppComponentBase implements OnInit {
 

  constructor(
    private _approvalSetupServiceProxy: ApprovalSetupServiceProxy,
    public router: Router,
    injector: Injector) {
    super(injector);
  }

  
  ngOnInit(): void {
    this.loadFormSections()
  }

  @ViewChild('dt') table: Table;
  loading: Boolean = false;
  displayItemDialog: Boolean = false; 
  lstFormSection :any = [];
  item_id:number = 0;
  item_name:string = '';
  status = Utils.getStatus();

  statusOptions = Object.keys(this.status).map(item => {
    return { label: item, value: item.toLowerCase() };
  });
  
  cols = [
    { field: 'id', header: 'S/N' },
    { field: 'name', header: 'Section Name' }, 
  ];
   
  buttonItems = [ 
    {
      label: 'Edit', command: (e) => {
        console.log(e);
        //this.editFormSection(e);
      }
    }
  ];
  showDialog() {
    this.displayItemDialog = !this.displayItemDialog;
  } 

  editFormSection(data:any=null){
    if(data==null){
      this.item_id = 0;
      this.item_name = '';
    } else { 
      this.item_id = data.id;
      this.item_name = data.name;
    }
    this.displayItemDialog = true;
  }

  loadFormSections() {
    this.loading=true;
    this._approvalSetupServiceProxy.getFormSections().subscribe((retval:FormSectionIListApiResult)=>{
      if(retval.hasError){
        this.router.navigate(['login']);
      } else {
        this.lstFormSection = retval.result;
        console.log(this.lstFormSection);
        this.loading=false;
      }
    });  
  }
 
}
