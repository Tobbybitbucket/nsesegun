import {
  DocTemplate,
  DocTemplateServiceProxy,
  DocTemplateApiResult,
  DocTemplateIListApiResult,
  ApprovalSetupServiceProxy,
  ApprovalProcess,
  ApprovalProcessIListApiResult,
} from "@shared/service-proxies/service-proxies";
import { Router } from "@angular/router";
import { async } from "rxjs";
import {
  Component,
  OnInit,
  Injector,
  ChangeDetectionStrategy,
  ViewChild,
} from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { Table } from "primeng/table";
import { Utils } from "../../../shared/helpers/Utils";

@Component({
  selector: "app-doc-template-list",
  templateUrl: "./doc-template-list.component.html",
  styleUrls: ["./doc-template-list.component.css"],
})
export class DocTemplateListComponent
  extends AppComponentBase
  implements OnInit {
  constructor(
    private _docTemplateServiceProxy: DocTemplateServiceProxy,
    private _approvalSetupServiceProxy: ApprovalSetupServiceProxy,
    public router: Router,
    injector: Injector
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.loadDocTemplates();
  }

  @ViewChild("dt") table: Table;
  loading: Boolean = false;
  displayItemDialog: Boolean = false;
  lstDocTemplates: any = [];
  item_id: number = 0;
  item_name: string = "";
  docTemplate: DocTemplate;
  status = Utils.getStatus();
  approvalProcessList: ApprovalProcess[];

  statusOptions = Object.keys(this.status).map((item) => {
    return { label: item, value: item.toLowerCase() };
  });

  cols = [
    // { field: 'id', header: 'S/N' },
    { field: "name", header: "Template Name" },
  ];

  buttonItems = [
    {
      label: "Edit",
      command: (e) => {
        console.log(e);
      },
    },
  ];
  showDialog() {
    this.displayItemDialog = !this.displayItemDialog;
  }

  editDocTemplate(data: any = null) {
    console.log(data, "00");

    if (data == null) {
      this.item_id = 0;
      this.item_name = "";
      this.displayItemDialog = true;
    } else {
      this.item_id = data.id;
      this.item_name = data.name;
      this._docTemplateServiceProxy
        .getDocTemplate("", this.item_id)
        .subscribe((retval: DocTemplateApiResult) => {
          console.log(retval);
          if (retval.hasError) {
            abp.notify.error(retval.message);
          } else {
            this.docTemplate = retval.result;
          }
          console.log(this.docTemplate);

          this.displayItemDialog = true;
        });
    }
    this.loadApprovalProcessList();
  }

  loadApprovalProcessList() {
    this._approvalSetupServiceProxy
      .getApprovalProcesses(0)
      .subscribe((retval: ApprovalProcessIListApiResult) => {
        if (retval.hasError) {
          abp.notify.error(retval.message);
        } else {
          this.approvalProcessList = retval.result;
          console.log(this.approvalProcessList);
        }
      });
  }

  loadDocTemplates() {
    this.loading = true;
    this.displayItemDialog = false;
    this._docTemplateServiceProxy
      .getDocTemplateList()
      .subscribe((retval: DocTemplateIListApiResult) => {
        if (retval.hasError) {
          this.router.navigate(["login"]);
        } else {
          this.lstDocTemplates = retval.result;
          console.log(this.lstDocTemplates);
          this.loading = false;
        }
      });
  }
}
