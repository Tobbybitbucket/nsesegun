import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocTemplateListComponent } from './doc-template-list.component';

describe('DocTemplateListComponent', () => {
  let component: DocTemplateListComponent;
  let fixture: ComponentFixture<DocTemplateListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocTemplateListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocTemplateListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
