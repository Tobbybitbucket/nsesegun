import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { finalize } from 'rxjs/operators';
import {ApprovalSetupServiceProxy, MessageOutApiResult, ApprovalProcess } from  "@shared/service-proxies/service-proxies";


@Component({
  selector: 'app-process-item-dialog',
  templateUrl: './process-item-dialog.component.html',
  styleUrls: ['./process-item-dialog.component.css']
})
export class ProcessItemDialogComponent implements OnInit {

  @Input() visible: boolean;
  @Input() processItem: ApprovalProcess;
  @Input() name: string;

  @Output("loadProcesses") loadProcesses: EventEmitter<any> = new EventEmitter();

  dialogTitle:string ="New Section";
  selectedValue = {}; 
 
  constructor(private _approvalSetupServiceProxy: ApprovalSetupServiceProxy,) { }

  ngOnInit(): void {
    if(this.processItem.id>0){
      this.dialogTitle = "Update Process";
    } else {
      this.dialogTitle = "New Process";
    }
  }

  arr = [ 
    { label: 'Application Type 1', value: { id: 1, name: 'Application Type 1', code: 'LT1' } },
    { label: 'Application Type 2', value: { id: 2, name: 'Application Type 2', code: 'LT2' } }
  ];

  saveform(){ 
    this._approvalSetupServiceProxy.saveApprovalProcesses(this.processItem).subscribe((retval:MessageOutApiResult)=>{
      if(retval.hasError){
        abp.notify.error(retval.message);
      } else {
        abp.notify.success(retval.message);
        this.loadProcesses.emit();
        this.visible=false;
      }
    }); 
  }

}