import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessItemDialogComponent } from './process-item-dialog.component';

describe('ProcessItemDialogComponent', () => {
  let component: ProcessItemDialogComponent;
  let fixture: ComponentFixture<ProcessItemDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessItemDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessItemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
