import { PaymentCalculatorComponent } from './payment-calculator/payment-calculator/payment-calculator.component';
import { ConfirmPaymentComponent } from './payments/confirm-payment/confirm-payment.component';
import { InvoiceDetailsComponent } from './application-invoice/invoice-details.component';
import { InvoiceComponent } from './application-invoice/invoice.component';
import { EmailMessageViewResolver, SentEmailMessageViewResolver } from './setting.resolver';
import { SentEmailMessagesComponent } from './email-messages/sent-email-messages/sent-email-messages';
import { ParticipantUsersComponent } from './users/participant-users/participant-users.component';
import { RequiredApplicationDocumentsComponent } from './nse-admin/setup/required-application-documents/required-application-documents.component';
import { OrganizationUsersComponent } from './users/organization-users/organization-users.component';
import { FormSectionComponent } from './workflow/form-section/form-section.component';
import { PendingListComponent } from './workflow/pending-list/pending-list.component';
import { DocTemplateListComponent } from './workflow/doc-template-list/doc-template-list.component';
import { ProcessListComponent } from './workflow/process-list/process-list.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { UsersComponent } from './users/users.component';
import { TenantsComponent } from './tenants/tenants.component';
import { RolesComponent } from 'app/roles/roles.component';
import { ChangePasswordComponent } from './users/change-password/change-password.component';
import { FaqComponent } from './faq/faq.component';
import { EmailMessagesComponent } from './email-messages/email-messages.component';
import { UserGuidesComponent } from './user-guides/user-guides.component';
import { ApplicationComponent } from './application/application.component';
import { ProfileComponent } from './profile/profile.component';
import { SettingsComponent } from './settings/settings.component';
import { EquityApplicationComponent } from './workspace/equity-application/equity-application.component';
import { StateBondsComponent } from './workspace/state-bonds/state-bonds.component';
import { CorporateBondsComponent } from './workspace/corporate-bonds/corporate-bonds.component';

// import { FormSectionComponent } from './workflow/form-section/form-section.component';
// import { ProcessListComponent } from './workflow/process-list/process-list.component';
// import { PendingListComponent } from './workflow/pending-list/pending-list.component';
import { PendingItemComponent } from "./workflow/pending-item/pending-item.component";
// import { DocTemplateListComponent } from './workflow/doc-template-list/doc-template-list.component';
// import { DocTemplateServiceProxy } from '@shared/service-proxies/workflow-service-proxy.service';

import { ApplicationStatusesComponent } from "./nse-admin/setup/application-statuses/application-statuses.component";
import { ApplicationTypesComponent } from "./nse-admin/setup/application-types/application-types.component";
import { CountriesComponent } from "./nse-admin/setup/countries/countries.component";
import { EmailSetupsComponent } from "./nse-admin/setup/email-setups/email-setups.component";
import { GendersComponent } from "./nse-admin/setup/genders/genders.component";
import { OrganizationsComponent } from "./nse-admin/setup/organizations/organizations.component";
import { IssuingCompaniesComponent } from "./nse-admin/setup/issuing-companies/issuing-companies.component";
import { LgasComponent } from "./nse-admin/setup/lgas/lgas.component";
import { ListingTypesComponent } from "./nse-admin/setup/listing-types/listing-types.component";
import { SmsSetupsComponent } from "./nse-admin/setup/sms-setups/sms-setups.component";
import { StatesComponent } from "./nse-admin/setup/states/states.component";
import { TitlesComponent } from "./nse-admin/setup/titles/titles.component";
import { ApplicationOverviewComponent } from "./application/application-overview/application-overview.component";
import { ProvideInformationComponent } from "./application/provide-information/provide-information.component";
import { DocumentChecklistComponent } from "./application/document-checklist/document-checklist.component";
import { ReviewComponent } from "./application/review/review.component";
import { ViewApplicationComponent } from "./view-application/view-application.component";
import { PendingItemReviewsComponent } from "./workflow/pending-item-reviews/pending-item-reviews.component";
import { FeesCalculationsComponent } from "./nse-admin/setup/fees-calculations/fees-calculations.component";
import { DocumentsComponent } from "./documents/documents.component";

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: "",
        component: AppComponent,
        children: [
          {
            path: "home",
            component: HomeComponent,
            canActivate: [AppRouteGuard],
          },
          {
            path: "users",
            component: UsersComponent,
            data: { permission: "ViewUser" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "participant-users/:organizationId",
            component: ParticipantUsersComponent,
            data: { permission: "ViewUser" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "organization-users",
            component: OrganizationUsersComponent,
            data: { permission: "ViewUser" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "roles",
            component: RolesComponent,
            data: { permission: "ViewRole" },
            canActivate: [AppRouteGuard],
          },
          { path: "about", component: AboutComponent },
          {
            path: "application-statuses",
            component: ApplicationStatusesComponent,
            data: { permission: "ViewApplicationStatus" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "application-types",
            component: ApplicationTypesComponent,
            data: { permission: "ViewApplicationType" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "required-application-documents",
            component: RequiredApplicationDocumentsComponent,
            data: { permission: "ViewRequiredApplicationDocument" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "countries",
            component: CountriesComponent,
            data: { permission: "ViewCountry" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "email-setups",
            component: EmailSetupsComponent,
            data: { permission: "ViewEmailSetup" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "genders",
            component: GendersComponent,
            data: { permission: "ViewGender" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "organizations",
            component: OrganizationsComponent,
            data: { permission: "ViewOrganization" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "issuing-companies",
            component: IssuingCompaniesComponent,
            data: { permission: "ViewIssuingCompany" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "lgas",
            component: LgasComponent,
            data: { permission: "ViewLga" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "listing-types",
            component: ListingTypesComponent,
            data: { permission: "ViewListingType" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "fees-calculation",
            component: FeesCalculationsComponent,
            data: { permission: "ViewFeesCalculation" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "sms-setups",
            component: SmsSetupsComponent,
            data: { permission: "ViewSmsSetup" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "states",
            component: StatesComponent,
            data: { permission: "ViewState" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "titles",
            component: TitlesComponent,
            data: { permission: "ViewTitle" },
            canActivate: [AppRouteGuard],
          },
          { path: "update-password", component: ChangePasswordComponent },
          { path: "faq", component: FaqComponent },
          {
            path: "email-messages",
            component: EmailMessagesComponent,
            resolve: { data: EmailMessageViewResolver },
            canActivate: [AppRouteGuard],
          },
          {
            path: "payment-calculator",
            component: PaymentCalculatorComponent,
            canActivate: [AppRouteGuard],
          },
          {
            path: 'sent-email-messages',
            component: SentEmailMessagesComponent,
            resolve: { data: SentEmailMessageViewResolver },
            canActivate: [AppRouteGuard]
          },
          {
            path: 'pay',
            component: InvoiceComponent,
            canActivate: [AppRouteGuard],
          },
          {
            path: "pay/:ref",
            component: InvoiceComponent,
            canActivate: [AppRouteGuard],
          },
          {
            path: 'invoice',
            component: InvoiceDetailsComponent,
            canActivate: [AppRouteGuard]
          },
          {
            path: 'ConfirmPayment',
            component: ConfirmPaymentComponent
          },
          {
            path: 'ConfirmPayment/:transactionRef',
            component: ConfirmPaymentComponent
          },
          { path: "userguides", component: UserGuidesComponent },
          {
            path: "new-application",
            component: ApplicationComponent,
            children: [
              { path: "", component: ApplicationOverviewComponent },
              { path: "instruction", component: ApplicationOverviewComponent },
              {
                path: "provide-information",
                component: ProvideInformationComponent,
              },
              {
                path: "document-checklist",
                component: DocumentChecklistComponent,
              },
              { path: "review", component: ReviewComponent },
            ],
          },
          { path: "documents", component: DocumentsComponent },
          { path: "view-application", component: ViewApplicationComponent },
          { path: "equity-application", component: EquityApplicationComponent },
          { path: "state-bonds", component: StateBondsComponent },
          { path: "corporate-bonds", component: CorporateBondsComponent },
          { path: "profile", component: ProfileComponent },
          { path: "settings", component: SettingsComponent },
          { path: "workflow/form-section", component: FormSectionComponent },
          { path: "workflow/pending-list", component: PendingListComponent },
          { path: "workflow/pending-item", component: PendingItemComponent },
          {
            path: "workflow/pending-item/review",
            component: PendingItemReviewsComponent,
          },
          { path: "workflow/process-list", component: ProcessListComponent },
          {
            path: "workflow/doc-template",
            component: DocTemplateListComponent,
          },
        ],
      },
    ]),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
