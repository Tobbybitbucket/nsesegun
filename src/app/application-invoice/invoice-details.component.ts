import { AppComponentBase } from 'shared/app-component-base';
import { Component, OnInit, Renderer2, Inject, AfterViewInit, Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { PaymentServiceProxy, InitializeXpayPaymentResponse, InitializeXpayPaymentRequest } from '@shared/service-proxies/service-proxies';
import { DOCUMENT } from '@angular/common';
import { finalize } from 'rxjs/operators';
import SWAL from 'sweetalert2';
import Swal from 'sweetalert2';

@Component({
    templateUrl: 'invoice-details.component.html'
})

export class InvoiceDetailsComponent extends AppComponentBase implements OnInit {

    webPayModel: InitializeXpayPaymentResponse | undefined;
    model: InitializeXpayPaymentRequest | undefined;
    transactionRef = '';

    constructor(injector: Injector,
        private route: ActivatedRoute,
        private router: Router,
        public _paymentService: PaymentServiceProxy,
        private ngxService: NgxUiLoaderService,
        private renderer2: Renderer2,
        @Inject(DOCUMENT) private _document: Document
    ) {
        super(injector);
        this.webPayModel = new InitializeXpayPaymentResponse();
    }

    ngOnInit(): void {
        this.transactionRef = this.route.snapshot.queryParams.ref;
        this.getWebPayDetail(this.transactionRef);
    }
    getWebPayDetail(transactionRef: string): void {
        this.ngxService.start();
        this.model = new InitializeXpayPaymentRequest();
        this.model.transactionRef = transactionRef;
        this._paymentService.initializeXpayValues(this.model).pipe(
            finalize(() => {
                this.ngxService.stop();
            })
        ).subscribe((result: InitializeXpayPaymentResponse) => {
            this.webPayModel = result;
        });
    }
}
