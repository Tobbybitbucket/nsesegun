import { AppComponentBase } from 'shared/app-component-base';
import { Component, OnInit, Renderer2, Inject, AfterViewInit, Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { PaymentServiceProxy, InitializeXpayPaymentResponse, InitializeXpayPaymentRequest } from '@shared/service-proxies/service-proxies';
import { DOCUMENT } from '@angular/common';
import { finalize } from 'rxjs/operators';
import SWAL from 'sweetalert2';
import Swal from 'sweetalert2';

@Component({
    templateUrl: 'invoice.component.html'
})

export class InvoiceComponent extends AppComponentBase implements OnInit {

    webPayModel: InitializeXpayPaymentResponse | undefined;
    model: InitializeXpayPaymentRequest | undefined;
    transactionRef = '';

    constructor(injector: Injector,
        private route: ActivatedRoute,
        private router: Router,
        public _paymentService: PaymentServiceProxy,
        private ngxService: NgxUiLoaderService,
        private renderer2: Renderer2,
        @Inject(DOCUMENT) private _document: Document
    ) {
        super(injector);
        this.webPayModel = new InitializeXpayPaymentResponse();
    }

    ngOnInit(): void {
        this.transactionRef = this.route.snapshot.queryParams.ref;
        this.getWebPayDetail(this.transactionRef);
    }
    getWebPayDetail(transactionRef: string): void {
        this.ngxService.start();
        this.model = new InitializeXpayPaymentRequest();
        this.model.transactionRef = transactionRef;
        this._paymentService.initializeXpayValues(this.model).pipe(
            finalize(() => {
                this.ngxService.stop();
            })
        ).subscribe((result: InitializeXpayPaymentResponse) => {
            this.webPayModel = result;
        });
    }
    onSubmit(e: any): void {
        SWAL.fire({
            title: 'Are you sure?',
            text: `Click confirm to pay with card`,
            icon: 'warning',
            confirmButtonColor: '#a8ac2e',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, proceed!',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                this.ngxService.start();
                console.log(e);
                e.target.submit();
            }
        });
    }

    payOffline(): void {
        const message = `The details of this transaction has been sent to your email.
        Kindly proceed to a bank branch or use your internet bank transfer to make your payment.
        Ensure you put this transaction ref : ${this.webPayModel.tranactionRef} when you are making your payment and send payment evidence to Listing Regulation Department of the Nigerian Stock Exchange.`;
        SWAL.fire({
            title: 'Are you sure?',
            text: `Click confirm to pay offline`,
            icon: 'warning',
            confirmButtonColor: '#a8ac2e',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, proceed!',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                this.ngxService.start();
                this._paymentService.sendApplicationInvoice(this.model)
                    .pipe(finalize(() => {
                        this.ngxService.stop();
                    }))
                    .subscribe((res: boolean) => {
                        if (res) {
                            SWAL.fire(
                                'Successful!',
                                `${message}`,
                                'success'
                            );
                        } else {
                            SWAL.fire(
                                'Failed!',
                                `Something went wrong`,
                                'error'
                            );
                        }
                        this.router.navigate(['/app/home']);
                    });
                this.notify.success(message);
            }
        });
    }
}
