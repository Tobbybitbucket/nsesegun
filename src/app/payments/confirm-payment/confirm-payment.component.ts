import { XpayGetTransactionDetailsRequest, XpayGetTransactionDetailsResponseStringTuple } from './../../../shared/service-proxies/service-proxies';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRouteSnapshot, Router, ActivatedRoute } from '@angular/router';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { XpayGetTransactionDetailsResponse, PaymentServiceProxy } from '@shared/service-proxies/service-proxies';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { appModuleAnimation } from '@shared/animations/routerTransition';
@Component({
    selector: 'app-confirm-payment',
    templateUrl: './confirm-payment.component.html',
    styleUrls: ['./confirm-payment.component.css'],
    animations: [appModuleAnimation()],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmPaymentComponent implements OnInit {

    transactionRef: string;
    webPayPayment: XpayGetTransactionDetailsResponse | undefined;
    webPayPaymentError: string | undefined;
    order: any;
    loading = false;
    isWebPayPayment = false;

    constructor(
        private route: ActivatedRoute,
        private ngxService: NgxUiLoaderService,
        private paymentService: PaymentServiceProxy) {
    }

    ngOnInit() {
        this.transactionRef = this.route.snapshot.queryParams.transactionRef;
        this.confirmPayment(this.transactionRef);

    }
    confirmPayment(transRef: string): void {
        const getTransRef = new XpayGetTransactionDetailsRequest();
        getTransRef.transactionRef = transRef;
        this.paymentService.finalizePayment(getTransRef)
            .subscribe((result: XpayGetTransactionDetailsResponseStringTuple) => {
                if (result.item1 !== null || result.item1 !== undefined) {
                    this.webPayPayment = result.item1;
                    this.webPayPaymentError = result.item2;
                    if (this.webPayPaymentError !== '' || this.webPayPaymentError !== undefined) {

                    }

                    if (this.webPayPayment.confirmationStatus === '00' || this.webPayPayment.confirmationStatus === '0') {

                    } else {
                    }

                }

            },
                error => {
                    console.log(error);
                });
    }

    pad2(n) {
        return n < 10 ? '0' + n : n;
    }

    convertToPdf() {
        const date = new Date();
        const dddd = this.pad2(date.getMonth() + 1) + this.pad2(date.getDate())
            + this.pad2(date.getHours()) + this.pad2(date.getMinutes()) + this.pad2(date.getSeconds());
        const data = document.getElementById('contentToConvert');
        html2canvas(data).then(x => {
            const imgWidth = 208;
            const pageHeight = 295;
            const imgHeight = x.height * imgWidth / x.width;
            const heightLeft = imgHeight;

            const contentDataURL = x.toDataURL('image/png');
            // const pdf = new jspdf('p', 'mm', 'a4');
            // const position = 10;
            // pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
            // pdf.save('Subscriptions_Payment_' + dddd + '.pdf');
        });
    }
    printReceipt() {
        const divToPrint = document.getElementById('print-content');
        const newWin = window.open('', 'Print-Window');
        newWin.document.open();
        newWin.document.write('<html><link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" media="print"/><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 10);
    }
}
