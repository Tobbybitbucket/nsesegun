import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Observable, throwError as _observableThrow, of as _observableOf, of } from 'rxjs';
import { Injectable } from '@angular/core';


@Injectable()
export class StaticDataService {
    FieldsData: any = {
        "result": [
        {
            "id": 1,
            "type": "initial_listing",
            "sections": [
                {
                  "name":"Introduction",
                  "fields":[
                    {
                    "id": 1,
                    "name": "nameOfIssuer",
                    "label": "Name of Issuer",
                    "fieldType": "input",
                    "required": true
                    },
                    {
                    "id": 2,
                    "name": "applicationType",
                    "label": "Type of Application",
                    "fieldType": "dropdown",
                    "required": true,
                    "options":[
                     {label: "Initial Public Offering", value: "Initial Public Offering" },
                     {label:"Listing by Introduction", value:"Listing by Introduction"},
                     {label:"Merger and Acquisition", value:"Merger and Acquisition"},
                     {label:"Others"}
                    ]
                    },
                    {
                    "id": 3,
                    "name": "description",
                    "label": "Description of Application",
                    "fieldType": "textarea",
                    "required": true
                    }
                  ]
                },
                {
                  "name": "Parties to the Listing",
                  "fields":[
                    {
                    "id": 1,
                    "name": "sponsoringTradingLicenseHolder",
                    "label": "Sponsoring Trading License Holder",
                    "fieldType": "input",
                    "required": true
                    },
                    {
                    "id": 2,
                    "name": "jointSponsoringTradingLicenseHolders",
                    "label": "Joint Sponsoring Trading License Holder(s)",
                    "fieldType": "input",
                    "required": false
                    },
                    {
                    "id": 3,
                    "name": "issuingHouse",
                    "label": "Issuing House",
                    "fieldType": "input",
                    "required": false
                    },
                    {
                    "id": 4,
                    "name": "jointIssuingHouses",
                    "label": "Joint Issuing House(s)",
                    "fieldType": "input",
                    "required": false
                    },
                    {
                    "id": 5,
                    "name": "auditor",
                    "label": "Auditor",
                    "fieldType": "input",
                    "required": false
                    },
                    {
                    "id": 6,
                    "name": "reportingAccountant",
                    "label": "Reporting Accountant",
                    "fieldType": "input",
                    "required": false
                    },
                    {
                    "id": 7,
                    "name": "solicitorsToTheIssue",
                    "label": "Solicitor(s) to the Issue",
                    "fieldType": "input",
                    "required": false
                    },
                    {
                    "id": 8,
                    "name": "registrar",
                    "label": "Registrar",
                    "fieldType": "input",
                    "required": false
                    }
                  ]
                },
                {
                  "name": "Share Capital Base & Listing Data",
                  "fields":[
                    {
                    "id": 1,
                    "name": "authorizedShareCapitalVolume",
                    "label": "Authorized Share Capital (Volume)",
                    "fieldType": "number",
                    "required": true
                    },
                    {
                    "id": 2,
                    "name": "authorizedShareCapitalValue",
                    "label": "Authorized Share Capital (Value)",
                    "fieldType": "number",
                    "required": true
                    },
                    {
                    "id": 3,
                    "name": "issuedAndFullyPaidVolume",
                    "label": "Issued and Fully Paid (Volume)",
                    "fieldType": "number",
                    "required": true
                    },
                    {
                    "id": 4,
                    "name": "issuedAndFullyPaidValue",
                    "label": "Issued and Fully Paid (Value)",
                    "fieldType": "number",
                    "required": true
                    },
                    {
                    "id": 5,
                    "name": "nominalValue",
                    "label": "Nominal Value",
                    "fieldType": "number",
                    "required": true
                    },
                    {
                    "id": 6,
                    "name": "proposedOfferOrListingSizeVolume",
                    "label": "Proposed Offer/Listing Size (Volume)",
                    "fieldType": "number",
                    "required": false
                    },
                    {
                    "id": 7,
                    "name": "postOfferSizeVolume",
                    "label": "Post Offer Size (Volume)",
                    "fieldType": "number",
                    "required": false
                    },
                    {
                    "id": 8,
                    "name": "offerOrListingPrice",
                    "label": "Offer/Listing Price ",
                    "fieldType": "input",
                    "required": false
                    },
                    {
                    "id": 9,
                    "name": "marketCapitalisation",
                    "label": "Market Capitalisation",
                    "fieldType": "number",
                    "required": false
                    },
                    {
                    "id": 10,
                    "name": "board",
                    "label": "Board",
                    "fieldType": "dropdown",
                    "required": true,
                    "options": [
                      {label:"Premium", value:"Premium"},
                      {label:"Main", value :"Main"},
                      {label:"Growth – Entry Segment", value :"Growth – Entry Segment"},
                      {label:"Growth – Standard Segment",value : "Growth – Standard Segment"},
                      { label:"ASeM", value :"ASeM"}
                    ]
                    },
                    {
                      "id": 11,
                      "label": "",
                      "fieldType": "sub-section",
                      "subSection":[
                          {
                            "id": 1,
                            "name": "priceRange",
                            "label": "Price Range",
                            "fieldType": "number",
                            "required": false
                          },
                          {
                            "id": 2,
                            "name": "transactionValue",
                            "label": "Transaction Value",
                            "fieldType": "number",
                            "required": false
                          }
                        ]
                      }
                  ]
                },
                {
                  "name": "Utilization of the Proceeds (Public Offer)",
                  "fields":[
                    {
                      "id": 1,
                      "name": "utilization",
                      "label": "Utilization",
                      "fieldType": "input",
                      "required": false
                      },
                      {
                      "id": 2,
                      "name": "amountInNaira",
                      "label": "Amount (N)",
                      "fieldType": "input",
                      "required": false
                      },
                      {
                      "id": 3,
                      "name": "percentageOfGrossProceed",
                      "label": "% of Gross proceed ",
                      "fieldType": "input",
                      "required": false
                      },
                      {
                      "id": 4,
                      "name": "estimatedCompletionPeriod",
                      "label": "Estimated Completion Period",
                      "fieldType": "input",
                      "required": false
                      },
                      {
                      "id": 5,
                      "label": "Subtotal",
                      "fieldType": "sub-section",
                      "subSection": [
                        {
                          "id": 1,
                          "name": "amountInNairaSubtotal",
                          "label": "Amount (N)",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                        "id": 2,
                        "name": "percentageOfGrossProceedSubtotal",
                        "label": "% Of Gross Proceed",
                        "fieldType": "input",
                        "required": false
                        },
                        {
                        "id": 2,
                        "name": "estimatedCompletionPeriodSubtotal",
                        "label": "Estimated Completion Period",
                        "fieldType": "input",
                        "required": false
                        }
                      ]
                      },
                      {
                      "id": 6,
                      "label": "Cost of Issue",
                      "fieldType": "sub-section",
                      "subSection": [
                        {
                          "id": 1,
                          "name": "amountInNairaCostOfIssue",
                          "label": "Subtotal",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                        "id": 2,
                        "name": "percentageOfGrossProceedCostOfIssue",
                        "label": "% Of Gross Proceed",
                        "fieldType": "input",
                        "required": false
                        },
                        {
                        "id": 2,
                        "name": "estimatedCompletionPeriodCostOfIssue",
                        "label": "Estimated Completion Period",
                        "fieldType": "input",
                        "required": false
                        }
                      ]
                      },
                      {
                      "id": 7,
                      "label": "Grand Total ",
                      "fieldType": "sub-section",
                      "subSection": [
                        {
                          "id": 1,
                          "name": "amountInNairaGrandTotal",
                          "label": "Subtotal",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                        "id": 2,
                        "name": "percentageOfGrossProceedGrandTotal",
                        "label": "% Of Gross Proceed",
                        "fieldType": "input",
                        "required": false
                        },
                        {
                        "id": 2,
                        "name": "estimatedCompletionPeriodGrandTotal",
                        "label": "Estimated Completion Period",
                        "fieldType": "input",
                        "required": false
                        }
                      ]
                      }
                  ]
                },
                {
                  "name":"Outstanding Debt Obligation",
                  "fields":[
                    {
                    "id": 1,
                    "name": "outstandingDebtObligation",
                    "label": "",
                    "description": "Provide brief details of the issuer’s debt obligations, other than in the ordinary course of business",
                    "fieldType": "textarea",
                    "required": false
                    }
                  ]
                },
                {
                  "name":"Claims and Litigation",
                  "fields":[
                    {
                    "id": 1,
                    "name": "totalNumberOfCasesInstitutedAgainstTheIssuer",
                    "label": "Total number of cases instituted against the Issuer",
                    "fieldType": "number",
                    "required": false
                    },
                    {
                    "id": 2,
                    "name": "totalNumberOfCasesInstitutedByTheIssuer",
                    "label": "Total number of cases instituted by the Issuer",
                    "fieldType": "number",
                    "required": false
                    },
                    {
                    "id": 3,
                    "name": "totalNumberOfCasesInvolvedByTheIssuer",
                    "label": "Total number of cases involved by the Issuer",
                    "fieldType": "number",
                    "required": false
                    },
                    {
                      "id": 4,
                      "label": "Indebtedness",
                      "fieldType": "sub-section",
                      "subSection": [
                        {
                          "id": 1,
                          "name": "indebtednessValueInNaira",
                          "label": "Value (N)",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                          "id": 2,
                          "name": "",
                          "label": "Value (Other currency)",
                          "fieldType": "input",
                          "required": false
                        }
                      ]
                    },
                    {
                      "id": 5,
                      "label": "Total amount claimed from the number of cases instituted against the Issuer",
                      "fieldType": "sub-section",
                      "subSection": [
                        {
                          "id": 1,
                          "name": "totalAmountClaimedFromTheNumberOfCasesInstitutedAgainstTheIssuerInNaira",
                          "label": "Value (N)",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                          "id": 2,
                          "name": "",
                          "label": "Value (Other currency)",
                          "fieldType": "input",
                          "required": false
                        }
                      ]
                    },
                    {
                      "id": 6,
                      "label": "Total amount claimed from the number of cases instituted by the Issuer",
                      "fieldType": "sub-section",
                      "subSection": [
                        {
                          "id": 1,
                          "name": "totalAmountClaimedFromTheNumberOfCasesInstitutedByTheIssuerInNaira",
                          "label": "Value (N)",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                          "id": 2,
                          "name": "",
                          "label": "Value (Other currency)",
                          "fieldType": "input",
                          "required": false
                        }
                      ]
                    }
                  ]
                },
                {
                  "name":"Issuer’s Profile",
                  "fields":[
                    {
                      "id": 1,
                      "name": "briefDescriptionOfIssuer",
                      "label": "Brief Description of Issuer",
                      "tooltip": "The Issuer’s profile should include the date of incorporation, nature of business, industry.",
                      "fieldType": "textarea",
                      "required": false
                    }
                  ]
                },
                {
                  "name":"Reason for Listing",
                  "fields":[
                    {
                      "id": 1,
                      "name": "briefDescriptionOfReasonForListing",
                      "label": "Brief Description of reason for listing",
                      "fieldType": "textarea",
                      "required": false
                    }
                  ]
                },
                {
                  "name":"Promoter(s) Profile",
                  "fields":[
                    {
                      "id": 1,
                      "label": "",
                      "fieldType": "sub-section",
                      "subSection": [
                        {
                          "id": 1,
                          "name": "fullName",
                          "label": "Full Name",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                          "id": 2,
                          "name": "briefProfile",
                          "label": "Brief Profile ",
                          "fieldType": "textarea",
                          "required": false
                        }
                      ]
                    }
                  ]
                },
                {
                  "name":"Shareholding Structure of the Issuer",
                  "fields":[
                    {
                      "id": 1,
                      "label": "",
                      "fieldType": "sub-section",
                      "subSection": [
                        {
                          "id": 1,
                          "name": "nameOfShareholder",
                          "label": "Name of Shareholder",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                          "id": 2,
                          "label": "Pre-Offer/Merger",
                          "fieldType": "sub-sub-section",
                          "subSubSection": [
                            {
                              "id": 1,
                              "name": "noOfSharesHeldPreOfferOrMerger",
                              "label": "No of Shares Held",
                              "fieldType": "number",
                              "required": false
                            },
                            {
                              "id": 2,
                              "name": "percentageOfHoldingPreOfferOrMerger",
                              "label": "% of Holding",
                              "fieldType": "number",
                              "required": false
                            }
                          ]
                        },
                        {
                          "id": 3,
                          "label": "Post-Offer/Merger ",
                          "fieldType": "sub-sub-section",
                          "subSubSection": [
                            {
                              "id": 1,
                              "name": "noOfSharesHeldPostOfferOrMerger",
                              "label": "No of Shares Held",
                              "fieldType": "number",
                              "required": false
                            },
                            {
                              "id": 2,
                              "name": "percentageOfHoldingPostOfferOrMerger",
                              "label": "% of Holding",
                              "fieldType": "number",
                              "required": false
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "id": 1,
                      "label": "Total",
                      "fieldType": "sub-section",
                      "subSection": [
                        {
                          "id": 1,
                          "label": "Pre-Offer/Merger",
                          "fieldType": "sub-sub-section",
                          "subSubSection": [
                            {
                              "id": 1,
                              "name": "noOfSharesHeldPreOfferOrMergerTotal",
                              "label": "No of Shares Held",
                              "fieldType": "number",
                              "required": false
                            },
                            {
                              "id": 2,
                              "name": "percentageOfHoldingPreOfferOrMergerTotal",
                              "label": "% of Holding",
                              "fieldType": "number",
                              "required": false
                            }
                          ]
                        },
                        {
                          "id": 2,
                          "label": "Post-Offer/Merger ",
                          "fieldType": "sub-sub-section",
                          "subSubSection": [
                            {
                              "id": 1,
                              "name": "noOfSharesHeldPostOfferOrMergerTotal",
                              "label": "No of Shares Held",
                              "fieldType": "number",
                              "required": false
                            },
                            {
                              "id": 2,
                              "name": "percentageOfHoldingPostOfferOrMergerTotal",
                              "label": "% of Holding",
                              "fieldType": "number",
                              "required": false
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "name":"Directors and Their Beneficial Interests",
                  "fields": [
                    {
                      "id": 1,
                      "label": "",
                      "fieldType": "sub-section",
                      "subSection": [
                        {
                          "id": 1,
                          "name": "nameOfDirectorsBeneficialInterests",
                          "label": "Name of Directors",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                          "id": 2,
                          "name": "designationBeneficialInterests",
                          "label": "Designation",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                          "id": 3,
                          "name": "directShareholdingBeneficialInterests",
                          "label": "Direct Shareholding",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                          "id": 4,
                          "name": "indirectHoldingBeneficialInterests",
                          "label": "Indirect Holding",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                          "id": 5,
                          "name": "totalShareholdingBeneficialInterests",
                          "label": "Total Shareholding",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                          "id": 6,
                          "name": "percentageHeldBeneficialInterests",
                          "label": "%tage held",
                          "fieldType": "input",
                          "required": false
                        }
                      ]
                    },
                    {
                      "id": 2,
                      "label": "Total",
                      "fieldType": "sub-section",
                      "subSection": [
                        {
                          "id": 1,
                          "name": "",
                          "label": "Direct Shareholding",
                          "fieldType": "number",
                          "required": false
                        },
                        {
                          "id": 2,
                          "name": "",
                          "label": "Indirect Holding",
                          "fieldType": "number",
                          "required": false
                        },
                        {
                          "id": 3,
                          "name": "",
                          "label": "Total Shareholding",
                          "fieldType": "number",
                          "required": false
                        },
                        {
                          "id": 4,
                          "name": "",
                          "label": "%tage held",
                          "fieldType": "number",
                          "required": false
                        }
                      ]
                    }
                  ]
                },
                {
                  "name":"Subsidiaries of the Issuer",
                  "fields":[
                    {
                      "id": 1,
                      "label": "",
                      "fieldType": "sub-section",
                      "subSection": [
                        {
                          "id": 1,
                          "name": "nameOfSubsidiary",
                          "label": "Name of subsidiary ",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                          "id": 2,
                          "name": "dateOfAcquisition",
                          "label": "Date of Acquisition",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                          "id": 3,
                          "name": "percentageHeldByTheIssuerInTheSubsidiary",
                          "label": "Percentage Held by the Issuer in the Subsidiary ",
                          "fieldType": "input",
                          "required": false
                        }
                      ]
                    }
                  ]
                },
                {
                  "name":"Profile of the Other Companies (For Merger only)",
                  "fields":[
                    {
                      "id": 1,
                      "name": "provideABriefProfileOfTheOtherCompaniesInvolvedInTheMerger",
                      "label": "",
                      "description": "Provide a brief profile of the other companies involved in the merger and names of their directors.",
                      "fieldType": "textarea",
                      "required": false
                    }
                  ]
                },
                {
                  "name":"Material Contracts",
                  "fields":[
                    {
                      "id": 1,
                      "name": "provideAListOfTheAgreementsAndContractsWhichTheIssuerHasEnteredIntoMaterialContracts",
                      "label": "Provide a list of the agreements and contracts which the Issuer has entered into that are material to offer and/or  listing or to its business, other than in the ordinary course of business.",
                      "fieldType": "textarea",
                      "required": false
                    }
                  ]
                },
                {
                  "name":"Upload Supporting Documents",
                  "fields":[
                    {
                      "id": 1,
                      "name": "",
                      "label": "Prospectus",
                      "fieldType": "file",
                      "required": true
                    },
                    {
                      "id": 2,
                      "name": "",
                      "label": "Memorandum and Articles of Association ",
                      "fieldType": "file",
                      "required": false
                    },
                    {
                      "id": 3,
                      "name": "",
                      "label": "Certificate of Incorporation",
                      "fieldType": "file",
                      "required": false
                    },
                    {
                      "id": 4,
                      "name": "",
                      "label": "Board Resolution",
                      "fieldType": "file",
                      "required": false
                    },
                    {
                      "id": 4,
                      "name": "",
                      "label": "Other Documents",
                      "tooltip": "link for checklist",
                      "fieldType": "file",
                      "required": false
                    }
                  ]
                },
                {
                  "name":"Declaration",
                  "fields":[
                    {
                      "id": 1,
                      "name": "managingDirectorOfTradingLicenseHolderDeclaration",
                      "label": "Managing Director of the Sponsoring Trading License Holder",
                      "fieldType": "textarea",
                      "description": "I confirm that the above information is accurate to the best of my knowledge.",
                      "required": false
                    }
                  ]
                }
              ]
        },
        {
            "id": 2,
            "type": "mutual_Funds",
            "sections": [
              {
                "name":"Introduction",
                "fields":[
                  {
                  "id": 1,
                  "name": "nameOfFundManagerOrIssuer",
                  "label": "Name of Fund Manager/Issuer",
                  "fieldType": "input",
                  "required": true
                  },
                  {
                  "id": 2,
                  "name": "listingMethodsByIntroductionOrPublicOffer",
                  "label": "Listing Methods (by introduction or Public Offer)",
                  "fieldType": "input",
                  "required": true
                  },
                  {
                  "id": 3,
                  "name": "description",
                  "label": "Description of Application",
                  "fieldType": "textarea",
                  "required": true
                  }
                ]
              },
              {
                "name": "Professional parties to the Listing",
                "fields":[
                  {
                    "id": 1,
                    "label": "Name",
                    "fieldType": "sub-section",
                    "subSection": [
                      {
                        "id": 1,
                        "name": "sponsoringTradingLicenseHolder",
                        "label": "Sponsoring Trading License Holder ",
                        "fieldType": "input",
                        "required": false
                      },
                      {
                        "id": 2,
                        "name": "nameOfFundManagerOrIssuer",
                        "label": "Fund Manager/Issuer",
                        "fieldType": "input",
                        "required": false
                      },
                      {
                        "id": 3,
                        "name": "issuingHouse",
                        "label": "Issuing House",
                        "fieldType": "input",
                        "required": false
                      },
                      {
                        "id": 4,
                        "name": "assetsManager",
                        "label": "Assets Manager",
                        "fieldType": "input",
                        "required": false
                      }
                    ]
                  }
                ]
              },
              {
                "name":"Listing Data",
                "fields":[
                  {
                    "id": 1,
                    "name":"methodOfOffer",
                    "label": "Method of Offer",
                    "fieldType": "input",
                    "required":false
                  },
                  {
                    "id": 2,
                    "name":"volumeBeingOffered",
                    "label": "Volume being offered",
                    "fieldType": "input",
                    "required":false
                  },
                  {
                    "id": 3,
                    "name":"priceValue",
                    "label": "Price (Value)",
                    "fieldType": "input",
                    "required":false
                  },
                  {
                    "id": 4,
                    "name":"valueOfTheOffer",
                    "label": "Value of the offer",
                    "fieldType": "input",
                    "required":false
                  },
                  {
                    "id": 5,
                    "name":"minimumSubscription",
                    "label": "Minimum subscription",
                    "fieldType": "input",
                    "required":false
                  },
                  {
                    "id": 6,
                    "name":"rating",
                    "label": "Rating",
                    "fieldType": "input",
                    "required":false
                  }
                ]
              },
              {
                "name":"Background information about the Fund",
                "fields":[
                  {
                    "id": 1,
                    "name":"",
                    "label": "",
                    "fieldType": "textarea",
                    "required":false
                  }
                ]
              },
              {
                "name":"Proposed Utilisation of Funds",
                "fields":[
                    {
                      "id": 1,
                      "description": "The assets of the funds will from time to time be allocated as stated in the table below.",
                      "label": "",
                      "fieldType": "sub-section",
                      "subSection": [
                        {
                          "id": 1,
                          "name": "descriptionOfProposedAsset",
                          "label": "Description of Proposed Asset",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                          "id": 2,
                          "name": "amountAllocatedInNaira",
                          "label": "Amount Allocated (N)",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                          "id": 3,
                          "name": "targetWeightingPercentage",
                          "label": "Target Weighting (%)",
                          "fieldType": "input",
                          "required": false
                        }
                      ]
                    },
                    {
                      "id": 2,
                      "label": "Total ",
                      "fieldType": "sub-section",
                      "subSection": [
                        {
                          "id": 1,
                          "name": "amountAllocatedInNaira",
                          "label": "Amount Allocated (N)",
                          "fieldType": "input",
                          "required": false
                        },
                        {
                          "id": 2,
                          "name": "targetWeightingPercentage",
                          "label": "Target Weighting (%)",
                          "fieldType": "input",
                          "required": false
                        }
                      ]
                    }
                ]
              },
              {
                "name":"Investment Objectives/Strategy and Policies of the Fund",
                "fields":[
                  {
                    "id": 1,
                    "name":"",
                    "label": "",
                    "fieldType": "textarea",
                    "required":false
                  }
                ]
              },
              {
                "name":"About the Fund Manager",
                "fields":[
                  {
                    "id": 1,
                    "name":"briefDescriptionOfFundManager",
                    "label": "Brief description of Fund Manager",
                    "fieldType": "textarea",
                    "required":false
                  }
                ]
              },
              {
                "name":"Outstanding Debt Obligation",
                "fields":[
                  {
                    "id": 1,
                    "name":"provideBriefDetailsOfTheFundManagersCurrentDebtObligationsOtherThanInTheOrdinaryCourseOfBusiness",
                    "description": "(Provide brief details of the Fund Manager’s current debt obligations, other than in the ordinary course of business.)",
                    "label": "",
                    "fieldType": "textarea",
                    "required":false
                  }
                ]
              },
              {
                "name":"Claims, Litigation and Indebtedness",
                "fields":[
                  {
                  "id": 1,
                  "label": "Cases instituted against the Fund Manager",
                  "fieldType": "sub-section",
                  "subSection": [
                    {
                      "id": 1,
                      "name": "numberOfCasesInstitutedAgainstTheFundManager",
                      "label": "Number",
                      "fieldType": "input",
                      "required": false
                    },
                    {
                      "id": 2,
                      "name": "numberOfCasesInstitutedAgainstTheFundManagerValueClaimedInNaira",
                      "label": "Value Claimed (N)",
                      "fieldType": "input",
                      "required": false
                    }
                  ]
                  },
                  {
                  "id": 2,
                  "label": "Cases instituted by the Fund Manager",
                  "fieldType": "sub-section",
                  "subSection": [
                    {
                      "id": 1,
                      "name": "numberOfCasesInstitutedAgainstTheFundManager",
                      "label": "Number",
                      "fieldType": "input",
                      "required": false
                    },
                    {
                      "id": 2,
                      "name": "numberOfCasesInstitutedAgainstTheFundManagerValueClaimedInNaira",
                      "label": "Value Claimed (N)",
                      "fieldType": "input",
                      "required": false
                    }
                  ]
                  },
                  {
                  "id": 3,
                  "label": "Cases involved by the Fund Manager",
                  "fieldType": "sub-section",
                  "subSection": [
                    {
                      "id": 1,
                      "name": "numberOfCasesInvolvedByTheFundManager",
                      "label": "Number",
                      "fieldType": "input",
                      "required": false
                    },
                    {
                      "id": 2,
                      "name": "numberOfCasesInvolvedByTheFundManagerValueClaimedInNaira",
                      "label": "Value Claimed (N)",
                      "fieldType": "input",
                      "required": false
                    }
                  ]
                  },
                  {
                  "id": 4,
                  "label": "Indebtedness",
                  "fieldType": "sub-section",
                  "subSection": [
                    {
                      "id": 1,
                      "name": "indebtednessValueInNaira",
                      "label": "Value (N)",
                      "fieldType": "input",
                      "required": false
                    },
                    {
                      "id": 2,
                      "name": "",
                      "label": "Value (Other currency)",
                      "fieldType": "input",
                      "required": false
                    }
                  ]
                  }
                ]
              },
              {
                "name":"Material Contracts",
                "fields":[
                  {
                    "id": 1,
                    "name":"provideAListOfTheAgreementsAndContractsWhichTheIssuerHasEnteredIntoMaterialContracts",
                    "label": "Provide a list of the agreements and contracts which the Fund Manager has entered into that are material to offer and/or  listing or to its business, other than in the ordinary course of business.",
                    "fieldType": "textarea",
                    "required":false
                  }
                ]
              },
              {
                "name":"Upload Supporting Documents",
                "fields":[
                  {
                    "id": 1,
                    "name": "",
                    "label": "Prospectus",
                    "fieldType": "file",
                    "required": true
                  },
                  {
                    "id": 2,
                    "name": "",
                    "label": "Listing Memorandum",
                    "fieldType": "file",
                    "required": true
                  },
                  {
                    "id": 3,
                    "name": "",
                    "label": "Other Documents (as listed out in the checklists)",
                    "fieldType": "file",
                    "required": false
                  }
                ]
              },
              {
                "name":"Declaration",
                "fields":[
                  {
                    "id": 1,
                    "name": "managingDirectorOfTheSponsoringTradingLicenseHolder",
                    "label": "Managing Director of the Sponsoring Trading License Holder",
                    "fieldType": "textarea",
                    "description": "I confirm that the above information is accurate to be best of my knowledge.",
                    "required": false
                  }
                ]
              }
            ]
        }
    ]
    };
    constructor() { }

    getData(appId: number): Observable<any> {
        this.FieldsData = this.FieldsData.result.filter(data => {
            return data.id = appId;
        });
        return of(this.FieldsData);
    };
}
