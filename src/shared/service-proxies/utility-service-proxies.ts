import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Observable, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { Injectable, Inject, Optional, InjectionToken } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';
import * as moment from 'moment';
@Injectable({
    providedIn: 'root'
})
export class UtilityServiceProxyService {
    errors: any = [];
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;
    private http: HttpClient;
    private baseUrl: string;

    constructor(@Inject(HttpClient) http: HttpClient) {
        this.http = http;
    }

    /**
     * @param body (optional)
     * @return Success
     */
    isValidFiles(files, fileExt: any, maxSize) {
        if (files.length > 1) {
            this.errors.push('Error: At a time you can upload only one file');
            return;
        }
        this.isValidFileExtension(files, fileExt, maxSize);
        return this.errors.length === 0;
    }
    isValidFileExtension(files, fileExt, maxSize) {
        const extensions = (fileExt.split(',')).map(
            function (x) {
                return x.toLocaleUpperCase().trim();
            });

        const ext = files[0].name.toUpperCase().split('.').pop() || files[0].name;
        const exists = extensions.includes(ext);
        if (!exists) {
            this.errors.push('Error (Extension): ' + files[0].name);
        }
        this.isValidFileSize(files[0], maxSize);

    }

    isValidFileSize(file, maxSize) {
        const fileSizeinMB = file.size / (1024 * 1000);
        const size = Math.round(fileSizeinMB * 100) / 100;
        if (size > maxSize) {
            this.errors.push('Error (File Size): ' + file.name + ': exceed file size limit of ' + maxSize + 'MB ( ' + size + 'MB )');
        }
    }
    updateProfilePicture(formData: FormData) {
        return this.http.post<any>(AppConsts.remoteServiceBaseUrl + '/api/tokenAuth/UpdateProfilePicture', formData);
    }
}
