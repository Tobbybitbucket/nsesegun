import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AbpHttpInterceptor, customHttpConfiguration } from '@shared/service-proxies/abpHttpInterceptor';

import * as ApiServiceProxies from './service-proxies';

@NgModule({
  providers: [
    ApiServiceProxies.ApplicationServiceProxy,
    ApiServiceProxies.FeesCalculationServiceProxy,
    ApiServiceProxies.PaymentServiceProxy,
    ApiServiceProxies.ApplicationStatusServiceProxy,
    ApiServiceProxies.ApplicationTypeServiceProxy,
    ApiServiceProxies.RequiredApplicationDocumentServiceProxy,
    ApiServiceProxies.CountryServiceProxy,
    ApiServiceProxies.EmailSetupServiceProxy,
    ApiServiceProxies.EmailMessageServiceProxy,
    ApiServiceProxies.GenderServiceProxy,
    ApiServiceProxies.OrganizationServiceProxy,
    ApiServiceProxies.IssuingCompanyServiceProxy,
    ApiServiceProxies.LgaServiceProxy,
    ApiServiceProxies.ListingTypeServiceProxy,
    ApiServiceProxies.SmsSetupServiceProxy,
    ApiServiceProxies.StateServiceProxy,
    ApiServiceProxies.TitleServiceProxy,
    ApiServiceProxies.RoleServiceProxy,
    ApiServiceProxies.SessionServiceProxy,
    ApiServiceProxies.TenantServiceProxy,
    ApiServiceProxies.UserServiceProxy,
    ApiServiceProxies.TokenAuthServiceProxy,
    ApiServiceProxies.AccountServiceProxy,
    ApiServiceProxies.ConfigurationServiceProxy,
    ApiServiceProxies.ApprovalLogDocServiceProxy,
    ApiServiceProxies.DocLogServiceProxy,
    ApiServiceProxies.ApprovalReviewServiceProxy,
    ApiServiceProxies.SystemlogServiceProxy,
    ApiServiceProxies.XissuerServiceProxy, customHttpConfiguration,
    { provide: HTTP_INTERCEPTORS, useClass: AbpHttpInterceptor, multi: true },
  ],
})
export class ServiceProxyModule { }
