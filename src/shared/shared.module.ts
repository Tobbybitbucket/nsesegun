import { AbpModalCancelFooterComponent } from './components/modal/abp-modal-cancel-footer.component';
import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';
import {
    NgxUiLoaderModule,
    NgxUiLoaderConfig,
    POSITION,
    SPINNER,
    PB_DIRECTION
} from 'ngx-ui-loader';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import { AppSessionService } from './session/app-session.service';
import { AppUrlService } from './nav/app-url.service';
import { AppAuthService } from './auth/app-auth.service';
import { AppRouteGuard } from './auth/auth-route-guard';
import { LocalizePipe } from '@shared/pipes/localize.pipe';

import { AbpPaginationControlsComponent } from './components/pagination/abp-pagination-controls.component';
import { AbpValidationSummaryComponent } from './components/validation/abp-validation.summary.component';
import { AbpModalHeaderComponent } from './components/modal/abp-modal-header.component';
import { AbpModalFooterComponent } from './components/modal/abp-modal-footer.component';
import { LayoutStoreService } from './layout/layout-store.service';

import { BusyDirective } from './directives/busy.directive';
import { EqualValidator } from './directives/equal-validator.directive';
import {
    CurrencyMaskModule, CurrencyMaskConfig,
    CURRENCY_MASK_CONFIG
} from 'ng2-currency-mask';
const ngxUiLoaderConfig: NgxUiLoaderConfig = {
    bgsColor: '#a8ac2e',
    bgsPosition: POSITION.bottomCenter,
    bgsSize: 40,
    bgsType: SPINNER.rectangleBounce,
    pbDirection: PB_DIRECTION.leftToRight, // progress bar direction
    pbThickness: 2,
    fgsType: 'ball-spin-fade-rotating',
    fgsColor: '#a8ac2e',
    bgsOpacity: 0.9,
    // overlayColor:'#ffffff00',
    pbColor: '#a8ac2e' // progress bar thickness
};
export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
    align: 'left',
    allowNegative: true,
    decimal: '.',
    precision: 2,
    prefix: '₦',
    suffix: '',
    thousands: ','
};
@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        NgxPaginationModule,
        CurrencyMaskModule,
        // NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
        // NgxLoadingModule.forRoot({
        //     animationType: ngxLoadingAnimationTypes.circle,
        //     fullScreenBackdrop: true,
        //     backdropBackgroundColour: 'rgba(0,0,0,0.1)',
        //     backdropBorderRadius: '14px',
        //     primaryColour: '#536838',
        //     secondaryColour: '#a8ac2e',
        //     tertiaryColour: '#ffffff'
        // }),
    ],
    declarations: [
        AbpPaginationControlsComponent,
        AbpValidationSummaryComponent,
        AbpModalHeaderComponent,
        AbpModalFooterComponent,
        AbpModalCancelFooterComponent,
        LocalizePipe,
        BusyDirective,
        EqualValidator
    ],
    exports: [
        AbpPaginationControlsComponent,
        AbpValidationSummaryComponent,
        AbpModalHeaderComponent,
        AbpModalFooterComponent,
        AbpModalCancelFooterComponent,
        LocalizePipe,
        BusyDirective,
        CurrencyMaskModule,
        NgxUiLoaderModule,
        EqualValidator
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders<SharedModule> {
        return {
            ngModule: SharedModule,
            providers: [
                AppSessionService,
                AppUrlService,
                AppAuthService,
                AppRouteGuard,
                LayoutStoreService,
                { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }
            ]
        };
    }
}
