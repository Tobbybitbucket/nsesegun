import { EFilingTemplatePage } from './app.po';

describe('EFiling App', function() {
  let page: EFilingTemplatePage;

  beforeEach(() => {
    page = new EFilingTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
